// StandListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "PropertyTabView.h"
#include "StandListFormView.h"
#include "StandsInPropertyFormView.h"

//#include "ResLangFileReader.h"


/////////////////////////////////////////////////////////////////////////////
// CStandSelectionListFrame

IMPLEMENT_DYNCREATE(CStandSelectionListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CStandSelectionListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CStandSelectionListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_CREATE, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CStandSelectionListFrame::CStandSelectionListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CStandSelectionListFrame::~CStandSelectionListFrame()
{
}

void CStandSelectionListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_STANDS_SELLIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CStandSelectionListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR5);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
		return -1;      // fail to create

	// docking for filter editing
	// Set to 0 = No Docking; 090224 p�d
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setLanguage();
	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CStandSelectionListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_STANDS_SELLIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CStandSelectionListFrame::OnSetFocus(CWnd*)
{
}

BOOL CStandSelectionListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CStandSelectionListFrame diagnostics

#ifdef _DEBUG
void CStandSelectionListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CStandSelectionListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CStandSelectionListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CONTACTS_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CONTACTS_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CStandSelectionListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CStandSelectionListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CStandSelectionListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CStandSelectionListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

void CStandSelectionListFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CStandSelectionListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sToolTipFilter = xml.str(IDS_STRING121/*300*/);
			m_sToolTipFilterOff = xml.str(IDS_STRING122/*301*/);
			m_sToolTipAdd = xml.str(IDS_STRING296/*317*/);

			m_wndFilterEdit.SetWindowText((xml.str(IDS_STRING269/*243*/)));
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CStandSelectionListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR5)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RSTR_TB_FILTER,m_sToolTipFilter);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RSTR_TB_FILTER_OFF,m_sToolTipFilterOff);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RSTR_TB_ADD,m_sToolTipAdd);	//
				}	// if (nBarID == IDR_TOOLBAR5)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CStandSelectionListFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CStandsReportFilterEditControl

IMPLEMENT_DYNCREATE(CStandsReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CStandsReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CStandsReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

// CStandSelListFormView

IMPLEMENT_DYNCREATE(CStandSelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CStandSelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)
	ON_COMMAND(ID_TBBTN_OPEN, OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_CREATE, OnShowFieldFilterOff)
	ON_COMMAND(ID_TBBTN_TOOLS, OnAddStandToProperty)
END_MESSAGE_MAP()

CStandSelListFormView::CStandSelListFormView()
	: CXTPReportView()
{
	m_pDB = NULL;
	m_nSelectedColumn = -1;	// Nothing selected yet; 070125 p�d
}

CStandSelListFormView::~CStandSelListFormView()
{
}

void CStandSelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setupReport();

	populateReport();

	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if (m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL1, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14,FW_BOLD);
	}

	LoadReportState();
}

BOOL CStandSelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CStandSelListFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;
	SaveReportState();
	CXTPReportView::OnDestroy();	
}

BOOL CStandSelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CStandSelListFormView diagnostics

#ifdef _DEBUG
void CStandSelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CStandSelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CStandSelListFormView message handlers

// CStandSelListFormView message handlers
void CStandSelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CStandSelListFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Catch message sent from (WM_USER_MSG_SUITE)
LRESULT CStandSelListFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_SHOWVIEW_MSG :
		{
			break;
		}
	}	// switch (wParam)

	return 0L;
}


// Create and add Assortment settings reportwindow
BOOL CStandSelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
				m_sGroupByThisField	= (xml.str(IDS_STRING127/*244*/));
				m_sGroupByBox				= (xml.str(IDS_STRING128/*245*/));
				m_sFieldChooser			= (xml.str(IDS_STRING129/*246*/));
				m_sFilterOn					= (xml.str(IDS_STRING130/*2430*/));

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP3));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					GetReportCtrl().SetImageList(&m_ilIcons);

					GetReportCtrl().ShowWindow( SW_NORMAL );
					// Add these 3 lines to add scrollbars for View; 070319 p�d
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 18,FALSE,12));
					pCol->AllowRemove(FALSE);
					pCol->SetEditable( TRUE );

					//TractID
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING140), 120));
					pCol->SetHeaderAlignment(DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->AllowRemove(FALSE);	//don't allow removal off column

					//Tract Acres
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING141), 120));
					pCol->SetHeaderAlignment(DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					//Cruise Method
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING161), 120));
					pCol->SetHeaderAlignment(DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					//Cruise Date
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING142), 120));
					pCol->SetHeaderAlignment(DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					//Tract Loc.
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, xml.str(IDS_STRING143), 120));
					pCol->SetHeaderAlignment(DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetVisible(FALSE);

					//Tract Owner
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, xml.str(IDS_STRING144), 120));
					pCol->SetHeaderAlignment(DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->SetVisible(FALSE);

					//Cruiser
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, xml.str(IDS_STRING145), 120));
					pCol->SetHeaderAlignment(DT_LEFT);
					pCol->SetAlignment(DT_LEFT);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					populateReport();		

					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );

					GetReportCtrl().AllowEdit(FALSE);
					GetReportCtrl().FocusSubItems(TRUE);
					GetReportCtrl().SetFocus();

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CStandSelListFormView::populateReport(void)
{
	// Get Stand with No property, from database; 090812 p�d
	getStandsNoProperty();
	// populate report; 090812 p�d
	GetReportCtrl().ResetContent();

	if (m_vecStandsNoProperty.size() > 0)
	{
		for (UINT i = 0;i < m_vecStandsNoProperty.size();i++)
		{
			CTransaction_TCStand rec = m_vecStandsNoProperty[i];			
			GetReportCtrl().AddRecord(new CNoPropStandsReportRec(rec));	
			
		}
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();

}


void CStandSelListFormView::getStandsNoProperty(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktNoProperty(m_vecStandsNoProperty);
		
	}
}

void CStandSelListFormView::setFilterWindow(void)
{
	if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}	// for (int i = 0;i < pCols->GetCount();i++)
		}	// if (pCols && nColumn < pCols->GetCount())
		m_wndLbl.SetWindowText(m_sFilterOn + _T(" :"));
		m_wndLbl1.SetWindowText(pColumn->GetCaption());
	}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
}

void CStandSelListFormView::OnShowFieldFilter()
{
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		setFilterWindow();
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}
}

void CStandSelListFormView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}
}

void CStandSelListFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}

	// Check if Fileter window is visible. If so change filter column to selected column; 090224 p�d
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		if (pWnd->m_wndFilterEdit.IsVisible())
		{
			setFilterWindow();
			pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
		}	// if (pWnd->m_wndFilterEdit.IsVisible())
	}	// if (pWnd != NULL)
	pWnd = NULL;

}

void CStandSelListFormView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	BOOL bClose = TRUE;
	
	CNoPropStandsReportRec *pRec = NULL;
	//CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pTabView)
	{
		CPropertyFormView *pView0 = (CPropertyFormView *)pTabView->getPropertyFormView();
		if (pView0 != NULL)
		{
			int nPropID = pView0->getActiveProperty().getID();
			// We'll start to connect selected Stands to this property; 090812 p�d
			//if (pRecs != NULL)
			{
				CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
				if(pRow)
				//for (int i = 0;i < pRecs->GetCount();i++)
				{
					//pRec = (CNoPropStandsReportRec*)pRecs->GetAt(i);
					pRec = (CNoPropStandsReportRec*)pRow->GetRecord();
					if(pRec)
					//if (pRec->getColumnCheck(COLUMN_0))
					{
						// Connect Stands to Propery; 090812 p�d
						if (m_pDB != NULL)
							bClose = m_pDB->updConnectPropertyToStand(pRec->getFileIndex(),nPropID);
							
					}	// if (pRec->getColumnCheck(COLUMN_0))
				}	// for (int i = 0;i < pRecs->GetCount();i++)
			}	// if (pRecs != NULL)
		}	// if (pView0 != NULL)

		// Get CStandsInPropertyFormView; 090812 p�d
		CStandsInPropertyFormView *pView = (CStandsInPropertyFormView *)pTabView->getPropStandsFormView();
		if (pView)
		{
			pView->doRePopulate();
			pView = NULL;
		}	// if (pView)
		pTabView = NULL;
	}	// if (pTabView)

	if(bClose)
	{
		// Close form
		PostMessage(WM_COMMAND, ID_FILE_CLOSE);
	}
	
}

void CStandSelListFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
	}

}

void CStandSelListFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;
}

void CStandSelListFormView::OnAddStandToProperty(void)
{
	CNoPropStandsReportRec *pRec = NULL;
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pTabView)
	{
		CPropertyFormView *pView0 = (CPropertyFormView *)pTabView->getPropertyFormView();
		if (pView0 != NULL)
		{
			int nPropID = pView0->getActiveProperty().getID();
			// We'll start to connect selected Stands to this property; 090812 p�d
			if (pRecs != NULL)
			{
				for (int i = 0;i < pRecs->GetCount();i++)
				{
					pRec = (CNoPropStandsReportRec*)pRecs->GetAt(i);
					if (pRec->getColumnCheck(COLUMN_0))
					{
						// Connect Stands to Propery; 090812 p�d
						if (m_pDB != NULL)
							m_pDB->updConnectPropertyToStand(pRec->getFileIndex(),nPropID);
							
					}	// if (pRec->getColumnCheck(COLUMN_0))
				}	// for (int i = 0;i < pRecs->GetCount();i++)
			}	// if (pRecs != NULL)
		}	// if (pView0 != NULL)

		// Get CStandsInPropertyFormView; 090812 p�d
		CStandsInPropertyFormView *pView = (CStandsInPropertyFormView *)pTabView->getPropStandsFormView();
		if (pView)
		{
			pView->doRePopulate();
			pView = NULL;
		}	// if (pView)
		pTabView = NULL;
	}	// if (pTabView)
}

// CStandSelListFormView message handlers

void CStandSelListFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("FilterText"), _T(""));
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("SelColIndex"),0);

	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(sFilterText != "");
	}
}

void CStandSelListFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("FilterText"), sFilterText);
	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);

}

