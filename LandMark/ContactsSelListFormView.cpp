// ContactsSelListFormView.cpp : implementation file
//

#include "stdafx.h"
//#include "Forrest.h"
#include "MDIContactsFrame.h"
#include "ContactsTabView.h"
#include "ContactsFormView.h"
#include "ContactsSelListFormView.h"

#include "ResLangFileReader.h"

#include "XTPPreviewView.h"

#include "PropertyTabView.h"

/////////////////////////////////////////////////////////////////////////////
// CContactsSelectListFrame

IMPLEMENT_DYNCREATE(CContactsSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CContactsSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CContactsSelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_FILTER_OFF, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CContactsSelectListFrame::CContactsSelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CContactsSelectListFrame::~CContactsSelectListFrame()
{
}

void CContactsSelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTS_SELLEIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CContactsSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooser.Create(this, IDD_FIELD_CHOOSER,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS))
		return -1;      // fail to create

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
		return -1;      // fail to create

	// docking for field chooser
	m_wndFieldChooser.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooser, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooser, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	// Set to 0 = No Docking; 090224 p�d
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CContactsSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTS_SELLEIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CContactsSelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CContactsSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CContactsSelectListFrame diagnostics

#ifdef _DEBUG
void CContactsSelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CContactsSelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CContactsSelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CONTACTS_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CONTACTS_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CContactsSelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CContactsSelectListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CContactsSelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CContactsSelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while(pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE, wParam, lParam);

		}
	}

	return 0L;
}

void CContactsSelectListFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CContactsSelectListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sToolTipFilter = xml.str(IDS_STRING121/*300*/);
			m_sToolTipFilterOff = xml.str(IDS_STRING122/*301*/);
			m_sToolTipPrintOut = xml.str(IDS_STRING123/*302*/);
			m_sToolTipSearch = xml.str(IDS_STRING244/*310*/);
			m_sToolTipAdd = xml.str(IDS_STRING267/*321*/);

			m_wndFieldChooser.SetWindowText((xml.str(IDS_STRING268/*242*/)));
			m_wndFilterEdit.SetWindowText((xml.str(IDS_STRING269/*243*/)));
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CContactsSelectListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR6)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RSTR_TB_FILTER,m_sToolTipFilter);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RSTR_TB_FILTER_OFF,m_sToolTipFilterOff);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RSTR_TB_PRINT,m_sToolTipPrintOut);	//
					setToolbarBtn(sTBResFN,p->GetAt(3),RSTR_TB_SEARCH,m_sToolTipSearch,FALSE);	//
					setToolbarBtn(sTBResFN,p->GetAt(4),RSTR_TB_ADD,m_sToolTipAdd);	//
				}	// if (nBarID == IDR_TOOLBAR6)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CContactsSelectListFrame message handlers



/////////////////////////////////////////////////////////////////////////////
// CContactsReportFilterEditControl

IMPLEMENT_DYNCREATE(CContactsReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CContactsReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CContactsReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
	
}

// CContactsSelListFormView

IMPLEMENT_DYNCREATE(CContactsSelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CContactsSelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)
	ON_COMMAND(ID_TBBTN_COLUMNS, OnShowFieldChooser)
	ON_COMMAND(ID_TBBTN_FILTER, OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_FILTER_OFF, OnShowFieldFilterOff)
	ON_COMMAND(ID_TBBTN_PRINT, OnPrintPreview)
	ON_COMMAND(ID_TBBTN_REFRESH, OnRefresh)
	ON_COMMAND(ID_TBBTN_ADD, OnAddContactToProperty)

	ON_UPDATE_COMMAND_UI(ID_TBBTN_ADD, OnUpdateDBToolbarBtnAdd)

END_MESSAGE_MAP()

CContactsSelListFormView::CContactsSelListFormView()
	: CXTPReportView()
{
	m_pDB = NULL;
	m_nSelectedColumn = -1;	// Nothing selected yet; 070125 p�d
	// This data member is used to set value, based on
	// value set in showFormView(...), to set if
	// OnReportItemClick will result in sending a message
	// to, in this case the Property Owner window, or not; 070126 p�d
	m_nDoAddPropertyOwners	= -1;
}

CContactsSelListFormView::~CContactsSelListFormView()
{
}

void CContactsSelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	CContactsTabView *pView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW8);
	if (pView)
	{
		m_nDBIndex = pView->getContactsFormView()->getDBIndex();
	}

	setupReport();

	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST, &pWnd->m_wndFieldChooser);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if (m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL1, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14,FW_BOLD);
	}

	LoadReportState();
}

BOOL CContactsSelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CContactsSelListFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;
	SaveReportState();
	CXTPReportView::OnDestroy();	
}

BOOL CContactsSelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CContactsSelListFormView diagnostics

#ifdef _DEBUG
void CContactsSelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CContactsSelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CContactsSelListFormView message handlers

// CContactsSelListFormView message handlers
void CContactsSelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CContactsSelListFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Catch message sent from (WM_USER_MSG_SUITE)
LRESULT CContactsSelListFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	CString csFrom;
	switch (wParam)
	{
		case ID_SHOWVIEW_MSG :
		{
			m_nDoAddPropertyOwners	= (int)lParam;
			CXTPReportColumn *pCol0 = GetReportCtrl().GetColumns()->Find(0);
			if (pCol0) pCol0->SetVisible(m_nDoAddPropertyOwners == 1);
			getContacts();
			populateReport();		
			break;
		}
		case ID_WPARAM_VALUE_FROM + 0x02:
		{
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			if(sizeof(*msg) == sizeof(_doc_identifer_msg))
			{
				csFrom.Format(_T("%s"),msg->getSendFrom());
				if(msg->getValue1() == 1 && (csFrom == MODULE928))
				{
					OnRefresh();	//refresh window
				}
			}
		}
		break;
	}	// switch (wParam)

	return 0L;
}


// Create and add Assortment settings reportwindow
BOOL CContactsSelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
				m_sGroupByThisField	= (xml.str(IDS_STRING127/*244*/));
				m_sGroupByBox				= (xml.str(IDS_STRING128/*245*/));
				m_sFieldChooser			= (xml.str(IDS_STRING129/*246*/));
				m_sFilterOn					= (xml.str(IDS_STRING130/*2430*/));

				m_sarrTypeOfContact.Add(xml.str(IDS_STRING262/*2353*/));
				m_sarrTypeOfContact.Add(xml.str(IDS_STRING263/*2354*/));
				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP3));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					GetReportCtrl().SetImageList(&m_ilIcons);

					GetReportCtrl().ShowWindow( SW_NORMAL );
					// Add these 3 lines to add scrollbars for View; 070319 p�d
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(0, _T(""), 18,FALSE,12));
					pCol->AllowRemove(FALSE);
					pCol->SetEditable( TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(1, (xml.str(IDS_STRING270/*221*/)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					pCol->SetVisible(FALSE);	//Don't show

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(2, (xml.str(IDS_STRING247/*2350*/)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
	
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(3, (xml.str(IDS_STRING272/*223*/)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(4, (xml.str(IDS_STRING271/*222*/)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(5, (xml.str(IDS_STRING273/*224*/)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(6, (xml.str(IDS_STRING274/*225*/)), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(7, (xml.str(IDS_STRING275/*226*/)), 120));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(8, (xml.str(IDS_STRING276/*227*/)), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(9, (xml.str(IDS_STRING277/*228*/)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					sColText.Format(_T("%s %s"),xml.str(IDS_STRING278/*229*/),xml.str(IDS_STRING278/*2290*/));
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(10, (sColText), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					sColText.Format(_T("%s %s"),xml.str(IDS_STRING278/*229*/),xml.str(IDS_STRING279/*2291*/));
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(11, (sColText), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(12, (xml.str(IDS_STRING246/*247*/)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(13, (xml.str(IDS_STRING281/*230*/)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(14, (xml.str(IDS_STRING282/*231*/)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(15, (xml.str(IDS_STRING283/*232*/)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					/*
					GetReportCtrl().SetPaintManager(new CReportMultilinePaintManager());
					GetReportCtrl().EnableToolTips(FALSE);

					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
					GetReportCtrl().GetPaintManager()->m_bUseColumnTextAlignment = TRUE;
					*/

					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CContactsSelListFormView::populateReport(void)
{
	CXTPReportRecord *pRec = NULL;
	CTransaction_contacts dataSel;
	CString sType;
	if (m_nDBIndex >= 0 && m_nDBIndex < (int)m_vecContactsData.size())
	{
		dataSel = m_vecContactsData[m_nDBIndex];
	}
//	getContacts();
	GetReportCtrl().GetRecords()->RemoveAll();
	for (UINT i = 0;i < m_vecContactsData.size();i++)
	{
		CTransaction_contacts data = m_vecContactsData[i];
		if (data.getTypeOf() > -1 && data.getTypeOf() < m_sarrTypeOfContact.GetCount())
			sType = m_sarrTypeOfContact.GetAt(data.getTypeOf());
		else
			sType = _T("");
		if (data.getID() == dataSel.getID())
		{
			pRec = GetReportCtrl().AddRecord(new CContactsReportDataRec(i,data,sType)); //m_sarrTypeOfContact));
		}
		else
		{
			GetReportCtrl().AddRecord(new CContactsReportDataRec(i,data,sType)); //m_sarrTypeOfContact));
		}
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	if (pRec)
	{
		CXTPReportRow *pRow = GetReportCtrl().GetRows()->Find(pRec);
		if (pRow)
		{
			GetReportCtrl().SetFocusedRow(pRow);
		}
	}

}

void CContactsSelListFormView::setFilterWindow(void)
{
	if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}	// for (int i = 0;i < pCols->GetCount();i++)
		}	// if (pCols && nColumn < pCols->GetCount())
		m_wndLbl.SetWindowText(m_sFilterOn + _T(" :"));
		m_wndLbl1.SetWindowText(pColumn->GetCaption());
	}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
}

void CContactsSelListFormView::OnShowFieldChooser()
{
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooser.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooser, bShow, FALSE);
	}
	
}

void CContactsSelListFormView::OnShowFieldFilter()
{
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		setFilterWindow();
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}
	
}

void CContactsSelListFormView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}
	
}

void CContactsSelListFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}
	if (m_nDoAddPropertyOwners == 0)
	{

		if (pItemNotify->pRow)
		{
			CContactsReportDataRec *pRec = (CContactsReportDataRec*)pItemNotify->pItem->GetRecord();
			CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW8);
			if (pTabView)
			{
				// Get ContactsFormView
				CContactsFormView *pView = (CContactsFormView *)pTabView->getContactsFormView();
				if (pView)
				{
					pView->doPopulate(pRec->getIndex(), FALSE);
				}	// if (pView)
			}	// if (pTabView)
		}	// if (pItemNotify->pRow)
	}	// if (m_nDoTransacions == 1)

	// Check if Fileter window is visible. If so change filter column to selected column; 090224 p�d
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		if (pWnd->m_wndFilterEdit.IsVisible())
		{
			setFilterWindow();
			pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
		}	// if (pWnd->m_wndFilterEdit.IsVisible())
	}	// if (pWnd != NULL)
	pWnd = NULL;
	
}

void CContactsSelListFormView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	BOOL bClose = TRUE;

	if (m_nDoAddPropertyOwners == 1)
	{
		//add contact to property
		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
		if (pTabView)
		{
			CPropertyOwnerFormView *pView = (CPropertyOwnerFormView *)pTabView->getPropOwnerFormView();
			if (pView)
			{
				CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
				//CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
				//if (pRecs != NULL)
				if(pRow)
				{
					//for (int i = 0;i < pRecs->GetCount();i++)
					//{
						//CContactsReportDataRec *pRec = (CContactsReportDataRec *)pRecs->GetAt(i);
					CContactsReportDataRec *pRec = (CContactsReportDataRec *)pRow->GetRecord();
						if (pRec != NULL)
						{
							//if (pRec->getColumnCheck(0))
							{
								UINT nIndex = pRec->getIndex();
								if (nIndex >= 0 && nIndex < m_vecContactsData.size())
								{
									bClose = pView->setPropOwner(m_vecContactsData[nIndex]);
								}	// if (nIndex >= 0 && nIndex < m_vecContactsData.size())
								pRec->setColumnCheck(0,FALSE);
							}	// if (pRec->getColumnCheck(0))
						}	// if (pRec != NULL)
					//}	// for (int i = 0;i < pRecs->GetCount();i++)
					
					// Update report; 070129 p�d
					GetReportCtrl().Populate();
				}	// if (pRecs != NULL)
			}	// if (pView)
		}	// if (pTabView)
	}	// if (m_nDoAddPropertyOwners == 1)


	if(bClose)
	{
		// Close form
		PostMessage(WM_COMMAND, ID_FILE_CLOSE);
	}
}

void CContactsSelListFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
		case ID_SHOW_FIELDCHOOSER:
			OnShowFieldChooser();
			break;
	}

}

void CContactsSelListFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;
}


void CContactsSelListFormView::OnPrintPreview()
{
	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,
		RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		AfxMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}

}

void CContactsSelListFormView::OnRefresh()
{
	getContacts();
	populateReport();
}

void CContactsSelListFormView::OnAddContactToProperty()
{
	if (m_nDoAddPropertyOwners == 1)
	{
		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
		if (pTabView)
		{
			CPropertyOwnerFormView *pView = (CPropertyOwnerFormView *)pTabView->getPropOwnerFormView();
			if (pView)
			{
				CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
				if (pRecs != NULL)
				{
					for (int i = 0;i < pRecs->GetCount();i++)
					{
						CContactsReportDataRec *pRec = (CContactsReportDataRec *)pRecs->GetAt(i);
						if (pRec != NULL)
						{
							if (pRec->getColumnCheck(0))
							{
								UINT nIndex = pRec->getIndex();
								if (nIndex >= 0 && nIndex < m_vecContactsData.size())
								{
									pView->setPropOwner(m_vecContactsData[nIndex]);
								}	// if (nIndex >= 0 && nIndex < m_vecContactsData.size())
								pRec->setColumnCheck(0,FALSE);
							}	// if (pRec->getColumnCheck(0))
						}	// if (pRec != NULL)
					}	// for (int i = 0;i < pRecs->GetCount();i++)
					
					// Update report; 070129 p�d
					GetReportCtrl().Populate();

					// Close form
					PostMessage(WM_COMMAND, ID_FILE_CLOSE);

				}	// if (pRecs != NULL)
			}	// if (pView)
		}	// if (pTabView)
	}	// if (m_nDoAddPropertyOwners == 1)
	else if (m_nDoAddPropertyOwners == 2)
	{
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW8);
		if (pTabView)
		{
			// Get ContactsFormView
			CContactsFormView *pView = (CContactsFormView *)pTabView->getContactsFormView();
			if (pView)
			{
				CContactsReportDataRec *pRec = (CContactsReportDataRec*)GetReportCtrl().GetFocusedRow()->GetRecord();
				if (pRec != NULL)
				{
					pView->setCompany(pRec->getColumnText(2));
				}
			}	// if (pView)
		}	// if (pTabView)
	}
	
}

void CContactsSelListFormView::OnUpdateDBToolbarBtnAdd(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_nDoAddPropertyOwners > 0);
}

// CContactsSelListFormView message handlers

void CContactsSelListFormView::getContacts(void)
{
	if (m_pDB != NULL)
	{
		if (m_nDoAddPropertyOwners == 2)	// Add Owner, set as Company to a Owner; 090922 p�d
		{
			CString sSQL;
			sSQL.Format(_T("select * from %s where type_of=%d"),TBL_CONTACTS,1 /* 1 = Company */);	
			m_pDB->getContacts(sSQL,m_vecContactsData);
		}
		else // Add Owner(s) to Property; 090922 p�d
			m_pDB->getContacts(m_vecContactsData);

	}	// if (m_pDB != NULL)

	if (m_vecContactsData.size() == 0) m_nDoAddPropertyOwners = 0;
}

void CContactsSelListFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("FilterText"), _T(""));
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"),0);

	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(sFilterText != "");
	}
	
}

void CContactsSelListFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("FilterText"), sFilterText);
	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);

}

