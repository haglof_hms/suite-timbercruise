// LandMarkView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "LandMarkView.h"
#include "MDIBaseFrame.h"
#include "XTPPreviewView.h"
#include "ReportView.h"


//CLandMarkReportFilterEditControl
IMPLEMENT_DYNCREATE(CStandReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CStandReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CStandReportFilterEditControl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CString S;
	CMDILandMarkFrame* pWnd = (CMDILandMarkFrame*)getFormViewParentByID(IDD_FORMVIEW);	
	if(pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != _T(""));
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar, nRepCnt, nFlags);
}


// CLandMarkView

IMPLEMENT_DYNCREATE(CLandMarkView, CXTPReportView)

CLandMarkView::CLandMarkView() : CXTPReportView()
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_nSelectedColumn = -1;
}

CLandMarkView::~CLandMarkView() 
{
	if(m_pDB != NULL)
	{
		delete m_pDB;		
	}
}

BEGIN_MESSAGE_MAP(CLandMarkView, CXTPReportView)
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDBClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
	ON_COMMAND(ID_TBBTN_COLUMNS, OnShowFieldChooser)
	ON_COMMAND(ID_TBBTN_FILTER,OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_FILTER_OFF, OnShowFieldFilterOff)
	ON_COMMAND(ID_TBBTN_PRINT, OnPrintPreview)
	ON_COMMAND(ID_TBBTN_REFRESH, OnRefresh)
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_COMMAND(ID_TBBTN_REPORTS, OnTBBTNReports)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnBtnDown)
END_MESSAGE_MAP()

void CLandMarkView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

// CLandMarkView diagnostics

#ifdef _DEBUG
void CLandMarkView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

#ifndef _WIN32_WCE
void CLandMarkView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
}
#endif
#endif //_DEBUG


// CLandMarkView message handlers

void CLandMarkView::OnSize(UINT nType, int cx, int cy)
{
	CXTPReportView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

BOOL CLandMarkView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	if(pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memset(&m_dbConnectionData, 0x00, sizeof(DB_CONNECTION_DATA));
		memcpy(&m_dbConnectionData, pCopyDataStruct->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}
	
	return CXTPReportView::OnCopyData(pWnd, pCopyDataStruct);
}

void CLandMarkView::OnDestroy()
{
	SaveReportState();
	
	CXTPReportView::OnDestroy();
}

BOOL CLandMarkView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTPReportView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}



void CLandMarkView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

	CMDILandMarkFrame *pWnd = (CMDILandMarkFrame*)getFormViewParentByID(IDD_FORMVIEW);	

	if(m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST, &pWnd->m_wndFieldChooserDlg);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if(m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if(m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if(m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL1, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14, FW_BOLD);
	}

	setupReport();	
	if(getStand() == FALSE)
	{
		//disable all buttons except TCruise button
		CMDILandMarkFrame *pFrame = (CMDILandMarkFrame*)getFormViewParentByID(IDD_FORMVIEW);
		if(pFrame)
			pFrame->disableTBBtns();		

	}
	else
	{
		populateReport();
	}
	
	LoadReportState();

}


BOOL CLandMarkView::getStand()
{
	if(m_pDB != NULL)
	{
		if(m_pDB->getStands(m_vecStandData) == FALSE)
		{
			AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			return FALSE;
		}

		CMDILandMarkFrame* pWnd = (CMDILandMarkFrame*)getFormViewParentByID(IDD_FORMVIEW);
		if(pWnd != NULL)
		{
			pWnd->setEnableTBBTNReports(m_vecStandData.size() > 0);
		}
	}
	else
	{
			AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			return FALSE;
	}

	return TRUE;
}


BOOL CLandMarkView::setupReport()
{
	CString csColText;
	int nNUmOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	//add scrollbars
	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing(FALSE);
	GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE);
	GetReportCtrl().EnableScrollBar(SB_VERT, TRUE);

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sDBErrorMsg = xml->str(IDS_STRING105);
			m_sGroupByThisField = xml->str(IDS_STRING127);
			m_sGroupByBox = xml->str(IDS_STRING128);
			m_sFieldChooser = xml->str(IDS_STRING129);
			m_sFilterOn = xml->str(IDS_STRING130);
			m_sTractGISError = xml->str(IDS_STRING229);
		
			//setup columns
			if(GetReportCtrl().GetSafeHwnd() != NULL)
			{
				GetReportCtrl().ShowWindow(SW_NORMAL);

				VERIFY(m_ilIcons.Create(16, 16, ILC_COLOR24|ILC_MASK, 0, 1));
				CBitmap bmp;
				VERIFY(bmp.LoadBitmap(IDB_BITMAP3));
				m_ilIcons.Add(&bmp, RGB(255, 0, 255));

				GetReportCtrl().SetImageList(&m_ilIcons);

				//TractID
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_TRACTID, xml->str(IDS_STRING140), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->AllowRemove(FALSE);	//don't allow removal off column

				//Tract Acres
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_TRACTACRES, xml->str(IDS_STRING141), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				//Cruise Method
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_CRUISEMETHOD, xml->str(IDS_STRING161), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				//Cruise Date
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_CRUISEDATE, xml->str(IDS_STRING142), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_nMaxLength = 40;

				//Tract Loc.
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_TRACTLOC, xml->str(IDS_STRING143), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_nMaxLength = 128;

				//Tract Owner
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_TRACTOWNER, xml->str(IDS_STRING144), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_nMaxLength = 128;

				//Cruiser
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_CRUISER, xml->str(IDS_STRING145), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_nMaxLength = 128;

				//External documents attached icon
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_EXTDOC_ATTACHED, xml->str(IDS_STRING215), 20, FALSE, 7));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				//Snapshots icon
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_SNAPSHOTS, xml->str(IDS_STRING371), 20, FALSE, 0));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
				GetReportCtrl().GetReportHeader()->AllowColumnReorder(TRUE);
				GetReportCtrl().GetReportHeader()->AllowColumnResize( TRUE);	
				GetReportCtrl().GetReportHeader()->AllowColumnSort( TRUE);
				GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
				GetReportCtrl().SetMultipleSelection( TRUE );	//FALSE );
				GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
				GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
				GetReportCtrl().AllowEdit(TRUE);
				GetReportCtrl().FocusSubItems(TRUE);				

				GetReportCtrl().GetRecords()->SetCaseSensitive(FALSE);				
			}//if(GetReportCtrl().GetSafeHwnd() != NULL)
		}//if(xml->Load(m_sLangFN))
		delete xml;
	}//if(fileExists(m_sLangFN))

	return TRUE;
}

void CLandMarkView::populateReport()
{
	int index;
	GetReportCtrl().GetRecords()->RemoveAll();
	
	for(UINT i=0; i<m_vecStandData.size(); i++)
	{
		CTransaction_TCStand data = m_vecStandData[i];
		if(m_pDB != NULL)
		{
			if(m_pDB->standHasExternalDocs(TBL_STAND, data.getFileIndex()))		
			{
				index = 8;
			}
			else
			{
				index = XTP_REPORT_NOICON;
			}
		}
		GetReportCtrl().AddRecord(new CStandSelListReportRec(data, index, m_pDB->standHasSnapshots(data.getFileIndex())));
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	//GetReportCtrl().SetFocusedRow(0);
}

void CLandMarkView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if(!AfxGetApp()->GetProfileBinary(REG_WP_TIMBERCRUISE_SEL_STAND_KEY, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar(&memFile, CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);
	}
	catch(COleException *pEx)
	{
		pEx->Delete();
	}
	catch(CArchiveException *pEx)
	{
		pEx->Delete();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	
	//Get selected column index into registry
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(REG_WP_TIMBERCRUISE_SEL_STAND_KEY, _T("SelColIndex"), 0);

/*	
	//Get filtertext for this report
	sFilterText = AfxGetApp()->GetProfileString(REG_WP_TIMBERCRUISE_SEL_STAND_KEY, _T("FilterText"), _T(""));

	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
*/
}

void CLandMarkView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar(&memFile, CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_WP_TIMBERCRUISE_SEL_STAND_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString(REG_WP_TIMBERCRUISE_SEL_STAND_KEY, _T("FilterText"), sFilterText);

	//Set selected column index into registry
	AfxGetApp()->WriteProfileInt(REG_WP_TIMBERCRUISE_SEL_STAND_KEY, _T("SelColIndex"), m_nSelectedColumn);
}

void CLandMarkView::OnReportItemClick(NMHDR *pNotifyStruct, LRESULT *)
{
	CRect rect;
	POINT pt;
	CStandSelListReportRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;

	if (pItemNotify != NULL)
	{
		if(pItemNotify->pColumn)
		{
			m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
		}

		if (pItemNotify->pRow != NULL && pItemNotify->pColumn->GetItemIndex() == COLUMN_EXTDOC_ATTACHED)
		{
			// Do a hit-test; 080513 p�d
			rect = pItemNotify->pColumn->GetRect();		
			pt = pItemNotify->pt;
			// Check if the user clicked on the Icon or not; 080513 p�d
			if (hitTest_X(pt.x,rect.left,13))
			{
				pRec = (CStandSelListReportRec*)pItemNotify->pRow->GetRecord();

				//if(m_pDB != NULL)
				if(pRec)
				{
					//if(m_pDB->standHasExternalDocs(TBL_STAND, pRec->getFileIndex()))
					if(pRec->getIconIndex() == 8)
					{
						//show external docs view
						showFormView(IDD_FORMVIEW7, m_sLangFN, 0);

						//send msg to view with stand id
						AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
							(LPARAM)&_doc_identifer_msg(MODULE920,MODULE927,_T(""),pRec->getFileIndex(),0,0));
					}
				}
				//else
				//	AfxMessageBox(m_sDBErrorMsg);

			}	// if (hitTest_X(pt.x,rect.left,13))
		}	// if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
		else if (pItemNotify->pRow != NULL && pItemNotify->pColumn->GetItemIndex() == COLUMN_SNAPSHOTS)
		{
			// Do a hit-test; 080513 p�d
			rect = pItemNotify->pColumn->GetRect();		
			pt = pItemNotify->pt;
			// Check if the user clicked on the Icon or not; 080513 p�d
			if (hitTest_X(pt.x,rect.left,13))
			{
				pRec = (CStandSelListReportRec*)pItemNotify->pRow->GetRecord();

				//if(m_pDB != NULL)
				if(pRec)
				{
					//if(m_pDB->standHasExternalDocs(TBL_STAND, pRec->getFileIndex()))
					if(pRec->hasSnapshots())
					{
						//show external docs view
						showFormView(IDD_FORMVIEW18, m_sLangFN, 0);

						//send msg to view with stand id
						AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
							(LPARAM)&_doc_identifer_msg(MODULE920,MODULE942,_T(""),pRec->getFileIndex(),0,0));
					}
				}
			}
		}
	}	// if (pItemNotify != NULL)
}

void CLandMarkView::OnReportItemDBClick(NMHDR* pNotifyStruct, LRESULT* result)
{
//open reportview when dubbleclicking
/*	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	if(!pRow)
		return;

	CStandSelListReportRec *pRec = (CStandSelListReportRec*)pRow->GetRecord();
	if(!pRec)
		return;

	//CTransaction_TCStand rec = pRec->getRecord();

	//open report view
	showFormView(IDD_FORMVIEW1, m_sLangFN, 0);
	CReportView* pView = (CReportView*)getFormViewByID(IDD_FORMVIEW1);
	if(pView)
		pView->setReportOrderStr(pRec->getColumnText(COLUMN_TRACTID));//rec.getTractID());
*/
}

void CLandMarkView::OnValueChanged(NMHDR *pNotifyStruct, LRESULT *)
{
	//TODO: save changes here to database or must the user press the save button?
	saveStandToDB();
}

void CLandMarkView::OnReportColumnRClick(NMHDR *pNotifyStruct, LRESULT *result)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	ASSERT(pItemNotify->pColumn);

	CPoint ptClick = pItemNotify->pt;
	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	//create menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	if(GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if(GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn *pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	//track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	switch(nMenuResult)
	{
	case ID_GROUP_BYTHIS:
		if(pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
		{
			pColumns->GetGroupsOrder()->Add(pColumn);
		}
		GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
		GetReportCtrl().Populate();
		break;
	case ID_SHOW_GROUPBOX:
		GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
		break;
	case ID_SHOW_FIELDCHOOSER:
		OnShowFieldChooser();
		break;
	}

}

void CLandMarkView::OnShowFieldChooser()
{
	CMDILandMarkFrame *pWnd = (CMDILandMarkFrame*)getFormViewParentByID(IDD_FORMVIEW);	
	if(pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooserDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg, bShow, FALSE);
	}

}

void CLandMarkView::OnShowFieldFilter()
{
	CMDILandMarkFrame *pWnd = (CMDILandMarkFrame*)getFormViewParentByID(IDD_FORMVIEW);	
	if(pWnd != NULL)
	{
		if(m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
		{
			CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
			CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
			int nColumn = pColumn->GetIndex();

			if(pCols && nColumn < pCols->GetCount())
			{
				for(int i=0; i<pCols->GetCount(); i++)
				{
					pCols->GetAt(i)->SetFiltrable(i == nColumn);
				}
			}//if(pCols && nColumn < pCols->GetCount())
			m_wndLbl.SetWindowText(m_sFilterOn + _T(" : "));
			m_wndLbl1.SetWindowText(pColumn->GetCaption());
		}//if(m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())

		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}//if(pWnd != NULL)
}

void CLandMarkView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));

	CMDILandMarkFrame *pWnd = (CMDILandMarkFrame*)getFormViewParentByID(IDD_FORMVIEW);	
	if(pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}
}

void CLandMarkView::OnPrintPreview()
{
	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,
		RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		AfxMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}


}

void CLandMarkView::OnRefresh()
{
	getStand();
	populateReport();

	CMDILandMarkFrame *pWnd = (CMDILandMarkFrame*)getFormViewParentByID(IDD_FORMVIEW);	
	if(pWnd != NULL)
	{
		pWnd->setEnableTBBTNReports(m_vecStandData.size() > 0);
	}

}

LRESULT CLandMarkView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	CString csFrom;
	switch(wParam)
	{
	case ID_DELETE_ITEM:
		deleteStandFromDB();
		break;
	case ID_SAVE_ITEM:
		saveStandToDB();
		break;
	case ID_WPARAM_VALUE_FROM + 0x02:
		{
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			if(sizeof(*msg) == sizeof(_doc_identifer_msg))
			{
				csFrom.Format(_T("%s"),msg->getSendFrom());
				if(msg->getValue1() == 1 && (csFrom == MODULE927 || csFrom == MODULE923 || csFrom == MODULE942))
				{
					OnRefresh();	//refresh window
				}
				else if(msg->getValue1() == 3 && csFrom == MODULE888)
				{
					SetFocusedTract(msg->getValue2());
				}
			}
		}
		break;
	default:
		break;
	}

	return 0L;
}


void CLandMarkView::deleteStandFromDB(void)
{
	CString csStr,csTmp, csCaption,csStrOk, csStrTxt;
	CString csAll;
	int num_of;
	BOOL bShow;
	
	//get focused row
	//CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	//if(!pRow)
	//	return;

	//ask if to delete
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csStrTxt = xml->str(IDS_STRING138);
			csStrOk = xml->str(IDS_STRING139);
			csAll = xml->str(IDS_STRING224);
		}
		delete xml;
	}

	CXTPReportSelectedRows* pRows = GetReportCtrl().GetSelectedRows();

	if(pRows != NULL)
	{
		num_of  = pRows->GetCount();
		
		bShow = TRUE;
		if(num_of > 1)
		{
			if(AfxMessageBox(csAll,MB_ICONQUESTION | MB_YESNO) != IDYES)
				return;
			else bShow = FALSE;
		}
		

		POSITION pos = pRows->GetFirstSelectedRowPosition();
		while(pos)
		{
			CXTPReportRow *pRow = pRows->GetNextSelectedRow(pos);
			if(pRow)
			{
				CStandSelListReportRec *pRec = (CStandSelListReportRec*)pRow->GetRecord();

				if(pRec)
				{
					csCaption = csStrTxt + _T("!");
					csTmp.Format(_T(": %s?"),pRec->getColumnText(COLUMN_TRACTID));
					csStr = csStrTxt;
					csStr += csTmp;

					if(bShow == FALSE || MessageBox(csStr,csCaption, MB_ICONQUESTION | MB_YESNO/*CANCEL*/) == IDYES)
					{
						AfxGetApp()->BeginWaitCursor();
						//remove data from db
						if(m_pDB != NULL)
						{
							if(m_pDB->delExternalDocuments(TBL_STAND, pRec->getFileIndex()))
							{
								m_pDB->removeDataFromDB(pRec->getFileIndex());
							}
						}
						else
							AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);

						AfxGetApp()->EndWaitCursor();
					}
				}//if(pRec)
			}
		}//while(pos)
		OnRefresh();
	}//if(pRows != NULL)
}


void CLandMarkView::saveStandToDB()
{
	CString csStrNotOk;

	CXTPReportSelectedRows* pRows = GetReportCtrl().GetSelectedRows();

	if(pRows != NULL)
	{
	//get focused row
	//CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
		POSITION pos = pRows->GetFirstSelectedRowPosition();

		while(pos)
		{
			CXTPReportRow *pRow = pRows->GetNextSelectedRow(pos);
			if(!pRow)
				return;

			//get record
			CStandSelListReportRec *pRec = (CStandSelListReportRec*)pRow->GetRecord();
			if(pRec)
			{
				if(m_pDB != NULL)
				{
					if(m_pDB->saveDataToDB(pRec) == FALSE)
					{
						if(fileExists(m_sLangFN))
						{
							RLFReader *xml = new RLFReader;
							if(xml->Load(m_sLangFN))
							{
								csStrNotOk = xml->str(IDS_STRING160);
							}
							delete xml;
						}
						AfxMessageBox(csStrNotOk);
					}
				}
				else
					AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			}
		}//while(pos)
	}//if(pRows != NULL)
}

void CLandMarkView::OnTBBTNReports(void)
{
	int index = 0;

	CXTPReportSelectedRows* pRows = GetReportCtrl().GetSelectedRows();
	CXTPReportRow * pRow = NULL;

	if(pRows != NULL)
	{
		if(pRows->GetCount() == 1)
		{
			pRow = GetReportCtrl().GetFocusedRow();
			if(pRow)
			{
				CStandSelListReportRec *pRec = (CStandSelListReportRec*)pRow->GetRecord();
				if(pRec)
					index = pRec->getFileIndex();
			}			
		}
	}

	showFormView(IDD_FORMVIEW1, m_sLangFN, index);
}

void CLandMarkView::OnBtnDown(NMHDR *pNotifyStruct, LRESULT *)
{
	LPNMKEY lpNMKEY = (LPNMKEY)pNotifyStruct;

	if(!GetReportCtrl().GetFocusedRow())
		return;

	// Delete focused class
	if(lpNMKEY->nVKey == VK_DELETE)
	{
		deleteStandFromDB();
	}

	
}


void CLandMarkView::SetFocusedTract(int tractid)
{
	CXTPReportRows* pRows = GetReportCtrl().GetRows();
	CXTPReportRow * pRow = NULL;
	BOOL bFound = FALSE;

	if(pRows != NULL)
	{
		for(int i=0; i<=pRows->GetCount()-1;i++)
		{
			pRow = pRows->GetAt(i);
			if(pRow)
			{
				CStandSelListReportRec *pRec = (CStandSelListReportRec*)pRow->GetRecord();
				if(pRec)
				{
					if(tractid == pRec->getFileIndex())
					{
						GetReportCtrl().SetFocusedRow(pRow);
						bFound = TRUE;
					}
				}
			}			
		}
	}

	if(bFound == FALSE)
	{
		AfxMessageBox(m_sTractGISError,MB_ICONEXCLAMATION);
	}
}