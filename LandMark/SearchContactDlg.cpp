// SearchContactDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "SearchContactDlg.h"


// CSearchContactDlg dialog

IMPLEMENT_DYNAMIC(CSearchContactDlg, CDialog)

CSearchContactDlg::CSearchContactDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSearchContactDlg::IDD, pParent)
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_sTitle = _T("");
	m_sNoCategory = _T("");
}

CSearchContactDlg::~CSearchContactDlg()
{
	if(m_pDB != NULL)
		delete m_pDB;
}

void CSearchContactDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LBL_SR_CATEGORIES, m_wndLblCategories);
	DDX_Control(pDX, IDC_LBL_SR_NAME, m_wndLblName);
	DDX_Control(pDX, IDC_EDIT_SR_NAME, m_wndEditName);
	DDX_Control(pDX, IDC_BTN_SR_SEARCH, m_wndBtnSearch);
	DDX_Control(pDX, IDC_BTN_SR_CLEAR, m_wndBtnClear);
	DDX_Control(pDX, IDC_COMBO_SR_CATEGORIES, m_wndCBoxCategories);
	DDX_Control(pDX, IDOK, m_wndBtnOk);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
}


BEGIN_MESSAGE_MAP(CSearchContactDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_SR_SEARCH, &CSearchContactDlg::OnBnClickedBtnSrSearch)
	ON_BN_CLICKED(IDC_BTN_SR_CLEAR, &CSearchContactDlg::OnBnClickedBtnSrClear)
	ON_BN_CLICKED(IDOK, &CSearchContactDlg::OnBnClickedOk)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()


// CSearchContactDlg message handlers


BOOL CSearchContactDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);
	
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sTitle = xml->str(IDS_STRING360);
			SetWindowText((m_sTitle));
			m_sNoCategory = (_T(""));

			m_wndLblCategories.SetWindowTextW(xml->str(IDS_STRING249));
			m_wndLblName.SetWindowTextW(xml->str(IDS_STRING271));

			m_wndBtnSearch.SetWindowTextW(xml->str(IDS_STRING361));
			m_wndBtnClear.SetWindowTextW(xml->str(IDS_STRING362));
			m_wndBtnOk.SetWindowTextW(xml->str(IDS_STRING363));
			m_wndBtnCancel.SetWindowTextW(xml->str(IDS_STRING104));
			}
		delete xml;
	}

	setupReport();
	getCategories();
	addCategoriesToCBox();
	getContacts();
	populateData();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CSearchContactDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_SEARCH_CONTACT, TRUE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING271)), 100));	//name
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING273)), 100));	//address
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING274)), 100));	//zipcode
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING275)), 100));	//city
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING276)), 100));	//state
				pCol->SetEditable( FALSE );
	
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING277)), 100));	//country
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING278)), 100));	//phone
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING246)), 100));	//fax
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_8, (xml->str(IDS_STRING282)), 100));	//email
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_9, (xml->str(IDS_STRING283)), 100));	//website
				pCol->SetEditable( FALSE );

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( TRUE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.AllowEdit(FALSE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.SetFocus();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_REPORT_SEARCH_CONTACT),2,75,rect.right - 4,rect.bottom - 115);
			}
			delete xml;
		}	// if (fileExists(sLangFN))
	
	}
}

void CSearchContactDlg::populateData(void)
{
	m_wndReport.ResetContent();
	if (m_vecContacts.size() > 0)
	{
		for (UINT i = 0;i < m_vecContacts.size();i++)
		{
			CTransaction_contacts rec = m_vecContacts[i];
			m_wndReport.AddRecord(new CSearchContactsReportDataRec(rec.getID(),rec));
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CSearchContactDlg::getCategories(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getCategories(m_vecCategories);
	}	// if (m_pDB != NULL)
}

void CSearchContactDlg::getContacts(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getContacts(m_vecContacts);
	}	// if (m_pDB != NULL)
}

void CSearchContactDlg::addCategoriesToCBox(void)
{
	// Set categories in ComboBox; 080630 p�d
	m_wndCBoxCategories.ResetContent();
	if (m_vecCategories.size() > 0)
	{
		// Start by adding 'No catrgory'; 080630 p�d
		m_wndCBoxCategories.AddString((m_sNoCategory));
		for (UINT i = 0;i < m_vecCategories.size();i++)
		{
			CTransaction_category rec = m_vecCategories[i];
			m_wndCBoxCategories.AddString((rec.getCategory()));
		}
		m_wndCBoxCategories.SetCurSel(0);	// Select first item in ComboBox; 080630 p�d
	}	// if (m_vecCategories.size() > 0)
}




BOOL CSearchContactDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	if(pData->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData, pData->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}

	return CDialog::OnCopyData(pWnd, pData);
}


void CSearchContactDlg::OnBnClickedBtnSrSearch()
{
	CString sSQL,sSQLWhere;
	BOOL bFirstAdded = FALSE;
	BOOL bIsSomethingAdded = FALSE;
	CString sCategory;
	int nIndex = m_wndCBoxCategories.GetCurSel();

	if (nIndex > 0)
	{
		m_wndCBoxCategories.GetWindowText(sCategory);
		sSQL = _T("");
	}	// if (nIndex != CB_ERR)
	else
		sSQL.Format(_T("select * from %s where "), TBL_CONTACTS);

	if (!m_wndEditName.getText().IsEmpty())
	{
		if (!bFirstAdded)
		{
			sSQLWhere.Format(_T("name_of like '%c%s%c'"),'%',m_wndEditName.getText(),'%');
			bFirstAdded = TRUE;
		}
		else
			sSQLWhere.Format(_T(" and name_of like '%c%s%c'"),'%',m_wndEditName.getText(),'%');
		sSQL += sSQLWhere;
		bIsSomethingAdded = TRUE;
	}

	
	if (m_pDB != NULL)
	{
		if (nIndex >= 1)
			m_pDB->getContacts(sCategory,sSQL,m_vecContacts);
		else if (nIndex < 1 && bIsSomethingAdded)
			m_pDB->getContacts(sSQL,m_vecContacts);
		else
			m_pDB->getContacts(m_vecContacts);

	}

	populateData();
	
}

void CSearchContactDlg::OnBnClickedBtnSrClear()
{
	m_wndEditName.SetWindowTextW(_T(""));
	m_wndCBoxCategories.SetCurSel(0);
	getContacts();
	populateData();
}

void CSearchContactDlg::OnBnClickedOk()
{
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL)
	{
		CSearchContactsReportDataRec *pRec = (CSearchContactsReportDataRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			m_nContactID = pRec->getIndex();
		}
	}
			
	OnOK();
}
