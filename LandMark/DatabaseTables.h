#pragma once

#include "Stdafx.h"

//Table names for TCruise db
#define NUM_OF_DB_TABLES 17 

#ifdef USE_TC_CONTACT
#ifdef USE_TC_PROJECTS
#define CREATE_NUM_OF_DB_TABLES 29
#else
#define CREATE_NUM_OF_DB_TABLES 26
#endif
#else
#define CREATE_NUM_OF_DB_TABLES 21 
#endif



const LPCTSTR TBL_SUM_DBH	= _T("tc_SumDbh");
const LPCTSTR TBL_PLOT_VOLUME	= _T("tc_PlotVolume");
const LPCTSTR TBL_PLOT_GRDVOLS	= _T("tc_PlotGrdVols");
const LPCTSTR TBL_TREE_GRDVOL	= _T("tc_TreeGrdVol");
const LPCTSTR TBL_STRATA	= _T("tc_Strata");
const LPCTSTR TBL_MERCH_SPECS	= _T("tc_MerchSpecs");
const LPCTSTR TBL_PROD_NMS	= _T("tc_ProdNms");
const LPCTSTR TBL_GRADE_NMS	= _T("tc_GradeNms");
const LPCTSTR TBL_PRICES	= _T("tc_Prices");
const LPCTSTR TBL_GROUPS	= _T("tc_Groups");
const LPCTSTR TBL_SPECIES	= _T("tc_Species");
const LPCTSTR TBL_SITE_INDEX	= _T("tc_SiteIndex");
const LPCTSTR TBL_TREE	= _T("tc_Tree");
const LPCTSTR TBL_PLOT	= _T("tc_Plot");
const LPCTSTR TBL_STAND	= _T("tc_Stand");
const LPCTSTR TBL_GROUP_ORDER = _T("tc_GroupOrder");
const LPCTSTR TBL_GROUP_ORDER_HDR = _T("tc_GroupOrderHdr");
const LPCTSTR TBL_COMPANY_INFO = _T("tc_CompanyInfo");
const LPCTSTR TBL_EXTERNAL_DOCS = _T("tc_ExternalDocs");
const LPCTSTR TBL_JOINEDPLOTDATA = _T("tc_JoinedPlotData");
const LPCTSTR TBL_AUDITREMEASDATA = _T("tc_AuditRemeasData");

const LPCTSTR TBL_CONTACTS		= _T("tc_Contacts");
const LPCTSTR TBL_PROPERTIES	= _T("tc_Properties");
const LPCTSTR TBL_PROPERTY_OWNER = _T("tc_Property_Owner");
const LPCTSTR TBL_CATEGORIES	=_T("tc_Categories");
const LPCTSTR TBL_CATEGORY_FOR_CONTACTS = _T("tc_Categories_for_contacts");

const LPCTSTR TBL_PROJECTS = _T("tc_Projects");
const LPCTSTR TBL_STANDS_IN_PROJECTS = _T("tc_Stands_in_Projects");
const LPCTSTR TBL_PROJECT_TYPES	= _T("tc_Project_Types");

struct DB_tables
{
	CString oldTable;
	const LPCTSTR newTable;
};

const struct DB_tables DBTables[NUM_OF_DB_TABLES] = 
{
	{_T("Stand"),TBL_STAND},
	{_T("Plot"),TBL_PLOT},
	{_T("Tree"),TBL_TREE},
	{_T("SiteIndex"),TBL_SITE_INDEX},
	{_T("Species"),TBL_SPECIES},
	{_T("Groups"),TBL_GROUPS},
	{_T("Prices"),TBL_PRICES},
	{_T("GradeNms"),TBL_GRADE_NMS},
	{_T("ProdNms"),TBL_PROD_NMS},
	{_T("MerchSpecs"),TBL_MERCH_SPECS},
	{_T("Strata"),TBL_STRATA},
	{_T("TreeGrdVol"),TBL_TREE_GRDVOL},
	{_T("PlotGrdVols"),TBL_PLOT_GRDVOLS},
	{_T("PlotVolume"),TBL_PLOT_VOLUME},
	{_T("SumDbh"),TBL_SUM_DBH},
	{_T("JoinedPlotData"), TBL_JOINEDPLOTDATA},
	{_T("AuditRemeasData"), TBL_AUDITREMEASDATA}
};

//SQL database tables

const LPCTSTR CREATE_TBL_STAND = _T("CREATE TABLE dbo.tc_Stand ( FileIndex INT NOT NULL, TractID NVARCHAR(128) NOT NULL,")
				_T("TractAcres FLOAT NULL, SpecialC SMALLINT NULL, CruiseType TINYINT NULL, RemeasType TINYINT NULL,") 
				_T("IsIBFC TINYINT NULL, CruiseDate NVARCHAR(40) NULL, TractLoc NVARCHAR(128) NULL, TractOwner NVARCHAR(128) NULL,") 
				_T("Cruiser NVARCHAR(128) NULL, OtherInfo NVARCHAR(128) NULL, FilePath NVARCHAR(255) NULL, CR01 NVARCHAR(40) NULL,")
				_T("CR02 NVARCHAR(40) NULL, CR03 NVARCHAR(40) NULL, CR04 NVARCHAR(40) NULL, CR05 NVARCHAR(40) NULL, CR06 NVARCHAR(40) NULL,") 
				_T("CR07 NVARCHAR(40) NULL, CR08 NVARCHAR(40) NULL, CR09 NVARCHAR(40) NULL, CR10 NVARCHAR(40) NULL, CR11 NVARCHAR(40) NULL,") 
				_T("CR12 NVARCHAR(40) NULL, CR13 NVARCHAR(40) NULL, CR14 NVARCHAR(40) NULL, CR15 NVARCHAR(40) NULL, CR16 NVARCHAR(40) NULL,") 
				_T("CR17 NVARCHAR(40) NULL, CR18 NVARCHAR(40) NULL, CR19 NVARCHAR(40) NULL, CR20 NVARCHAR(40) NULL, CR21 NVARCHAR(40) NULL,") 
				_T("CR22 NVARCHAR(40) NULL, CR23 NVARCHAR(40) NULL, CR24 NVARCHAR(40) NULL, CR25 NVARCHAR(40) NULL, CR26 NVARCHAR(40) NULL,") 
				_T("CR27 NVARCHAR(40) NULL, CR28 NVARCHAR(40) NULL, CR29 NVARCHAR(40) NULL, CR30 NVARCHAR(40) NULL, CR31 NVARCHAR(40) NULL,") 
				_T("CR32 NVARCHAR(40) NULL, CR33 NVARCHAR(40) NULL, CR34 NVARCHAR(40) NULL, CR35 NVARCHAR(40) NULL,")
				_T("CR36 NVARCHAR(40) NULL, CR37 NVARCHAR(40) NULL, CR38 NVARCHAR(40) NULL, CR39 NVARCHAR(40) NULL, CR40 NVARCHAR(40) NULL,") 
				_T("CR41 NVARCHAR(40) NULL, CR42 NVARCHAR(40) NULL, CR43 NVARCHAR(40) NULL, CR44 NVARCHAR(40) NULL, CR45 NVARCHAR(40) NULL,") 
				_T("CR46 NVARCHAR(40) NULL, CR47 NVARCHAR(40) NULL, CR48 NVARCHAR(40) NULL, CR49 NVARCHAR(40) NULL, CR50 NVARCHAR(40) NULL,") 
				_T("CR51 NVARCHAR(40) NULL, CR52 NVARCHAR(40) NULL, CR53 NVARCHAR(40) NULL, CR54 NVARCHAR(40) NULL, CR55 NVARCHAR(40) NULL,") 
				_T("CR56 NVARCHAR(40) NULL, CR57 NVARCHAR(40) NULL, CR58 NVARCHAR(40) NULL, CR59 NVARCHAR(40) NULL, CR60 NVARCHAR(40) NULL,") 
				_T("CR61 NVARCHAR(40) NULL, CR62 NVARCHAR(40) NULL, CR63 NVARCHAR(40) NULL, CR64 NVARCHAR(40) NULL, CR65 NVARCHAR(40) NULL,") 
				_T("CR66 NVARCHAR(40) NULL, CR67 NVARCHAR(40) NULL, CR68 NVARCHAR(40) NULL, CR69 NVARCHAR(40) NULL, CR70 NVARCHAR(40) NULL,") 
				_T("CR71 NVARCHAR(40) NULL, CR72 NVARCHAR(40) NULL, CR73 NVARCHAR(40) NULL, CR74 NVARCHAR(40) NULL, CR75 NVARCHAR(40) NULL,")
#ifdef USE_TC_CONTACT
				_T("PropID INT NULL,")
#endif
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("CrsAggM TINYINT NULL, CrsPlots INT NULL,")
				_T("\r\nPRIMARY KEY (FileIndex)")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PLOT = _T("CREATE TABLE dbo.tc_Plot (FileIndex INT NOT NULL, PlotIndex INT NOT NULL, PlotType TINYINT NULL," )
				_T("Stratum NVARCHAR(30) NULL, PlotID NVARCHAR(30) NOT NULL, Cruiser NVARCHAR(50) NULL, Longitude FLOAT NULL,")
				_T("Latitude FLOAT NULL, SwPlotType TINYINT NULL, SwPlotSz FLOAT NULL, PwPlotType TINYINT NULL, UsePwPlot TINYINT NULL," )
				_T("PwPlotSz FLOAT NULL, SmPlotType TINYINT NULL, SmPlotSz FLOAT NULL, RpPlotSz FLOAT NULL, OnPlotTime INT NULL," )
				_T("GPSVisitTyp TINYINT NULL, CR01 NVARCHAR(40) NULL, CR02 NVARCHAR(40) NULL, CR03 NVARCHAR(40) NULL, CR04 NVARCHAR(40) NULL," )
				_T("CR05 NVARCHAR(40) NULL, CR06 NVARCHAR(40) NULL, CR07 NVARCHAR(40) NULL, CR08 NVARCHAR(40) NULL, CR09 NVARCHAR(40) NULL," )
				_T("CR10 NVARCHAR(40) NULL, CR11 NVARCHAR(40) NULL, CR12 NVARCHAR(40) NULL, CR13 NVARCHAR(40) NULL, CR14 NVARCHAR(40) NULL," )
				_T("CR15 NVARCHAR(40) NULL, CR16 NVARCHAR(40) NULL, CR17 NVARCHAR(40) NULL, CR18 NVARCHAR(40) NULL, CR19 NVARCHAR(40) NULL," )
				_T("CR20 NVARCHAR(40) NULL, CR21 NVARCHAR(40) NULL, CR22 NVARCHAR(40) NULL, CR23 NVARCHAR(40) NULL, CR24 NVARCHAR(40) NULL," )
				_T("CR25 NVARCHAR(40) NULL, CR26 NVARCHAR(40) NULL, CR27 NVARCHAR(40) NULL, CR28 NVARCHAR(40) NULL, CR29 NVARCHAR(40) NULL," )
				_T("CR30 NVARCHAR(40) NULL, CR31 NVARCHAR(40) NULL, CR32 NVARCHAR(40) NULL, CR33 NVARCHAR(40) NULL, CR34 NVARCHAR(40) NULL," )
				_T("CR35 NVARCHAR(40) NULL,")
				_T("CR36 NVARCHAR(40) NULL, CR37 NVARCHAR(40) NULL, CR38 NVARCHAR(40) NULL, CR39 NVARCHAR(40) NULL, CR40 NVARCHAR(40) NULL," )
				_T("CR41 NVARCHAR(40) NULL, CR42 NVARCHAR(40) NULL, CR43 NVARCHAR(40) NULL, CR44 NVARCHAR(40) NULL, CR45 NVARCHAR(40) NULL," )
				_T("CR46 NVARCHAR(40) NULL, CR47 NVARCHAR(40) NULL, CR48 NVARCHAR(40) NULL, CR49 NVARCHAR(40) NULL, CR50 NVARCHAR(40) NULL," )
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, PlotIndex)," )
				_T("\r\nCONSTRAINT FK_TC_Plot")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_TREE = _T("CREATE TABLE dbo.tc_Tree (FileIndex INT NOT NULL, PlotIndex INT NOT NULL, TRecIndex INT NOT NULL, ")
				_T("TRecType TINYINT NULL, Measurement TINYINT NULL, SpeciesCode NVARCHAR(10) NULL, SpeciesName NVARCHAR(25) NULL," )
				_T("GroupName NVARCHAR(30) NULL, Dbh REAL NULL, ProductNumb TINYINT NULL, ProductName NVARCHAR(16) NULL, TQIPrd TINYINT NULL," )
				_T("Count_ INT NULL, GSF Real NULL,")
				_T("PACF FLOAT NULL, DefStumpHt REAL NULL, HmObs REAL NULL, HsObs REAL NULL, HpObs REAL NULL, HmEst REAL NULL, HsEst REAL NULL," )
				_T("HpEst REAL NULL, Top_ REAL NULL, Defect SMALLINT NULL, FcObs REAL NULL, FcEst REAL NULL, Age REAL NULL, Ht REAL NULL," )
				_T("RadialGr REAL NULL, SngBark REAL NULL, ReproPlot TINYINT NULL, OffPlot TINYINT NULL, SubMerch TINYINT NULL, PWOnly TINYINT NULL," )
				_T("TClass TINYINT NULL, TClassName NVARCHAR(10) NULL, TClass2 TINYINT NULL, TClassName2 NVARCHAR(10) NULL, IsGradeTree TINYINT NULL," )
				_T("GrdStumpHt REAL NULL, StopTop REAL NULL, TopType TINYINT NULL, TopTypeV NVARCHAR(4) NULL, SGL1 TINYINT NULL, GRD1 TINYINT NULL," )
				_T("GRDV1 NVARCHAR(4) NULL, DEF1 TINYINT NULL, SGL2 TINYINT NULL, GRD2 TINYINT NULL, GRDV2 NVARCHAR(4) NULL, DEF2 TINYINT NULL," )
				_T("SGL3 TINYINT NULL, GRD3 TINYINT NULL, GRDV3 NVARCHAR(4) NULL, DEF3 TINYINT NULL, SGL4 TINYINT NULL, GRD4 TINYINT NULL," )
				_T("GRDV4 NVARCHAR(4) NULL, DEF4 TINYINT NULL, SGL5 TINYINT NULL, GRD5 TINYINT NULL, GRDV5 NVARCHAR(4) NULL, DEF5 TINYINT NULL," )
				_T("SGL6 TINYINT NULL, GRD6 TINYINT NULL, GRDV6 NVARCHAR(4) NULL, DEF6 TINYINT NULL, SGL7 TINYINT NULL, GRD7 TINYINT NULL, GRDV7 NVARCHAR(4) NULL," )
				_T("DEF7 TINYINT NULL, LogTrim REAL NULL, BA FLOAT NULL, PW_CFVOB FLOAT NULL, PW_CFVIB FLOAT NULL, PW_GreenTons FLOAT NULL, PW_WtCords FLOAT NULL," )
				_T("SW_CFVOB FLOAT NULL, SW_CFVIB FLOAT NULL, SW_GreenTons FLOAT NULL, SW_WtCords FLOAT NULL, IntQuarter FLOAT NULL, Doyle FLOAT NULL," )
				_T("Scribner FLOAT NULL, PW_Dollars FLOAT NULL, SW_Dollars FLOAT NULL, CC01 NVARCHAR(40) NULL, CC02 NVARCHAR(40) NULL, CC03 NVARCHAR(40) NULL," )
				_T("CC04 NVARCHAR(40) NULL, CC05 NVARCHAR(40) NULL, CC06 NVARCHAR(40) NULL, CC07 NVARCHAR(40) NULL, CC08 NVARCHAR(40) NULL, CC09 NVARCHAR(40) NULL," )
				_T("CC10 NVARCHAR(40) NULL, CC11 NVARCHAR(40) NULL, CC12 NVARCHAR(40) NULL, CC13 NVARCHAR(40) NULL, CC14 NVARCHAR(40) NULL, CC15 NVARCHAR(40) NULL," )
				_T("CC16 NVARCHAR(40) NULL, CC17 NVARCHAR(40) NULL, CC18 NVARCHAR(40) NULL, CC19 NVARCHAR(40) NULL, CC20 NVARCHAR(40) NULL, CC21 NVARCHAR(40) NULL," )
				_T("CC22 NVARCHAR(40) NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("SGL8 tinyint null, GRD8 tinyint null, GRDV8 varchar(4) null, DEF8 tinyint null, ")	//#3750
                _T("SGL9 tinyint null, GRD9 tinyint null, GRDV9 varchar(4) null, DEF9 tinyint null, ")
                _T("SGL10 tinyint null, GRD10 tinyint null, GRDV10 varchar(4) null, DEF10 tinyint null, ")
                _T("SGL11 tinyint null, GRD11 tinyint null, GRDV11 varchar(4) null, DEF11 tinyint null, ")
                _T("SGL12 tinyint null, GRD12 tinyint null, GRDV12 varchar(4) null, DEF12 tinyint null, ")
                _T("SGL13 tinyint null, GRD13 tinyint null, GRDV13 varchar(4) null, DEF13 tinyint null, ")
                _T("SGL14 tinyint null, GRD14 tinyint null, GRDV14 varchar(4) null, DEF14 tinyint null, ")
                _T("SGL15 tinyint null, GRD15 tinyint null, GRDV15 varchar(4) null, DEF15 tinyint null, ")
                _T("SGL16 tinyint null, GRD16 tinyint null, GRDV16 varchar(4) null, DEF16 tinyint null, ")
                _T("SGL17 tinyint null, GRD17 tinyint null, GRDV17 varchar(4) null, DEF17 tinyint null, ")
                _T("SGL18 tinyint null, GRD18 tinyint null, GRDV18 varchar(4) null, DEF18 tinyint null, ")
                _T("SGL19 tinyint null, GRD19 tinyint null, GRDV19 varchar(4) null, DEF19 tinyint null, ")
                _T("SGL20 tinyint null, GRD20 tinyint null, GRDV20 varchar(4) null, DEF20 tinyint null, ")
				_T("\r\nPRIMARY KEY (FileIndex, PlotIndex, TRecIndex),")
				_T("\r\nCONSTRAINT FK_TC_Tree")
				_T("\r\nFOREIGN KEY (FileIndex, Plotindex)")
				_T("\r\nREFERENCES tc_Plot (FileIndex, PlotIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_SITE_INDEX = _T("CREATE TABLE dbo.tc_SiteIndex (FileIndex INT NOT NULL, SiSpcCode NVARCHAR(8) NULL, SiSpcName NVARCHAR(32) NULL," )
				_T("SiTrees SMALLINT NULL, SiBaseAge TINYINT NULL, AvAge REAL NULL, AvHD REAL NULL, SiteIndex REAL NOT NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, SiteIndex),")
				_T("\r\nCONSTRAINT FK_TC_SiteIndex")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_SPECIES = _T("CREATE TABLE dbo.tc_Species (FileIndex INT NOT NULL,  Spc SMALLINT NOT NULL, SpcCode NVARCHAR(10) NULL, SpcName NVARCHAR(25) NULL," )
				_T("Profl INT NULL, ProflAlt INT NULL, SpcGrp SMALLINT NULL, GroupName NVARCHAR(30),")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, Spc),")
				_T("\r\nCONSTRAINT FK_TC_Species")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_GROUPS = _T("CREATE TABLE dbo.tc_Groups (FileIndex INT NOT NULL,  SpcGrp SMALLINT NOT NULL, GroupName NVARCHAR(30) NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex,SpcGrp),")
				_T("\r\nCONSTRAINT FK_TC_Groups")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PRICES = _T("CREATE TABLE dbo.tc_Prices (FileIndex INT NOT NULL,  SpcGrp SMALLINT NOT NULL, Case_ TINYINT NULL," )
				_T("Prd TINYINT NOT NULL, PwPrc REAL NULL, PwVu TINYINT NULL, SwPrc REAL NULL, SwVu TINYINT NULL, G01Prc REAL NULL," )
				_T("G01Vu TINYINT NULL, G02Prc REAL NULL, G02Vu TINYINT NULL, G03Prc REAL NULL, G03Vu TINYINT NULL, G04Prc REAL NULL," )
				_T("G04Vu TINYINT NULL, G05Prc REAL NULL, G05Vu TINYINT NULL, G06Prc REAL NULL, G06Vu TINYINT NULL, G07Prc REAL NULL," )
				_T("G07Vu TINYINT NULL, G08Prc REAL NULL, G08Vu TINYINT NULL, G09Prc REAL NULL, G09Vu TINYINT NULL, G10Prc REAL NULL," )
				_T("G10Vu TINYINT NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, SpcGrp, Prd),")
				_T("\r\nCONSTRAINT FK_TC_Prices")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_GRADE_NMS = _T("CREATE TABLE dbo.tc_GradeNms (FileIndex INT NOT NULL,  SpcGrp SMALLINT NOT NULL, SwCd NVARCHAR(12) NULL," )
				_T("PwCd NVARCHAR(12) NULL, CullCd NVARCHAR(12) NULL, TipCd NVARCHAR(12) NULL, G01Cd NVARCHAR(12) NULL, G02Cd NVARCHAR(12) NULL," )
				_T("G03Cd NVARCHAR(12) NULL, G04Cd NVARCHAR(12) NULL, G05Cd NVARCHAR(12) NULL, G06Cd NVARCHAR(12) NULL, G07Cd NVARCHAR(12) NULL," )
				_T("G08Cd NVARCHAR(12) NULL, G09Cd NVARCHAR(12) NULL, G10Cd NVARCHAR(12) NULL, SwNm NVARCHAR(30) NULL, PwNm NVARCHAR(30) NULL," )
				_T("CullNm NVARCHAR(30) NULL, TipNm NVARCHAR(30) NULL, G01Nm NVARCHAR(30) NULL, G02Nm NVARCHAR(30) NULL, G03Nm NVARCHAR(30) NULL," )
				_T("G04Nm NVARCHAR(30) NULL, G05Nm NVARCHAR(30) NULL, G06Nm NVARCHAR(30) NULL, G07Nm NVARCHAR(30) NULL, G08Nm NVARCHAR(30) NULL," )
				_T("G09Nm NVARCHAR(30) NULL, G10Nm NVARCHAR(30) NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, SpcGrp),")
				_T("\r\nCONSTRAINT FK_TC_GradeNms")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PROD_NMS = _T("CREATE TABLE dbo.tc_ProdNms (FileIndex INT NOT NULL,  SpcGrp SMALLINT NOT NULL, PwCd NVARCHAR(12) NULL," )
				_T("CsCd NVARCHAR(12) NULL, PlCd NVARCHAR(12) NULL, SlCd NVARCHAR(12) NULL, PwNm NVARCHAR(30) NULL, CsNm NVARCHAR(30) NULL," )
				_T("PlNm NVARCHAR(30) NULL, SlNm NVARCHAR(30) NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, SpcGrp),")
				_T("\r\nCONSTRAINT FK_TC_ProdNms")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_MERCH_SPECS = _T("CREATE TABLE dbo.tc_MerchSpecs (FileIndex INT NOT NULL, SpcGrp SMALLINT NOT NULL, GroupName NVARCHAR(30) NULL," )
				_T("Prd SMALLINT NOT NULL, Comp TINYINT NULL, TDbh REAL NULL, PwTop REAL NULL, SlTop REAL NULL, MaxEndD REAL NULL, LogLen REAL NULL,") 
				_T("StmpHt REAL NULL, PCF REAL NULL, PCD REAL NULL, FC REAL NULL, MinHt REAL NULL, MaxLen REAL NULL, PCFIsIB REAL NULL, HtRCTop REAL NULL," )
				_T("PrdRD TINYINT NULL, PwTopPCF REAL NULL, PwTopPCD REAL NULL, MinPwLen REAL NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, SpcGrp, Prd),")
				_T("\r\nCONSTRAINT FK_TC_MerchSpecs")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_STRATA = _T("CREATE TABLE dbo.tc_Strata (FileIndex INT NOT NULL, StratumIndex INT NOT NULL, RecType TINYINT NULL," )
				_T("IsDeleted TINYINT NULL, IsDefValue TINYINT NULL, Stratum NVARCHAR(30) NULL, Acres FLOAT NULL, CR01 NVARCHAR(40) NULL," )
				_T("CR02 NVARCHAR(40) NULL, CR03 NVARCHAR(40) NULL, CR04 NVARCHAR(40) NULL, CR05 NVARCHAR(40) NULL, CR06 NVARCHAR(40) NULL, CR07 NVARCHAR(40) NULL,") 
				_T("CR08 NVARCHAR(40) NULL, CR09 NVARCHAR(40) NULL, CR10 NVARCHAR(40) NULL, CR11 NVARCHAR(40) NULL, CR12 NVARCHAR(40) NULL, CR13 NVARCHAR(40) NULL,") 
				_T("CR14 NVARCHAR(40) NULL, CR15 NVARCHAR(40) NULL, CR16 NVARCHAR(40) NULL, CR17 NVARCHAR(40) NULL, CR18 NVARCHAR(40) NULL, CR19 NVARCHAR(40) NULL,") 
				_T("CR20 NVARCHAR(40) NULL, CR21 NVARCHAR(40) NULL, CR22 NVARCHAR(40) NULL, CR23 NVARCHAR(40) NULL, CR24 NVARCHAR(40) NULL, CR25 NVARCHAR(40) NULL,") 
				_T("CR26 NVARCHAR(40) NULL, CR27 NVARCHAR(40) NULL, CR28 NVARCHAR(40) NULL, CR29 NVARCHAR(40) NULL, CR30 NVARCHAR(40) NULL, CR31 NVARCHAR(40) NULL," )
				_T("CR32 NVARCHAR(40) NULL, CR33 NVARCHAR(40) NULL, CR34 NVARCHAR(40) NULL, CR35 NVARCHAR(40) NULL,")
				_T("CR36 NVARCHAR(40) NULL, CR37 NVARCHAR(40) NULL, CR38 NVARCHAR(40) NULL, CR39 NVARCHAR(40) NULL, CR40 NVARCHAR(40) NULL," )
				_T("CR41 NVARCHAR(40) NULL, CR42 NVARCHAR(40) NULL, CR43 NVARCHAR(40) NULL, CR44 NVARCHAR(40) NULL, CR45 NVARCHAR(40) NULL," )
				_T("CR46 NVARCHAR(40) NULL, CR47 NVARCHAR(40) NULL, CR48 NVARCHAR(40) NULL, CR49 NVARCHAR(40) NULL, CR50 NVARCHAR(40) NULL," )
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, StratumIndex),")
				_T("\r\nCONSTRAINT FK_TC_Strata")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_TREE_GRDVOL = _T("CREATE TABLE dbo.tc_TreeGrdVol (FileIndex INT NOT NULL, PlotIndex INT NOT NULL, TRecIndex INT NOT NULL," )
				_T("LogBolt TINYINT NOT NULL, Seg TINYINT NULL, OrgGrd TINYINT NULL, OrgGrdNm NVARCHAR(4) NULL, AsgGrd TINYINT NULL," )
				_T("AsgGrdNm NVARCHAR(4) NULL, Len FLOAT NULL, TrimLen FLOAT NULL, SoundFrac REAL NULL, SeDob REAL NULL, SeDib REAL NULL," )
				_T("LeDob REAL NULL, LeDib REAL NULL, PwVob FLOAT NULL, PwVib FLOAT NULL, PwTons FLOAT NULL, PwCords FLOAT NULL, PwDollar FLOAT NULL," )
				_T("SawVob FLOAT NULL, SawVib FLOAT NULL, SawTons FLOAT NULL, SawCords FLOAT NULL,  IntQuarter FLOAT NULL, Doyle FLOAT NULL," )
				_T("Scribner FLOAT NULL, SawDollar FLOAT NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, PlotIndex, TRecIndex, LogBolt),")
				_T("\r\nCONSTRAINT FK_TC_TreeGrdVol")
				_T("\r\nFOREIGN KEY (FileIndex, PlotIndex, TRecIndex)")
				_T("\r\nREFERENCES tc_Tree (FileIndex, PlotIndex, TRecIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PLOT_GRDVOLS = _T("CREATE TABLE dbo.tc_PlotGrdVols (FileIndex INT NOT NULL, PlotIndex INT NOT NULL, GroupName NVARCHAR(30) NOT NULL," )
				_T("PrdNum TINYINT NOT NULL, PrdNm NVARCHAR(16), GrdNum TINYINT NOT NULL, GrdNm NVARCHAR(4) NULL, CO FLOAT NULL, CI FLOAT NULL," )
				_T("TN FLOAT NULL, CD FLOAT NULL, IN_ FLOAT NULL, DY FLOAT NULL, SC FLOAT NULL, DL FLOAT NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, PlotIndex, GroupName, PrdNum, GrdNum),")
				_T("\r\nCONSTRAINT FK_TC_PlotGrdVols")
				_T("\r\nFOREIGN KEY (FileIndex, PlotIndex)")
				_T("\r\nREFERENCES tc_Plot (FileIndex, PlotIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PLOT_VOLUME = _T("CREATE TABLE dbo.tc_PlotVolume (FileIndex INT NOT NULL, PlotIndex INT NOT NULL, GroupName NVARCHAR(30) NOT NULL," )
				_T("ProductNumb TINYINT NOT NULL, ProductName NVARCHAR(16), TPA FLOAT NULL, BA FLOAT NULL, PW_CFVOB FLOAT NULL, PW_CFVIB FLOAT NULL," )
				_T("PW_GreenTons FLOAT NULL, PW_WtCords FLOAT NULL, SW_CFVOB FLOAT NULL, SW_CFVIB FLOAT NULL, SW_GreenTons FLOAT NULL, SW_WtCords FLOAT NULL," )
				_T("IntQuarter FLOAT NULL, Doyle FLOAT NULL, Scribner FLOAT NULL, PW_Dollars FLOAT NULL, SW_Dollars FLOAT NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, PlotIndex, GroupName, ProductNumb),")
				_T("\r\nCONSTRAINT FK_TC_PlotVolume")
				_T("\r\nFOREIGN KEY (FileIndex, PlotIndex)")
				_T("\r\nREFERENCES tc_Plot (FileIndex, PlotIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_SUM_DBH = _T("CREATE TABLE dbo.tc_SumDbh (FileIndex INT NOT NULL, GroupName NVARCHAR(30) NOT NULL, DbhCls TINYINT NOT NULL," )
				_T("ProductNumb TINYINT NOT NULL, ProductName NVARCHAR(16), TPA FLOAT NULL, BA FLOAT NULL, PW_CFVOB FLOAT NULL, PW_CFVIB FLOAT NULL," )
				_T("PW_GreenTons FLOAT NULL, PW_WtCords FLOAT NULL, SW_CFVOB FLOAT NULL, SW_CFVIB FLOAT NULL, SW_GreenTons FLOAT NULL," )
				_T("SW_WtCords FLOAT NULL, IntQuarter FLOAT NULL, Doyle FLOAT NULL, Scribner FLOAT NULL, PW_Dollars FLOAT NULL, SW_Dollars FLOAT NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, GroupName, DbhCls, ProductNumb),")
				_T("\r\nCONSTRAINT FK_TC_SumDBH")
				_T("\r\nFOREIGN KEY (FileIndex)")
				_T("\r\nREFERENCES tc_Stand (FileIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_GROUP_ORDER = _T("CREATE TABLE dbo.tc_GroupOrder (ID INT NOT NULL, PrintOrder INT NULL,")
				_T("GroupName NVARCHAR(50), ClassIndex INT NULL,")
				_T("ClassName NVARCHAR(50) NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (ID),")
				_T("\r\n);");

//********************************************************************************************************
//updated 091009 with print order
const LPCTSTR CREATE_TBL_GROUP_ORDER_HDR = _T("CREATE TABLE dbo.tc_GroupOrderHdr (ClassIndex INT NOT NULL, ClassName NVARCHAR(50) NOT NULL,PrintOrder INT NULL")
											 _T("\r\nPRIMARY KEY(ClassIndex),")
											 _T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_COMPANY_INFO_2000 = _T("CREATE TABLE dbo.tc_CompanyInfo (Id INT NOT NULL, Company NVARCHAR(50) NULL,  Address NVARCHAR(50) NULL,")
										  _T("Address2 NVARCHAR(50) NULL, City NVARCHAR(50) NULL, ZipCode NVARCHAR(20) NULL, State NVARCHAR(50) NULL,")
										  _T("Country NVARCHAR(50) NULL, PhoneNumber NVARCHAR(30) NULL,")
										  _T("FaxNumber NVARCHAR(30) NULL, Email NVARCHAR(50) NULL, WebSite NVARCHAR(50) NULL,")
										  _T("PictureType INT NULL, Picture image NULL,")
										  _T("\r\nPRIMARY KEY (Id)")
										  _T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_COMPANY_INFO_2005 = _T("CREATE TABLE dbo.tc_CompanyInfo (Id INT NOT NULL, Company NVARCHAR(50) NULL,  Address NVARCHAR(50) NULL,")
										  _T("Address2 NVARCHAR(50) NULL, City NVARCHAR(50) NULL, ZipCode NVARCHAR(20) NULL, State NVARCHAR(50) NULL,")
										  _T("Country NVARCHAR(50) NULL, PhoneNumber NVARCHAR(30) NULL,")
										  _T("FaxNumber NVARCHAR(30) NULL, Email NVARCHAR(50) NULL, WebSite NVARCHAR(50) NULL,")
										  _T("PictureType INT NULL, Picture VARBINARY(MAX) NULL,")
										  _T("\r\nPRIMARY KEY (Id)")
										  _T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_EXTERNAL_DOCS = _T("CREATE TABLE dbo.tc_ExternalDocs (extdoc_id int NOT NULL,")	//IDENTITY(1,1) NOT NULL,")
										_T("extdoc_link_tbl varchar(127) NOT NULL,")
										_T("extdoc_link_id int,") /* Ex. ObjectID,PropertyID,ContatctID etc. */\
										_T("extdoc_link_type tinyint,")
										_T("extdoc_url varchar(255),") /* URL to document */\
										_T("extdoc_note varchar(127),") /* Small note */\
										_T("extdoc_created_by varchar(30),")
										_T("extdoc_created varchar(20),")
										_T("PRIMARY KEY (extdoc_id,extdoc_link_tbl));");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_JOINEDPLOTDATA =	_T("CREATE TABLE dbo.tc_JoinedPlotData (FileIndex INT NOT NULL, PlotIndex INT NOT NULL,")
											_T("ProductNumb TINYINT NOT NULL, SumOfTPA FLOAT NULL, SumOfBA FLOAT NULL, SumOfPW_GreenTons FLOAT NULL, ")
											_T("SumOfSW_GreenTons FLOAT NULL, SumOfPW_Dollars FLOAT NULL, SumOfSW_Dollars FLOAT NULL, ")
											_T("SumOf_Doyle FLOAT NULL, SumOf_Scribner FLOAT NULL, SumOf_IntQuarter FLOAT NULL, ")
											_T("PlotID NVARCHAR(30) NOT NULL, Stratum NVARCHAR(30) NULL, ")
											_T("CR01 NVARCHAR(40) NULL, CR02 NVARCHAR(40) NULL, CR03 NVARCHAR(40) NULL, CR04 NVARCHAR(40) NULL,")
											_T("CR05 NVARCHAR(40) NULL, CR06 NVARCHAR(40) NULL, CR07 NVARCHAR(40) NULL, CR08 NVARCHAR(40) NULL, CR09 NVARCHAR(40) NULL," )
											_T("CR10 NVARCHAR(40) NULL, CR11 NVARCHAR(40) NULL, CR12 NVARCHAR(40) NULL, CR13 NVARCHAR(40) NULL, CR14 NVARCHAR(40) NULL," )
											_T("CR15 NVARCHAR(40) NULL, CR16 NVARCHAR(40) NULL, CR17 NVARCHAR(40) NULL, CR18 NVARCHAR(40) NULL, CR19 NVARCHAR(40) NULL," )
											_T("CR20 NVARCHAR(40) NULL, CR21 NVARCHAR(40) NULL, CR22 NVARCHAR(40) NULL, CR23 NVARCHAR(40) NULL, CR24 NVARCHAR(40) NULL," )
											_T("CR25 NVARCHAR(40) NULL, CR26 NVARCHAR(40) NULL, CR27 NVARCHAR(40) NULL, CR28 NVARCHAR(40) NULL, CR29 NVARCHAR(40) NULL," )
											_T("CR30 NVARCHAR(40) NULL, CR31 NVARCHAR(40) NULL, CR32 NVARCHAR(40) NULL, CR33 NVARCHAR(40) NULL, CR34 NVARCHAR(40) NULL," )
											_T("CR35 NVARCHAR(40) NULL,")
											_T("CR36 NVARCHAR(40) NULL, CR37 NVARCHAR(40) NULL, CR38 NVARCHAR(40) NULL, CR39 NVARCHAR(40) NULL, CR40 NVARCHAR(40) NULL," )
											_T("CR41 NVARCHAR(40) NULL, CR42 NVARCHAR(40) NULL, CR43 NVARCHAR(40) NULL, CR44 NVARCHAR(40) NULL, CR45 NVARCHAR(40) NULL," )
											_T("CR46 NVARCHAR(40) NULL, CR47 NVARCHAR(40) NULL, CR48 NVARCHAR(40) NULL, CR49 NVARCHAR(40) NULL, CR50 NVARCHAR(40) NULL," )
											_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
											_T("\r\nPRIMARY KEY (FileIndex, PlotIndex),")
											_T("\r\nCONSTRAINT FK_TC_JoinedPlotData")
											_T("\r\nFOREIGN KEY (FileIndex, PlotIndex)")
											_T("\r\nREFERENCES tc_Plot (FileIndex, PlotIndex)")
											_T("\r\nON DELETE CASCADE")
											_T("\r\nON UPDATE CASCADE")
											_T("\r\n);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_AUDITREMEASDATA = _T("CREATE TABLE dbo.tc_AuditRemeasData (FileIndex INT NOT NULL, PlotIndex INT NOT NULL, TRecIndex INT NOT NULL, ")
				_T("TRecType REAL NULL, Measurement REAL NULL, SpeciesCode REAL NULL, SpeciesName REAL NULL," )
				_T("GroupName REAL NULL, Dbh REAL NULL, ProductNumb REAL NULL, ProductName REAL NULL, TQIPrd REAL NULL," )
				_T("Count_ REAL NULL, GSF Real NULL,")
				_T("PACF FLOAT NULL, DefStumpHt REAL NULL, HmObs REAL NULL, HsObs REAL NULL, HpObs REAL NULL, HmEst REAL NULL, HsEst REAL NULL," )
				_T("HpEst REAL NULL, Top_ REAL NULL, Defect REAL NULL, FcObs REAL NULL, FcEst REAL NULL, Age REAL NULL, Ht REAL NULL," )
				_T("RadialGr REAL NULL, SngBark REAL NULL, ReproPlot REAL NULL, OffPlot REAL NULL, SubMerch REAL NULL, PWOnly REAL NULL," )
				_T("TClass REAL NULL, TClassName REAL NULL, TClass2 REAL NULL, TClassName2 REAL NULL, IsGradeTree REAL NULL," )
				_T("GrdStumpHt REAL NULL, StopTop REAL NULL, TopType REAL NULL, TopTypeV REAL NULL, SGL1 REAL NULL, GRD1 REAL NULL," )
				_T("GRDV1 REAL NULL, DEF1 REAL NULL, SGL2 REAL NULL, GRD2 REAL NULL, GRDV2 REAL NULL, DEF2 REAL NULL," )
				_T("SGL3 REAL NULL, GRD3 REAL NULL, GRDV3 REAL NULL, DEF3 REAL NULL, SGL4 REAL NULL, GRD4 REAL NULL," )
				_T("GRDV4 REAL NULL, DEF4 REAL NULL, SGL5 REAL NULL, GRD5 REAL NULL, GRDV5 REAL NULL, DEF5 REAL NULL," )
				_T("SGL6 REAL NULL, GRD6 REAL NULL, GRDV6 REAL NULL, DEF6 REAL NULL, SGL7 REAL NULL, GRD7 REAL NULL, GRDV7 REAL NULL," )
				_T("DEF7 REAL NULL, LogTrim REAL NULL, BA FLOAT NULL, PW_CFVOB FLOAT NULL, PW_CFVIB FLOAT NULL, PW_GreenTons FLOAT NULL, PW_WtCords FLOAT NULL," )
				_T("SW_CFVOB FLOAT NULL, SW_CFVIB FLOAT NULL, SW_GreenTons FLOAT NULL, SW_WtCords FLOAT NULL, IntQuarter FLOAT NULL, Doyle FLOAT NULL," )
				_T("Scribner FLOAT NULL, PW_Dollars FLOAT NULL, SW_Dollars FLOAT NULL, CC01 FLOAT NULL, CC02 FLOAT NULL, CC03 FLOAT NULL," )
				_T("CC04 FLOAT NULL, CC05 FLOAT NULL, CC06 FLOAT NULL, CC07 FLOAT NULL, CC08 FLOAT NULL, CC09 FLOAT NULL," )
				_T("CC10 FLOAT NULL, CC11 FLOAT NULL, CC12 FLOAT NULL, CC13 FLOAT NULL, CC14 FLOAT NULL, CC15 FLOAT NULL," )
				_T("CC16 FLOAT NULL, CC17 FLOAT NULL, CC18 FLOAT NULL, CC19 FLOAT NULL, CC20 FLOAT NULL, CC21 FLOAT NULL," )
				_T("CC22 FLOAT NULL,")
				_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
				_T("\r\nPRIMARY KEY (FileIndex, PlotIndex, TRecIndex),")
				_T("\r\nCONSTRAINT FK_TC_AuditRemeas")
				_T("\r\nFOREIGN KEY (FileIndex, Plotindex)")
				_T("\r\nREFERENCES tc_Plot (FileIndex, PlotIndex)")
				_T("\r\nON DELETE CASCADE")
				_T("\r\nON UPDATE CASCADE")
				_T("\r\n);");


//********************************************************************************************************
const LPCTSTR CREATE_TBL_CONTACTS = _T("CREATE TABLE dbo.tc_Contacts (")
																	 _T("id int NOT NULL IDENTITY,")
																	 _T("pnr_orgnr NVARCHAR(50),")		/* Not Used, Personnummer/Organinsationsnummer */\
																	 _T("name_of NVARCHAR(50),")		/* Namn */\
																	 _T("company NVARCHAR(50),")		/* F�retag */\
																	 _T("address NVARCHAR(50),")		/* Adress */\
																	 _T("post_num NVARCHAR(10),")		/* Postnummer */\
																	 _T("post_address NVARCHAR(60),")	/* Postadress */\
																	 _T("county NVARCHAR(40),")		/* L�n */\
																	 _T("country NVARCHAR(40),")		/* Land */\
																	 _T("phone_work NVARCHAR(25),")	/* Telefon arbete */\
																	 _T("phone_home NVARCHAR(25),")	/* Telefon hem */\
																	 _T("fax_number NVARCHAR(25),")	/* Faxnummer */\
																	 _T("mobile NVARCHAR(30),")		/* Mobil */\
																	 _T("e_mail NVARCHAR(50),")		/* E-mail adress */\
																	 _T("web_site NVARCHAR(50),")		/* Hemsida */\
																	 _T("notes NTEXT,")				/* Noteringar BLOB */\
																	 _T("created_by NVARCHAR(20),")	/* Skapad av */\
																	 _T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
																	 _T("vat_number NVARCHAR(40),")		/* Not Used, Momsredovisningsnummer */\
																	 _T("connect_id int,")						/* Pekar p� ett F�retag i detta register */\
																	 _T("type_of int,")								/* Kan vara 0 = Person,1 = F�retag */\
																	 _T("picture varbinary(MAX),")		/* Inneh�ller ex. Logo */																	 
																	 _T("co_address NVARCHAR(50),")		/* C/O Adress */
																	 _T("\r\nPRIMARY KEY (id));");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PROPERTIES = _T("CREATE TABLE dbo.tc_Properties (")
																	_T("id int NOT NULL IDENTITY,")
																	_T("location_code NVARCHAR(5),")		/* Location kod */\
																	_T("state_code NVARCHAR(5),")	/* state kod */\
																	_T("county_code NVARCHAR(5),")		/* county kod */\
																	_T("location_name NVARCHAR(31),")	/* Namn p� Location */\
																	_T("state_name NVARCHAR(31),")	/* Namn p� State */\
																	_T("county_name NVARCHAR(31),")	/* Namn p� County */\
																	_T("prop_number NVARCHAR(20),")	/* Fastighetsnummer */\
																	_T("prop_name NVARCHAR(40),")		/* Namn p� Fastighet */\
																	_T("block_number NVARCHAR(5),")	/* Not Used, Block */\
																	_T("unit_number NVARCHAR(5),")		/* Not Used, Enhet */\
																	_T("areal_ha float,")				/* Not Used, Areal (Total) */\
																	_T("areal_measured_ha float,")	/* Not Used, Inm�tt areal f�r fastigheten */\
																	_T("created_by NVARCHAR(20),")		/* Skapad av (Username) */\
																	_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
																	_T("obj_id NVARCHAR(30),")				/* Id for selection of Properties */\
																	_T("prop_full_name NVARCHAR(40),")		/* Name of prop incl. block and unit */\
																	_T("\r\nPRIMARY KEY (id));");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PROPERTY_OWNER = _T("CREATE TABLE dbo.tc_Property_Owner (")
																	_T("prop_id int NOT NULL,")
																	_T("contact_id int NOT NULL,")
																	_T("owner_share nvarchar(5),")	/* Andel, i br�kform. Ex 1/2 */\
																	_T("is_contact smallint,")	/* Yes/No kontaktperson f�r ex. Aviseringar */\
																	_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
																	_T("\r\nPRIMARY KEY (prop_id,contact_id),")
																	_T("\r\nCONSTRAINT FK_TC_Contacts")
																	_T("\r\nFOREIGN KEY (contact_id)")
																	_T("\r\nREFERENCES tc_Contacts(id)")
																	_T("\r\nON DELETE CASCADE")
																	_T("\r\nON UPDATE CASCADE,")
																	_T("\r\nCONSTRAINT FK_TC_Property")
																	_T("\r\nFOREIGN KEY (prop_id)")
																	_T("\r\nREFERENCES tc_Properties(id)")
																	_T("\r\nON DELETE CASCADE")
																	_T("\r\nON UPDATE CASCADE);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_CATEGORIES = _T("CREATE TABLE dbo.tc_Categories (")
																		_T("id int NOT NULL IDENTITY,")
																		_T("category NVARCHAR(45),")
																		_T("notes NVARCHAR(255),")
																		_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
																		_T("\r\nPRIMARY KEY (id));");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_CATEGORY_FOR_CONTACT = _T("CREATE TABLE dbo.tc_Categories_for_contacts (")
																	_T("contact_id int NOT NULL,")
																	_T("category_id int NOT NULL,")
																	_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
																	_T("\r\nPRIMARY KEY (contact_id,category_id),")
																	_T("\r\nCONSTRAINT FK_TC_Contact")
																	_T("\r\nFOREIGN KEY (contact_id)")
																	_T("\r\nREFERENCES tc_Contacts (id)")
																	_T("\r\nON DELETE CASCADE")
																	_T("\r\nON UPDATE CASCADE,")
																	_T("\r\nCONSTRAINT FK_TC_Category")
																	_T("\r\nFOREIGN KEY (category_id)")
																	_T("\r\nREFERENCES tc_Categories(id)")
																	_T("\r\nON DELETE CASCADE")
																	_T("\r\nON UPDATE CASCADE);");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PROJECTS = _T("CREATE TABLE dbo.tc_Projects (")
																	_T("id int NOT NULL IDENTITY,")
																	_T("name NVARCHAR(40),")		/* Namn p� Projekt */\
																	_T("date NVARCHAR(8),")					/* Datum */\
																	_T("info NVARCHAR(255),")		/* Information */\
																	_T("type_of_project INT,")		/* Typ av projekt*/
																	_T("created_by NVARCHAR(20),")		/* Skapad av (Username) */\
																	_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
																	_T("\r\nPRIMARY KEY (id));");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_PROJECT_TYPES = _T("CREATE TABLE dbo.tc_Project_Types (")
																		_T("id int NOT NULL IDENTITY,")
																		_T("type NVARCHAR(45),")
																		_T("notes NVARCHAR(255),")
																		_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
																		_T("\r\nPRIMARY KEY (id));");

//********************************************************************************************************
const LPCTSTR CREATE_TBL_STANDS_IN_PROJECTS = _T("CREATE TABLE dbo.tc_Stands_In_Projects (")
																	_T("project_id int NOT NULL,")
																	_T("stand_id int NOT NULL,")
																	_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
																	_T("\r\nPRIMARY KEY (project_id, stand_id),")
																	_T("\r\nCONSTRAINT FK_TC_Projects")
																	_T("\r\nFOREIGN KEY (project_id)")
																	_T("\r\nREFERENCES tc_Projects(id)")
																	_T("\r\nON DELETE CASCADE")
																	_T("\r\nON UPDATE CASCADE,")
																	_T("\r\nCONSTRAINT FK_TC_Stands")
																	_T("\r\nFOREIGN KEY (stand_id)")
																	_T("\r\nREFERENCES tc_Stand(FileIndex)")
																	_T("\r\nON DELETE CASCADE")
																	_T("\r\nON UPDATE CASCADE);");

//********************************************************************************************************
struct create_DB_tables
{
	const LPCTSTR tablename;
	const LPCTSTR tablescript;
};
			
const struct create_DB_tables createDBTables_2000[CREATE_NUM_OF_DB_TABLES] = 
{
	{TBL_STAND,CREATE_TBL_STAND},
	{TBL_PLOT,CREATE_TBL_PLOT},
	{TBL_TREE,CREATE_TBL_TREE},
	{TBL_SITE_INDEX,CREATE_TBL_SITE_INDEX},
	{TBL_SPECIES,CREATE_TBL_SPECIES},
	{TBL_GROUPS,CREATE_TBL_GROUPS},
	{TBL_PRICES,CREATE_TBL_PRICES},
	{TBL_GRADE_NMS,CREATE_TBL_GRADE_NMS},
	{TBL_PROD_NMS,CREATE_TBL_PROD_NMS},
	{TBL_MERCH_SPECS,CREATE_TBL_MERCH_SPECS},
	{TBL_STRATA,CREATE_TBL_STRATA},
	{TBL_TREE_GRDVOL,CREATE_TBL_TREE_GRDVOL},
	{TBL_PLOT_GRDVOLS,CREATE_TBL_PLOT_GRDVOLS},
	{TBL_PLOT_VOLUME,CREATE_TBL_PLOT_VOLUME},
	{TBL_SUM_DBH,CREATE_TBL_SUM_DBH},
	{TBL_GROUP_ORDER, CREATE_TBL_GROUP_ORDER},
	{TBL_GROUP_ORDER_HDR, CREATE_TBL_GROUP_ORDER_HDR},
	{TBL_COMPANY_INFO, CREATE_TBL_COMPANY_INFO_2000},
	{TBL_EXTERNAL_DOCS, CREATE_TBL_EXTERNAL_DOCS},
	{TBL_JOINEDPLOTDATA, CREATE_TBL_JOINEDPLOTDATA},
	{TBL_AUDITREMEASDATA, CREATE_TBL_AUDITREMEASDATA}
#ifdef USE_TC_CONTACT
	,{TBL_CONTACTS, CREATE_TBL_CONTACTS},
	{TBL_PROPERTIES, CREATE_TBL_PROPERTIES},
	{TBL_PROPERTY_OWNER, CREATE_TBL_PROPERTY_OWNER},
	{TBL_CATEGORIES, CREATE_TBL_CATEGORIES},
	{TBL_CATEGORY_FOR_CONTACTS, CREATE_TBL_CATEGORY_FOR_CONTACT}
#ifdef USE_TC_PROJECTS
	,{TBL_PROJECTS, CREATE_TBL_PROJECTS},
	{TBL_PROJECT_TYPES, CREATE_TBL_PROJECT_TYPES},
	{TBL_STANDS_IN_PROJECTS, CREATE_TBL_STANDS_IN_PROJECTS}
#endif
#endif
};

const struct create_DB_tables createDBTables_2005[CREATE_NUM_OF_DB_TABLES] = 
{
	{TBL_STAND,CREATE_TBL_STAND},
	{TBL_PLOT,CREATE_TBL_PLOT},
	{TBL_TREE,CREATE_TBL_TREE},
	{TBL_SITE_INDEX,CREATE_TBL_SITE_INDEX},
	{TBL_SPECIES,CREATE_TBL_SPECIES},
	{TBL_GROUPS,CREATE_TBL_GROUPS},
	{TBL_PRICES,CREATE_TBL_PRICES},
	{TBL_GRADE_NMS,CREATE_TBL_GRADE_NMS},
	{TBL_PROD_NMS,CREATE_TBL_PROD_NMS},
	{TBL_MERCH_SPECS,CREATE_TBL_MERCH_SPECS},
	{TBL_STRATA,CREATE_TBL_STRATA},
	{TBL_TREE_GRDVOL,CREATE_TBL_TREE_GRDVOL},
	{TBL_PLOT_GRDVOLS,CREATE_TBL_PLOT_GRDVOLS},
	{TBL_PLOT_VOLUME,CREATE_TBL_PLOT_VOLUME},
	{TBL_SUM_DBH,CREATE_TBL_SUM_DBH},
	{TBL_GROUP_ORDER, CREATE_TBL_GROUP_ORDER},
	{TBL_GROUP_ORDER_HDR, CREATE_TBL_GROUP_ORDER_HDR},
	{TBL_COMPANY_INFO, CREATE_TBL_COMPANY_INFO_2005},
	{TBL_EXTERNAL_DOCS, CREATE_TBL_EXTERNAL_DOCS},
	{TBL_JOINEDPLOTDATA, CREATE_TBL_JOINEDPLOTDATA},
	{TBL_AUDITREMEASDATA, CREATE_TBL_AUDITREMEASDATA}
#ifdef USE_TC_CONTACT
	,{TBL_CONTACTS, CREATE_TBL_CONTACTS},
	{TBL_PROPERTIES, CREATE_TBL_PROPERTIES},
	{TBL_PROPERTY_OWNER, CREATE_TBL_PROPERTY_OWNER},
	{TBL_CATEGORIES, CREATE_TBL_CATEGORIES},
	{TBL_CATEGORY_FOR_CONTACTS, CREATE_TBL_CATEGORY_FOR_CONTACT}
#ifdef USE_TC_PROJECTS
	,{TBL_PROJECTS, CREATE_TBL_PROJECTS},
	{TBL_PROJECT_TYPES, CREATE_TBL_PROJECT_TYPES},
	{TBL_STANDS_IN_PROJECTS, CREATE_TBL_STANDS_IN_PROJECTS}
#endif
#endif
};

#define NUM_OF_AUDITREMEAS_COLS 108
#define NUM_OF_CC 22

struct AuditRemeasType
{
	int FileIndex;
	int PlotIndex;
	int TRecIndex;
	int TRecType;
	int Measurement;
	CString SpeciesCode;
	CString SpeciesName;
	CString GroupName;
	double Dbh;
	int ProductNumb;

	CString ProductName;
	int TQIPrd;
	int Count_;
	double GSF;
	double PACF;
	double DefStumpHt;
	double HmObs;
	double HsObs;
	double HpObs;
	double HmEst;
	
	double HsEst;
	double HpEst;
	double Top_;
	int Defect;
	double FcObs;
	double FcEst;
	double Age;
	double Ht;
	double RadialGr;
	double SngBark;

	int ReproPlot;
	int OffPlot;
	int SubMerch;
	int PWOnly;
	int TClass;
	CString TClassName;
	int TClass2;
	CString TClassName2;
	int IsGradeTree;
	double GrdStumpHt;
	
	double StopTop;
	int TopType;
	CString TopTypeV;
	int SGL1;
	int GRD1;
	CString GRDV1;
	int DEF1;
	int SGL2;
	int GRD2;
	CString GRDV2;

	int DEF2;
	int SGL3;
	int GRD3;
	CString GRDV3;
	int DEF3;
	int SGL4;
	int GRD4;
	CString GRDV4;
	int DEF4;
	int SGL5;

	int GRD5;
	CString GRDV5;
	int DEF5;
	int SGL6;
	int GRD6;
	CString GRDV6;
	int DEF6;
	int SGL7;
	int GRD7;
	CString GRDV7;

	int DEF7;
	double LogTrim;
	double BA;
	double PW_CFVOB;
	double PW_CFVIB;
	double PW_GreenTons;
	double PW_WtCords;
	double SW_CFVOB;
	double SW_CFVIB;
	double SW_GreenTons;

	double SW_WtCords;
	double IntQuarter;
	double Doyle;
	double Scribner;
	double PW_Dollars;
	double SW_Dollars;

	CString CC[NUM_OF_CC];
	
	};

const LPCTSTR SAVE_TBL_AUDITREMEASDATA = _T("INSERT INTO tc_AuditRemeasData (FileIndex, PlotIndex, TRecIndex, ")
				_T("TRecType, Measurement, SpeciesCode, SpeciesName," )
				_T("GroupName, Dbh, ProductNumb, ProductName, TQIPrd," )
				_T("Count_, GSF,")
				_T("PACF, DefStumpHt, HmObs, HsObs, HpObs, HmEst, HsEst," )
				_T("HpEst, Top_, Defect, FcObs, FcEst, Age, Ht," )
				_T("RadialGr, SngBark, ReproPlot, OffPlot, SubMerch, PWOnly," )
				_T("TClass, TClassName, TClass2, TClassName2, IsGradeTree," )
				_T("GrdStumpHt, StopTop, TopType, TopTypeV, SGL1, GRD1," )
				_T("GRDV1, DEF1, SGL2, GRD2, GRDV2, DEF2," )
				_T("SGL3, GRD3, GRDV3, DEF3, SGL4, GRD4," )
				_T("GRDV4, DEF4, SGL5, GRD5, GRDV5, DEF5," )
				_T("SGL6, GRD6, GRDV6, DEF6, SGL7, GRD7, GRDV7," )
				_T("DEF7, LogTrim, BA, PW_CFVOB, PW_CFVIB, PW_GreenTons, PW_WtCords," )
				_T("SW_CFVOB, SW_CFVIB, SW_GreenTons, SW_WtCords, IntQuarter, Doyle," )
				_T("Scribner, PW_Dollars, SW_Dollars, CC01, CC02, CC03," )
				_T("CC04, CC05, CC06, CC07, CC08, CC09," )
				_T("CC10, CC11, CC12, CC13, CC14, CC15," )
				_T("CC16, CC17, CC18, CC19, CC20, CC21," )
				_T("CC22) VALUES(");


const LPCTSTR ALTER_TBL_GROUPORDERHDR = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = 'tc_GroupOrderHdr' and column_name = 'PrintOrder')  ")
											_T("alter table tc_GroupOrderHdr add PrintOrder INT");

const LPCTSTR ALTER_TBL_STAND = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = 'tc_Stand' and column_name = 'PropID')  ")
											_T("alter table tc_Stand add PropID INT");

const LPCTSTR ALTER_TBL_CRSAGGM= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = 'tc_Stand' and column_name = 'CrsAggM') ")
											_T("alter table tc_Stand add CrsAggM TINYINT NULL");

const LPCTSTR CRSAGGM_COLUMN_NAME = _T("CrsAggM");

const LPCTSTR ALTER_TBL_CRSPLOTS= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = 'tc_Stand' and column_name = 'CrsPlots') ")
											_T("alter table tc_Stand add CrsPlots INT NULL");
const LPCTSTR CRSPLOTS_COLUMN_NAME=_T("CrsPlots");

#ifdef USE_TC_CONTACT
const LPCTSTR ALTER_TBL_CONTACTS = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
												_T("where table_name = 'tc_Contacts' and column_name = 'co_address') ")
												_T("alter table tc_Contacts add co_address nvarchar(50)");
#endif

//#3750
const LPCTSTR SGL8_COLUMN_NAME = _T("SGL8");
const LPCTSTR ALTER_TBL_TREE = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
								_T("where table_name = 'tc_Tree' and column_name = 'SGL8')  ")
								_T("alter table tc_Tree add ")
								_T("SGL8 tinyint null, GRD8 tinyint null, GRDV8 varchar(4) null, DEF8 tinyint null, ")
								_T("SGL9 tinyint null, GRD9 tinyint null, GRDV9 varchar(4) null, DEF9 tinyint null, ")
								_T("SGL10 tinyint null, GRD10 tinyint null, GRDV10 varchar(4) null, DEF10 tinyint null, ")
								_T("SGL11 tinyint null, GRD11 tinyint null, GRDV11 varchar(4) null, DEF11 tinyint null, ")
								_T("SGL12 tinyint null, GRD12 tinyint null, GRDV12 varchar(4) null, DEF12 tinyint null, ")
								_T("SGL13 tinyint null, GRD13 tinyint null, GRDV13 varchar(4) null, DEF13 tinyint null, ")
								_T("SGL14 tinyint null, GRD14 tinyint null, GRDV14 varchar(4) null, DEF14 tinyint null, ")
								_T("SGL15 tinyint null, GRD15 tinyint null, GRDV15 varchar(4) null, DEF15 tinyint null, ")
								_T("SGL16 tinyint null, GRD16 tinyint null, GRDV16 varchar(4) null, DEF16 tinyint null, ")
								_T("SGL17 tinyint null, GRD17 tinyint null, GRDV17 varchar(4) null, DEF17 tinyint null, ")
								_T("SGL18 tinyint null, GRD18 tinyint null, GRDV18 varchar(4) null, DEF18 tinyint null, ")
								_T("SGL19 tinyint null, GRD19 tinyint null, GRDV19 varchar(4) null, DEF19 tinyint null, ")
								_T("SGL20 tinyint null, GRD20 tinyint null, GRDV20 varchar(4) null, DEF20 tinyint null ");


// #4213
const LPCTSTR ALTER_TBL_STAND_4213_1 = _T("ALTER TABLE dbo.tc_Stand ALTER COLUMN CrsPlots int NULL");
const LPCTSTR ALTER_TBL_STAND_4213_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = 'tc_Stand' and column_name = 'TemplateId') ")
											_T("alter table tc_Stand add TemplateId VARCHAR(64) NULL DEFAULT ''");
const LPCTSTR ALTER_TBL_STAND_4213_3 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = 'tc_Stand' and column_name = 'IsMetric') ")
											_T("alter table tc_Stand add IsMetric TINYINT NULL DEFAULT 0");

