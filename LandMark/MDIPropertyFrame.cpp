#include "stdafx.h"
#include "MDIPropertyFrame.h"
#include "Resource.h"
#include "PropertyTabView.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////
//
IMPLEMENT_XTP_CONTROL( CMyControlPopup, CXTPControlPopup)

CMyControlPopup::CMyControlPopup(void)
{
	vecMenuItems.clear();
	vecSubMenuItems.clear();
	vecDisabledItems.clear();
}

CMyControlPopup::~CMyControlPopup(void)
{
	vecMenuItems.clear();
	vecSubMenuItems.clear();
	vecDisabledItems.clear();
}

BOOL CMyControlPopup::OnSetPopup(BOOL bPopup)
{
	CMenu menu;
	HMENU sub_menu;
	// Make sure there's any menuitems to add; 080425 p�d
	if (bPopup && vecMenuItems.size() > 0)
	{
		menu.CreatePopupMenu();
		for (UINT i = 0;i < vecMenuItems.size();i++)
		{
			_menu_items item = vecMenuItems[i];
			// create menu items
			if (item.m_nID > 0)		
			{
				menu.AppendMenu(MF_STRING, item.m_nID, item.m_sText);

				if (isExcluded(item.m_nID))
				{
					menu.EnableMenuItem(item.m_nID,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
				}	// if (isIncluded(item.m_nID))

			}
			else if (item.m_nID == 0)		
			{
				menu.AppendMenu(MF_SEPARATOR, -1, _T(""));
			}
			else if (item.m_nID <= -1 && vecSubMenuItems.size() > 0) // Setup a Sub-menu
			{
				if (vecSubMenuItems.size() > 0)
				{
					sub_menu = CreatePopupMenu();
					for (UINT i1 = 0;i1 < vecSubMenuItems.size();i1++)
					{
						_menu_items sub_item = vecSubMenuItems[i1];
						if (sub_item.m_nSubMenuID == item.m_nSubMenuID)
						{
							// create sub menu item(s)
							AppendMenu(sub_menu,MF_STRING, sub_item.m_nID, sub_item.m_sText);
						}
						else if (sub_item.m_nID == 0)
						{
							AppendMenu(sub_menu,MF_SEPARATOR, -1, _T(""));
						}
					}
					menu.AppendMenu(MF_POPUP, (UINT)sub_menu, item.m_sText);
				}
			}	// for (UINT i = 0;i < vecSubMenuItems.size();i++)
		}
		SetCommandBar(&menu);
		menu.DestroyMenu();
	}
	return CXTPControlPopup::OnSetPopup(bPopup);
}

BOOL CMyControlPopup::isExcluded(int item_id)
{
	if (vecDisabledItems.size() == 0) return FALSE;
	for (UINT i = 0;i < vecDisabledItems.size();i++)
		if (item_id == vecDisabledItems[i]) return TRUE;
	return FALSE;
}

void CMyControlPopup::addMenuIDAndText(int id,LPCTSTR text,int sub_menu_id)
{
	vecMenuItems.push_back(_menu_items(id,text,sub_menu_id));
}

void CMyControlPopup::setSubMenuIDAndText(int id,LPCTSTR text,int sub_menu_id)
{
	vecSubMenuItems.push_back(_menu_items(id,text,sub_menu_id));
}

void CMyControlPopup::setExcludedItems(vecInt &vec)
{
	if (vec.size() > 0)
	{
		vecDisabledItems.clear();
		vecDisabledItems = vec;
	}	// if (vec.size() > 0)
}


/////////////////////////////////////////////////////////////////////////////
// CMDIPropertyFrame

HWND CMDIPropertyFrame::m_TopWindow = NULL;

IMPLEMENT_DYNCREATE(CMDIPropertyFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIPropertyFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIPropertyFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_OPEN, OnUpdateTBBTNImport)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_CREATE, OnUpdateTBBTNCreate)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_TOOLS, OnUpdateTBBTNTools)

	ON_XTP_CREATECONTROL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIPropertyFrame::CMDIPropertyFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_pToolsPopup = NULL;
	m_bIsTBtnEnabledOpen = TRUE;
	m_bIsTBtnEnabledCreate = TRUE;
	m_bIsTBtnEnabledTools = TRUE;
}

CMDIPropertyFrame::~CMDIPropertyFrame()
{
}

void CMDIPropertyFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTYFRAME_KEY);
	SavePlacement(this, csBuf);
}

int CMDIPropertyFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResFN();

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR5);
	m_wndToolBar.SetPosition(xtpBarTop);


	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING240/*213*/);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING252/*2360*/),
						xml.str(IDS_STRING242/*237*/));
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();

					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR5)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RSTR_TB_IMPORT,xml.str(IDS_STRING243/*312*/),FALSE);	//don't show
						setToolbarBtn(sTBResFN,p->GetAt(1),RSTR_TB_SEARCH,xml.str(IDS_STRING244/*310*/),FALSE);	//don't show
						setToolbarBtn(sTBResFN,p->GetAt(2),RSTR_TB_TOOLS,xml.str(IDS_STRING293/*318*/));	//
					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDIPropertyFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	m_TopWindow = m_hWnd;

	return TRUE;
}

// CMDIPropertyFrame diagnostics

#ifdef _DEBUG
void CMDIPropertyFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIPropertyFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIPropertyFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROPERTY;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROPERTY;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIPropertyFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIPropertyFrame::OnSetFocus(CWnd* wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pTabView != NULL)
	{
		CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView != NULL)
		{
			pView->doSetNavigationBar();
			pView = NULL;
		}		
		pTabView = NULL;
	}

	CMDIChildWnd::OnSetFocus(wnd);
}

// load the placement in OnShowWindow()
void CMDIPropertyFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTYFRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIPropertyFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIPropertyFrame::OnClose(void)
{
	CPropertyTabView *pView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pView)
	{
		CPropertyOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CPropertyOwnerFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(1)->GetHandle()));
		if (pView1)
		{
			pView1->isDataChanged();
		}
		
		CPropertyFormView* pView0 = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView0)
		{
			pView0->isDataChanged();
		}		
	}

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

 // Check if the top-most window is closing
 if (m_hWnd==m_TopWindow)
  m_TopWindow = NULL;

	CMDIChildWnd::OnClose();
}

void CMDIPropertyFrame::OnUpdateTBBTNImport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledOpen);
}

void CMDIPropertyFrame::OnUpdateTBBTNCreate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledCreate);
}

void CMDIPropertyFrame::OnUpdateTBBTNTools(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledTools);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIPropertyFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
		if (pTabView != NULL)
		{
			CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pView != NULL)
			{
				pView->isDataChanged();
				pView = NULL;
			}		
			pTabView = NULL;
		}
		showFormView(IDD_REPORTVIEW3,m_sLangFN,0);		
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		CString S;
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			if (msg->getValue1() == 122)
			{
				CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
				if (pTabView != NULL)
				{
					CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView != NULL)
					{
						pView->refreshProperties();
						pView = NULL;
					}		
					pTabView = NULL;
				}
			}	// if (msg->getValue1() == 122)
			/*TODO: if (msg->getValue1() == 10)	// From GIS; 090810 p�d
			{
				CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
				if (pTabView != NULL)
				{
					CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView != NULL)
					{
						pView->doPopulate(msg->getValue2(),false);
						pView = NULL;
					}		
				}
			}	// if (msg->getValue1() == 10)
*/
		}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
	}		
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(WM_USER_MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}



// MY METHODS
void CMDIPropertyFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

int CMDIPropertyFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_TBBTN_TOOLS)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_vecExcudedItemsOnTab0.clear();
				m_vecExcudedItemsOnTab1.clear();
				m_vecExcudedItemsOnTab2.clear();

				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_ADD_STAND_TO_PROP);
				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_DEL_STAND_FROM_PROP);
				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_ADD_CONTACT);
				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_REMOVE_CONTACT);

				//m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_CHANGE_OBJID);
				m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_ADD_STAND_TO_PROP);
				m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_DEL_STAND_FROM_PROP);

				//m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_CHANGE_OBJID);
				m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_ADD_CONTACT);
				m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_REMOVE_CONTACT);

				m_pToolsPopup = new CMyControlPopup();
				m_pToolsPopup->SetStyle(xtpButtonIcon);
				m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab0);
				
				//m_pToolsPopup->addMenuIDAndText(ID_TOOLS_CHANGE_OBJID,xml.str(IDS_STRING294));///*320));
				//m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_ADD_CONTACT,xml.str(IDS_STRING295/*311*/));
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_REMOVE_CONTACT,xml.str(IDS_STRING328/*322*/));	
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_ADD_STAND_TO_PROP,xml.str(IDS_STRING296/*317*/));
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_DEL_STAND_FROM_PROP,xml.str(IDS_STRING297/*319*/));

				lpCreateControl->pControl = m_pToolsPopup;
				xml.clean();		
			}
		}
		return TRUE;
	}
	return FALSE;
}


void CMDIPropertyFrame::setExcludedToolItems(int tab_selected)
{
	switch (tab_selected)
	{
		case 0 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab0);
		break;
		case 1 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab1);
		break;
		case 2 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab2);
		break;
	};
}

void CMDIPropertyFrame::setEnableToolbar(BOOL enable_open,BOOL enable_create,BOOL enable_tools)
{
	m_bIsTBtnEnabledOpen = enable_open;
	m_bIsTBtnEnabledCreate = enable_create;
	m_bIsTBtnEnabledTools = enable_tools;
}

// CMDIPropertyFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CPropertySelectListFrame

IMPLEMENT_DYNCREATE(CPropertySelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPropertySelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CPropertySelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_FILTER_OFF, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPropertySelectListFrame::CPropertySelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CPropertySelectListFrame::~CPropertySelectListFrame()
{
}

void CPropertySelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTY_SELLIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CPropertySelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooser.Create(this, IDD_FIELD_CHOOSER,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS))
		return -1;      // fail to create

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
		return -1;      // fail to create

	// docking for field chooser
	m_wndFieldChooser.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooser, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooser, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	// Set to 0 = No Docking; 090224 p�d
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CPropertySelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTY_SELLIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CPropertySelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CPropertySelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertySelectListFrame diagnostics

#ifdef _DEBUG
void CPropertySelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPropertySelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CPropertySelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROPERTY_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROPERTY_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CPropertySelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CPropertySelectListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CPropertySelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CPropertySelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while(pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE, wParam, lParam);

		}
	}

	return 0L;
}

void CPropertySelectListFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CPropertySelectListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sToolTipFilter = xml.str(IDS_STRING121/*300*/);
			m_sToolTipFilterOff = xml.str(IDS_STRING122/*301*/);
			m_sToolTipPrintOut = xml.str(IDS_STRING123/*302*/);
			m_sToolTipSearch = xml.str(IDS_STRING244/*310*/);

			m_wndFieldChooser.SetWindowText((xml.str(IDS_STRING268/*242*/)));
			m_wndFilterEdit.SetWindowText((xml.str(IDS_STRING269/*243*/)));
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CPropertySelectListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR6)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RSTR_TB_FILTER,m_sToolTipFilter);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RSTR_TB_FILTER_OFF,m_sToolTipFilterOff);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RSTR_TB_PRINT,m_sToolTipPrintOut);	//
					setToolbarBtn(sTBResFN,p->GetAt(3),RSTR_TB_IMPORT,_T(""),FALSE);	//don't show
					setToolbarBtn(sTBResFN,p->GetAt(4),RSTR_TB_IMPORT,_T(""),FALSE);	//don't show

				}	// if (nBarID == IDR_TOOLBAR6)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CPropertySelectListFrame message handlers


