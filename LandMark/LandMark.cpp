// LandMark.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <afxdllx.h>

#include "LandMark.h"
#include "MDIBaseFrame.h"
#include "LandMarkView.h"
#include "MDIReportFrame.h"
#include "ReportView.h"
#include "MDIImportReportFrame.h"
#include "ImportReportView.h"
#include "MDIImportAccessFrame.h"
#include "ImportAccessView.h"
#include "MDIAssignGroupsFrame.h"
#include "AssignGroupsView.h"
#include "MDIClassNamesFrame.h"
#include "ClassNamesView.h"
#include "MDICompanyInfoFrame.h"
#include "CompanyInfoTabView.h"
#include "MDIExternalDocsFrame.h"
#include "ExternalDocsView.h"
#include "MDISnapshotFrame.h"
#include "SnapshotView.h"

#ifdef USE_TC_CONTACT
#include "ContactsTabView.h"
#include "MDIContactsFrame.h"
#include "ContactsSelListFormView.h"
#include "CategoryFormView.h"

#include "MDIPropertyFrame.h"
#include "PropertyTabView.h"
#include "PropertySelListFormView.h"
#include "StandListFormView.h"

#include "MDISettingsFrame.h"
#include "SettingsTabView.h"
#include "PathSettingsFormView.h"

#ifdef USE_TC_PROJECTS
#include "MDIProjectFrame.h"
#include "ProjectTabView.h"
#include "ProjectSelListFormView.h"
#include "ProjectTypeFormView.h"
#endif
#endif

std::vector<HINSTANCE> m_vecHInstTable;

static CWinApp* pApp = AfxGetApp();
HINSTANCE hInst = NULL;

static AFX_EXTENSION_MODULE LandMarkDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	hInst = hInstance;
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("LandMark.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(LandMarkDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("LandMark.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(LandMarkDLL);
	}
	return 1;   // ok
}


void DLL_BUILD InitSuite(CStringArray *user_modules, vecINDEX_TABLE &vecIndex, vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(LandMarkDLL);

	CString csVersion;
	CString csCopyright;
	CString csCompany;

	CString csModuleFN = getModuleFN(hInst);
	CString csLangFN;
	csLangFN.Format(_T("%s%s"), getLanguageDir(), PROGRAM_NAME);

	m_vecHInstTable.clear();


	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDILandMarkFrame),
		RUNTIME_CLASS(CLandMarkView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW, csModuleFN, csLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW1,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDIReportFrame),
		RUNTIME_CLASS(CReportView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW1, csModuleFN, csLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDIImportReportFrame),
		RUNTIME_CLASS(CImportReportView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW2, csModuleFN, csLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDIImportAccessFrame),
		RUNTIME_CLASS(CImportAccessView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW3, csModuleFN, csLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW4,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDICompanyInfoFrame),
		RUNTIME_CLASS(CCompanyInfoTabView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW4, csModuleFN, csLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW5,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDIAssignGroupsFrame),
		RUNTIME_CLASS(CAssignGroupsView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW5, csModuleFN, csLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW6,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDIClassNamesFrame),
		RUNTIME_CLASS(CClassNamesView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW6, csModuleFN, csLangFN, TRUE));

	//added 090108
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW7,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDIExternalDocsFrame),
		RUNTIME_CLASS(CExternalDocsView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW7, csModuleFN, csLangFN, TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW18,
		RUNTIME_CLASS(CMDIFrameDoc),
		RUNTIME_CLASS(CMDISnapshotFrame),
		RUNTIME_CLASS(CSnapshotView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW18, csModuleFN, csLangFN, TRUE));

#ifdef USE_TC_CONTACT
	//added 100127 Contacts formview
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW8, 	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIContactsFrame),
			RUNTIME_CLASS(CContactsTabView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW8,csModuleFN,csLangFN,TRUE));

	//added 100202
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW9, 	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDICategoryFrame),
			RUNTIME_CLASS(CCategoryFormView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW9,csModuleFN,csLangFN,TRUE));

	//added 100209
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW11, 	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIPropertyFrame),
			RUNTIME_CLASS(CPropertyTabView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW11,csModuleFN,csLangFN,TRUE));

	//added 110317 settings formview
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW15, 	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDISettingsFrame),
			RUNTIME_CLASS(CSettingsTabView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW15,csModuleFN,csLangFN,TRUE));

#ifdef USE_TC_PROJECTS
	//added 100706
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW13, 	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIProjectFrame),
			RUNTIME_CLASS(CProjectTabView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW13,csModuleFN,csLangFN,TRUE));

	//added 100709
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW14, 	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIProjectTypeFrame),
			RUNTIME_CLASS(CProjectTypeFormView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW14,csModuleFN,csLangFN,TRUE));
#endif
#endif

	///////////////////////////////////////////////////////////////////////////////////////
	// Doc\Views NOT to be displayed on HMSShell navigation bar.
	// I.e. don't include the Item in the vecIndex vector; 070123 p�d
#ifdef USE_TC_CONTACT
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW1, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CContactsSelectListFrame),
			RUNTIME_CLASS(CContactsSelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW1,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW3, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CPropertySelectListFrame),
			RUNTIME_CLASS(CPropertySelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW1,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW4, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CStandSelectionListFrame),
			RUNTIME_CLASS(CStandSelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW1,sModuleFN,sLangFN,TRUE));

#ifdef USE_TC_PROJECTS
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW5, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CProjectSelectListFrame),
			RUNTIME_CLASS(CProjectSelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW1,sModuleFN,sLangFN,TRUE));
#endif
#endif
	
	//version information
	const LPCTSTR VER_NUMBER = _T("FileVersion");
	const LPCTSTR VER_COMPANY = _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT = _T("LegalCopyright");

	csVersion = getVersionInfo(hInst, VER_NUMBER);
	csCopyright = getVersionInfo(hInst, VER_COPYRIGHT);
	csCompany = getVersionInfo(hInst, VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		1 /* Suite */,
		csLangFN.GetBuffer(),
		csVersion.GetBuffer(),
		csCopyright.GetBuffer(),
		csCompany.GetBuffer()
		));

	csLangFN.ReleaseBuffer();
	csVersion.ReleaseBuffer();
	csCopyright.ReleaseBuffer();
	csCompany.ReleaseBuffer();
	
	/* *****************************************************************************
		Load user module(s), specified in the ShellTree data file for this SUITE
	****************************************************************************** */
	typedef CRuntimeClass *(*Func)(CWinApp *,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);
  Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"),getModulesDir(),user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
#ifdef UNICODE
				proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], CW2A(INIT_MODULE_FUNC) );
#else
					proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], (INIT_MODULE_FUNC) );
#endif
					if (proc != NULL)
					{
						// call the function
						proc(pApp,csModuleFN,vecIndex,vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)


	// Do a check to see if database tables are created
	DoDatabaseTables(_T(""));
	DoAlterTables(_T(""));
	DoRegistryChecks();
}

void DoDatabaseTables(LPCTSTR db_name)
{
	if(getIsDBConSet() == 1)
	{
		createTable(db_name);
	}
}

void DoAlterTables(LPCTSTR db_name)
{
	if(getIsDBConSet() == 1)
	{
		alterTable(db_name);
	}
}

void DLL_BUILD OpenSuite(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;
	CString sDocTitle;
	CString S;
	ASSERT(pApp != NULL);


	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(hInst);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;

		if (nTableIndex == idx && 
				sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;

/*
	S.Format(_T("nTableIndex %d\nidx  %d\nsVecIndexTableModuleFN %s\nsModuleFN %s"),
						nTableIndex,
						idx,
						sVecIndexTableModuleFN,
						sModuleFN);
	AfxMessageBox(S);
*/

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)
	
	if (bFound)
	{
		// Make sure the languagefile is in directory
		if (!fileExists(sLangFN))
		{
			S.Format(_T("NB! Languagefile missing for suite/module:\n%s\n\nCan not open ..."),
				sVecIndexTableModuleFN);
			AfxMessageBox(S);
			return;
		}

		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		RLFReader *xml = new RLFReader();
		if (xml->Load(sLangFN))
		{
			sCaption = xml->str(nTableIndex);
		}
		delete xml;


		// Check if the document or module is in this SUITE; 051213 p�d
		POSITION pos = pApp->GetFirstDocTemplatePosition();
		while(pos != NULL)
		{
			pTemplate = pApp->GetNextDocTemplate(pos);
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			ASSERT(pTemplate != NULL);
			// Need to add a linefeed, infront of the docName.
			// This is because, for some reason, the document title,
			// set in resource, must have a linefeed.
			// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
			sDocName = '\n' + sDocName;
			if (pTemplate && sDocName.Compare(sResStr) == 0)
			{
				if (bIsOneInst)
				{
					POSITION posDOC = pTemplate->GetFirstDocPosition();

					while(posDOC != NULL)
					{
						CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
						POSITION posView = pDocument->GetFirstViewPosition();
						if(posView != NULL)
						{
							CView* pView = pDocument->GetNextView(posView);
							pView->GetParent()->BringWindowToTop();
							pView->GetParent()->SetFocus();
							// Check if window is minimiced.
							// If so, show window in normal state; 071030 p�d
							if (pView->GetParent()->IsIconic())
							{
								pView->GetParent()->ShowWindow(SW_NORMAL);
							}
							posDOC = (POSITION)1;
							break;
						}	// if(posView != NULL)
					}	// while(posDOC != NULL)

					if (posDOC == NULL)
					{

						pTemplate->OpenDocumentFile(NULL);

						// Find the CDocument for this tamplate, and set title.
						// Title is set in Languagefile; OBS! The nTableIndex
						// matches the string id in the languagefile; 051129 p�d
						POSITION posDOC = pTemplate->GetFirstDocPosition();
						while (posDOC != NULL)
						{
							CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
							// Set the caption of the document. Can be a resource string,
							// a string set in the language xml-file etc.
							sDocTitle.Format(_T("%s"),sCaption);
							pDocument->SetTitle(sDocTitle);
							POSITION posView = pDocument->GetFirstViewPosition();
							if(posView != NULL)
							{
								CView* pView = pDocument->GetNextView(posView);
								// Check if window is minimiced.
								// If so, show window in normal state; 071030 p�d
								if (pView->GetParent()->IsIconic())
								{
									pView->GetParent()->ShowWindow(SW_NORMAL);
								}
								posDOC = (POSITION)1;
								break;
							}	// if(posView != NULL)
						}	// while (posDOC != NULL)
						break;
					}	// if (posDOC == NULL)
				}	// if (bIsOneInst)
				else
				{
						pTemplate->OpenDocumentFile(NULL);

						// Find the CDocument for this tamplate, and set title.
						// Title is set in Languagefile; OBS! The nTableIndex
						// matches the string id in the languagefile; 051129 p�d
						POSITION posDOC = pTemplate->GetFirstDocPosition();
						nDocCounter = 1;

						while (posDOC != NULL)
						{
							CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
							// Set the caption of the document. Can be a resource string,
							// a string set in the language xml-file etc.
							sDocTitle.Format(_T("%s (%d)"),sCaption,nDocCounter);
							pDocument->SetTitle(sDocTitle);
							nDocCounter++;
						}

						break;
				}
			}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
		}	// while(pos != NULL)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}
}
