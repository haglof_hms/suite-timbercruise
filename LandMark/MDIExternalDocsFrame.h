#pragma once


// CMDIExternalDocsFrame frame
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CMDIExternalDocsFrame : public CChildFrameBase//CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDIExternalDocsFrame)

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CXTPDockingPaneManager m_paneManager;
	CXTPToolBar m_wndToolBar;

protected:
	HICON m_hIcon;
	BOOL m_bFirstOpen;
	CString m_sMsgCap;
	CString m_sToolTipAdd;
	CString m_sToolTipRem;

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}

	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	void setLanguage(void);

protected:
	CMDIExternalDocsFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIExternalDocsFrame();

public:
	static XTPDockingPanePaintTheme m_themeCurrent;

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnClose();
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
};


