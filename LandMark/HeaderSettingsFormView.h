#pragma once

#include "Resource.h"
#include "LandMarkDB.h"
#include "picturectrl.h"
#include "afxwin.h"

// CHeaderSettingsFormView form view

class CHeaderSettingsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CHeaderSettingsFormView)

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sDBErrorMsg; 

	CString m_csErrMsgOpen;

	BOOL m_bInitialized;

	
	CLandMarkDB* m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;

	
	CMyExtStatic m_wndStaticName;
	CMyExtStatic m_wndStaticAddress;
	CMyExtStatic m_wndStaticCity;
	CMyExtStatic m_wndStaticCountry;
	CMyExtStatic m_wndStaticPhone;
	CMyExtStatic m_wndStaticFax;
	CMyExtStatic m_wndStaticEmail;
	CMyExtStatic m_wndStaticWebsite;

	CMyExtStatic m_wndLblPhone;
	CMyExtStatic m_wndLblFax;
	CMyExtStatic m_wndLblEmail;
	CMyExtStatic m_wndLblWebsite;
	CMyExtStatic m_wndLblReportHeader;

	CString m_csPhone;
	CString m_csFax;
	CString m_csEmail;
	CString m_csWebsite;

	CPictureCtrl m_wndPicCtrl;
	CButton m_wndBtnSelect;
	CButton m_wndBtnRemove;
	CStatic m_wndGroupBox;

	int m_NReg_Contact_Id;

protected:
	CHeaderSettingsFormView();           // protected constructor used by dynamic creation
	virtual ~CHeaderSettingsFormView();

	void setLanguage(void);
	void clearPreview(void);
	void populateData(void);
	BOOL getRegistryValue(void);
	BOOL setRegistryValue(void);

public:
	enum { IDD = IDD_FORMVIEW17 };	
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnInitialUpdate();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedBtnChooseContact();
	afx_msg void OnBnClickedBtnRemoveContact();
};


