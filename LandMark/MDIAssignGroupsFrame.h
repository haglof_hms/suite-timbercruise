#pragma once


// CMDIAssignGroupsFrame frame
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CMDIAssignGroupsFrame : public CChildFrameBase	//CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDIAssignGroupsFrame)

	CXTPDockingPaneManager m_paneManager;
	CString m_sAbrevLangSet;
	CString m_sLangFN;
protected:
	CString m_sToolTipFilter;
	CString m_sToolTipFilterOff;
	CString m_sToolTipRefresh;
	CString m_sToolTipClasses;
	CString m_sToolTipMulti;
	CString m_sErrorMsg;

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager()
	{
		return &m_paneManager;
	}

	BOOL m_bFirstOpen;
	BOOL m_bEnableTBBTNFilterOff;

	CXTPToolBar m_wndToolBar;
	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;

	static XTPDockingPanePaintTheme m_themeCurrent;
public:
	BOOL m_bDBConnOk;
	CDialogBar m_wndFieldChooserDlg;
	CDialogBar m_wndFilterEdit;

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

protected:
	CMDIAssignGroupsFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIAssignGroupsFrame();

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnPaint();
	afx_msg void OnClose();
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
};


