
#include "stdafx.h"

#include "ContactsFormView.h"
#include "ContactsTabView.h"

#include "ResLangFileReader.h"


///////////////////////////////////////////////////////////////
// CContactsTabView 

IMPLEMENT_DYNCREATE(CContactsTabView, CView)

BEGIN_MESSAGE_MAP(CContactsTabView, CView)
	//{{AFX_MSG_MAP(CPricelistsView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL, OnSelectedChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CContactsTabView::CContactsTabView()
{
	m_bInitialized = FALSE;
}

CContactsTabView::~CContactsTabView()
{
}

BOOL CContactsTabView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if (!CView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;

}

void CContactsTabView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	if (m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,1,1,rect.right-1,rect.bottom-1);
	}
}

void CContactsTabView::OnDestroy()
{
	CView::OnDestroy();
}

void CContactsTabView::OnClose()
{
	CView::OnClose();
}

int CContactsTabView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Tab control
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, CRect(0, 0, 0, 0), this, ID_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = FALSE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = FALSE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
//	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );
//	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);

		AddView(RUNTIME_CLASS(CContactsFormView), _T("1"),1, -1);
		if (m_wndTabControl.getNumOfTabPages() > 0)
		{
			CFrameWnd* pFrame = GetParentFrame();
			CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(0)->GetHandle()));
			ASSERT_KINDOF(CContactsFormView, pView);
			// Sets the Active view.
			// OBS! This is used on function GetActiveView() in 
			//	e.g. CMDIContactsFrame::OnClose(); 070111 p�d
			pFrame->SetActiveView(pView);
		}	// if (m_wndTabControl.getNumOfTabPages() > 0)

	return 0;
}

void CContactsTabView::OnInitialUpdate( )
{
	CView::OnInitialUpdate();

	if (!m_bInitialized)
	{

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setLanguage();

		m_bInitialized = TRUE;
	}	// if (!m_bInitialized)
}


#ifdef _DEBUG
void CContactsTabView::AssertValid() const
{
	CView::AssertValid();
}

void CContactsTabView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMDIFrameDoc* CContactsTabView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMDIFrameDoc)));
	return (CMDIFrameDoc*)m_pDocument;
}

#endif

void CContactsTabView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();

	if (!m_bInitialized)
	{
		m_bInitialized = TRUE;
	}	// 	if (!m_bInitialized)
}

void CContactsTabView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;

	CFrameWnd* pFrame = GetParentFrame();
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{

		CView* pView = DYNAMIC_DOWNCAST(CView, CWnd::FromHandle(m_wndTabControl.GetSelectedItem()->GetHandle()));
		ASSERT_KINDOF(CView, pView);

		pFrame->SetActiveView(pView);
	}
}

// Catch message sent from (WM_USER_MSG_SUITE)
LRESULT CContactsTabView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

CContactsFormView *CContactsTabView::getContactsFormView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CContactsFormView, pView);
		return pView;
	}
	return NULL;
}

BOOL CContactsTabView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int tab_id, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();
	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_tabManager =	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	m_tabManager->SetData(tab_id);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CContactsTabView::setLanguage()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			if (m_tabManager)
			{
				m_tabManager->SetCaption((xml.str(IDS_STRING245/*2480*/)));
			}	// if (m_tabManager)
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))
}