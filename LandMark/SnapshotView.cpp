// SnapshotView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "SnapshotView.h"
#include "MDIAssignGroupsFrame.h"
#include "MDIClassNamesFrame.h"


// CSnapshotView

IMPLEMENT_DYNCREATE(CSnapshotView, CXTResizeFormView)

CSnapshotView::CSnapshotView()
	: CXTResizeFormView(CSnapshotView::IDD),
		m_offsetx(8),
		m_offsety(8),
		m_sizex(48),
		m_sizey(48),
		m_height(0),
		m_width(0),
		m_rowlength(16),
		m_fileIndex(0),
		m_numSnapshots(0),
		m_selectedIdx(-1)
{
	m_pDB = NULL;
	m_pImages = NULL;
}

CSnapshotView::~CSnapshotView()
{
	if( m_pImages )
	{
		delete[] m_pImages;
		m_pImages = NULL;
	}
}

void CSnapshotView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSnapshotView, CXTResizeFormView)
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


// CSnapshotView diagnostics

#ifdef _DEBUG
void CSnapshotView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSnapshotView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CSnapshotView::LoadSnapshots()
{
	vecTransactionTCSnapshot vecSnapshots;

	// Get snapshots for this tract
	m_pDB->getSnapshots(m_fileIndex, vecSnapshots);

	if( m_pImages ) delete[] m_pImages;
	m_pImages = new CSnapshotImg[vecSnapshots.size()];
	m_numSnapshots = vecSnapshots.size();

	m_height = 0;
	for(UINT i=0; i<vecSnapshots.size(); i++)
	{
		// Store buffer to temp file
		CString tmpname = getTempFilePath();
		CFile tmp(tmpname, CFile::modeWrite);
		tmp.Write(vecSnapshots[i].getBuffer(), vecSnapshots[i].getBufferLength());
		tmp.Close();

		// Load image (png)
		m_pImages[i].img.Load(tmpname);
		m_pImages[i].id = vecSnapshots[i].getId();
		m_pImages[i].name = vecSnapshots[i].getName();
		DeleteFile(tmpname);
		m_names.Add(vecSnapshots[i].getName());

		// Calculate form height
		SIZE size;
		GetTextExtentPoint32(GetDC()->GetSafeHdc(), vecSnapshots[i].getName(), vecSnapshots[i].getName().GetLength(), &size);
		if( i % 2 == 0 )
		{
			m_height += m_sizey + size.cy;
			if( i < vecSnapshots.size()-1 ) m_height += m_offsety;
		}
	}

	if( vecSnapshots.size() > 0 )
	{
		m_width = (int(vecSnapshots.size()) > m_rowlength ? m_rowlength : vecSnapshots.size()) * (m_sizex + m_offsetx) - m_offsetx;
	}

	SIZE size;
	size.cx = m_width;
	size.cy = m_height;
	SetScrollSizes(MM_TEXT, size);

	// Redraw form
	Invalidate();
}

void CSnapshotView::SaveImageToFile(int id, CString path)
{
	// Save to disk
	for( int i=0; i < m_numSnapshots; i++ )
	{
		if( id == m_pImages[i].id )
		{
			m_pImages[i].img.Save(path);
		}
	}
}

bool CSnapshotView::LocateSnapshot(CPoint pt, int &idx)
{
	int x,y,n,i,ct=0;
	x = pt.x / (m_sizex + m_offsetx);
	y = pt.y / (m_sizey + m_offsety);
	n = x + y * m_rowlength;

	// Make sure coordinates aren't in between two thumbs
	if( pt.x > ((n % m_rowlength) * (m_sizex + m_offsetx) + m_sizex) ||
		pt.y > ((n / m_rowlength) * (m_sizey + m_offsety) + m_sizey) )
	{
		// in offset area
		return false;
	}

	// Find thumb in array, ignore deleted images
	for( i = 0; i < m_numSnapshots; i++ )
	{
		if( !m_pImages[i].deleted )
		{
			if( n == ct )
			{
				idx = i;
				return true;
			}
			ct++;
		}
	}

	return false;
}

void CSnapshotView::PreviewSnapshot(int idx)
{
	// Create temp image file
	CString path = getTempFilePath() + _T(".png");
	SaveImageToFile(m_pImages[idx].id, path);

	// Show snapshot in a report
	CString reportPath; reportPath.Format(_T("%s%s\\showimg.rpt"), getReportsDir(), getLangSet());
	CString args; args.Format(_T("filename=%s;caption=%s"), path, m_pImages[idx].name);
	AfxGetApp()->BeginWaitCursor();
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
		(LPARAM)&_user_msg(333, _T("OpenSuiteEx"), 
		_T("Reports2.dll"),
		reportPath,
		_T(""),
		args));
	AfxGetApp()->EndWaitCursor();
}

void CSnapshotView::UpdateToolbarButtons()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,		FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,	FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,	m_selectedIdx >= 0);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,	m_selectedIdx >= 0);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_PREVIEW_ITEM,	m_selectedIdx >= 0);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}


// CSnapshotView message handlers

BOOL CSnapshotView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;
}

void CSnapshotView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Load language string
	CString sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	RLFReader *xml = new RLFReader;
	if(xml->Load(sLangFN))
	{
		m_sConfirmRemove = xml->str(IDS_STRING372);
	}

	// Create overlay image (box used for marking)
	m_overlay.Create(m_sizex, m_sizey, 32);
	for( int x=0; x<m_sizex; x++ )
	{
		for( int y=0; y<m_sizey; y++ )
		{
			m_overlay.SetPixel(x, y, RGB(0,0,0xFF));
		}
	}
}

void CSnapshotView::OnPaint()
{
	CXTResizeFormView::OnPaint();

	int ct=0;
	POINT origin, pos;
	CPoint pt = GetScrollPosition();
	origin.x = -pt.x;
	origin.y = -pt.y;
	pos = origin;

	SIZE sz;
	sz.cx = m_sizex;
	sz.cy = m_sizey;

	CDC *pDC = GetDC();

	// Draw all snapshots
	for(int i=0; i<m_numSnapshots; i++)
	{
		RECT rect;

		if( m_pImages[i].deleted ) continue;

		pos.x = origin.x + (ct % m_rowlength) * (m_sizex + m_offsetx);
		rect.top = pos.y;  rect.bottom = pos.y + m_sizex;
		rect.left = pos.x; rect.right  = pos.x + m_sizey;
		m_pImages[i].img.StretchBlt(*pDC, rect);
		
		// Mark selected thumb
		if( i == m_selectedIdx )
		{
			m_overlay.AlphaBlend(*pDC, rect.left, rect.top, 0x80);
		}

		// Move down
		if( (ct + 1) % m_rowlength == 0 ) pos.y += sz.cy + m_offsety;
		ct++;
	}

	ReleaseDC(pDC);
}

void CSnapshotView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// Select clicked thumb
	int idx=0, old=m_selectedIdx;
	if( LocateSnapshot(point, idx) )
	{
		m_selectedIdx = idx;
	}
	else
	{
		m_selectedIdx = -1;
	}

	// Redraw only when after selection change, avoid flickering
	if( m_selectedIdx != old )
	{
		Invalidate();
	}
	UpdateToolbarButtons();
}

void CSnapshotView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	int idx=0;
	if( LocateSnapshot(point, idx) )
	{
		PreviewSnapshot(idx);
	}
}

void CSnapshotView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
}

BOOL CSnapshotView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		if( !m_pDB )
		{
			m_pDB = new CLandMarkDB((*((DB_CONNECTION_DATA*)pData->lpData)));
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

LRESULT CSnapshotView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_SAVE_ITEM :
		{
			// Export to file
			if( m_selectedIdx >= 0 && m_selectedIdx < m_numSnapshots )
			{
				CFileDialog fd(FALSE, _T(".png"), NULL, OFN_OVERWRITEPROMPT, _T("PNG (*.png)|*.png|JPG (*.jpg)|*.jpg|BMP (*.bmp)|*.bmp||"));
				if( fd.DoModal() == IDOK )
				{
					SaveImageToFile(m_pImages[m_selectedIdx].id, fd.GetPathName());
				}
			}
			break;
		}
		
		case ID_DELETE_ITEM :
		{
			// Remove from db
			if( m_selectedIdx >= 0 && m_selectedIdx < m_numSnapshots &&
				AfxMessageBox(m_sConfirmRemove, MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES )
			{
				m_pDB->removeSnapshot(m_pImages[m_selectedIdx].id);

				// Mark as deleted
				m_pImages[m_selectedIdx].deleted = true;
				Invalidate();

				// Drop selection
				m_selectedIdx = -1;
				UpdateToolbarButtons();

				//send msg to update tract view
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
					(LPARAM)&_doc_identifer_msg(MODULE942, MODULE920, _T(""), 1, 0, 0));
			}
			break;
		}

		case ID_PREVIEW_ITEM :
		{
			// Generate report
			if( m_selectedIdx >= 0 && m_selectedIdx < m_numSnapshots )
			{
				PreviewSnapshot(m_selectedIdx);
			}
			break;
		}

		case ID_WPARAM_VALUE_FROM + 0x02 :
		{
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			m_fileIndex = msg->getValue1();
			LoadSnapshots();
			break;
		}
	}

	return 0L;
}
