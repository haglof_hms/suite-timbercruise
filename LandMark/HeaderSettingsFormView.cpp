// HeaderSettingsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "HeaderSettingsFormView.h"
#include "MDISettingsFrame.h"
#include "SearchContactDlg.h"


// CHeaderSettingsFormView

IMPLEMENT_DYNCREATE(CHeaderSettingsFormView, CXTResizeFormView)

CHeaderSettingsFormView::CHeaderSettingsFormView()
	: CXTResizeFormView(CHeaderSettingsFormView::IDD)
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_bInitialized = FALSE;

	m_csPhone = _T("");
	m_csFax = _T("");
	m_csEmail = _T("");
	m_csWebsite = _T("");
	m_NReg_Contact_Id = -1;
}

CHeaderSettingsFormView::~CHeaderSettingsFormView()
{
	if(m_pDB != NULL)
		delete m_pDB;
}

void CHeaderSettingsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_RH_NAME, m_wndStaticName);
	DDX_Control(pDX, IDC_RH_ADDRESS, m_wndStaticAddress);
	DDX_Control(pDX, IDC_RH_CITY, m_wndStaticCity);
	DDX_Control(pDX, IDC_RH_COUNTRY, m_wndStaticCountry);
	DDX_Control(pDX, IDC_RH_PHONE, m_wndStaticPhone);
	DDX_Control(pDX, IDC_RH_FAX, m_wndStaticFax);
	DDX_Control(pDX, IDC_RH_EMAIL, m_wndStaticEmail);
	DDX_Control(pDX, IDC_RH_WEBSITE, m_wndStaticWebsite);

	DDX_Control(pDX, IDC_ST_RH_PHONE, m_wndLblPhone);
	DDX_Control(pDX, IDC_ST_RH_FAX, m_wndLblFax);
	DDX_Control(pDX, IDC_ST_RH_EMAIL, m_wndLblEmail);
	DDX_Control(pDX, IDC_ST_RH_WEBSITE, m_wndLblWebsite);

	DDX_Control(pDX, IDC_RH_PIC, m_wndPicCtrl);
	DDX_Control(pDX, IDC_BTN_CHOOSE_CONT_RPT, m_wndBtnSelect);
	DDX_Control(pDX, IDC_BTN_REMOVE_CONT_RPT, m_wndBtnRemove);
	DDX_Control(pDX, IDC_RH_GROUP, m_wndGroupBox);
	DDX_Control(pDX, IDC_STATIC_RH, m_wndLblReportHeader);
}

BEGIN_MESSAGE_MAP(CHeaderSettingsFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTN_CHOOSE_CONT_RPT, OnBnClickedBtnChooseContact)
	ON_BN_CLICKED(IDC_BTN_REMOVE_CONT_RPT, OnBnClickedBtnRemoveContact)
END_MESSAGE_MAP()


// CHeaderSettingsFormView diagnostics

#ifdef _DEBUG
void CHeaderSettingsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CHeaderSettingsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CHeaderSettingsFormView message handlers

BOOL CHeaderSettingsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;
}

void CHeaderSettingsFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CHeaderSettingsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);
	
	if(!m_bInitialized)
	{
		setLanguage();
		clearPreview();
		
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

		m_bInitialized = TRUE;

		if(getRegistryValue())
		{
			populateData();
		}
	}
}

void CHeaderSettingsFormView::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_csPhone.Format(_T("%s:"), xml->str(IDS_STRING278));
			m_csFax.Format(_T("%s:"),xml->str(IDS_STRING191));
			m_csEmail.Format(_T("%s:"),xml->str(IDS_STRING282));
			m_csWebsite.Format(_T("%s:"),xml->str(IDS_STRING283));
			m_wndGroupBox.SetWindowTextW(xml->str(IDS_STRING359));
			m_csErrMsgOpen = xml->str(IDS_STRING168);
			m_sDBErrorMsg = xml->str(IDS_STRING105);

			m_wndBtnSelect.SetWindowTextW(xml->str(IDS_STRING361));
			m_wndBtnRemove.SetWindowTextW(xml->str(IDS_STRING362));
			m_wndLblReportHeader.SetWindowTextW(xml->str(IDS_STRING360));
		}
		delete xml;
	}
}

BOOL CHeaderSettingsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	if(pData->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData, pData->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


void CHeaderSettingsFormView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();

	
}

void CHeaderSettingsFormView::OnBnClickedBtnChooseContact()
{
	CSearchContactDlg *pDlg = new CSearchContactDlg();

	if(pDlg != NULL)
	{
		if(pDlg->DoModal() == IDOK)
		{
			m_NReg_Contact_Id = pDlg->getContactID();

			if(m_NReg_Contact_Id >= 0)
			{
				populateData();
			}
		}
	}	

	delete pDlg;
}

void CHeaderSettingsFormView::OnBnClickedBtnRemoveContact()
{
	clearPreview();
	m_NReg_Contact_Id = -1;
	setRegistryValue();
}

void CHeaderSettingsFormView::clearPreview()
{
	m_wndStaticName.SetWindowTextW(_T(""));
	m_wndStaticAddress.SetWindowTextW(_T(""));
	m_wndStaticCity.SetWindowTextW(_T(""));
	m_wndStaticCountry.SetWindowTextW(_T(""));
	m_wndStaticPhone.SetWindowTextW(_T(""));
	m_wndStaticFax.SetWindowTextW(_T(""));
	m_wndStaticEmail.SetWindowTextW(_T(""));
	m_wndStaticWebsite.SetWindowTextW(_T(""));

	m_wndLblPhone.SetWindowTextW(_T(""));
	m_wndLblFax.SetWindowTextW(_T(""));
	m_wndLblEmail.SetWindowTextW(_T(""));
	m_wndLblWebsite.SetWindowTextW(_T(""));

	m_wndPicCtrl.FreeData();
	Invalidate();	
	UpdateWindow();
	m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);		
	m_wndGroupBox.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);
}

void CHeaderSettingsFormView::populateData()
{
	CString  csSQLContacts, csTmp;
	vecTransactionContacts vec;
	CTransaction_contacts contact;
	
	if(m_pDB != NULL && m_NReg_Contact_Id >= 0)
	{
		csSQLContacts.Format(_T("select * from %s where id = %d"),TBL_CONTACTS, m_NReg_Contact_Id);
		if(m_pDB->getContacts(csSQLContacts,vec))
		{
			if(vec.size() > 0)
			{
			contact = vec[0];
			m_wndStaticName.SetWindowTextW(contact.getName());
			m_wndStaticAddress.SetWindowTextW(contact.getAddress());
			csTmp = _T("");
			if(contact.getPostAddress() != _T(""))
				csTmp = contact.getPostAddress();
			if(contact.getCounty() != _T(""))
			{
				if(csTmp != _T(""))
					csTmp += _T(", ");
				csTmp += contact.getCounty();
			}
			if(contact.getPostNum())
			{
				if(csTmp != _T(""))
					csTmp += _T(", ");
				csTmp += contact.getPostNum();
			}
			//csTmp.Format(_T("%s, %s, %s"), contact.getPostAddress(), contact.getCounty(), contact.getPostNum());
			m_wndStaticCity.SetWindowTextW(csTmp);
			m_wndStaticCountry.SetWindowTextW(contact.getCountry());
			m_wndStaticPhone.SetWindowTextW(contact.getPhoneWork());
			m_wndStaticFax.SetWindowTextW(contact.getFaxNumber());
			m_wndStaticEmail.SetWindowTextW(contact.getEMail());
			m_wndStaticWebsite.SetWindowTextW(contact.getWebSite());

			m_wndLblPhone.SetWindowTextW(m_csPhone);
			m_wndLblFax.SetWindowTextW(m_csFax);
			m_wndLblEmail.SetWindowTextW(m_csEmail);
			m_wndLblWebsite.SetWindowTextW(m_csWebsite);

			// Try to load image from DB and save to disk; 
			CString sPicPath;
			sPicPath.Format(_T("%s%s"),getProgDir(),_T("tmp_tc.jpg"));
			if (m_pDB)
			{
				if (m_pDB->loadImage(sPicPath, contact))
				{
					if (fileExists(sPicPath)) m_wndPicCtrl.Load(sPicPath);
				}
				else
				{
					m_wndPicCtrl.FreeData();
				}
			}
			Invalidate();
			UpdateWindow();
			m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);		
			removeFile(sPicPath);
			m_wndGroupBox.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);

			setRegistryValue();
			}
		}
	}
}

//get value from registry
//get as REG_SZ string because m_NReg_Contact_Id = -1 if no value is set
BOOL CHeaderSettingsFormView::getRegistryValue()
{
	BOOL bRet = FALSE;

	HKEY hKeyCU = NULL;
	TCHAR szData[BUFSIZ];
	DWORD dwSize = BUFSIZ;
	
	//open key
	if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_HMSTC_SETTINGS_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyCU) == ERROR_SUCCESS)
	{
		if(RegQueryValueEx(hKeyCU, REG_REPORT_HEADER_ITEM, NULL, NULL, (LPBYTE)szData, &dwSize) == ERROR_SUCCESS)
		{
			m_NReg_Contact_Id = _tstoi(szData);
			bRet = TRUE;
		}
	}

	if(hKeyCU != NULL)
		RegCloseKey(hKeyCU);

	return bRet;
}

//save value to registry
//set as REG_SZ string because m_NReg_Contact_Id = -1 if no value is set

BOOL CHeaderSettingsFormView::setRegistryValue()
{
	BOOL bRet = FALSE;

	HKEY hKeyCU = NULL;
	TCHAR szData[BUFSIZ];
	DWORD dwSize = BUFSIZ;
	CString csTmp;
	
	//open key
	if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_HMSTC_SETTINGS_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyCU) == ERROR_SUCCESS)
	{
		csTmp.Format(_T("%d"),m_NReg_Contact_Id);
		dwSize = _stprintf_s(szData,csTmp);
		dwSize *= sizeof(TCHAR);
		if(dwSize >= 0)
			if(RegSetValueEx(hKeyCU, REG_REPORT_HEADER_ITEM, NULL, REG_SZ, (LPBYTE)szData, dwSize) == ERROR_SUCCESS)
			{
				bRet = TRUE;
			}
	}

	if(hKeyCU != NULL)
		RegCloseKey(hKeyCU);

	return bRet;
}



