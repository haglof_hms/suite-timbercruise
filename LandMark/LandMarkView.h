#pragma once

#include "Resource.h"
#include "stdafx.h"
#include "LandMarkDB.h"

class CStandReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CStandReportFilterEditControl)
public:
	CStandReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	afx_msg void OnKeyUp(UINT, UINT, UINT);
	
	DECLARE_MESSAGE_MAP()
};


//CLandMarkSelListReportRec
class CStandSelListReportRec : public CXTPReportRecord
{
	UINT m_nIndex;					

protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
		double m_fValue;
	public:
		CFloatItem(double fValue, LPCTSTR fmt_str = sz1dec)
			: CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
		}

		void setFloatItem(double value)
		{
			m_fValue = value;
			SetValue(value);
		}

		double getFloatItem(void)
		{
			return m_fValue;
		}
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
		int m_nValue;
	public:
		CIntItem(int nValue)
			: CXTPReportRecordItemNumber(nValue)
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		void setIntItem(int value)
		{
			m_nValue = value;
			SetValue(value);
		}

		int getIntItem(void)
		{
			return m_nValue;
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
		CString m_sText;
	public:
		CTextItem(CString sValue)
			: CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_sText = szText;
			SetValue(m_sText);
		}

		void setTextItem(LPCTSTR text)
		{
			m_sText = text;
			SetValue(m_sText);
		}

		CString getTextItem(void)
		{
			return m_sText;
		}
	};

	class CIconItem : public CXTPReportRecordItem
	{
		//private:
	public:
		CIconItem(int icon_id) : CXTPReportRecordItem()
		{
			SetIconIndex(icon_id);
		}

		void setIconId(int id)
		{
			SetIconIndex(id);
		}

		int getIconIndex()
		{
			return GetIconIndex();
		}
	};

public:

	CStandSelListReportRec(void)
	{
		m_nIndex = -1;									// FileIndex
		AddItem(new CTextItem(_T("")));					// TractID
		AddItem(new CFloatItem(0.0,sz1dec));			// TractAcres
		AddItem(new CTextItem(_T("")));					// Cruise Method
		AddItem(new CTextItem(_T("")));					// CruiseDate
		AddItem(new CTextItem(_T("")));					// TractLoc
		AddItem(new CTextItem(_T("")));					// TractOwner
		AddItem(new CTextItem(_T("")));					// Cruiser
		AddItem(new CIconItem(XTP_REPORT_NOICON));		// External documents icon
		AddItem(new CIntItem(-1));						// Property id 
	}

	CStandSelListReportRec(CTransaction_TCStand &rec)
	{
		m_nIndex = rec.getFileIndex();								// FileIndex
		AddItem(new CTextItem(rec.getTractID()));					// TractID
		AddItem(new CFloatItem(rec.getTractAcres(),sz1dec));		// TractAcres
		AddItem(new CTextItem(rec.getCruiseMethod()));				// Cruise Method
		AddItem(new CTextItem(rec.getCruiseDate()));				// CruiseDate
		AddItem(new CTextItem(rec.getTractLoc()));					// TractLoc
		AddItem(new CTextItem(rec.getTractOwner()));				// TractOwner
		AddItem(new CTextItem(rec.getCruiser()));					// Cruiser
		AddItem(new CIconItem(XTP_REPORT_NOICON));					// External documents icon
		AddItem(new CIntItem(rec.getPropID()));						// Property id
	}

	CStandSelListReportRec(CTransaction_TCStand &rec, int icon_index, bool snapshot)
	{
		m_nIndex = rec.getFileIndex();									//FileIndex
		AddItem(new CTextItem(rec.getTractID()));					// TractID
		AddItem(new CFloatItem(rec.getTractAcres(),sz1dec));			// TractAcres
		AddItem(new CTextItem(rec.getCruiseMethod()));				// Cruise Method
		AddItem(new CTextItem(rec.getCruiseDate()));				// CruiseDate
		AddItem(new CTextItem(rec.getTractLoc()));					// TractLoc
		AddItem(new CTextItem(rec.getTractOwner()));				// TractOwner
		AddItem(new CTextItem(rec.getCruiser()));					// Cruiser
		AddItem(new CIconItem(icon_index));							//External documents icon
		AddItem(new CIconItem(snapshot?10:XTP_REPORT_NOICON));		//Snapshots icon
		AddItem(new CIntItem(rec.getPropID()));						// Property id
	}

	
	int getFileIndex(void)
	{
		return m_nIndex;
	}

	int getIndex(void)
	{
		return m_nIndex;
	}

	int getIconIndex()
	{
		return ((CIconItem*)GetItem(7))->getIconIndex();
	}

	bool hasSnapshots()
	{
		return (((CIconItem*)GetItem(8))->getIconIndex() == 10);
	}

	double getColumnFloat(int item)
	{
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item, double value)
	{
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)
	{
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item, int value)
	{
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)
	{
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item, LPCTSTR text)
	{
		((CTextItem*)GetItem(item))->setTextItem(text);
	}
};




// CLandMarkView form view

class CLandMarkView : public CXTPReportView
{
	DECLARE_DYNCREATE(CLandMarkView)
protected:
	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sDBErrorMsg;

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;
	CString m_sTractGISError;

	int m_nSelectedColumn;

	vecTransactionTCStand m_vecStandData;
	BOOL getStand(void);

	BOOL setupReport(void);
	void populateReport(void);

	CXTPReportSubListControl m_wndSubList;
	CStandReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	//CXTPReportControl m_wndReport;

	void LoadReportState(void);
	void SaveReportState(void);

	CLandMarkDB *m_pDB;	
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;

	CImageList m_ilIcons;

	void SetFocusedTract(int tractid);

protected:
	CLandMarkView();           // protected constructor used by dynamic creation
	virtual ~CLandMarkView();

public:
	enum { IDD = IDD_FORMVIEW };
	virtual void OnDraw(CDC* pDC); 

	void deleteStandFromDB(void);
	void saveStandToDB(void);
	BOOL isDBConnected(void) { return m_pDB != NULL ? TRUE : FALSE;}
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual void OnInitialUpdate();
protected:
	
	DECLARE_MESSAGE_MAP()
public:
	void OnDestroy(void);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	
	afx_msg void OnReportItemClick(NMHDR *pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR *pNotifyStruct, LRESULT* result);
	afx_msg void OnShowFieldChooser(void);
	afx_msg void OnShowFieldFilter(void);
	afx_msg void OnShowFieldFilterOff(void);
	afx_msg void OnPrintPreview(void);
	afx_msg void OnRefresh(void);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTBBTNReports(void);
	afx_msg void OnReportItemDBClick(NMHDR *pNotifuStruct, LRESULT* result);
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnBtnDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
};


