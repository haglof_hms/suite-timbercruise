#pragma once
#include "Resource.h"

// CMDIFrameDoc document

class CMDIFrameDoc : public CDocument
{
	DECLARE_DYNCREATE(CMDIFrameDoc)

public:
	CMDIFrameDoc();
	virtual ~CMDIFrameDoc();
#ifndef _WIN32_WCE
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual BOOL OnNewDocument();

	DECLARE_MESSAGE_MAP()
};
#pragma once


// CLandMarkFrame frame

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CMDILandMarkFrame : public CChildFrameBase	//CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDILandMarkFrame)

	CXTPDockingPaneManager m_paneManager;
	CString m_sAbrevLangSet;
	CString m_sLangFN;

protected:
	CString m_sToolTipTCruise;
	CString m_sToolTipFilter;
	CString m_sToolTipFilterOff;
	CString m_sToolTipPrintOut;
	CString m_sToolTipRefresh;
	CString m_sToolTipReports;
	//CString m_sToolTipImport;
	CString m_sErrorMsg;
	CString m_sToolTipExtDoc;
	CString m_sToolTipGIS;

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager()
	{
		return &m_paneManager;
	}

	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	BOOL m_bDBConnOk;
	BOOL m_bEnableTBBTNFilterOff;
	BOOL m_bEnableTBBTNReports;
	CXTPToolBar m_wndToolBar;
	//RECT toolbarRect;	//?
	//CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HWND m_hWndTC;
	HICON m_hIcon;
public:
	CMDILandMarkFrame();           // protected constructor used by dynamic creation
	virtual ~CMDILandMarkFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	CDialogBar m_wndFieldChooserDlg;
	CDialogBar m_wndFilterEdit;

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

	void setEnableTBBTNReports(BOOL v)
	{
		m_bEnableTBBTNReports = v;
	}

	void disableTBBtns(void);

protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnPaint();
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
	afx_msg void OnBtnTCruise(void);
	afx_msg void OnUpdateTBBTNReports(CCmdUI* pCmdUI);
	afx_msg void OnClose();
	//afx_msg void OnBtnImport(void);
	afx_msg void OnBtnExternalDocs(void);
	afx_msg void OnBtnGIS(void);

	int CreatePublicDirectory(CString csPath);
};


