#pragma once
#include "Resource.h"
#include "LandMarkDB.h"


class CSnapshotImg
{
public:
	CSnapshotImg() : deleted(false) {}

	CImage img;
	int id;
	CString name;
	bool deleted;
};

// CSnapshotView form view

class CSnapshotView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSnapshotView)

protected:
	CSnapshotView();           // protected constructor used by dynamic creation
	virtual ~CSnapshotView();

	const int m_offsetx;
	const int m_offsety;
	const int m_sizex;
	const int m_sizey;
	int m_height;
	int m_width;
	int m_rowlength;
	int m_fileIndex;
	int m_numSnapshots;
	int m_selectedIdx;

	CLandMarkDB *m_pDB;
	CSnapshotImg *m_pImages;
	CStringArray m_names;
	CImage m_overlay;

	CString m_sConfirmRemove;

	bool LocateSnapshot(CPoint pt, int &idx);
	void LoadSnapshots();
	void SaveImageToFile(int id, CString path);

public:
	enum { IDD = IDD_FORMVIEW18 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();

	void PreviewSnapshot(int idx);
	void UpdateToolbarButtons();
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
};
