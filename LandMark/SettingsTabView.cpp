// SettingsTabView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "SettingsTabView.h"
#include "PathSettingsFormView.h"
#include "HeaderSettingsFormView.h"



// CSettingsTabView

IMPLEMENT_DYNCREATE(CSettingsTabView, CView)

CSettingsTabView::CSettingsTabView()
{
	m_bInitialized = FALSE;
}

CSettingsTabView::~CSettingsTabView()
{
}

BEGIN_MESSAGE_MAP(CSettingsTabView, CView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL3, OnSelectedChanged)
END_MESSAGE_MAP()


// CSettingsTabView drawing

void CSettingsTabView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}


// CSettingsTabView diagnostics

#ifdef _DEBUG
void CSettingsTabView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CSettingsTabView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSettingsTabView message handlers

BOOL CSettingsTabView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CSettingsTabView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	if(!m_bInitialized)
	{
		m_sAbrevLangSet = getLangSet();
		m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);
		m_bInitialized = TRUE;
	}
}


LRESULT CSettingsTabView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	return 0L;
}

int CSettingsTabView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Tab control
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, CRect(0,0,0,0), this, ID_TABCONTROL3);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = FALSE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = FALSE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	RLFReader xml;
	if (xml.Load(m_sLangFN))
	{
		AddView(RUNTIME_CLASS(CPathSettingsFormView), xml.str(IDS_STRING940),0, -1);
		AddView(RUNTIME_CLASS(CHeaderSettingsFormView), xml.str(IDS_STRING941),1, -1);
		
		xml.clean();
	}
//set active view, need this if to use GetActiveView in MDISettingsFrame::OnClose
	if(m_wndTabControl.getNumOfTabPages() > 0)
	{
		CFrameWnd* pFrame = GetParentFrame();
		CPathSettingsFormView* pView = DYNAMIC_DOWNCAST(CPathSettingsFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(0)->GetHandle()));
		ASSERT_KINDOF(CPathSettingsFormView, pView);
		pFrame->SetActiveView(pView);
	}

	return 0;
}

void CSettingsTabView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndTabControl.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndTabControl,1,1,rect.right-1,rect.bottom-1);
	}
}

CMDIFrameDoc* CSettingsTabView::GetDocument()
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMDIFrameDoc)));
	return (CMDIFrameDoc*)m_pDocument;
}

BOOL CSettingsTabView::AddView(CRuntimeClass *pViewClass, LPCTSTR lpszTitle, int tab_id, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc = GetDocument();
	contextT.m_pNewViewClass = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();
	CWnd* pWnd;

	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if(pWnd == NULL)
			AfxThrowMemoryException();
	}
	CATCH_ALL(e)
	{
		TRACE0("Out of memory creating a view.\n");
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();
	CRect rect(0,0,0,0);

	if(!pWnd->Create(NULL, NULL, dwStyle, rect, &m_wndTabControl, AFX_IDW_PANE_FIRST + nTab, &contextT))
	{
		TRACE0("Couldn't create client tab for view.\n");
		// pWnd will be cleaned up by PostNcDestroy
		return FALSE;
	}

	m_tabManager = m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	m_tabManager->SetData(tab_id);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);
	
	return TRUE;
}

CPathSettingsFormView* CSettingsTabView::getPathSettingsFormView()
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if(m_tabManager)
	{
		CPathSettingsFormView* pView = DYNAMIC_DOWNCAST(CPathSettingsFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CPathSettingsFormView, pView);
		return pView;
	}

	return NULL;
}

CHeaderSettingsFormView* CSettingsTabView::getHeaderSettingsFormView()
{
	m_tabManager = m_wndTabControl.getTabPage(1);
	if(m_tabManager)
	{
		CHeaderSettingsFormView* pView = DYNAMIC_DOWNCAST(CHeaderSettingsFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CHeaderSettingsFormView, pView);
		return pView;
	}

	return NULL;
}

void CSettingsTabView::OnSelectedChanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	
	UNUSED_ALWAYS(pNMHDR);
	pResult = 0;

	CFrameWnd* pFrame = GetParentFrame();
	if(pFrame && m_wndTabControl.getNumOfTabPages() > 0)
	{
		CView *pView = DYNAMIC_DOWNCAST(CView, CWnd::FromHandle(m_wndTabControl.GetSelectedItem()->GetHandle()));
		ASSERT_KINDOF(CView, pView);

		pFrame->SetActiveView(pView);
	}
	
}
