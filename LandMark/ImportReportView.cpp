// ImportReportView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "ImportReportView.h"
#include "ReportView.h"


// CImportReportView

IMPLEMENT_DYNCREATE(CImportReportView, CView)

CImportReportView::CImportReportView()
{

}

CImportReportView::~CImportReportView()
{
}

BEGIN_MESSAGE_MAP(CImportReportView, CView)
END_MESSAGE_MAP()


// CImportReportView drawing

void CImportReportView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}


// CImportReportView diagnostics

#ifdef _DEBUG
void CImportReportView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CImportReportView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CImportReportView message handlers
/*
BOOL CImportReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}*/


void CImportReportView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	CString csMsg, csTitle;

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csTitle = xml->str(IDS_STRING151);
			m_sErrorMsg = xml->str(IDS_STRING152);
		}
		delete xml;
	}

	CString buf_filename;
	CFileDialog *pOpenFileDlg = new CFileDialog(TRUE);

	pOpenFileDlg->GetOFN().lpstrTitle = csTitle;
	pOpenFileDlg->GetOFN().lpstrFile = buf_filename.GetBuffer(2048*(_MAX_PATH + 1) +1);
	pOpenFileDlg->GetOFN().nMaxFile = 2048 * _MAX_PATH;

	if(!pOpenFileDlg)
	{
		buf_filename.ReleaseBuffer();
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}

	pOpenFileDlg->m_ofn.lpstrFilter = _T("Report Files (*.rpt)\0*.rpt\0");
	pOpenFileDlg->m_ofn.Flags = OFN_ALLOWMULTISELECT|OFN_EXPLORER|OFN_HIDEREADONLY;

	if(pOpenFileDlg->DoModal() != IDOK)
	{
		buf_filename.ReleaseBuffer();
		delete pOpenFileDlg;
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}

	HKEY hKey;
	TCHAR szDir[BUFSIZ];
	DWORD dwDirSize = BUFSIZ;
	CString csDir;

	//get ..\Reports\TC directory from registry
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, HMS_INST_DIR, NULL, KEY_QUERY_VALUE, &hKey) != ERROR_SUCCESS)
	//if(RegOpenKeyEx(HKEY_CURRENT_USER, HMS_INST_DIR, NULL, KEY_QUERY_VALUE, &hKey) != ERROR_SUCCESS)
	{
		buf_filename.ReleaseBuffer();
		delete pOpenFileDlg;
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}

	if(RegQueryValueEx(hKey, REPORTS_INST_DIR_KEY/*HMS_INST_DIR_KEY*/, NULL, NULL, (LPBYTE)szDir, &dwDirSize) != ERROR_SUCCESS)
	{
		buf_filename.ReleaseBuffer();
		RegCloseKey(hKey);
		delete pOpenFileDlg;
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}

	RegCloseKey(hKey);
	//_stprintf_s(szDir, _T("%s%s\\"),szDir,REPORTS_TC_DIR);
	_stprintf_s(szDir, _T("%s\\"),szDir);
	csDir = szDir;


	POSITION pos;
	CString csFile, csOutFN,csFN;

	pos = pOpenFileDlg->GetStartPosition();
	while(pos)
	{
		csFile = pOpenFileDlg->GetNextPathName(pos);
		csFN = csFile.Right(csFile.GetLength() - csFile.ReverseFind('\\') - 1);
		csOutFN = csDir + csFN;

		if(CopyFile(csFile, csOutFN, TRUE) == 0)
		{
			if(GetLastError() == ERROR_FILE_EXISTS)
			{
				csMsg.Format(_T("%s\r\n%s"),csOutFN, m_sErrorMsg);
				if(AfxMessageBox(csMsg, MB_ICONEXCLAMATION|MB_YESNO) == IDYES)
					CopyFile(csFile, csOutFN, FALSE);
			}
		}
	}

	buf_filename.ReleaseBuffer();
	delete pOpenFileDlg;

	//refresh report view
	CReportView* pView = (CReportView*)getFormViewByID(IDD_FORMVIEW1);
	if(pView)
	{
		pView->getReportData();
		pView->populateReport();
	}

	GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}
