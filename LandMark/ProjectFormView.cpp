


// ProjectFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIProjectFrame.h"
#include "ProjectFormView.h"
#include "StandsInProjectFormView.h"
#include "ProjectTabView.h"

// CProjectFormView

IMPLEMENT_DYNCREATE(CProjectFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CProjectFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_KEYUP()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)

	ON_COMMAND(ID_TBBTN_CREATE, OnSearchReplace)	// 090820 p�d

	/*TODO: ON_COMMAND(ID_TOOLS_CHANGE_OBJID, OnChangeObjID)*/
	ON_EN_SETFOCUS(IDC_EDIT6_1, &CProjectFormView::OnEnSetfocusEdit61)
	ON_EN_SETFOCUS(IDC_EDIT6_2, &CProjectFormView::OnEnSetfocusEdit62)
	ON_EN_SETFOCUS(IDC_EDIT6_17, &CProjectFormView::OnEnSetfocusEdit617)
	ON_EN_SETFOCUS(IDC_EDIT6_4, &CProjectFormView::OnEnSetfocusEdit64)
END_MESSAGE_MAP()

CProjectFormView::CProjectFormView()
	: CXTResizeFormView(CProjectFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bIsDataEnabled = FALSE;
	m_pDB = NULL;
	m_bSetFocusOnInitDone = FALSE;
}

CProjectFormView::~CProjectFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CProjectFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_PROJECT_GROUP, m_wndGroup);

	DDX_Control(pDX, IDC_LBL6_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL6_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL6_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL6_9, m_wndLbl9);

	DDX_Control(pDX, IDC_EDIT6_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT6_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT6_4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT6_17, m_wndEdit9);
	//}}AFX_DATA_MAP

}

BOOL CProjectFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CProjectFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit4.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit9.SetDisabledColor(BLACK,COL3DFACE);
		
		m_wndEdit9.SetAsNumeric();

		setLanguage();

		m_sSQLProjects.Format(_T("select * from %s"),TBL_PROPERTIES);	/*TODO: change for project*/
		getProjects();
		m_nDBIndex = (int)m_vecProjectData.size() - 1;
		populateData(m_nDBIndex);

		m_bIsDirty = FALSE;
		m_bInitialized = TRUE;
	}	// if (! m_bInitialized )
}

void CProjectFormView::OnSetFocus(CWnd *pWnd)
{
	setEnableOwnerTab(m_vecProjectData.size() > 0);

	CXTResizeFormView::OnSetFocus(pWnd);
}

// Handle key strokes and send message to
// HMSShell, �which the sell interptret and sends back
// a message like ID_NEW_ITEM etc; 070103 p�d
void CProjectFormView::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
//	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
//	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, nChar,bControlKey);
}

BOOL CProjectFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CProjectFormView diagnostics

#ifdef _DEBUG
void CProjectFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CProjectFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// PRIVATE
void CProjectFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			m_wndLbl1.SetWindowText((xml.str(IDS_STRING344)));
			m_wndLbl2.SetWindowText((xml.str(IDS_STRING345)));
			m_wndLbl4.SetWindowText((xml.str(IDS_STRING342)));
			m_wndLbl9.SetWindowText((xml.str(IDS_STRING343)));

			m_sStandTabCaption = (xml.str(IDS_STRING310));
			m_sFrameCaption = (xml.str(IDS_STRING342/*256*/));

			m_sOKBtn = (xml.str(IDS_STRING250));
			m_sCancelBtn = (xml.str(IDS_STRING251));

			m_sErrCap = (xml.str(IDS_STRING240));
			m_sSaveMsg.Format(_T("%s\n\n%s\n"),
								(xml.str(IDS_STRING348)),
								(xml.str(IDS_STRING242)));

			m_sDoneSavingMsg =	(xml.str(IDS_STRING349));

			m_sMsgCap1 = (xml.str(IDS_STRING240));
			m_sMsgCap = (xml.str(IDS_STRING350));
			m_sDeleteMsg = (xml.str(IDS_STRING351));

			
			m_sDataMissinMsg.Format(_T("%s\n\n%s"),
				(xml.str(IDS_STRING352)),
				(xml.str(IDS_STRING353)));

			/*TODO: change for project*/
			m_sProjectActiveMsg.Format(_T("%s\n%s\n\n%s\n%s\n"),
				(xml.str(IDS_STRING320/*3000*/)),
				(xml.str(IDS_STRING321/*3001*/)),
				(xml.str(IDS_STRING322/*3002*/)),
				(xml.str(IDS_STRING323/*3003*/)));
			
			m_sNoProjectsMsg.Format(_T("%s\n\n%s"),
				xml.str(IDS_STRING324/*3110*/),
				xml.str(IDS_STRING325/*3111*/));

			m_sNoResultInSearch = xml.str(IDS_STRING326/*3221*/);
		}
		xml.clean();
	}
}

void CProjectFormView::populateData(int idx, BOOL bSetBars)
{
	CString sTmp;

	if (m_vecProjectData.size() > 0 && 
		  idx >= 0 && 
			idx < (int)m_vecProjectData.size())
	{
		m_enumAction = UPD_ITEM;

		m_activeProjectData = m_vecProjectData[idx];

		/*TODO: change for project*/
		m_wndEdit1.SetWindowText(m_activeProjectData.getCountyName());
		m_wndEdit1.setIdentifer(m_activeProjectData.getCountyCode());
		m_wndEdit2.SetWindowText(m_activeProjectData.getMunicipalName());
		m_wndEdit2.setIdentifer(m_activeProjectData.getMunicipalCode());
		m_wndEdit9.SetWindowText(m_activeProjectData.getPropertyNum());		
		m_wndEdit4.SetWindowText(m_activeProjectData.getPropertyName());	
		
		CProjectTabView *pTabView = (CProjectTabView *)getFormViewByID(IDD_FORMVIEW13);
		if (pTabView)
		{
			sTmp.Format(_T("%s; %s"),
								m_sFrameCaption,
								m_wndEdit4.getText()	//Project name
								);	
			pTabView->setMsg(sTmp);
			

			CXTPTabManagerItem *pManager = pTabView->getTabControl()->getTabPage(1);
			if (pManager)
			{
				pManager->SetCaption(m_sStandTabCaption);
				pManager->SetData(DWORD_PTR(&m_activeProjectData));
				
				// Also update owners for selected property; 070126 p�d
				/*TODO: CProjectOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CProjectOwnerFormView, 
					CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle()));
				{
					if (pView1 != NULL)
					{
						// Check if data's been changed on ProjectOwners for property; 070126 p�d
						//pView1->isDataChanged();
						// Display propertyowner(s) for this property; 070126 p�d
						pView1->populateReport();
					}	// if (pView1 != NULL)
				}	// CMDIPropOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CMDIPropOwnerFormView, 
				*/
				
				// Also update stands for selected property; 070126 p�d
				CStandsInProjectFormView* pView2 = DYNAMIC_DOWNCAST(CStandsInProjectFormView, 
					CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle()));
				{
					if (pView2 != NULL)
					{
						pView2->populateReport();
					}	// if (pView1 != NULL)
				}	// CMDIPropOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CMDIPropOwnerFormView, 
				
			}	// if (pManager)
		}	// if (pTabView)

		setEnableData(TRUE);
		setEnableOwnerTab(TRUE);

		if(bSetBars)
		{
			if (m_vecProjectData.size() == 1)
			{
				setNavigationButtons(FALSE,FALSE);
			}
			else if (m_vecProjectData.size() > 1)
			{
				setNavigationButtons(idx > 0,idx < (int)m_vecProjectData.size() - 1);
			}
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
		}

	}
	else
	{
		// Not used; 090219 p�d
		//::MessageBox(this->GetSafeHwnd(),(m_sNoPropertiesMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
		setNavigationButtons(FALSE,FALSE);
		setEnableData(FALSE);
		setEnableOwnerTab(FALSE);
		setSearchToolbarBtn(FALSE);
		m_nDBIndex = -1;
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		if (m_enumAction == NEW_ITEM)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}
		else
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}

	}
}

// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CProjectFormView::isDataChanged(void)
{
	if (saveProject())
	{
		resetIsDirty();
	}
	return FALSE;
}

BOOL CProjectFormView::getIsDirty(void)
{
	if (m_bIsDirty)
		return m_bIsDirty;

	if (m_wndEdit1.isDirty() ||
			m_wndEdit2.isDirty() ||
			m_wndEdit4.isDirty() ||
			m_wndEdit9.isDirty()) 
	{
		return TRUE;
	}
	return FALSE;
}

void CProjectFormView::resetIsDirty(void)
{

	m_wndEdit1.resetIsDirty();
	m_wndEdit2.resetIsDirty();
	m_wndEdit4.resetIsDirty();
	m_wndEdit9.resetIsDirty();
}

void CProjectFormView::clearAll()
{

	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit9.SetWindowText(_T(""));

	CProjectTabView *pView = (CProjectTabView*)getFormViewByID(IDD_FORMVIEW13);
	if (pView != NULL)
	{
		CString sTmp;
		sTmp.Format(_T("%s:"),m_sFrameCaption);

		pView->setMsg((sTmp));
		/*TODO: CProjectOwnerFormView *pViewOwner = pView->getPropOwnerFormView();
		if (pViewOwner != NULL)
		{
			pViewOwner->getReportCtrl().ClearReport();
			pViewOwner->getReportCtrl().Populate();
			pViewOwner->getReportCtrl().UpdateWindow();
		}
		*/
	}

	m_wndEdit4.SetFocus();	//project name

}

void CProjectFormView::doSetSearchBtn(BOOL enable)
{
	setSearchToolbarBtn(enable);
}

// CProjectFormView message handlers

BOOL CProjectFormView::getProjects(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			/*TODO: m_pDB->getProjects(m_sSQLProps,m_vecProjectData);
			*/
		}	// if (pDB != NULL)
	}
	return (m_vecProjectData.size() > 0);
}

BOOL CProjectFormView::doRePopulateFromSearch(LPCTSTR sql,bool goto_first)
{
	BOOL bFound = FALSE;
	m_sSQLProjects = sql;
	getProjects();
	if (m_vecProjectData.size() > 0)
	{

		// We'll try to find index of active property and set m_nDBIndex to point 
		// to this item; 091008 p�d
		for (UINT i = 0;i < m_vecProjectData.size();i++)
		{
			if (m_activeProjectData.getID() == m_vecProjectData[i].getID())
			{
				m_nDBIndex = i;	
				bFound = TRUE;
				break;
			}	// if (m_activeProjectData.getID() == m_vecProjectData[i].getID())
		}	// for (UINT i = 0;i < m_vecProjectData.size();i++)

		// If we couldn't find the item, set to first or last, depending on
		// "goto_first"; 091008 p�d
		if (!bFound)
		{
			// Go to last entry; 090820 p�d
			m_nDBIndex = (m_vecProjectData.size() - 1);
		}

		populateData(m_nDBIndex);
		doSetNavigationBar();
	}
	else
		::MessageBox(this->GetSafeHwnd(),m_sNoResultInSearch,m_sMsgCap1,MB_ICONASTERISK | MB_OK);

	return FALSE;
}

void CProjectFormView::doSetNavigationBar(void)
{
	// If there's more than one item in list, set Navigationbar; 090217 p�d
	// If only one item, disable Navigationbar; 090217 p�d
	if (m_vecProjectData.size() > 1)
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecProjectData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,(m_vecProjectData.size() >= 1) || (m_enumAction == NEW_ITEM));
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,(m_vecProjectData.size() >= 1));
}


// PROTECTED METHODS

void CProjectFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CProjectFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_WPARAM_VALUE_FROM + 0x02 :
		{
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			if (sizeof(*msg) == sizeof(_doc_identifer_msg))
			{
				if (msg->getValue1() == 10)
				{
					// Try to match the Project from GIS to properties in Forrest; 090128 p�d
					if (m_vecProjectData.size() > 0)
					{
						for (UINT i = 0;i < m_vecProjectData.size();i++)
						{
							if (m_vecProjectData[i].getID() == msg->getValue2())
							{
								m_nDBIndex = i;
								populateData(m_nDBIndex);
								break;
							}
						}
					}
				}	// if (msg->getValue1() == 10)
			}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
			break;
		}
		case ID_NEW_ITEM :
		{
			saveProject();		// Save before adding a new one; 090511 p�d
			// Set as new item; 081219 p�d
			m_activeProjectData = CTransaction_property();	/*TODO: change for project*/
			addProject();
			setEnableData(TRUE);
			setEnableOwnerTab(FALSE);
			setActiveTab(0);
			break;
		}	// case ID_NEW_ITEM :
		case ID_DELETE_ITEM :
		{
			setActiveTab(0);
			removeProject();	
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (saveProject())
			{
				m_nDBIndex = 0;
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (saveProject())
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			// Just save data; 080609 p�d
			if (saveProject())
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((int)m_vecProjectData.size() - 1))
					m_nDBIndex = (int)m_vecProjectData.size() - 1;
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			// Just save data; 080609 p�d
			if (saveProject())
			{
				m_nDBIndex = (int)m_vecProjectData.size()-1;
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_DBNAVIG_END :
	}	// switch (wParam)

	return 0L;
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CProjectFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{

	m_bNavButtonStartPrev = start_prev;
	m_bNavButtonEndNext = end_next;
/*
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
*/
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

}


void CProjectFormView::setEnableData(BOOL enable)
{
	m_wndEdit1.EnableWindow(enable);
	m_wndEdit1.SetReadOnly(!enable);
	m_wndEdit2.EnableWindow(enable);
	m_wndEdit2.SetReadOnly(!enable);
	m_wndEdit4.EnableWindow(enable);
	m_wndEdit4.SetReadOnly(!enable);
	m_wndEdit9.EnableWindow(enable);
	m_wndEdit9.SetReadOnly(!enable);
	
	m_bIsDataEnabled = enable;


}

void CProjectFormView::setEnableOwnerTab(BOOL enable)
{
	enableTabPage(1,enable);
	enableTabPage(2,enable);
	
}

void CProjectFormView::setActiveTab(int tab)
{
	activeTabPage(tab);
	
}

// Handle transaction on database species table; 060317 p�d

BOOL CProjectFormView::getEnteredData(void)
{
	int nID = -1;

	BOOL bIsOK = (!m_wndEdit4.getText().IsEmpty());

	if (!bIsOK)
		return FALSE;


	return TRUE;

}

BOOL CProjectFormView::addProject(void)
{
	clearAll();
	m_bIsDirty = TRUE;
	m_enumAction = NEW_ITEM;
	return TRUE;
}

BOOL CProjectFormView::saveProject(void)
{
	/*TODO: check that not all fields empty*/
	if(m_wndEdit4.getText().IsEmpty())
	{
		//AfxMessageBox(_T("Project name missing!"));
		return FALSE;
	}
	

	if (!m_bIsDataEnabled) return FALSE;
	// Datbase info data members
	BOOL bReturn = FALSE;
	int nNumOf=0;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Check number of entries in the contacts table.
			// If there's no entries, reset the identity field to start
			// from 1; 070102 p�d

			if (m_activeProjectData.getID() == -1)
			{
				/*TODO: nNumOf = m_pDB->getNumOfRecordsInProject();
				*/
				if (nNumOf < 1)
				{
					/*TODO: m_pDB->resetProjectIdentityField();
					*/
				}
				
			}	// if (m_enumAction = NEW_ITEM)
/*
			if (nNumOf > 0)
			{
				if (!getEnteredData())
				{
					::MessageBox(this->GetSafeHwnd(),(m_sDataMissinMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
					return FALSE;
				}
			}	// if (nNumOf > 0)
*/
			/*TODO: m_enteredProjectData = CTransaction_property(m_activeProjectData.getID(),	
																									m_wndEdit1.getIdentifer(),
																									m_wndEdit2.getIdentifer(),
																									m_wndEdit3.getIdentifer(),
																									m_wndEdit1.getText(),
																									m_wndEdit2.getText(),
																									m_wndEdit3.getText(),
																									m_wndEdit9.getText(),
																									m_wndEdit4.getText(),
																									m_wndEdit5.getText(),
																									m_wndEdit6.getText(),
																									m_wndEdit7.getFloat(),
																									m_wndEdit8.getFloat(),
																									getUserName(),
																									_T(""),
																									m_wndEdit10.getText(),
																									_T(""));
																									*/

			/*TODO: if (m_pDB->addProject(m_enteredProjectData))
			{
				// Reload Properties; 081219 p�d
				getProjects();
				m_nDBIndex = (m_vecProjectData.size()-1);
				populateData(m_nDBIndex);
			}
			else if (m_pDB->updProject(m_enteredProjectData))
			{
				// Reload Properties; 081219 p�d
				getProjects();
				populateData(m_nDBIndex);
			}
			*/
			bReturn = TRUE;

		}	// if (m_pDB != NULL)
		m_bIsDirty = FALSE;
		resetIsDirty();
		// Check who has focus, to determin if we should enable tool-butttons; 091008 p�d
		setSearchToolbarBtn(TRUE);
	}

	m_enumAction = UPD_ITEM;

	if(bReturn)
	{
		//send msg to update Project select list view
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
				(LPARAM)&_doc_identifer_msg(MODULE936, MODULE937, _T(""), 1, 0, 0));
				
	}

	return bReturn;
}

BOOL CProjectFormView::removeProject(void)
{
	CXTPReportRow *pRow = NULL;
	CTransaction_property data;	/*TODO: change for projects*/
	CString sMsg;
	int nIndex = -1;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_vecProjectData.size() > 0)
		{
			if (m_pDB != NULL)
			{
				data = m_vecProjectData[m_nDBIndex];
				if (!doIsProjectUsed(data))	/*TODO: change this for project*/
				{
					//user needs to remove all owners and\or tracts from property first 
					sMsg.Format(_T("%s\n\n%s: %s %s\n\n%s"),
									m_sMsgCap,
									m_sName,
									data.getPropertyName(),	/*TODO: change for project*/
									data.getPropertyNum(),	/*TODO: change for project*/
									//data.getBlock(),
									//data.getUnit(),
									m_sDeleteMsg);

					if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					{
						/*TODO: bReturn = m_pDB->removeProject(data);
						*/
						
					}	// if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				}	// if (!doIsProjectUsed(data))
				else
					::MessageBox(this->GetSafeHwnd(),m_sProjectActiveMsg,m_sErrCap,MB_ICONINFORMATION | MB_OK);
			}	// if (m_pDB != NULL)
		}	// if (m_vecMachineData.size() > 0)

		if (bReturn)
		{
			// Reload information
			getProjects();

			if (m_vecProjectData.size() == 0)
			{
				setNavigationButtons(FALSE,FALSE);
				clearAll();
				setEnableData(FALSE);
				setEnableOwnerTab(FALSE);

			}
			else if (m_vecProjectData.size() == 1)
			{
				setNavigationButtons(FALSE,FALSE);
				setEnableData(TRUE);
				setEnableOwnerTab(TRUE);

			}
			else if (m_vecProjectData.size() > 1)
			{
				setNavigationButtons(m_nDBIndex > 0,
  													 m_nDBIndex < ((int)m_vecProjectData.size()-1));
				setEnableData(TRUE);
				setEnableOwnerTab(TRUE);
			}

			// After a delete, set to last item; 060103 p�d
			m_nDBIndex = (int)m_vecProjectData.size() - 1;
			populateData(m_nDBIndex);
//			m_enumAction = UPD_ITEM;
			m_bIsDirty = FALSE;


			//send msg to update Project select list view
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
				(LPARAM)&_doc_identifer_msg(MODULE936, MODULE937, _T(""), 1, 0, 0));
				

		}	// if (bReturn)

		return TRUE;
	}
	return FALSE;
}

BOOL CProjectFormView::doIsProjectUsed(CTransaction_property &rec)	/*TODO: change for project*/
{
	BOOL bRet1 = FALSE;
	BOOL bRet2 = FALSE;
	CProjectTabView *pTabView = (CProjectTabView *)getFormViewByID(IDD_FORMVIEW13);
	if (pTabView)
	{
		if (CStandsInProjectFormView *pView1 = DYNAMIC_DOWNCAST(CStandsInProjectFormView,CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle())))
		{
			if (pView1) bRet1 = pView1->isTraktInProject();
		}	// if (CStandsInProjectFormView*pView = DYNAMIC_DOWNCAST(CStandsInProjectFormView,CWnd::FromHandle(pTabView->getTabControl()->getTabPage(2)->GetHandle())))

		/*TODO: if (CProjectOwnerFormView *pView2 = DYNAMIC_DOWNCAST(CProjectOwnerFormView,CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle())))
		{
			if (pView2) bRet2 = pView2->isOwnerInProject();
		}	// if (CProjectOwnerFormView *pView2 = DYNAMIC_DOWNCAST(CMDIPropOwnerFormView,CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle())))
	*/
	}	// if (pTabView)

	if (bRet1 || bRet2) return TRUE;
	else return FALSE;
}

/*TODO: 
void CProjectFormView::setCountyMunicpalAndParish(CTransaction_county_municipal_parish data)
{
	CString sValue;
	m_recSelectedCMP = data;
	m_wndEdit1.SetWindowText(m_recSelectedCMP.getCountyName());
	sValue.Format(_T("%d"),m_recSelectedCMP.getCountyID());
	m_wndEdit1.setIdentifer(sValue);
	
	m_wndEdit2.SetWindowText(m_recSelectedCMP.getMunicipalName());
	sValue.Format(_T("%d"),m_recSelectedCMP.getMunicipalID());
	m_wndEdit2.setIdentifer(sValue);
	
	
	m_wndEdit1.setIsDirty();
	m_wndEdit2.setIsDirty();

}
*/

BOOL CProjectFormView::doPopulateNext()
{
	m_nDBIndex++;
	if (m_nDBIndex > ((int)m_vecProjectData.size() - 1))
		m_nDBIndex = (int)m_vecProjectData.size() - 1;
	populateData(m_nDBIndex);

	return (m_nDBIndex < ((int)m_vecProjectData.size() - 1));
}

BOOL CProjectFormView::doPopulatePrev()
{
	m_nDBIndex--;
	if (m_nDBIndex < 0)	m_nDBIndex = 0;
	populateData(m_nDBIndex);

	return (m_nDBIndex > 0);
}

void CProjectFormView::doPopulate(int index,bool set_by_index, BOOL bSetBars)
{
	// Do a check, if user has changed
	// data for active contact and if so
	// ask user to save; 070111 p�d
	if(bSetBars)
	isDataChanged();

	if (set_by_index)
	{
		m_nDBIndex = index;
		populateData(m_nDBIndex, bSetBars);
	}
	else
	{
		// Find index of argument 'index' in m_vecProjectData; 090810 p�d
		if (m_vecProjectData.size() > 0)
		{
			// Set default to	laste entry, if we can't match index to ID; 090810 p�d
			m_nDBIndex = (int)m_vecProjectData.size() - 1;
			for (UINT i = 0;i < m_vecProjectData.size();i++)
			{
				if (m_vecProjectData[i].getID() == index)
				{
					m_nDBIndex = i;
					populateData(m_nDBIndex, bSetBars);
					break;
				}	// if (m_vecProjectData[i].getID() == index)
			}	// for (UINT i = 0;i < m_vecProjectData.size();i++)
		}	// if (m_vecProjectData.size() > 0)
	}

}

void CProjectFormView::doRePopulate(void)
{
	populateData(m_nDBIndex);
}


void CProjectFormView::OnImport()
{
	AfxMessageBox(_T("TODO: Import data"));
	/*TODO: showFormView(IDD_FORMVIEW7,m_sLangFN,122);
	*/
}

void CProjectFormView::OnSearchReplace()
{
	SEARCH_REPLACE_INFO info;
	info.m_nID = m_activeProjectData.getID();
	info.m_origin = ORIGIN_PROPERTY;			/*TODO: change for project*/
	// Setup SEARCH_REPLACE_INFO info depending on
	// which edit items selected; 090820 p�d
	info.m_focusedEdit = m_enumFocusedEdit; 
	info.m_nNumber = 0;
	info.m_fNumber = 0.0;
	info.m_bIsNumeric = FALSE;
	// Numeric edit on these: 090820 p�d
	if (m_enumFocusedEdit == ED_PROPNUM)	/*TODO: change for project*/
	{
		info.m_sText = m_wndEdit9.getText();
	}
	else if (m_enumFocusedEdit == ED_PROPNAME)	/*TODO: change for project*/
	{
		info.m_sText = m_wndEdit4.getText();
	}
	/*TODO: showFormView(IDD_FORMVIEW10,m_sLangFN,(LPARAM)&info);
	*/
	AfxMessageBox(_T("TODO: Search and Replace"));
}

void CProjectFormView::refreshProjects(void)
{
	// Reload information
	getProjects();

	if (m_vecProjectData.size() == 1)
	{
		setNavigationButtons(FALSE,FALSE);
	}
	else if (m_vecProjectData.size() > 1)
	{
		setNavigationButtons(m_nDBIndex > 0,
												 m_nDBIndex < ((int)m_vecProjectData.size()-1));
	}

	// After a delete, set to last item; 060103 p�d
	m_nDBIndex = (int)m_vecProjectData.size() - 1;

	populateData(m_nDBIndex);

}

void CProjectFormView::refreshNavButtons(void)
{
	if (m_vecProjectData.empty())
	{
		setNavigationButtons(FALSE,FALSE);
		clearAll();
	}
	else
	{
		setNavigationButtons(m_nDBIndex > 0,
												 m_nDBIndex < ((int)m_vecProjectData.size()-1));
	}
}


/*TODO: 
void CProjectFormView::OnBnClickedButton1()
{
	showFormView(IDD_REPORTVIEW2,m_sLangFN);
	CCMPFormView *pView = (CCMPFormView *)getFormViewByID(IDD_REPORTVIEW2); 
	if (pView)
	{
		pView->setFilterText(m_wndEdit1.getText(),3);
	}
	
}
*/

/*TODO: 
void CProjectFormView::OnBnClickedButton3()
{
	showFormView(IDD_REPORTVIEW2,m_sLangFN);
	CCMPFormView *pView = (CCMPFormView *)getFormViewByID(IDD_REPORTVIEW2); 
	if (pView)
	{
		pView->setFilterText(m_wndEdit2.getText(),4);
	}
	
}
*/

/*TODO: 
void CProjectFormView::OnBnClickedButton4()
{
	showFormView(IDD_REPORTVIEW2,m_sLangFN);
	CCMPFormView *pView = (CCMPFormView *)getFormViewByID(IDD_REPORTVIEW2); 
	if (pView)
	{
		pView->setFilterText(m_wndEdit3.getText(),5);
	}
	
}
*/

/*TODO: 
// Added 2009-05-12 P�D
// Delete properties (munltierase): 090512 p�d
void CProjectFormView::OnBnClickedButton65()
{
	CRemovePropertiesSelDlg *pDlg = new CRemovePropertiesSelDlg();
	if (pDlg != NULL)
	{
		pDlg->setDBConnection(m_pDB);
		if (pDlg->DoModal() == IDOK)
		{
		}	// if (pDlg->DoModal() == IDOK)

		delete pDlg;
	}	// if (pDlg != NULL)
	
}
*/
// Added 2009-08-12 P�D
/*TODO: 
void CProjectFormView::OnChangeObjID()
{
	CChangeObjIDDlg *pDlg = new CChangeObjIDDlg();
	if (pDlg != NULL)
	{
		pDlg->setDBConnection(m_pDB);
		if (pDlg->DoModal() == IDOK)
		{
			getProjects();
			populateData(m_nDBIndex);
		}
		delete pDlg;
	}
}
*/

BOOL CProjectFormView::checkFocus(CWnd *pWnd)
{
	CWnd *pWndFocus = GetFocus();

	return (pWnd->GetSafeHwnd() == pWndFocus->GetSafeHwnd());
}


void CProjectFormView::setSearchToolbarBtn(BOOL enable)
{
	CProjectTabView *pView = (CProjectTabView*)getFormViewByID(IDD_FORMVIEW13);
	if (pView != NULL)
	{
		CMDIProjectFrame *pPropFrame = (CMDIProjectFrame *)pView->GetParent();
		if (pPropFrame)
			pPropFrame->setEnableToolbar(TRUE,
			enable && m_enumFocusedEdit != ED_NONE  && m_enumFocusedEdit != ED_AREAL1 && m_enumFocusedEdit != ED_AREAL2,
			m_vecProjectData.size() > 0);

	}
}

void CProjectFormView::OnEnSetfocusEdit61()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}

void CProjectFormView::OnEnSetfocusEdit62()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}

void CProjectFormView::OnEnSetfocusEdit617()
{
	m_enumFocusedEdit = ED_PROPNUM;	/*TODO: change for project*/
	if (m_vecProjectData.size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CProjectFormView::OnEnSetfocusEdit64()
{
	m_enumFocusedEdit = ED_PROPNAME;	/*TODO: change for project*/
	if (m_vecProjectData.size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}
