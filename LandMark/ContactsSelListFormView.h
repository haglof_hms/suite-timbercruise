#pragma once

#include "Resource.h"

#include "LandMarkDB.h"

/////////////////////////////////////////////////////////////////////////////////
// CContactsReportDataRec

class CContactsReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};

public:
	CContactsReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CCheckItem(FALSE));

		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CContactsReportDataRec(UINT index,CTransaction_contacts data,CString type_of) //CStringArray& type_of_company)	 
	{
		m_nIndex = index;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem((data.getPNR_ORGNR())));
		AddItem(new CTextItem(type_of));
		AddItem(new CTextItem((data.getCompany())));
		AddItem(new CTextItem((data.getName())));
		AddItem(new CTextItem(cleanCRLF(data.getAddress(),L", ")));
		AddItem(new CTextItem((data.getPostNum())));
		AddItem(new CTextItem((data.getPostAddress())));
		AddItem(new CTextItem((data.getCounty())));
		AddItem(new CTextItem((data.getCountry())));
		AddItem(new CTextItem((data.getPhoneWork())));
		AddItem(new CTextItem((data.getPhoneHome())));
		AddItem(new CTextItem((data.getFaxNumber())));
		AddItem(new CTextItem((data.getMobile())));
		AddItem(new CTextItem((data.getEMail())));
		AddItem(new CTextItem((data.getWebSite())));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	BOOL getColumnCheck(int item)
	{
		return ((CCheckItem*)GetItem(item))->getChecked();
	}

	void setColumnCheck(int item,BOOL bChecked)
	{
		((CCheckItem*)GetItem(item))->setChecked(bChecked);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};





///////////////////////////////////////////////////////////////////////////////////////////
// CContactsSelectListFrame frame
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CContactsSelectListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CContactsSelectListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:
	CString m_sToolTipFilter;
	CString m_sToolTipFilterOff;
	CString m_sToolTipPrintOut;
	CString m_sToolTipSearch;
	CString m_sToolTipAdd;

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	BOOL m_bEnableTBBTNFilterOff;
	CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CContactsSelectListFrame();           // protected constructor used by dynamic creation
	virtual ~CContactsSelectListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	CDialogBar m_wndFieldChooser;   // Sample Field chooser window
	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

	void setFilterWndCap(LPCTSTR cap)
	{
		m_wndFilterEdit.SetWindowText(cap);
	}
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CContactsSelectListFrame; 070108 p�d

class CContactsReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CContactsReportFilterEditControl)
public:
	CContactsReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//////////////////////////////////////////////////////////////////////////////
// CContactsSelListFormView form view

class CContactsSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CContactsSelListFormView)

protected:
	CContactsSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CContactsSelListFormView();

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	int m_nSelectedColumn;

	//CMyReportCtrl m_wndContactsReport;
	vecTransactionContacts m_vecContactsData;
	BOOL setupReport(void);
	void getContacts(void);
	
	CXTPReportSubListControl m_wndSubList;
	CContactsReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	void LoadReportState(void);
	void SaveReportState(void);

	int m_nDBIndex;

	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

	int m_nDoAddPropertyOwners;

	CStringArray m_sarrTypeOfContact;
public:
	// Need to set this method public; 070126 p�d
	void populateReport(void);
	void refresh(void);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

	void setFilterWindow(void);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldChooser();
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnPrintPreview();
	afx_msg void OnRefresh();
	afx_msg void OnAddContactToProperty();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnUpdateDBToolbarBtnAdd(CCmdUI* pCmdUI);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
