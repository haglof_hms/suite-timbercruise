// stdafx.cpp : source file that includes just the standard includes
// LandMark.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "TCruisePathDlg.h"
#include "PropertyTabView.h"

BOOL License()
{
	TCHAR szBuf[MAX_PATH], szFilename[MAX_PATH];

	GetModuleFileName(hInst, szBuf, sizeof(szBuf) / sizeof(TCHAR));

	_tsplitpath(szBuf, NULL, NULL, szFilename, NULL);

	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), TIMBERCRUISE_LIC_ID, TIMBERCRUISE_LIC_HEADER, bShowLicenseMsgBox == TRUE? _T("1") : _T("0"), &szFilename);

	LRESULT nRet = AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);

	if(_tcscmp(msg.getFunc(), _T("CheckLicense")) == 0)	//license.dll not loaded or no licens file
	{
		CString csFN;
		csFN.Format(_T("%s%s"),getSuitesDir(),_T("License.dll"));
		if(!fileExists(csFN))	//check if license.dll exists
		{
			CString csLangFN;
			csLangFN.Format(_T("%s%s%s%s"),getLanguageDir(), SHELLPROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
			if(fileExists(csLangFN))
			{
				RLFReader *xml = new RLFReader;
				if(xml->Load(csLangFN))
				{
					DWORD id = 14011;
					AfxMessageBox(xml->str(id));
				}
				delete xml;
			}
		}
		return FALSE;
	}
	else if(_tcscmp(msg.getFunc(), _T("0")) == 0)	//error
	{
		return FALSE;
	}
	else if(_tcscmp(msg.getSuite(), _T("1")) != 0)	//no license
	{
		if(_tcscmp(msg.getArgStr(), _T("1")) != 0)	//no demo
			return FALSE;
	}

	bShowLicenseMsgBox = FALSE;
	return TRUE;

}


BOOL hitTest_X(int hit_x,int limit_x,int w_x)
{
	BOOL bHitX_OK = FALSE;
	// Check if hit, in x, is within lim�ts; 080513 p�d
	// if w_x = 0, don't check this value; 080513 p�d
	if (w_x == 0) bHitX_OK = TRUE;
	else if (w_x > 0)
	{
		bHitX_OK = (hit_x >= limit_x && hit_x <= limit_x+w_x);
	}

	return bHitX_OK;
}

CString getTCruiseDir()
{
	HKEY hk = NULL;
	TCHAR strValue[BUFSIZ];	//127];
	TCHAR szRoot[BUFSIZ];	//127];
	DWORD dwLength = BUFSIZ;	// Size of the value

	// Check the hKey node and look for 
	// the RemDlgPos node under it
	// If it exists, open it
	_stprintf_s(szRoot,_T("%s"),REG_TCRUISE_PATH_KEY);
	if(RegOpenKeyEx(HKEY_CURRENT_USER,
		szRoot,
		0,
		KEY_QUERY_VALUE,
		&hk) != ERROR_SUCCESS)
	{
		return _T("");
	}
	// Retrieve the value of the Left key
	RegQueryValueEx(hk,
		REG_TCRUISE_INSTALLDIR_ITEM,
		NULL,
		NULL,//&dwType,
		(LPBYTE)strValue,
		&dwLength);

	// We have finished reading the registry,
	// so free the resources we were using
	RegCloseKey(hk);

	return strValue;
}


CString getTCruiseDirFromUser()
{
	HKEY hk;
	TCHAR strValue[BUFSIZ];
	TCHAR szRoot[BUFSIZ];
	DWORD dwLength = BUFSIZ, dwDisp;
	CString csPath;

	CTCruisePathDlg *dlg = new CTCruisePathDlg();

	//get file path to TCruise.exe from user
	if(dlg->DoModal() == IDOK)
	{
		csPath = dlg->getPath();

		if(fileExists(csPath))
		{
			//Remove \\TCruise.exe if included in path
			if(csPath.Find(_T("\\TCruise.exe")))
				csPath.Replace(_T("\\TCruise.exe"),_T(""));
			dwLength = _stprintf_s(strValue, _T("%s"), csPath);
			dwLength *= sizeof(TCHAR);

			if(dwLength > 0)
			{
				//save path in registry
				_stprintf_s(szRoot,_T("%s"),REG_TCRUISE_PATH_KEY);
				if(RegCreateKeyEx(HKEY_CURRENT_USER,
					szRoot,
					0,
					NULL,
					REG_OPTION_NON_VOLATILE,
					KEY_ALL_ACCESS,
					NULL,
					&hk,
					&dwDisp) == ERROR_SUCCESS)
				{
					RegSetValueEx(hk,
						REG_TCRUISE_INSTALLDIR_ITEM,
						NULL,
						REG_SZ,
						(LPBYTE)strValue,
						dwLength);	//dwLength = sizeof strValue

					RegCloseKey(hk);
				}
			}

			delete dlg;
			return csPath;
		}
	}

	delete dlg;
	return _T("");
}


CString getToolBarResFN()
{
	CString csPath;
	CString csToolBarFN;

	::GetModuleFileName(AfxGetApp()->m_hInstance, csPath.GetBufferSetLength(_MAX_PATH),_MAX_PATH);
	csPath.ReleaseBuffer();

	int nIndex = csPath.ReverseFind(_T('\\'));
	if(nIndex > 0)
		csPath = csPath.Left(nIndex + 1);
	else
		csPath.Empty();

	csToolBarFN.Format(_T("%s%s"),csPath, TOOLBAR_RES_DLL);

	return csToolBarFN;
}

CString getMyPathToUserAppData(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	szPath[0] = '\0';
	if(SUCCEEDED(SHGetFolderPath(NULL, 
		CSIDL_APPDATA|CSIDL_FLAG_CREATE, 
		NULL, 
		0, 
		szPath))) 
	{
		sPath.Format(_T("%s"),szPath);
	}
	return sPath;
}

CString getMyPathToCommonAppData(void)
{
	CString sPath;
	TCHAR szPath[MAX_PATH];
	szPath[0] = '\0';
	if(SUCCEEDED(SHGetFolderPath(NULL, 
		CSIDL_COMMON_APPDATA|CSIDL_FLAG_CREATE, 
		NULL, 
		0, 
		szPath))) 
	{
		sPath.Format(_T("%s"),szPath);
	}
	return sPath;
}



CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{

			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(WM_USER_MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->SendMessage(WM_USER_MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}

CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{	
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

CWnd *getFormViewParentByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{

			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView->GetParent();
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
		COPYDATASTRUCT HSData;
		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

//CReportMultilinePaintManager
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CReportMultilinePaintManager::CReportMultilinePaintManager()
{
	m_bFixedRowHeight = FALSE;
}

CReportMultilinePaintManager::~CReportMultilinePaintManager()
{

}

int CReportMultilinePaintManager::GetRowHeight(CDC* pDC, CXTPReportRow* pRow, int nTotalWidth)
{
	if (pRow->IsGroupRow() || !pRow->IsItemsVisible())
		return CXTPReportPaintManager::GetRowHeight(pDC, pRow, nTotalWidth);

	CXTPReportColumns* pColumns = pRow->GetControl()->GetColumns();
	int nColumnCount = pColumns->GetCount();

	XTP_REPORTRECORDITEM_DRAWARGS drawArgs;
	drawArgs.pControl = pRow->GetControl();
	drawArgs.pDC = pDC;
	drawArgs.pRow = pRow;

	int nHeight = 0;

	for (int nColumn = 0; nColumn < nColumnCount; nColumn++)
	{
		CXTPReportColumn* pColumn = pColumns->GetAt(nColumn);
		if (pColumn && pColumn->IsVisible())
		{
			CXTPReportRecordItem* pItem = pRow->GetRecord()->GetItem(pColumn);
			drawArgs.pItem = pItem;

			XTP_REPORTRECORDITEM_METRICS itemMetrics;
			pRow->GetItemMetrics(&drawArgs, &itemMetrics);

			CXTPFontDC fnt(pDC, itemMetrics.pFont);

			int nWidth = pDC->IsPrinting()? pColumn->GetPrintWidth(nTotalWidth): pColumn->GetWidth();

			CRect rcItem(0, 0, nWidth - 4, 0);
			pRow->ShiftTreeIndent(rcItem, pColumn);

			pItem->GetCaptionRect(&drawArgs, rcItem);
			pDC->DrawText(pItem->GetCaption(pColumn), rcItem, DT_WORDBREAK|DT_CALCRECT);

			nHeight = max(nHeight, rcItem.Height());
		}
	}


	return max(nHeight + 5, m_nRowHeight) + (IsGridVisible(FALSE)? 1: 0);
}

void CReportMultilinePaintManager::DrawItemCaption(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pMetrics)
{
	CRect& rcItem = pDrawArgs->rcItem;
	CDC* pDC = pDrawArgs->pDC;
	CString strText = pMetrics->strText;

	// draw item text
	if(!strText.IsEmpty())
	{
		rcItem.DeflateRect(2, 1, 2, 0);
		pDC->DrawText(strText, rcItem, pDrawArgs->nTextAlign|DT_WORDBREAK);
	}
}



//CTransaction_TCStand
CTransaction_TCStand::CTransaction_TCStand(void)
{
	m_nFileIndex = -1;
	m_sTractID = _T("");
	m_fTractAcres = 0.0;
	m_sCruiseMethod = _T("");
	m_sCruiseDate = _T("");
	m_sTractLoc = _T("");
	m_sTractOwner = _T("");
	m_sCruiser = _T("");
	m_nPropID = -1;
}

CTransaction_TCStand::CTransaction_TCStand(int fileindex, LPCTSTR tractid, double tractacres, LPCTSTR cruisemethod, LPCTSTR cruisedate, LPCTSTR tractloc, LPCTSTR tractowner, LPCTSTR cruiser, int propid)
{
	m_nFileIndex = fileindex;
	m_sTractID = tractid;
	m_fTractAcres = tractacres;
	m_sCruiseMethod = cruisemethod;
	m_sCruiseDate = cruisedate;
	m_sTractLoc = tractloc;
	m_sTractOwner = tractowner;
	m_sCruiser = cruiser;
	m_nPropID = propid;
}

CTransaction_TCStand::CTransaction_TCStand(const CTransaction_TCStand &s)
{
	*this = s;
}

//CTransaction_TCFiles
CTransaction_TCFiles::CTransaction_TCFiles(void)
{
	m_sFilePath = _T("");
	m_sFileName = _T("");
	m_sFileTitle = _T("");
	m_sFileComments = _T("");
}

CTransaction_TCFiles::CTransaction_TCFiles(LPCTSTR path, LPCTSTR name, LPCTSTR title, LPCTSTR comm)
{
	m_sFilePath = path;
	m_sFileName = name;
	m_sFileTitle = title;
	m_sFileComments = comm;
}

CTransaction_TCFiles::CTransaction_TCFiles(const CTransaction_TCFiles &f)
{
	*this = f;
}


//CTransaction_TCReports
CTransaction_TCReports::CTransaction_TCReports()
{
	m_sFileName = _T("");
	m_sFilePath = _T("");
	m_sTitle = _T("");
	m_sSubject = _T("");
	m_sAuthor = _T("");
	m_sKeywords = _T("");
	m_sComment = _T("");
	m_sCreated = _T("");
}

CTransaction_TCReports::CTransaction_TCReports(LPCTSTR filename, LPCTSTR filepath, LPCTSTR title, LPCTSTR subject, LPCTSTR author, LPCTSTR keywords, LPCTSTR comment, LPCTSTR created)
{
	m_sFileName = filename;
	m_sFilePath = filepath;
	m_sTitle = title;
	m_sSubject = subject;
	m_sAuthor = author;
	m_sKeywords = keywords;	
	m_sComment = comment;
	m_sCreated = created;
}

CTransaction_TCReports::CTransaction_TCReports(const CTransaction_TCReports &r)
{
	*this = r;
}

//CTransaction_TCGroups
CTransaction_TCGroups::CTransaction_TCGroups(void)
{
	m_nID = 0;
	m_nPrintOrder = 0;
	m_csClassName = _T("");
	m_nClassIndex = 0;
	m_csClassName = _T("");
}

CTransaction_TCGroups::CTransaction_TCGroups(int id, int order, LPCTSTR groupname, int classindex, LPCTSTR classname)
{
	m_nID = id;
	m_nPrintOrder = order;
	m_csGroupName = groupname;
	m_nClassIndex = classindex;
	m_csClassName = classname;
}

CTransaction_TCGroups::CTransaction_TCGroups(const CTransaction_TCGroups &g)
{
	*this = g;
}

//CTransaction_TCClass
CTransaction_TCClass::CTransaction_TCClass(void)
{
	m_nIndex = 0;
	m_csClassName = _T("");
	m_nPrintOrder = 0;	//new 091009
}

CTransaction_TCClass::CTransaction_TCClass(int index, int printorder, LPCTSTR classname)
{
	m_nIndex = index;
	m_csClassName = classname;
	m_nPrintOrder = printorder;
}

CTransaction_TCClass::CTransaction_TCClass(const CTransaction_TCClass &c)
{
	*this = c;
}

//CTransaction_TCSnapshot
CTransaction_TCSnapshot::CTransaction_TCSnapshot(char *buf, int len, CString name, int id):
m_nLen(len),
m_strName(name),
m_nId(id)
{
	if( len>=0 )
	{
		m_pBuffer = new char[len];
		memcpy(m_pBuffer, buf, len);
	}
}

CTransaction_TCSnapshot::CTransaction_TCSnapshot(const CTransaction_TCSnapshot &src)
{
	m_nLen = src.m_nLen;
	m_strName = src.m_strName;
	m_nId = src.m_nId;

	if(src.m_nLen>=0)
	{
		m_pBuffer = new char[src.m_nLen];
		memcpy(m_pBuffer, src.m_pBuffer, src.m_nLen);
	}
}

CTransaction_TCSnapshot::~CTransaction_TCSnapshot()
{
	if( m_pBuffer )
	{
		delete m_pBuffer;
		m_pBuffer = NULL;
	}
}

char* CTransaction_TCSnapshot::getBuffer() const
{
	return m_pBuffer;
}

int CTransaction_TCSnapshot::getBufferLength() const
{
	return m_nLen;
}

CString CTransaction_TCSnapshot::getName() const
{
	return m_strName;
}

int CTransaction_TCSnapshot::getId() const
{
	return m_nId;
}

//CTransaction_TCSnapshotHeader
CTransaction_TCSnapshotHeader::CTransaction_TCSnapshotHeader(CString name, int id):
m_strName(name),
m_nId(id)
{}

CTransaction_TCSnapshotHeader::~CTransaction_TCSnapshotHeader()
{}

CString CTransaction_TCSnapshotHeader::getName() const
{
	return m_strName;
}

int CTransaction_TCSnapshotHeader::getId() const
{
	return m_nId;
}

//CTransaction_TCCompanyInfo
CTransaction_TCCompanyInfo::CTransaction_TCCompanyInfo()
{
	m_nId = 0;
	m_sCompany = _T("");
	m_sAddress = _T("");
	m_sAddress2 = _T("");
	m_sCity = _T("");
	m_sZipCode = _T("");
	m_sState = _T("");
	m_sCountry = _T("");
	m_sPhone = _T("");
	m_sFax = _T("");
	m_sEmail = _T("");
	m_sWebsite = _T("");

}

CTransaction_TCCompanyInfo::CTransaction_TCCompanyInfo(int id, LPCTSTR company, LPCTSTR adr, LPCTSTR adr2, LPCTSTR city, LPCTSTR zip, LPCTSTR state, LPCTSTR country, LPCTSTR phone, LPCTSTR fax, LPCTSTR email, LPCTSTR web)
{
	m_nId = id;
	m_sCompany = company;
	m_sAddress = adr;
	m_sAddress2 = adr2;
	m_sCity = city;
	m_sZipCode = zip;
	m_sState = state;
	m_sCountry = country;
	m_sPhone = phone;
	m_sFax = fax;
	m_sEmail = email;
	m_sWebsite = web;
}

CTransaction_TCCompanyInfo::CTransaction_TCCompanyInfo(const CTransaction_TCCompanyInfo &i)
{
	*this = i;
}

void CTransaction_TCCompanyInfo::clear()
{
	m_nId = 0;
	m_sCompany = _T("");
	m_sAddress = _T("");
	m_sAddress2 = _T("");
	m_sCity = _T("");
	m_sZipCode = _T("");
	m_sState = _T("");
	m_sCountry = _T("");
	m_sPhone = _T("");
	m_sFax = _T("");
	m_sEmail = _T("");
	m_sWebsite = _T("");
}


//database functions

BOOL createTable(CString db_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;
	BOOL bUse2000 = FALSE;

	CString tablename, tablescript;

	try
	{		
		GetAuthentication(&nAuthentication);

		if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		{
			if (!db_name.IsEmpty())
				_tcscpy_s(sDBName,127,db_name);

			if (nAuthentication == 0)	// Windows login
			{
				bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
					_tcscmp(sDBName,_T("")) != 0);
			}
			else if (nAuthentication == 1)	// Server login
			{
				bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
					_tcscmp(sDBName,_T("")) != 0 &&
					_tcscmp(sUserName,_T("")) != 0 &&
					_tcscmp(sPSW,_T("")) != 0);
			}
			else
				bIsOK = FALSE;

			if (bIsOK)
			{
				CMySQLServer_ADODirect *pDB = new CMySQLServer_ADODirect();
				if (pDB)
				{
					if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
					{
						// Set database as set in sDBName; 061109 p�d
						// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
						if (pDB->existsDatabase(sDBName))
						{
							bUse2000 = pDB->getServerVersion();

							for(int i=0; i<CREATE_NUM_OF_DB_TABLES; i++)
							{
								if(bUse2000 == TRUE)
								{
									tablename = createDBTables_2000[i].tablename;
									tablescript = createDBTables_2000[i].tablescript;
								}
								else
								{
									tablename = createDBTables_2005[i].tablename;
									tablescript = createDBTables_2005[i].tablescript;
								}
								if (!pDB->existsTableInDB(sDBName,tablename))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(tablescript);

									if(tablename == TBL_GROUP_ORDER_HDR)
										pDB->setDefaultGroupOrder();
									else if(tablename == TBL_COMPANY_INFO)
										pDB->setDefaultCompanyInfo();
								}
							}	// if (pDB->existsDatabase(sDBName))
							bReturn = TRUE;
						}
					}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
					delete pDB;
				}	// if (pDB)
			}	// if (_tcscmp(sLocation,_T("")) != 0 &&
		}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}

	return bReturn;

}


void CMySQLServer_ADODirect::setDefaultGroupOrder()
{
	CString csSQL;
	//updated 091009 with print order
	//Default values = Not Assigned, Hardwood, Softwood, Pine
	csSQL.Format(_T("insert into %s values (0, '',0);\r\n")
		_T("insert into tc_GroupOrderHdr values (1, 'Hardwood',1);\r\n")
		_T("insert into tc_GroupOrderHdr values (2, 'Softwood',2);\r\n")
		_T("insert into tc_GroupOrderHdr values (3, 'Pine',3);"),TBL_GROUP_ORDER_HDR);
	commandExecute(csSQL);

	/*
	BOOL bOk = TRUE;
	CString csLangFN, csSQL;
	csLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	if(fileExists(csLangFN))
	{
	RLFReader* xml = new RLFReader;
	if(xml->Load(csLangFN))
	{
	csSQL.Format(_T("insert into tc_GroupOrderHdr values (0, '');\r\n")
	_T("insert into tc_GroupOrderHdr values (1, '%s');\r\n")
	_T("insert into tc_GroupOrderHdr values (2, '%s');\r\n")
	_T("insert into tc_GroupOrderHdr values (3, '%s');"),
	xml->str(IDS_STRING177),xml->str(IDS_STRING178),xml->str(IDS_STRING179));
	commandExecute(csSQL);

	}
	else
	bOk = FALSE;

	delete xml;
	}
	else
	bOk = FALSE;

	if(bOk == FALSE)
	{
	csSQL.Format(_T("insert into %s values (0, '');\r\n")
	_T("insert into tc_GroupOrderHdr values (1, 'Hardwood');\r\n")
	_T("insert into tc_GroupOrderHdr values (2, 'Softwood');\r\n")
	_T("insert into tc_GroupOrderHdr values (3, 'Pine');"),TBL_GROUP_ORDER_HDR);
	commandExecute(csSQL);
	}
	*/
}

void CMySQLServer_ADODirect::setDefaultCompanyInfo()
{
	CString csSQL;

	csSQL.Format(_T("insert into %s values (0,'','','','','','','','','','','',-1,NULL);"), TBL_COMPANY_INFO);	//-1 to indicate that no file is loaded
	commandExecute(csSQL);

}




BOOL CMySQLServer_ADODirect::getServerVersion()
{
	CString csSQL = _T(""), csVersion = _T("");
	BOOL bRet = FALSE;

	csSQL.Format(_T("SELECT @@VERSION AS VER;"));

	if (recordsetQuery(csSQL))
	{
		csVersion = getStr(_T("VER"));

		csVersion.Remove(' ');

		if(csVersion.Find(_T("MicrosoftSQLServer2000")) >= 0)
			bRet = TRUE;	//e.g. Oden is SQL Server 2000
		else
			bRet = FALSE;

		recordsetClose();
	}

	return bRet;
}

BOOL alterTable(CString db_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;
	BOOL bUse2000 = FALSE;

	CString tablename, tablescript, csSQL, columnname;

	try
	{		
		GetAuthentication(&nAuthentication);

		if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		{
			if (!db_name.IsEmpty())
				_tcscpy_s(sDBName,127,db_name);

			if (nAuthentication == 0)	// Windows login
			{
				bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
					_tcscmp(sDBName,_T("")) != 0);
			}
			else if (nAuthentication == 1)	// Server login
			{
				bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
					_tcscmp(sDBName,_T("")) != 0 &&
					_tcscmp(sUserName,_T("")) != 0 &&
					_tcscmp(sPSW,_T("")) != 0);
			}
			else
				bIsOK = FALSE;

			if (bIsOK)
			{
				CMySQLServer_ADODirect *pDB = new CMySQLServer_ADODirect();
				if (pDB)
				{
					if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
					{
						// Set database as set in sDBName; 061109 p�d
						// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
						if (pDB->existsDatabase(sDBName))
						{
							//090528 check for tc_AuditRemeasData table, create if not existing
							tablename = TBL_AUDITREMEASDATA;
							tablescript = CREATE_TBL_AUDITREMEASDATA;

							if (!pDB->existsTableInDB(sDBName,tablename))
							{
								pDB->setDefaultDatabase(sDBName);
								pDB->commandExecute(tablescript);

							}
							//091009 check for column PrintOrder in tc_GroupOrderHdr, add if not exist
							tablename = TBL_GROUP_ORDER_HDR;
							tablescript = ALTER_TBL_GROUPORDERHDR;
							if (pDB->existsTableInDB(sDBName,tablename))
							{
								pDB->setDefaultDatabase(sDBName);
								pDB->commandExecute(tablescript);
								csSQL.Format(_T("UPDATE %s SET PrintOrder = ClassIndex WHERE PrintOrder IS NULL"), tablename);
								pDB->commandExecute(csSQL);
							}
							
							//20130111 Add columns for cruise method and number of plots
							//CrsAggM = 0 for regular point/plot to point/plt samples
							//		  = 1 for cumulative plot samples
							//        = 2 for cumulative point samples
							// CrsPlots = number of plots visited for all types of cruises.
							//#3532
							tablename = TBL_STAND;
							tablescript = ALTER_TBL_CRSAGGM;
							columnname = CRSAGGM_COLUMN_NAME;
							
							if (!pDB->existsColumnInTableInDB(sDBName,tablename,columnname))
							{
								pDB->setDefaultDatabase(sDBName);
								pDB->commandExecute(tablescript);
							}
							tablename = TBL_STAND;
							tablescript = ALTER_TBL_CRSPLOTS;
							columnname = CRSPLOTS_COLUMN_NAME;
							if (!pDB->existsColumnInTableInDB(sDBName,tablename,columnname))
							{
								pDB->setDefaultDatabase(sDBName);
								pDB->commandExecute(tablescript);
							}

							//#3750
							tablename = TBL_TREE;
							tablescript = ALTER_TBL_TREE;
							columnname = SGL8_COLUMN_NAME;
							if (!pDB->existsColumnInTableInDB(sDBName,tablename,columnname))
							{
								pDB->setDefaultDatabase(sDBName);
								pDB->commandExecute(tablescript);
							}

							//TODO: check for contacts and property tables
#ifdef USE_TC_CONTACT
							tablename = TBL_STAND;
							tablescript = ALTER_TBL_STAND;
							if (pDB->existsTableInDB(sDBName,tablename))
							{
								pDB->setDefaultDatabase(sDBName);
								pDB->commandExecute(tablescript);
							}

							tablename = TBL_CONTACTS;
							tablescript = ALTER_TBL_CONTACTS;
							if (pDB->existsTableInDB(sDBName,tablename))
							{
								pDB->setDefaultDatabase(sDBName);
								pDB->commandExecute(tablescript);
							}

#endif
							// #4213
							tablename = TBL_STAND;
							tablescript = ALTER_TBL_STAND_4213_1;
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(tablescript);

							tablescript = ALTER_TBL_STAND_4213_2;
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(tablescript);

							tablescript = ALTER_TBL_STAND_4213_3;
							pDB->setDefaultDatabase(sDBName);
							pDB->commandExecute(tablescript);

						}	// if (pDB->existsDatabase(sDBName))
						bReturn = TRUE;
					}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
					delete pDB;
				}	// if (pDB)
			}	// if (_tcscmp(sLocation,_T("")) != 0 &&
		}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}

	return bReturn;

}


void DoRegistryChecks()
{

	//* TODO: g�r �ven detta f�r installationsbibliotek f�r rapporterna*/


	//check that 
	HKEY hKeyC = NULL,hKeyL = NULL;
	TCHAR szDir[BUFSIZ];
	DWORD dwDirSize = BUFSIZ, dwDisp;
	BOOL bMustCopy, bSetValue;

	//install directory for TCruise.exe
	bMustCopy = FALSE;
	if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_TCRUISE_PATH_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyC) == ERROR_SUCCESS)
	{

		if(RegQueryValueEx(hKeyC, REG_TCRUISE_INSTALLDIR_ITEM, NULL, NULL, (LPBYTE)szDir, &dwDirSize) != ERROR_SUCCESS)
		{
			//copy key
			bMustCopy = TRUE;
		}
		else if(szDir[0] == '\0')
		{
			//copy key
			bMustCopy = TRUE;
		}

	}
	else
	{
		//create key
		RegCreateKeyEx(HKEY_CURRENT_USER,REG_TCRUISE_PATH_KEY,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKeyC,&dwDisp);
		bMustCopy = TRUE;
	}

	if(bMustCopy)
	{
		if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_TCRUISE_PATH_KEY, NULL, KEY_QUERY_VALUE, &hKeyL) == ERROR_SUCCESS)
		{
			dwDirSize = BUFSIZ;
			if(RegQueryValueEx(hKeyL, REG_TCRUISE_INSTALLDIR_ITEM, NULL, NULL, (LPBYTE)szDir, &dwDirSize) == ERROR_SUCCESS)
			{
				RegSetValueEx(hKeyC,	REG_TCRUISE_INSTALLDIR_ITEM, NULL, REG_SZ, (LPBYTE)szDir, dwDirSize);
			}
		}
	}

	if(hKeyC != NULL)
		RegCloseKey(hKeyC);
	if(hKeyL != NULL)
		RegCloseKey(hKeyL);

	//koll f�r Crystal Report maxRow nyckel, s�tt v�rde om den inte finns
	//HKEY_CURRENT_USER\Software\Business Objects\Suite 12.0\Crystal Reports\DatabaseOptions\LOV\MaxRowsetRecords
	//REG_CRYSTAL_LOV_PATH_KEY
	//REG_CRYSTAL_MAXROW_ITEM

	hKeyC = NULL;
	hKeyL = NULL;
	bMustCopy = FALSE;
	dwDirSize = BUFSIZ;
	if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_CRYSTAL_LOV_PATH_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyC) == ERROR_SUCCESS)
	{
		if(RegQueryValueEx(hKeyC, REG_CRYSTAL_MAXROW_ITEM, NULL, NULL, (LPBYTE)szDir, &dwDirSize) != ERROR_SUCCESS)
		{
			//copy key
			bMustCopy = TRUE;
		}
		else if(szDir[0] == '\0')	
		{
			//copy key
			bMustCopy = TRUE;
		}
	}
	else
	{
		//create key
		RegCreateKeyEx(HKEY_CURRENT_USER, REG_CRYSTAL_LOV_PATH_KEY,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKeyC,&dwDisp);
		bMustCopy = TRUE;
	}

	if(bMustCopy)
	{
		bSetValue = FALSE;
		if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_CRYSTAL_LOV_PATH_KEY, NULL, KEY_QUERY_VALUE, &hKeyL) == ERROR_SUCCESS)
		{
			dwDirSize = BUFSIZ;
			if(RegQueryValueEx(hKeyL, REG_CRYSTAL_MAXROW_ITEM, NULL, NULL, (LPBYTE)szDir, &dwDirSize) == ERROR_SUCCESS)
			{
				if(RegSetValueEx(hKeyC,REG_CRYSTAL_MAXROW_ITEM, NULL,REG_SZ, (LPBYTE)szDir, dwDirSize) != ERROR_SUCCESS)
					bSetValue = TRUE;
			}
			else
			{
				bSetValue = TRUE;
			}
		}
		else
		{
			bSetValue = TRUE;
		}

		if(bSetValue)
		{
			dwDirSize = _stprintf_s(szDir, _T("%d"), 5000000);
			dwDirSize *= sizeof(TCHAR);
			if(dwDirSize > 0)
				RegSetValueEx(hKeyC,REG_CRYSTAL_MAXROW_ITEM, NULL,REG_SZ, (LPBYTE)szDir, dwDirSize);
		}
	}

	if(hKeyC != NULL)
		RegCloseKey(hKeyC);
	if(hKeyL != NULL)
		RegCloseKey(hKeyL);

	//create and check settings\ReportHeaderID key, init to -1 if no value
	//open or create key
	if(RegCreateKeyEx(HKEY_CURRENT_USER, REG_HMSTC_SETTINGS_KEY,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKeyC,&dwDisp) == ERROR_SUCCESS)
	{
		//init item if not exists
		if(RegQueryValueEx(hKeyC, REG_REPORT_HEADER_ITEM, NULL, NULL, (LPBYTE)szDir, &dwDirSize) != ERROR_SUCCESS)
		{
			dwDirSize = _stprintf_s(szDir, _T("-1"));
			dwDirSize *= sizeof(TCHAR);
			if(dwDirSize > 0)
				RegSetValueEx(hKeyC,REG_REPORT_HEADER_ITEM, NULL,REG_SZ, (LPBYTE)szDir, dwDirSize);
		}
	}


	if(hKeyC != NULL)
		RegCloseKey(hKeyC);


	//create and check keys for TCruise
	//only need to do this, RegSetValueEx in PathSettinsFormView takes care of key items
	RegCreateKeyEx(HKEY_CURRENT_USER, REG_TCRUISE_OPTIONS_PATH_KEY,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKeyC,&dwDisp);
	if(hKeyC != NULL)
		RegCloseKey(hKeyC);

}


void setToolbarBtn(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show)
{
	if (pCtrl)
	{
		if (show)
		{
			pCtrl->SetTooltip(tool_tip);
			HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), rsource_dll_fn, icon_id);
			if (hIcon) pCtrl->SetCustomIcon(hIcon);
		}
		else
			pCtrl->SetVisible(FALSE);
	}
}


void enableTabPage(int idx,BOOL enable)
{
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pTabView != NULL)
	{
		pTabView->enablePage(idx,enable);
	}	// if (pTabView != NULL)
}

void activeTabPage(int idx)
{
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pTabView != NULL)
	{
		pTabView->activePage(idx);
	}	// if (pTabView != NULL)
}

BOOL checkExportDll()
{
	HKEY hKeyC = NULL,hKeyL = NULL;
	TCHAR szDir[BUFSIZ];
	DWORD dwDirSize = BUFSIZ, dwDisp;
	TCHAR szDirTC[BUFSIZ];
	DWORD dwDirSizeTC = BUFSIZ;
	BOOL bMustCopy=FALSE;
	BOOL bRet = TRUE;

	CString csLangFN;
	CString strCheckbox = _T("");
		CString strMsg = _T("");
	csLangFN.Format(_T("%s%s%s%s"),getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
	if(fileExists(csLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(csLangFN))
		{
			strMsg.Format(_T("%s\r\n%s\r\n\r\n%s\r\n\r\n%s"),xml->str(IDS_STRING355),xml->str(IDS_STRING356),xml->str(IDS_STRING357),xml->str(IDS_STRING341));
			strCheckbox.Format(_T("%s"),xml->str(IDS_STRING358));
		}
		delete xml;
	}

	//get value from Local Machine, value is set by installation
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_TCRUISE_OPTIONS_PATH_KEY, NULL, KEY_QUERY_VALUE, &hKeyL) != ERROR_SUCCESS)
	{
		return TRUE;
	}

	if(RegQueryValueEx(hKeyL, REG_TCRUISE_EXPORTDLL_ITEM, NULL, NULL, (LPBYTE)szDir, &dwDirSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hKeyL);
		return TRUE;
	}


	//check value in Current User 
	if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_TCRUISE_OPTIONS_PATH_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyC) == ERROR_SUCCESS)
	{
		if(RegQueryValueEx(hKeyC, REG_TCRUISE_EXPORTDLL_ITEM, NULL, NULL, (LPBYTE)szDirTC, &dwDirSizeTC) != ERROR_SUCCESS)
		{
			//copy key
			bMustCopy = TRUE;
		}
		else if(szDir[0] == '\0')
		{
			//copy key
			bMustCopy = TRUE;
		}
		else if(_tcscmp(szDirTC,_T("")) == 0)	
		{
			//copy key
			bMustCopy = TRUE;
		}
	}
	else
	{
		//create key
		RegCreateKeyEx(HKEY_CURRENT_USER,REG_TCRUISE_OPTIONS_PATH_KEY,0,NULL,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hKeyC,&dwDisp);
		bMustCopy = TRUE;
	}

	if(bMustCopy)
	{
		//ask if to change value
		int ans;
		PPMSGBOXPARAMS MsgParam;
	
		MsgParam.dwStyle = 0;
		MsgParam.lpszCheckBoxText	= strCheckbox;
		MsgParam.lpszCompanyName	= REG_HMS_KEY;
		MsgParam.lpszModuleName		= REG_PPMSGBOX_VALUE;
		
		CWinApp *pApp = AfxGetApp();
		if((ans = PPMessageBox(pApp->m_pMainWnd->m_hWnd, strMsg, _T("Timber Cruise"), MB_YESNOCANCEL | MB_CHECKBOX | MB_ICONQUESTION, IDCANCEL, &MsgParam)) == IDYES )
		{
			RegSetValueEx(hKeyC,REG_TCRUISE_EXPORTDLL_ITEM, NULL,REG_SZ, (LPBYTE)szDir, dwDirSize);
		}
		else if(ans == IDCANCEL)
		{
			//need to zero value for messagebox status key; otherwise messagebox function automatically returns IDCANCEL
			HKEY hKeyCuTC = NULL;
			DWORD dwResult = 0;
			if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_PPMSGBOX_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyCuTC) == ERROR_SUCCESS)
			{
				CString sDoNotAskKey;
				//format taken from PPMessageBox.cpp
				sDoNotAskKey.Format(_T("%s%d"), MsgParam.lpszModuleName, MsgParam.nLine);
				RegSetValueEx(hKeyCuTC,sDoNotAskKey, NULL,REG_DWORD, (const BYTE *)&dwResult, sizeof(DWORD));
				RegCloseKey(hKeyCuTC);
			}

			bRet = FALSE;
		}
	}

	//close keys
	if(hKeyC != NULL)
		RegCloseKey(hKeyC);
	if(hKeyL != NULL)
		RegCloseKey(hKeyL);

	return bRet;
}

//////////////////////////////////////////////////////////////////////////////////////////
// CTransaction_ProjectType
CTransaction_ProjectType::CTransaction_ProjectType(void)
{
	m_nID							= -1;
	m_sProjectType				= _T("");
	m_sNotes					= _T("");
	m_sCreated				= _T("");
}

CTransaction_ProjectType::CTransaction_ProjectType(int id,LPCTSTR ProjectType,LPCTSTR notes,LPCTSTR created)
{
	m_nID							= id;
	m_sProjectType				= (ProjectType);
	m_sNotes					= (notes);
	m_sCreated				= (created);
}

CTransaction_ProjectType::CTransaction_ProjectType(const CTransaction_ProjectType &c)
{
	*this = c;
}

int CTransaction_ProjectType::getID(void)									{	return m_nID;}

CString CTransaction_ProjectType::getProjectType(void)				{	return m_sProjectType;}

CString CTransaction_ProjectType::getNotes(void)						{	return m_sNotes;}

CString CTransaction_ProjectType::getCreated(void)					{	return m_sCreated;}


