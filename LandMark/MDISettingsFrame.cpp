// MDISettingsFrame.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "MDISettingsFrame.h"
#include "Resource.h"
#include "HeaderSettingsFormView.h"
#include "PathSettingsFormView.h"


// CMDISettingsFrame

IMPLEMENT_DYNCREATE(CMDISettingsFrame, CChildFrameBase)

CMDISettingsFrame::CMDISettingsFrame()
{
	m_bFirstOpen = FALSE;
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bDBConnOk = TRUE;
}

CMDISettingsFrame::~CMDISettingsFrame()
{
}


BEGIN_MESSAGE_MAP(CMDISettingsFrame, CChildFrameBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
END_MESSAGE_MAP()


// CMDISettingsFrame message handlers

BOOL CMDISettingsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTPFrameWndBase<CMDIChildWnd>::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CMDISettingsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTPFrameWndBase<CMDIChildWnd>::OnCreate(lpCreateStruct) == -1)
		return -1;

	if(m_hIcon)
	{
		SetIcon(m_hIcon, TRUE);
		SetIcon(m_hIcon, FALSE);
	}

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	m_bFirstOpen = TRUE;

	return 0;
}

void CMDISettingsFrame::OnSize(UINT nType, int cx, int cy)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CMDISettingsFrame::OnDestroy()
{
	//save window placement
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_TIMBERCRUISESETTINGS_WP);
	SavePlacement(this, csBuf);

}

void CMDISettingsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SETTINGS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SETTINGS;

	CXTPFrameWndBase<CMDIChildWnd>::OnGetMinMaxInfo(lpMMI);
}

void CMDISettingsFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDISettingsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	//load placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_TIMBERCRUISESETTINGS_WP);
		LoadPlacement(this, csBuf);
	}
}

void CMDISettingsFrame::OnSetFocus(CWnd* pOldWnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CMDISettingsFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

LRESULT CMDISettingsFrame::OnMessageFromShell(WPARAM wParam, LPARAM lParam)
{
	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while(pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE, wParam, lParam);
		}
	}

	return 0L;
}
