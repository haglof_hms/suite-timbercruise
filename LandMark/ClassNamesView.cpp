// ClassNamesView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "ClassNamesView.h"
#include "MDIAssignGroupsFrame.h"
#include "MDIClassNamesFrame.h"


// CClassNamesView

IMPLEMENT_DYNCREATE(CClassNamesView, CXTResizeFormView)

CClassNamesView::CClassNamesView()
	: CXTResizeFormView(CClassNamesView::IDD)
{
	m_pDB = NULL;
	m_bConnected = FALSE;
}

CClassNamesView::~CClassNamesView()
{
	if(m_pDB != NULL)
		delete m_pDB;
}

void CClassNamesView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CClassNamesView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT/*XTP_ID_REPORT_CONTROL*/, OnValueChanged)
	ON_NOTIFY(NM_KEYDOWN, IDC_REPORT, OnBtnDown)
	ON_NOTIFY(NM_RCLICK, IDC_REPORT, OnReportItemRClick)
END_MESSAGE_MAP()


// CClassNamesView diagnostics

#ifdef _DEBUG
void CClassNamesView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CClassNamesView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CClassNamesView message handlers

BOOL CClassNamesView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CClassNamesView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	setupReport();
}

BOOL CClassNamesView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

LRESULT CClassNamesView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addClass();
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			saveClassData();
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			removeClass();
			break;
		}	// case ID_DELETE_ITEM :
		
	}	// switch (wParam)

	return 0L;
}

void CClassNamesView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndReport,1,1,rect.right-1,rect.bottom-1);
	}
}

void CClassNamesView::setupReport()
{
	CXTPReportColumn *pCol = NULL;

	if(m_wndReport.GetSafeHwnd() == 0)
	{
		//create sheet
		if(!m_wndReport.Create(WS_CHILD | WS_TABSTOP | WS_VISIBLE | WM_VSCROLL|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_REPORT))
			return;
	}
	
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sErrorEmptyStr = xml->str(IDS_STRING183);
			m_sDBErrorMsg = xml->str(IDS_STRING105);
			m_sDeleteMsg = xml->str(IDS_STRING181);
			m_sDeleteMsg2 = xml->str(IDS_STRING182);	
			m_sMenuSort = xml->str(IDS_STRING230);
			m_sPrintOrderError = xml->str(IDS_STRING231);

			//setup columns
			if(m_wndReport.GetSafeHwnd() != NULL)
			{
				m_wndReport.ShowWindow(SW_NORMAL);

				//add Scrollbars
				m_wndReport.EnableScrollBar(SB_HORZ, TRUE);
				m_wndReport.EnableScrollBar(SB_VERT, TRUE);

				//printorder
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_CLASS_PRINTORDER, xml->str(IDS_STRING174), 50));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				//pCol->SetVisible(FALSE);		//TODO: for now not visible
				
				//class name
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_CLASS_CLASSNAME, xml->str(IDS_STRING176), 200));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_nMaxLength = 50;

				
				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( FALSE );
				m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);

				RECT rect;
				GetClientRect(&rect);
				setResize(&m_wndReport,1,1,rect.right-1,rect.bottom-1);

				
				populateReport();
				m_wndReport.UpdateWindow();
			}
		}
		delete xml;
	}


}

void CClassNamesView::populateReport()
{
	m_wndReport.GetRecords()->RemoveAll();

	if(getClassNames() == FALSE)	//update m_vecClasses
	{
		CMDIClassNamesFrame *pFrame = (CMDIClassNamesFrame*)getFormViewParentByID(IDD_FORMVIEW6);
		if(pFrame)
			pFrame->m_bDBConnOk = FALSE;
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}

	if(m_vecClasses.size() > 0)
	{
		for(UINT i=0; i<m_vecClasses.size(); i++)
		{
			if(m_vecClasses[i].getClassIndex() > 0)	//don't show default 'empty' 
			{
				m_wndReport.AddRecord(new CClassNamesReportRec(m_vecClasses[i]));
			}
		}
	}

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	//m_wndReport.SetFocusedRow(0);
}

BOOL CClassNamesView::getClassNames()
{
	if(m_pDB != NULL)
	{
		if(m_pDB->getClassNames(m_vecClasses) == TRUE)
			return TRUE;
	}

	AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
	return FALSE;
}

void CClassNamesView::addClass()
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	int printorder,tmp;
	CXTPReportRecords *pRecs = NULL;
	CClassNamesReportRec *pRec = NULL;

	printorder = 0;
	if (m_wndReport.GetSafeHwnd() != NULL)
	{
		//set index to -1, assign index in OnValueChanged
		//get next printorder
		pRecs = m_wndReport.GetRecords();		
		for(int i=0; i<pRecs->GetCount(); i++)
		{
			//don't save empty rows (index == -1)
			pRec = (CClassNamesReportRec*)pRecs->GetAt(i);
			if(pRec)
			{
				tmp = pRec->getColumnInt(COLUMN_CLASS_PRINTORDER);
				if(tmp > printorder)								
					printorder = tmp;
			}
		}

		printorder++;
		m_wndReport.AddRecord(new CClassNamesReportRec(CTransaction_TCClass(-1, printorder,_T(""))));
		m_wndReport.Populate();
		m_wndReport.GetRecords()->GetAt(m_wndReport.GetRecords()->GetCount()-1)->GetItem(COLUMN_CLASS_PRINTORDER)->SetEditable(FALSE);

		pRows = m_wndReport.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow)
			{
				pRow->SetSelected(TRUE);
				m_wndReport.SetFocusedRow(pRow);
			}
		}
		m_wndReport.UpdateWindow();

	}
	return;

}

void CClassNamesView::removeClass()
{
	CString csMsg;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if(!pRecs)
		return;

	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if(!pRow)
		return;

	CClassNamesReportRec *pRec = (CClassNamesReportRec*)pRow->GetRecord();

	if(!pRec)
		return;

	int index = pRec->getClassIndex();

	if(index < 0)
	{
		//just remove record, data not yet saved in database
		populateReport();
	}
	else if(index > 0)
	{
		csMsg.Format(_T("%s %s?\r\n\r\n%s"),m_sDeleteMsg, pRec->getColumnText(COLUMN_CLASS_CLASSNAME), m_sDeleteMsg2);
		if(AfxMessageBox(csMsg, MB_ICONQUESTION|MB_YESNO) == IDYES)
		{
			if(m_pDB != NULL)
			{
				m_pDB->removeClassFromOrderHdr(index);
				populateReport();
				
				//post message to ClassNamesView to remove class from record
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
					(LPARAM)&_doc_identifer_msg(MODULE926, MODULE925, _T(""), AGV_REMOVE_CLASS, index, 0));
			}
			else
				AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		}
	}
	else
	{
		//TODO: error msg, index should not be 0
		;
	}
}

void CClassNamesView::saveClassData()
{
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CClassNamesReportRec *pRec;

	if(m_pDB != NULL)
	{
		for(int i=0; i<pRecs->GetCount(); i++)
		{
			//don't save empty rows (index == -1)
			pRec = (CClassNamesReportRec*)pRecs->GetAt(i);
			if(pRec)
			{
				if(pRec->getClassIndex() >= 0)
					m_pDB->saveToTableOrderHdr(pRec);	
			}
		}
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
}

void CClassNamesView::OnValueChanged(NMHDR *pNotifyStruct, LRESULT *)
{
	CMDIAssignGroupsFrame *pFrame = (CMDIAssignGroupsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
	CXTPReportRow *pRow = NULL;
	CClassNamesReportRec *pRec = NULL;
	int index;
	BOOL bSave;
	int tmp, actrow,act_printorder;
	CXTPReportRecords *pRecs = NULL;
	CClassNamesReportRec *pRecord = NULL;
				

	pRow = m_wndReport.GetFocusedRow();
	if(pRow == NULL)
		return;

	actrow = pRow->GetIndex();

	pRec = (CClassNamesReportRec*)pRow->GetRecord();
	if(pRec == NULL)
		return;

	index = pRec->getClassIndex();
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if(pItemNotify->pRow != NULL && pItemNotify->pColumn->GetItemIndex() == COLUMN_CLASS_CLASSNAME)
		{
			if(checkClassName(pRec->getColumnText(COLUMN_CLASS_CLASSNAME)) == FALSE)
			{
				AfxMessageBox(m_sErrorEmptyStr);
				Refresh();	//get original data
				return;
			}

			if(index > 0)
			{
				//save
				if(m_pDB != NULL)
				{
					m_pDB->saveToTableOrderHdr(pRec);

					//post message to ClassNamesView to update class name in record
					AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
						(LPARAM)&_doc_identifer_msg(MODULE926, MODULE925, _T(""), AGV_UPDATE_CLASS_NAME, index, 0));
				}
				else
					AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			}
			else if(index < 0)
			{
				if(m_pDB != NULL)
				{
					//assign index if index == -1
					pRec->setClassIndex(m_pDB->getNextClassIndex());

					//save
					m_pDB->saveNewClassNameToTableOrderHdr(pRec);

					//set cell editable
					pRow->GetRecord()->GetItem(COLUMN_CLASS_PRINTORDER)->SetEditable(TRUE);

				}
				else
					AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);

			}
			else
				return;	//TODO: something wrong, index should not be 0

			//send message to update combolist
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
				(LPARAM)&_doc_identifer_msg(MODULE926, MODULE925, _T(""), AGV_UPDATE_ITEM, AGV_UPDATE_COMBO_LIST, 0));

			//repopulate
			Refresh();
			pRow = (m_wndReport.GetRows())->GetAt(actrow);
			if(pRow)
				m_wndReport.SetFocusedRow(pRow);

		}
		else if(pItemNotify->pRow != NULL && pItemNotify->pColumn->GetItemIndex() == COLUMN_CLASS_PRINTORDER)
		{
			if(index > 0 && checkClassName(pRec->getColumnText(COLUMN_CLASS_CLASSNAME)) == TRUE)
			{
				act_printorder = pRec->getColumnInt(COLUMN_CLASS_PRINTORDER);

				if(act_printorder <= 0)
				{
					
					AfxMessageBox(m_sPrintOrderError, MB_ICONEXCLAMATION);
					Refresh();	//get old value
					//TODO: set current row
					return;
				}
				
				pRecs = m_wndReport.GetRecords();
				if(!pRecs)
					return;

				bSave = FALSE;
				for(int i=0; i<pRecs->GetCount(); i++)
				{
					pRecord = (CClassNamesReportRec*)pRecs->GetAt(i);
					if(pRecord)
					{
						if(pRecord->getClassIndex() != pRec->getClassIndex() &&
							(tmp = pRecord->getColumnInt(COLUMN_CLASS_PRINTORDER)) == pRec->getColumnInt(COLUMN_CLASS_PRINTORDER))
						{
							pRecord->setColumnInt(COLUMN_CLASS_PRINTORDER, ++tmp);
							pRec = pRecord;
							i = -1;
							bSave = TRUE;
						}
					}
				}

				if(bSave == TRUE)
				{
					//save recordset to db
					saveClassData();
				}
				else
				{
					//only current record needs to be saved
					if(m_pDB != NULL)
					{
						m_pDB->saveToTableOrderHdr(pRec);
					}
				}
				
				//repopulate
				Refresh();
				
				//set focus on correct row
				CXTPReportRows *pRows = m_wndReport.GetRows();
				if(!pRows)
					return;

				for(int i=0; i<pRows->GetCount(); i++)
				{
					pRow = pRows->GetAt(i);
					if(!pRow)
						return;

					pRec = (CClassNamesReportRec*)pRow->GetRecord();
					if(pRec)
					{
						if(pRec->getColumnInt(COLUMN_CLASS_PRINTORDER) == act_printorder)
						{
							actrow = i;
							break;
						}
					}
				}

				pRow = pRows->GetAt(actrow);
				if(pRow)
					m_wndReport.SetFocusedRow(pRow);
			}
		}
	}

}

BOOL CClassNamesView::checkClassName(CString classname)
{
	BOOL bRet = TRUE;

	classname.Remove(' ');
	if(classname == _T(""))
	{
		bRet = FALSE;
	}

	return bRet;
}

void CClassNamesView::OnBtnDown(NMHDR *pNotifyStruct, LRESULT *)
{
	LPNMKEY lpNMKEY = (LPNMKEY)pNotifyStruct;

	if(!m_wndReport.GetFocusedRow())
		return;

	// Delete focused class
	if(lpNMKEY->nVKey == VK_DELETE)
	{
		removeClass();
	}

	//Insert new class
	if(lpNMKEY->nVKey == VK_INSERT)
	{
		addClass();
	}
}

void CClassNamesView::Refresh()
{
	populateReport();
}

void CClassNamesView::OnReportItemRClick(NMHDR *pNotifyStruct, LRESULT *)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	ASSERT(pItemNotify->pColumn);

	CPoint ptClick = pItemNotify->pt;
	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	//create menu
	menu.AppendMenuW(MF_SEPARATOR,  (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenuW(MF_STRING, ID_SET_PRINTORDER, m_sMenuSort);

	//get case
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY| TPM_RETURNCMD|TPM_LEFTALIGN|TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this,NULL);
	
	switch(nMenuResult)
	{
	case ID_SET_PRINTORDER:
		setPrintOrder();
		break;
	default:
		break;
	}
}

void CClassNamesView::setPrintOrder()
{
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if(!pRows)
		return;

	CXTPReportRow *pRow = NULL;
	CClassNamesReportRec *pRec = NULL;
	
	for(int i=0; i<pRows->GetCount(); i++)
	{
		pRow = pRows->GetAt(i);
		if(!pRow)
			return;

		pRec = (CClassNamesReportRec*)pRow->GetRecord();
		if(pRec)
		{
			pRec->setColumnInt(COLUMN_CLASS_PRINTORDER, i+1);
			if(m_pDB)
				m_pDB->savePrintOrderToTableOrderHdr(pRec);	
		}
	}

	Refresh();
}


