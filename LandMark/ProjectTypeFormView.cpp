// ProjectTypeFormView.cpp : implementation file
//

#include "stdafx.h"
//#include "Forrest.h"
#include "ProjectTypeFormView.h"

//#include "ResLangFileReader.h"

// CProjectTypeFormView

IMPLEMENT_DYNCREATE(CProjectTypeFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CProjectTypeFormView, CXTResizeFormView)
	ON_WM_SIZE()
	//ON_WM_ERASEBKGND()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CProjectTypeFormView::CProjectTypeFormView()
	: CXTResizeFormView(CProjectTypeFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CProjectTypeFormView::~CProjectTypeFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CProjectTypeFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CProjectTypeFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CProjectTypeFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
  m_wndReport1.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void CProjectTypeFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport1();
	
		m_bInitialized = TRUE;
	}
}

BOOL CProjectTypeFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CProjectTypeFormView diagnostics

#ifdef _DEBUG
void CProjectTypeFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CProjectTypeFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


void CProjectTypeFormView::doSetNavigationBar()
{
	if (m_vecProjectTypeData.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

}

// CProjectTypeFormView message handlers

BOOL CProjectTypeFormView::getProjectTypes(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			/*TODO:*/ if (m_bConnected = m_pDB->getProjectTypes(m_vecProjectTypeData))
			bReturn = TRUE;
			
		}	// if (pDB != NULL)
	}
	return bReturn;
}

// PROTECTED METHODS

void CProjectTypeFormView::setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}
}

BOOL CProjectTypeFormView::setupReport1(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_PROJECTTYPE_REPORT,FALSE,FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sMsgCap	= xml.str(IDS_STRING240/*213*/);
				m_sProjectTypeID	= xml.str(IDS_STRING285/*210*/);
				m_sProjectType	= xml.str(IDS_STRING345);
				m_sProjectTypeNotes	= xml.str(IDS_STRING287/*212*/);
				m_sOKBtn	= xml.str(IDS_STRING103/*216*/);
				m_sCancelBtn	= xml.str(IDS_STRING104/*217*/);

				/*TODO: change text strings for project types*/
				m_sText1	= xml.str(IDS_STRING288/*218*/);
				m_sText2	= xml.str(IDS_STRING289/*219*/);
				m_sText3	= xml.str(IDS_STRING290/*220*/);
				m_sDoneSavingMsg =	xml.str(IDS_STRING291/*2151*/);

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(0, (m_sProjectTypeID), 30,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( FALSE );
				pCol->SetVisible( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(1, (m_sProjectType), 80));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(2, (m_sProjectTypeNotes), 120));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT );

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();

				// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
				getProjectTypes();
				populateReport();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(IDC_PROJECTTYPE_REPORT),1,1,rect.right - 1,rect.bottom - 1);

				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();
			}
			xml.clean();
		}	// if (fileExists(sLangFN))

	}

	return TRUE;

}

void CProjectTypeFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	setResize(GetDlgItem(IDC_PROJECTTYPE_REPORT),1,1,rect.right - 2,rect.bottom - 2);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CProjectTypeFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addProjectType();
			if (saveProjectType())
			{
				getProjectTypes();
				populateReport();
				setReportFocus();
			}
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			getProjectTypes();
			if (saveProjectType())
			{
				getProjectTypes();
				populateReport();
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			getProjectTypes();
			if (removeProjectType())
			{
				getProjectTypes();
				populateReport();
			}
			break;
		}	// case ID_DELETE_ITEM :
		
	}	// switch (wParam)

	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL CProjectTypeFormView::populateReport(void)
{
	// Get species from database; 060317 p�d
	//getProjectTypes();

	// populate report; 060317 p�d
	m_wndReport1.ClearReport();

	if (m_vecProjectTypeData.size() > 0)
	{
		for (UINT i = 0;i < m_vecProjectTypeData.size();i++)
		{
			CTransaction_ProjectType rec = m_vecProjectTypeData[i];
			m_wndReport1.AddRecord(new CProjectTypeReportRec(i,rec));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	doSetNavigationBar();

	return TRUE;
}

BOOL CProjectTypeFormView::addProjectType(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		m_wndReport1.AddRecord(new CProjectTypeReportRec());
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRow->SetSelected(TRUE);
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		return TRUE;
	}
	return TRUE;
}

void CProjectTypeFormView::setReportFocus(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRow->SetSelected(TRUE);
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
	}
}

BOOL CProjectTypeFormView::saveProjectType(void)
{
	CTransaction_ProjectType rec;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Add records from Report to vector; 060317 p�d
			m_wndReport1.Populate();
			CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
			if (pRecs->GetCount() > 0)
			{
				for (int i = 0;i < pRecs->GetCount();i++)
				{
					CProjectTypeReportRec *pRec = (CProjectTypeReportRec *)pRecs->GetAt(i);

					rec = CTransaction_ProjectType(pRec->getColumnInt(0),
 										 									pRec->getColumnText(1),
											 								pRec->getColumnText(2),
																			_T(""));
					/*TODO:*/ if (!m_pDB->addProjectType(rec))
						m_pDB->updProjectType(rec);
						
				}	// for (int i = 0;i < pRecs->GetCount();i++)

	// Commented out (PL); 070402 p�d
	//			::MessageBox(0,m_sDoneSavingMsg,m_sMsgCap,MB_ICONINFORMATION | MB_OK);
			}	// if (pRecs->GetCount() > 0)

			m_wndReport1.setIsDirty(FALSE);
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}

	return bReturn;
}

BOOL CProjectTypeFormView::removeProjectType(void)
{
	CXTPReportRow *pRow = NULL;
	CTransaction_ProjectType data;
	CProjectTypeReportRec *pRec = NULL;
	CString sMsg;

	// Add records from Report to vector; 060317 p�d
	m_wndReport1.Populate();

	pRow = m_wndReport1.GetFocusedRow();
	if (pRow != NULL)
	{
		pRec = (CProjectTypeReportRec *)pRow->GetRecord();
		if (pRec != NULL)
		{
			if (m_vecProjectTypeData.size() > 0)
			{
				for (UINT i = 0;i < m_vecProjectTypeData.size();i++)
				{
					data = m_vecProjectTypeData[i];
					if (pRec->getColumnText(1) == data.getProjectType())
						break;
				}
			}
			else
				data = CTransaction_ProjectType(pRec->getID(),pRec->getColumnText(0),pRec->getColumnText(1),_T(""));		
			if (m_bConnected)
			{
				if (m_pDB != NULL)
				{
					// Get Regions in database; 061002 p�d	
					//data = m_vecProjectTypeData[nIndex];
					if (!isProjectTypeUsed(data))
					{
						// Setup a message for user upon deleting machine; 061010 p�d
						sMsg.Format(_T("%s\n\n%s : %s\n%s : %s\n\n%s\n\n%s"),
												m_sText1,
												//m_sProjectTypeID,
												//data.getID(),
												m_sProjectType,
												data.getProjectType(),
												m_sProjectTypeNotes,
												data.getNotes(),
												m_sText2,
												m_sText3);

						if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
						{
							// Delete ProjectType
							/*TODO:*/ m_pDB->removeProjectType(data);
							
						}	// if (::MessageBox(0,sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
					}	// if (!isMachineUsed(data))
				}	// if (m_pDB != NULL)
			}	// if (m_bConnected)
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)
	return TRUE;
}


BOOL CProjectTypeFormView::isProjectTypeUsed(CTransaction_ProjectType &)
{
	return FALSE;
}





/////////////////////////////////////////////////////////////////////////////
// CMDIProjectTypeFrame

IMPLEMENT_DYNCREATE(CMDIProjectTypeFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIProjectTypeFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIProjectTypeFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIProjectTypeFrame::CMDIProjectTypeFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIProjectTypeFrame::~CMDIProjectTypeFrame()
{
}

void CMDIProjectTypeFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROJECTTYPEFRAME_KEY);
	SavePlacement(this, csBuf);
}

int CMDIProjectTypeFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING214),
						xml.str(IDS_STRING215));
		}	// if (xml.Load(m_sLangFN))
		xml.clean();
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDIProjectTypeFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIProjectTypeFrame diagnostics

#ifdef _DEBUG
void CMDIProjectTypeFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIProjectTypeFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIProjectTypeFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROJECTTYPE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROJECTTYPE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIProjectTypeFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIProjectTypeFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CProjectTypeFormView *pView = (CProjectTypeFormView *)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}	// if (pView)

}

// load the placement in OnShowWindow()
void CMDIProjectTypeFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROJECTTYPEFRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIProjectTypeFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIProjectTypeFrame::OnClose(void)
{
	CProjectTypeFormView *pView = (CProjectTypeFormView *)GetActiveView();
	if (pView)
	{
//		if (pView->getIsDirty())
//		{
// Commented out (PL); 070402 p�d
//			if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//			{
				pView->saveProjectType();
//			}	// if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//		}	// if (pView->getIsDirty())
	}	// if (pView)
	
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIProjectTypeFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}
