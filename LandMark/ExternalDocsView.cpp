// ExternalDocsView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "ExternalDocsView.h"
#include "Resource.h"
#include "MDIExternalDocsFrame.h"

//CInplaceBtn
void CInplaceBtn::OnInplaceButtonDown(CXTPReportInplaceButton* pButton)
{

	CFileDialog f(TRUE, _T(""), _T(""), 4|2, m_sFilterText);
	if (f.DoModal() == IDOK)
	{
		m_sValue = f.GetPathName();
		SetValue(f.GetPathName());

		setInplaceItem(f.GetPathName());

//		SetTextColor(BLACK);
	}	// if (f.DoModal() == IDOK)
}

// CExternalDocsView

IMPLEMENT_DYNCREATE(CExternalDocsView, CXTPReportView)

CExternalDocsView::CExternalDocsView()
	: CXTPReportView()
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_bInitialized = FALSE;
}

CExternalDocsView::~CExternalDocsView()
{
	if(m_pDB != NULL)
		delete m_pDB;
}


BEGIN_MESSAGE_MAP(CExternalDocsView, CXTPReportView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_DESTROY()
	ON_COMMAND(ID_TBBTN_ADD_EXTDOC, OnAddRow)
	ON_COMMAND(ID_TBBTN_REM_EXTDOC, OnRemoveRow)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
END_MESSAGE_MAP()


// CExternalDocsView diagnostics

#ifdef _DEBUG
void CExternalDocsView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

#ifndef _WIN32_WCE
void CExternalDocsView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
}
#endif
#endif //_DEBUG


// CExternalDocsView message handlers

void CExternalDocsView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

	setLanguage();

	setupReport();

	LoadReportState();

	m_bInitialized = TRUE;
}

void CExternalDocsView::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sIcon = xml->str(IDS_STRING210);
			m_sURL = xml->str(IDS_STRING211);
			m_sNote = xml->str(IDS_STRING212);
			m_sDoneBy = xml->str(IDS_STRING213);
			m_sDate = xml->str(IDS_STRING214);
			m_sFilterText = xml->str(IDS_STRING207);
			m_sDBErrorMsg = xml->str(IDS_STRING105);
			m_sOrgTitle = xml->str(IDS_STRING927);
		}
		delete xml;
	}

	setWindowCaption();
}

BOOL CExternalDocsView::setupReport()
{
	CXTPReportColumn *pCol = NULL;

	if(GetReportCtrl().GetSafeHwnd() != NULL)
	{
		GetReportCtrl().GetReportHeader()->SetAutoColumnSizing(TRUE);
		GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE);
		GetReportCtrl().EnableScrollBar(SB_VERT, TRUE);
		GetReportCtrl().ShowWindow( SW_NORMAL);

		VERIFY(m_ilIcons.Create(16, 13, ILC_COLOR24|ILC_MASK, 0, 1));
		CBitmap bmp;
		VERIFY(bmp.LoadBitmap(IDB_BITMAP2));
		m_ilIcons.Add(&bmp, RGB(255, 0 , 255));

		GetReportCtrl().SetImageList(&m_ilIcons);

		//Icon
		pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_EXTDOC_ICON, m_sURL, 20, FALSE, 5));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;

		//URL
		pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_EXTDOC_URL, m_sURL, 200));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->AddExpandButton();
		pCol->GetEditOptions()->m_nMaxLength = 255;

		//Note
		pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_EXTDOC_NOTE, m_sNote, 150));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_nMaxLength = 127;

		//Done by
		pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_EXTDOC_DONEBY, m_sDoneBy, 50));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
		pCol->GetEditOptions()->m_nMaxLength = 30;

		//Date
		pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_EXTDOC_DATE, m_sDate, 100));
		pCol->SetHeaderAlignment(DT_WORDBREAK);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_nMaxLength = 20;

		GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
		GetReportCtrl().SetMultipleSelection(TRUE);
		GetReportCtrl().SetGridStyle(TRUE, xtpReportGridSolid);
		GetReportCtrl().SetGridStyle(FALSE, xtpReportGridSolid);
		GetReportCtrl().FocusSubItems(TRUE);
		GetReportCtrl().AllowEdit(TRUE);
		GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
	}

	return TRUE;
}

BOOL CExternalDocsView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CExternalDocsView::OnSize(UINT nType, int cx, int cy)
{
	CXTPReportView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CExternalDocsView::OnSetFocus(CWnd* pOldWnd)
{
	CXTPReportView::OnSetFocus(pOldWnd);

	// TODO: Add your message handler code here
}

void CExternalDocsView::OnDestroy()
{
	m_ilIcons.DeleteImageList();

	CXTPReportView::OnDestroy();
}

BOOL CExternalDocsView::OnCopyData(CWnd *pWnd, COPYDATASTRUCT *pData)
{
	if(pData->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData, pData->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}

	return CXTPReportView::OnCopyData(pWnd, pData);
}

LRESULT CExternalDocsView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	CString csFrom;
	switch(wParam)
	{
	case ID_WPARAM_VALUE_FROM + 0x02:
		{
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			if(sizeof(*msg) == sizeof(_doc_identifer_msg))
			{
				csFrom.Format(_T("%s"), msg->getSendFrom());
				if(msg->getValue3() == 0 && csFrom == MODULE920)	//Stand table
				{
					m_sTableName_pk = TBL_STAND;
					m_nLinkID_pk = msg->getValue1();
					setWindowCaption();
					populateReport();
				}
			}
		}
		break;
	case ID_SAVE_ITEM:
		saveExternalDocs();
		break;
	};

	return 0L;
}

void CExternalDocsView::setWindowCaption()
{
	CString csCaption, csTract;

	if(m_pDB != NULL)
	{
		csTract = m_pDB->getTractName(m_nLinkID_pk);
		CMDIExternalDocsFrame *pFrame = (CMDIExternalDocsFrame*)getFormViewParentByID(IDD_FORMVIEW7);
		if(pFrame)
		{
			CDocument* pDocument = pFrame->GetActiveDocument();
			if(pDocument && csTract != _T(""))
			{
				csCaption.Format(_T("%s - %s"), m_sOrgTitle, csTract);
				pDocument->SetTitle(csCaption);
			}	
		}
	}
}

void CExternalDocsView::LoadReportState()
{
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if(!AfxGetApp()->GetProfileBinary(REG_WP_TIMBERCRUISE_EXTERNALDOCS_KEY, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar(&memFile, CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);
	}
	catch(CArchiveException* pEx)
	{
		pEx->Delete();
	}
	catch(COleException* pEx)
	{
		pEx->Delete();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CExternalDocsView::SaveReportState()
{
	CMemFile memFile;
	CArchive ar(&memFile, CArchive::store);

	GetReportCtrl();

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_WP_TIMBERCRUISE_EXTERNALDOCS_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}

void CExternalDocsView::populateReport()
{
	getExternalDocs();

	GetReportCtrl().GetRecords()->RemoveAll();

	GetReportCtrl().BeginUpdate();

	for(UINT i=0; i<m_vecExternalDocs.size(); i++)
	{
		m_recExternalDocs = m_vecExternalDocs[i];
		GetReportCtrl().AddRecord(new CExternalDocsReportDataRec(m_recExternalDocs.getExtDocID_pk(), m_sFilterText, m_recExternalDocs));
	}

	GetReportCtrl().EndUpdate();
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CExternalDocsView::OnAddRow()
{
	GetReportCtrl().AddRecord(new CExternalDocsReportDataRec(m_sFilterText));
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CExternalDocsView::OnRemoveRow()
{
	int index;
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CExternalDocsReportDataRec *pRec = NULL;
	if(pRow)
	{
		pRec = (CExternalDocsReportDataRec*)pRow->GetRecord();
		if(pRec)
		{
			index = pRec->getExtDocId();
			if(index < 0)
			{
				//just remove record, data not yet saved in database
				populateReport();
			}
			else if(index > 0)
			{
				if(m_pDB != NULL)
				{
					m_pDB->delExternalDocument(pRec->getExtDocId(), m_sTableName_pk, m_nLinkID_pk);

					populateReport();
				}
				else
					AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			}
			else
				AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		}
	}
}

BOOL CExternalDocsView::saveExternalDocs()
{
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	CExternalDocsReportDataRec *pRec = NULL;
	int index;
	
	if(m_pDB != NULL)
	{
		if(pRecs)
		{
			for(int i=0; i<pRecs->GetCount(); i++)
			{
				pRec = (CExternalDocsReportDataRec*)pRecs->GetAt(i);
				if(pRec)
				{
					if(pRec->getExtDocId() > 0)
					{
						m_pDB->saveExternalDocument(pRec->getExtDocId(), m_sTableName_pk,  m_nLinkID_pk, 0,pRec);
					}
					else if(pRec->getExtDocId() == -1 && (pRec->getColumnText(COLUMN_EXTDOC_URL) != _T("") || pRec->getColumnText(COLUMN_EXTDOC_NOTE) != _T("")))
					{
						//needs to do this because havn't found how to capture when value changed using expand button first time
						index = m_pDB->getNextExternalDocIndex();
						if(index > 0)
						{
							pRec->setExtDocId(index);
							m_pDB->saveNewExternalDocument(index, m_sTableName_pk, m_nLinkID_pk, 0, pRec);
						}
					}						
				}
			}
		}
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONERROR);

		//send msg to update stand view
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
			(LPARAM)&_doc_identifer_msg(MODULE927, MODULE920, _T(""), 1, 0, 0));
	

	return TRUE;
}

BOOL CExternalDocsView::getExternalDocs()
{
	if(m_pDB != NULL)
	{
		m_pDB->getExternalDocuments(m_sTableName_pk, m_nLinkID_pk, m_vecExternalDocs);
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);

	return FALSE;
}

void CExternalDocsView::OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CExternalDocsReportDataRec *pRec = NULL;
	int index = 0;

	if(!pRow)
		return;

	pRec = (CExternalDocsReportDataRec*)pRow->GetRecord();

	if(!pRec)
		return;

	index = pRec->getExtDocId();

	if(index > 0)
	{
		if(m_pDB != NULL)
			m_pDB->saveExternalDocument(index, m_sTableName_pk, m_nLinkID_pk, 0, pRec);
		else
			AfxMessageBox(m_sDBErrorMsg, MB_ICONERROR);

	}
	else if(index == -1)
	{
		if(m_pDB != NULL)
		{
			index = m_pDB->getNextExternalDocIndex();
			if(index > 0)
			{
				pRec->setExtDocId(index);
				m_pDB->saveNewExternalDocument(index, m_sTableName_pk, m_nLinkID_pk, 0, pRec);
			}
		}
		else
			AfxMessageBox(m_sDBErrorMsg, MB_ICONERROR);
	}
	else
	{
		//TODO: error msg
		
	}
}


void CExternalDocsView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	CExternalDocsReportDataRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if(pItemNotify->pRow != NULL && pItemNotify->pColumn->GetItemIndex() == COLUMN_EXTDOC_ICON)
		{
			// Do a hit-test; 080513 p�d
			rect = pItemNotify->pColumn->GetRect();		
			pt = pItemNotify->pt;
			// Check if the user clicked on the Icon or not; 080513 p�d
			if (hitTest_X(pt.x,rect.left,13))
			{
				pRec = (CExternalDocsReportDataRec*)pItemNotify->pRow->GetRecord();
				viewExtrenalDocument(pRec->getColumnText(COLUMN_EXTDOC_URL));
			}	// if (hitTest_X(pt.x,rect.left,13))
		}	// if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
	}	// if (pItemNotify != NULL)
}

void CExternalDocsView::viewExtrenalDocument(LPCTSTR fn)
{
	::ShellExecute(NULL, _T("open"), fn, _T(""), _T(""), SW_SHOW);
}


