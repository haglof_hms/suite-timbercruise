#pragma once
#include "LandMarkDB.h"
#include "stdafx.h"

#define BMP_MAGNIFICATION_GLASS		5	// In IDB_BITMAP2


/////////////////////////////////////////////////////////////////////////////
// PrivItem for inplace button; 070306 p�d
class CInplaceBtn : public CXTPReportRecordItemText
{
//private:
	CString m_sValue;
	CString m_sFilterText;
public:
	CInplaceBtn(LPCTSTR sValue,LPCTSTR filter_text) : CXTPReportRecordItemText()
	{
		m_sValue = sValue;
		SetValue(m_sValue);
		m_sFilterText = filter_text;
		// Check if file exists, if NOT
		// mark it RED; 080926 p�d
//		if (!fileExists(m_sValue))
//			SetTextColor(RED);
//		else if (isURL(m_sValue))
//			SetTextColor(BLUE);
//		else
//			SetTextColor(BLACK);
	}
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
		m_sValue = szText;
		SetValue(m_sValue);
//		if (!fileExists(m_sValue))
//			SetTextColor(RED);
//		else if (isURL(m_sValue))
//			SetTextColor(BLUE);
//		else
//			SetTextColor(BLACK);
	}

	CString getInplaceItem(void)	{	return m_sValue; 	}
	
	void setInplaceItem(LPCTSTR value)	
	{ 
		m_sValue = value; 
		SetValue(value);
	}
protected:
	virtual void OnInplaceButtonDown(CXTPReportInplaceButton* pButton);
};


class CExternalDocsReportDataRec : public CXTPReportRecord
{
	int m_nIndex;
public:
	CTransaction_external_docs recExtDoc;	

protected:
	class CIconItem : public CXTPReportRecordItem
	{
		//private:
	public:
		CIconItem(int icon_id) : CXTPReportRecordItem()
		{
			SetIconIndex(icon_id);
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
		//private:
		CString m_sText;
		int m_nID;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_sText = szText;
			SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
		int getDwData(void)	{ return m_nID; }
		void setDwData(int id)	{ m_nID = id; }

		virtual void OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs,CXTPReportRecordItemConstraint* pConstraint)
		{
			if (pConstraint != NULL)
				m_nID = (int)pConstraint->m_dwData;
			else
				m_nID = -1;

			CXTPReportRecordItem::OnConstraintChanged(pItemArgs,pConstraint);
		}
	};
public:
	CExternalDocsReportDataRec(CString filter_text = _T("All *.*|*.*|"))
	{
		m_nIndex = -1;
		AddItem(new CIconItem(BMP_MAGNIFICATION_GLASS));
		AddItem(new CInplaceBtn(_T(""), filter_text));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(getUserName()));
		AddItem(new CTextItem(getDBDateTime()));
	}

	CExternalDocsReportDataRec(UINT index,LPCTSTR filter_text,CTransaction_external_docs& rec)	 
	{
		m_nIndex = index;
		recExtDoc = rec;
		AddItem(new CIconItem(BMP_MAGNIFICATION_GLASS));
		AddItem(new CInplaceBtn(rec.getExtDocURL(),filter_text));
		AddItem(new CTextItem(rec.getExtDocNote()));
		AddItem(new CTextItem(rec.getExtDocDoneBy()));
		AddItem(new CTextItem(rec.getExtDocCreated()));
	}

	CString getInplaceColumnText(int item)	
	{ 
		return ((CInplaceBtn*)GetItem(item))->getInplaceItem();
	}
	void setInplaceColumnText(int item,LPCTSTR text)	
	{ 
		return ((CInplaceBtn*)GetItem(item))->setInplaceItem(text);
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	void setColumnText(int item,LPCTSTR text)	
	{ 
		return ((CTextItem*)GetItem(item))->setTextItem(text);
	}

	int getExtDocId(void)
	{
		return m_nIndex;
	}

	void setExtDocId(int index)
	{
		m_nIndex = index;
	}

	CTransaction_external_docs& getRecord(void)	 { return recExtDoc; }
};


// CExternalDocsView form view

class CExternalDocsView : public CXTPReportView
{
	DECLARE_DYNCREATE(CExternalDocsView)

	CString m_sLangFN;
	CString m_sAbrevLangSet;
	CImageList m_ilIcons;
	
protected:
	CExternalDocsView();           // protected constructor used by dynamic creation
	virtual ~CExternalDocsView();

	BOOL m_bInitialized;
	BOOL setupReport(void);
	void setLanguage(void);

	CString m_sIcon;
	CString m_sURL;
	CString m_sNote;
	CString m_sDoneBy;
	CString m_sDate;
	CString m_sFilterText;
	CString m_sDBErrorMsg;
	CString m_sOrgTitle;

	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	vecTransaction_external_docs m_vecExternalDocs;
	CTransaction_external_docs m_recExternalDocs;

	int m_nLinkID_pk;
	CString m_sTableName_pk;

	void LoadReportState(void);
	void SaveReportState(void);

	void populateReport(void);
public:
	BOOL saveExternalDocs(void);
protected:
	BOOL getExternalDocs(void);
	void viewExtrenalDocument(LPCTSTR fn);
	void setWindowCaption(void);

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnDestroy();

	afx_msg void OnAddRow(void);
	afx_msg void OnRemoveRow(void);
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);

};


