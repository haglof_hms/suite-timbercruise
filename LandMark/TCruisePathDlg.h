#pragma once

#include "Resource.h"

// CTCruisePathDlg dialog

class CTCruisePathDlg : public CDialog
{
	DECLARE_DYNAMIC(CTCruisePathDlg)

protected:
	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CMyExtStatic m_wndLbl1;
	CMyExtEdit m_wndEdit1;
	CButton m_wndBtnBrowse;

	void setLanguage(void);

public:
	CTCruisePathDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTCruisePathDlg();

	CString getPath() { return m_wndEdit1.getItemStrData(); }//getText();}

// Dialog Data
	enum { IDD = IDD_TC_PATH_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnBrowse();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnEnChangeEdit1();
};
