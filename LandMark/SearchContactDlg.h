#pragma once
#include "afxwin.h"
#include "Resource.h"
#include "LandMarkDB.h"

class CSearchContactsReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CSearchContactsReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CSearchContactsReportDataRec(UINT index,CTransaction_contacts data)	 
	{
		m_nIndex = index;
		AddItem(new CTextItem((data.getName())));
		AddItem(new CTextItem((cleanCRLF(data.getAddress(),L", "))));
		AddItem(new CTextItem((data.getPostNum())));
		AddItem(new CTextItem((data.getPostAddress())));
		AddItem(new CTextItem((data.getCounty())));
		AddItem(new CTextItem((data.getCountry())));
		AddItem(new CTextItem((data.getPhoneWork())));
		AddItem(new CTextItem((data.getFaxNumber())));
		AddItem(new CTextItem((data.getEMail())));
		AddItem(new CTextItem((data.getWebSite())));
		
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


// CSearchContactDlg dialog

class CSearchContactDlg : public CDialog
{
	DECLARE_DYNAMIC(CSearchContactDlg)
	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sDBErrorMsg; 
	CString m_csErrMsgOpen;

	CLandMarkDB* m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;

	CMyExtStatic m_wndLblCategories;
	CMyExtStatic m_wndLblName;
	CMyExtEdit m_wndEditName;
	CButton m_wndBtnSearch;
	CButton m_wndBtnClear;
	CComboBox m_wndCBoxCategories;

	CString m_sNoCategory;
	CString m_sTitle;
	CMyReportCtrl m_wndReport;

	void setupReport(void);
	vecTransactionContacts m_vecContacts;
	void getContacts(void);
	void populateData(void);

	int m_nContactID;
	
	vecTransactionCategory m_vecCategories;
	void getCategories(void);
	void addCategoriesToCBox(void);

public:
	CSearchContactDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSearchContactDlg();

// Dialog Data
	enum { IDD = IDD_SEARCH_CONTACT_DLG };
	virtual BOOL OnInitDialog();

	int getContactID(void) {return m_nContactID;}

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:	
	afx_msg void OnBnClickedBtnSrSearch();
	afx_msg void OnBnClickedBtnSrClear();
	afx_msg void OnBnClickedOk();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	CButton m_wndBtnOk;
	CButton m_wndBtnCancel;
};
