#pragma once

#include "Resource.h"
#include "LandMarkDB.h"
#include "picturectrl.h"

// CContactsFormView form view

class CContactsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CContactsFormView)

private:
	BOOL m_bInitialized;
	BOOL m_bSetFocusOnInitDone;
	BOOL m_bIsDataEnabled;
	CString	m_sLangAbrev;
	CString m_sLangFN;
	CString m_sErrCap;
	CString m_sSaveMsg;
	CString m_sDoneSavingMsg;
	CString m_sDeleteMsg;
	CString m_sMsgCap;
	CString m_sPNR_ORGNR;
	CString m_sName;
	CString m_sCompany;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sContactActiveMsg;
	CString m_sNoContactsMsg;
	CString m_sNoResultInSearch;
	CString m_sNameMissing;

	BOOL m_bIsDirty;

	enumACTION m_enumAction;
	FOCUSED_EDIT m_enumFocusedEdit;

	void setLanguage(void);

	void setLanguages(void);

	BOOL getContacts(void);
	vecTransactionContacts m_vecContactsData;
	CTransaction_contacts m_enteredContactData;
	CTransaction_contacts m_activeContactData;
	BOOL isContactUsed(CTransaction_contacts &);
	
	BOOL getCategories(void);
	vecTransactionCategory m_vecCategoriesData;

	BOOL getCategoriesForContact(void);
	vecTransactionCategoriesForContacts m_vecCatgoriesForContactData;
	vecTransactionCategoriesForContacts m_vecSelectedCatgoriesForContactData;

	int m_nDBIndex;
	void populateData(UINT idx, BOOL bSetBars = TRUE);
	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	std::vector<CString>m_vecAddedLanguages;

	CString m_sSQLContacts;

	CPictureCtrl m_wndPicCtrl;

	BOOL m_bSavePic;
	CString m_sPicFilePath;
protected:
	CContactsFormView();           // protected constructor used by dynamic creation
	virtual ~CContactsFormView();

	CXTResizeGroupBox m_wndGroup1;
	CXTResizeGroupBox m_wndGroup2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLbl11;
	CMyExtStatic m_wndLbl12;
	CMyExtStatic m_wndLbl13;
	CMyExtStatic m_wndLbl14;
	CMyExtStatic m_wndLbl15;
	CMyExtStatic m_wndLbl16;
	CMyExtStatic m_wndLbl17;

	CComboBox m_wndCBox2;
	CComboBox m_wndCBox1;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit9;
	CMyExtEdit m_wndEdit10;
	CMyExtEdit m_wndEdit11;
	CMyExtEdit m_wndEdit12;
	CMyExtEdit m_wndEdit13;
	CMyExtEdit m_wndEdit14;
	CMyExtEdit m_wndEdit15;
	CMyExtEdit m_wndEdit16;
	CMyExtEdit m_wndEdit17;

	CButton	m_wndBtn1;

	CXTButton m_wndAddCompanyBtn;
	CXTButton m_wndPostNumBtn;
	CXTButton m_wndPostAdrBtn;

	CButton	m_wndBtnAddPic;
	CButton	m_wndBtnDelPic;


	// My methods
	BOOL getEnteredData(void);

	BOOL addContact(void);
	BOOL removeContact(void);


	CStringArray m_sarrTypeOfContact;

	void resetIsDirty(void);
	BOOL getIsDirty(void);

	void clearAll();

	CString setCategoriesPerContactStr(void);

	void setEnableData(BOOL);
	
	void setSearchToolbarBtn(BOOL enable);
public:
	// Need to be PUBLIC
	BOOL isDataChanged(void);

	BOOL saveContact(BOOL save_categories = FALSE);

	void setPostNumberAndPostAddress(CTransaction_postnumber);

	void doSetNavigationBar(void);
	BOOL doPopulateNext(void);
	BOOL doPopulatePrev(void);
	void doPopulate(int, BOOL);
	void doPopulateOnContactID(int);
	void doRePopulate(void);

	void refreshContacts(void);

	int getDBIndex(void)	{	return m_nDBIndex;	}

	BOOL doRePopulateFromSearch(LPCTSTR sql,bool goto_first);

	void setCompany(LPCTSTR company_name)	{ m_wndEdit3.SetWindowText(company_name); }


	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW8 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnDestroy();

	afx_msg void OnImport();
	afx_msg void OnSearchReplace();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedBtnAddPic();
	afx_msg void OnBnClickedBtnDelPic();
	afx_msg void OnEnSetfocusEdit11();
	afx_msg void OnEnSetfocusEdit12();
	afx_msg void OnEnSetfocusEdit13();
	afx_msg void OnEnSetfocusEdit14();
	afx_msg void OnEnSetfocusEdit15();
	afx_msg void OnEnSetfocusEdit16();
	afx_msg void OnEnSetfocusEdit17();
	afx_msg void OnEnSetfocusEdit19();
	afx_msg void OnEnSetfocusEdit115();
	afx_msg void OnEnSetfocusEdit116();
	afx_msg void OnEnSetfocusEdit110();
	afx_msg void OnEnSetfocusEdit111();
	afx_msg void OnEnSetfocusEdit112();
	afx_msg void OnEnSetfocusEdit113();
	afx_msg void OnEnSetfocusEdit114();
	afx_msg void OnEnSetfocusEdit117();
	afx_msg void OnCbSetfocusComobox11();
	afx_msg void OnCbSetfocusComobox12();

	afx_msg void OnCbnSelchangeCombo12();
};


