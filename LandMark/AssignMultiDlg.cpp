// AssignMultiDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "AssignMultiDlg.h"


// CAssignMultiDlg dialog

IMPLEMENT_DYNAMIC(CAssignMultiDlg, CDialog)

CAssignMultiDlg::CAssignMultiDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAssignMultiDlg::IDD, pParent)
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	classindex = -1;
}

CAssignMultiDlg::~CAssignMultiDlg()
{
	if(m_pDB != NULL)
		delete m_pDB;
}

void CAssignMultiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_listClass);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
}


BEGIN_MESSAGE_MAP(CAssignMultiDlg, CDialog)
	ON_WM_COPYDATA()
	ON_BN_CLICKED(IDOK, &CAssignMultiDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CAssignMultiDlg message handlers

BOOL CAssignMultiDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

	setLanguage();

	setListCtrl();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CAssignMultiDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CDialog::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CAssignMultiDlg::OnCopyData(CWnd *pWnd, COPYDATASTRUCT *pCopyDataStruct)
{
	if(pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memset(&m_dbConnectionData, 0x00, sizeof(DB_CONNECTION_DATA));
		memcpy(&m_dbConnectionData, pCopyDataStruct->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}
	
	return CDialog::OnCopyData(pWnd, pCopyDataStruct);
}


void CAssignMultiDlg::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING226));
			m_sEmpty = xml->str(IDS_STRING227);
			m_wndBtnOK.SetWindowText(xml->str(IDS_STRING103));
			m_wndBtnCancel.SetWindowText(xml->str(IDS_STRING104));
		}
		delete xml;
	}
}

void CAssignMultiDlg::setListCtrl()
{
	if(m_pDB)
	{
		m_pDB->getClassNames(m_vecClasses);

		for(UINT i=0; i<m_vecClasses.size(); i++)
		{
			if(i == 0)	//Empty
				m_listClass.AddString(m_sEmpty);
			else
				m_listClass.AddString(m_vecClasses[i].getClassName());
		}

		if(m_vecClasses.size() > 0)
			m_listClass.SetCurSel(classindex);
	}
}

void CAssignMultiDlg::OnBnClickedOk()
{
	classindex = m_listClass.GetCurSel();
	OnOK();
}