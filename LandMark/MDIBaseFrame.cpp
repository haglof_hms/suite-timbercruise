// MDIBaseFrame.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "MDIBaseFrame.h"
#include "LandMarkView.h"
#include <Aclapi.h>


// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

CMDIFrameDoc::CMDIFrameDoc()
{
}

BOOL CMDIFrameDoc::OnNewDocument()
{
	//check license
	if(!License())
		return FALSE;

	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

CMDIFrameDoc::~CMDIFrameDoc()
{
}


BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
END_MESSAGE_MAP()


// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

#ifndef _WIN32_WCE
void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif
#endif //_DEBUG

#ifndef _WIN32_WCE
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}
#endif


// CMDIFrameDoc commands
// MDIBaseFrame.cpp : implementation file
//


// CMDILandMarkFrame

IMPLEMENT_DYNCREATE(CMDILandMarkFrame, CChildFrameBase)	//CMDIChildWnd)

CMDILandMarkFrame::CMDILandMarkFrame()
{
	m_bFirstOpen = FALSE;
	m_bEnableTBBTNFilterOff = FALSE;
	m_bEnableTBBTNReports = FALSE;
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bDBConnOk = TRUE;

	//LOGFONT lfIcon;
	//VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon,0));
	//VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));

}

CMDILandMarkFrame::~CMDILandMarkFrame()
{
}


BEGIN_MESSAGE_MAP(CMDILandMarkFrame, CChildFrameBase)	//CMDIChildWnd)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_MDIACTIVATE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_SHOWWINDOW()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_FILTER_OFF, OnUpdateTBBTNFilterOff)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_REPORTS, OnUpdateTBBTNReports)
	ON_COMMAND(ID_BTN_TCRUISE, OnBtnTCruise)
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_CLOSE()
	//ON_COMMAND(ID_TBBTN_IMPORT, OnBtnImport)
	ON_COMMAND(ID_TBBTN_EXTDOC, OnBtnExternalDocs)
	ON_COMMAND(ID_BTN_GIS, OnBtnGIS)
END_MESSAGE_MAP()


// CMDILandMarkFrame message handlers

void CMDILandMarkFrame::OnDestroy()
{
	//CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	//save window placement
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_TIMBERCRUISE_WP);
	SavePlacement(this, csBuf);

	m_bFirstOpen = TRUE;
}

void CMDILandMarkFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}


void CMDILandMarkFrame::OnSetFocus(CWnd* pOldWnd)
{
	//CXTPFrameWndBase<CMDIChildWnd>::OnSetFocus(pOldWnd);

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	if(m_bDBConnOk == TRUE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);	
}

void CMDILandMarkFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);

	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}


LRESULT CMDILandMarkFrame::OnMessageFromShell(WPARAM wParam, LPARAM lParam)
{
	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while(pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE, wParam, lParam);

		}
	}

	return 0L;
}

BOOL CMDILandMarkFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTPFrameWndBase<CMDIChildWnd>::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CMDILandMarkFrame::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sToolTipTCruise = xml->str(IDS_STRING100);
			m_sToolTipFilter = xml->str(IDS_STRING121);
			m_sToolTipFilterOff = xml->str(IDS_STRING122);
			m_sToolTipPrintOut = xml->str(IDS_STRING123);
			m_sToolTipRefresh = xml->str(IDS_STRING124);
			m_sToolTipReports = xml->str(IDS_STRING125);
			//m_sToolTipImport = xml->str(IDS_STRING151);
			m_sErrorMsg = xml->str(IDS_STRING152);
			m_sToolTipExtDoc = xml->str(IDS_STRING206);
			m_sToolTipGIS = xml->str(IDS_STRING228);

			m_wndFieldChooserDlg.SetWindowText(xml->str(IDS_STRING126));
			m_wndFilterEdit.SetWindowText(xml->str(IDS_STRING121));

		}
		delete xml;
	}
}

void CMDILandMarkFrame::setupToolBarIcons(void)
{
	//set tooltips and icons
	CXTPControl *pCtrl = NULL;
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CString csResFN = getToolBarResFN();

	if(fileExists(csResFN))
	{
		CXTPToolBar *pToolBar = &m_wndToolBar;
		if(pToolBar->IsBuiltIn())
		{
			if(pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				if(nBarID == IDR_TOOLBAR1)
				{
					//hResModule = LoadLibraryEx(csResFN, NULL, DONT_RESOLVE_DLL_REFERENCES | LOAD_LIBRARY_AS_DATAFILE);
					//if(hResModule)
					{
						pCtrl = p->GetAt(0);
						pCtrl->SetTooltip(m_sToolTipTCruise);
						//hIcon = LoadIcon(hResModule, RSTR_TB_EXPORT);
						hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
						if(hIcon)
							pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(1);
						pCtrl->SetTooltip(m_sToolTipFilter);
						hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_FILTER);
						//hIcon = LoadIcon(hResModule, RSTR_TB_FILTER);
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(2);
						pCtrl->SetTooltip(m_sToolTipFilterOff);
						hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_FILTER_OFF);
						//hIcon = LoadIcon(hResModule, RSTR_TB_FILTER_OFF);
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(3);
						pCtrl->SetTooltip(m_sToolTipPrintOut);
						hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_PRINT);
						//hIcon = LoadIcon(hResModule, RSTR_TB_PRINT);
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(4);
						pCtrl->SetTooltip(m_sToolTipRefresh);
						hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_REFRESH);
						//hIcon = LoadIcon(hResModule, RSTR_TB_REFRESH);
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(5);
						pCtrl->SetTooltip(m_sToolTipReports);
						hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_TEXTDOC);
						//hIcon = LoadIcon(hResModule, RSTR_TB_TEXTDOC);
						if (hIcon) pCtrl->SetCustomIcon(hIcon);

						/*pCtrl = p->GetAt(6);
						pCtrl->SetTooltip(m_sToolTipImport);
						hIcon = LoadIcon(hResModule, RSTR_TB_IMPORT);
						if(hIcon) pCtrl->SetCustomIcon(hIcon);*/

						pCtrl = p->GetAt(6);
						pCtrl->SetTooltip(m_sToolTipExtDoc);
						hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_GEM);
						//hIcon = LoadIcon(hResModule, RSTR_TB_GEM);
						if(hIcon) pCtrl->SetCustomIcon(hIcon);

						pCtrl = p->GetAt(7);
						pCtrl->SetTooltip(m_sToolTipGIS);
						hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_GIS);
						//hIcon = AfxGetApp()->LoadIcon(IDI_ICON_GIS);
						if(hIcon) pCtrl->SetCustomIcon(hIcon);
						
						// Check if UMGIS exists, else disable button
						CString sPath;
						sPath.Format(_T("%s%s"),getSuitesDir(),_T("UMGIS.dll"));
						if (!fileExists(sPath))
						{
							pCtrl = p->GetAt(7);
							pCtrl->SetEnabled(FALSE);
						}

						FreeLibrary(hResModule);

					}//if(hResModule)
				}//if(nBarID == IDR_TOOLBAR1)
			}//if(pToolBar->GetType() != xtpBarTypeMenuBar)
		}//if(pToolBar->IsBuiltIn())
	}//if(fileExists(csResFN))


}



int CMDILandMarkFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	
	if (CXTPFrameWndBase<CMDIChildWnd>::OnCreate(lpCreateStruct) == -1)
		return -1;

	if(m_hIcon)
	{
		SetIcon(m_hIcon, TRUE);
		SetIcon(m_hIcon, FALSE);
	}

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	//set theme
	//CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	//create toolbar
	m_wndToolBar.CreateToolBar(WS_TABSTOP | WS_VISIBLE | WS_CHILD | CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);


	EnableDocking(CBRS_ALIGN_ANY);

	//Initialize dialog bar m_wndFieldChooser
	if(!m_wndFieldChooserDlg.Create(this, IDD_FIELD_CHOOSER,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS))
		return -1;

	//Initialize dialog bar m_wndFilterEdit
	if(!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN1))
		return -1;
	
	//docking for field chooser
	m_wndFieldChooserDlg.EnableDocking(0);
	setLanguage();
	
	ShowControlBar(&m_wndFieldChooserDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	//docking for filter edit
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	
	return 0;
}

void CMDILandMarkFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if(m_wndToolBar.GetSafeHwnd() != NULL)
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK | LM_HORZ | LM_COMMIT);
		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}

	CMDIChildWnd::OnPaint();
}

void CMDILandMarkFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	//load placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_TIMBERCRUISE_WP);
		LoadPlacement(this, csBuf);
	}
}



void CMDILandMarkFrame::OnBtnTCruise()
{
	TCHAR szTCruisDir[MAX_PATH];
	BOOL bRunTC = FALSE;
	CString csMsg;

	//check export registry key, v6.0.9
	if(checkExportDll() == FALSE)
		return;

	//Check file path to TCruise.exe before creating the process
	_stprintf_s(szTCruisDir,_T("%s\\%s"),getTCruiseDir(), TCRUISE_EXE_NAME);

	if(fileExists(szTCruisDir))
	{
		bRunTC = TRUE;	//found file path in registry
#ifdef SHOW_MY_EXTRA_MSG
		csMsg.Format(_T("path from registry!\r\n%s"),szTCruisDir);
		AfxMessageBox(csMsg); 
#endif
	}
	else
	{
		_stprintf_s(szTCruisDir,_T("%s\\%s"),TCRUISE_DEF_DIR, TCRUISE_EXE_NAME);
		if(fileExists(szTCruisDir))
		{
			bRunTC = TRUE;	//found file path in default directory
#ifdef SHOW_MY_EXTRA_MSG
			csMsg.Format(_T("path from default directory!\r\n%s"),szTCruisDir);
			AfxMessageBox(csMsg);
#endif
		}
		else
		{
			//can't find file path user must provide it
			_stprintf_s(szTCruisDir,_T("%s\\%s"),getTCruiseDirFromUser(), TCRUISE_EXE_NAME);
			if(fileExists(szTCruisDir))
				bRunTC = TRUE;
		}
	}

	TCHAR szNewCurrentDirectory[MAX_PATH*4];
	_stprintf_s(szNewCurrentDirectory, _T("%s\\%s\\"), getMyPathToCommonAppData(), _T("TCruise"));
	CreatePublicDirectory(szNewCurrentDirectory);

#ifdef SHOW_MY_EXTRA_MSG
		csMsg.Format(_T("current directory for TCruise\r\n%s"),szNewCurrentDirectory);
		AfxMessageBox(csMsg); 
#endif

	if(bRunTC == TRUE)
	{
		m_hWndTC = CreateProcessWithConditions(szTCruisDir,
			TCRUISE_WINDOW_TITLE,
			AfxGetMainWnd()->GetSafeHwnd(),
			NULL,
			60000,
			FALSE,
			szNewCurrentDirectory);	// Set current directory

		
		//check that TCruise is up and running
		if(m_hWndTC != NULL)
		{
			::ShowWindow(m_hWndTC, SW_NORMAL);
		}//if(m_hWndTC != NULL)
		
	}//if(fileExists(szTCruisDir) && ...
}

int CMDILandMarkFrame::CreatePublicDirectory(CString csPath)
{
	if(!CreateDirectory(csPath,NULL))
		return FALSE;

	HANDLE hDir = CreateFile(csPath,READ_CONTROL|WRITE_DAC,0,NULL,OPEN_EXISTING,FILE_FLAG_BACKUP_SEMANTICS,NULL);
	if(hDir == INVALID_HANDLE_VALUE)
		return FALSE; 

	ACL* pOldDACL;
	SECURITY_DESCRIPTOR* pSD = NULL;
	GetSecurityInfo(hDir, SE_FILE_OBJECT , DACL_SECURITY_INFORMATION,NULL, NULL, &pOldDACL, NULL, (void**)&pSD);

	PSID pSid = NULL;
	SID_IDENTIFIER_AUTHORITY authNt = SECURITY_NT_AUTHORITY;
	AllocateAndInitializeSid(&authNt,2,SECURITY_BUILTIN_DOMAIN_RID,DOMAIN_ALIAS_RID_USERS,0,0,0,0,0,0,&pSid);

	EXPLICIT_ACCESS ea={0};
	ea.grfAccessMode = GRANT_ACCESS;
	ea.grfAccessPermissions = GENERIC_ALL;
	ea.grfInheritance = CONTAINER_INHERIT_ACE|OBJECT_INHERIT_ACE;
	ea.Trustee.TrusteeType = TRUSTEE_IS_GROUP;
	ea.Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea.Trustee.ptstrName = (LPTSTR)pSid;

	ACL* pNewDACL = 0;
	DWORD err = SetEntriesInAcl(1,&ea,pOldDACL,&pNewDACL);

	if(pNewDACL)
		SetSecurityInfo(hDir,SE_FILE_OBJECT,DACL_SECURITY_INFORMATION,NULL, NULL, pNewDACL, NULL);

	FreeSid(pSid);
	LocalFree(pNewDACL);
	LocalFree(pSD);
	LocalFree(pOldDACL);
	CloseHandle(hDir);

	return TRUE;
}

void CMDILandMarkFrame::OnSize(UINT nType, int cx, int cy)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CMDILandMarkFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_TIMBERCRUISE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_TIMBERCRUISE;

	CXTPFrameWndBase<CMDIChildWnd>::OnGetMinMaxInfo(lpMMI);
}

void CMDILandMarkFrame::OnUpdateTBBTNFilterOff(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

void CMDILandMarkFrame::OnUpdateTBBTNReports(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNReports);
}

void CMDILandMarkFrame::disableTBBtns()
{
	//set tooltips and icons
	CXTPControl *pCtrl = NULL;
	CXTPToolBar *pToolBar = &m_wndToolBar;
	
	if(pToolBar)
	{
		UINT nBarID = pToolBar->GetBarID();
		
		if(nBarID == IDR_TOOLBAR1)
		{
			CXTPControls *p = pToolBar->GetControls();

			pCtrl = p->GetAt(1);
			pCtrl->SetEnabled(FALSE);

			pCtrl = p->GetAt(2);
			pCtrl->SetEnabled(FALSE);

			pCtrl = p->GetAt(3);
			pCtrl->SetEnabled(FALSE);

			pCtrl = p->GetAt(4);
			pCtrl->SetEnabled(FALSE);

			pCtrl = p->GetAt(5);
			pCtrl->SetEnabled(FALSE);

			pCtrl = p->GetAt(6);
			pCtrl->SetEnabled(FALSE);

			pCtrl = p->GetAt(7);
			pCtrl->SetEnabled(FALSE);
		}
	}

	m_bDBConnOk = FALSE;

}

void CMDILandMarkFrame::OnBtnExternalDocs()
{
	CLandMarkView* pView = (CLandMarkView*)GetActiveFrame()->GetActiveView();
	if(pView)
	{
		CXTPReportRow *pRow = pView->GetReportCtrl().GetFocusedRow();
		if(!pRow)
			return;

		CStandSelListReportRec *pRec = (CStandSelListReportRec*)pRow->GetRecord();
		if(!pRec)
			return;

		//show external docs view
		showFormView(IDD_FORMVIEW7, m_sLangFN, 0);

		//send msg to view with stand id
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
			(LPARAM)&_doc_identifer_msg(MODULE920,MODULE927,_T(""),pRec->getFileIndex(),0,0));
	}
}

void CMDILandMarkFrame::OnBtnGIS()
{
	CString sLangFN;
	CLandMarkView* pView = (CLandMarkView*)GetActiveFrame()->GetActiveView();
	if(pView)
	{
		CXTPReportRow *pRow = pView->GetReportCtrl().GetFocusedRow();
		if(!pRow)
			return;

		CStandSelListReportRec *pRec = (CStandSelListReportRec*)pRow->GetRecord();
		if(!pRec)
			return;

		sLangFN = getLanguageFN(getLanguageDir(),_T("UMGIS"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
			

		//show GIS view
		showFormView(888, sLangFN, 0);

		//send msg to view with stand id
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
			(LPARAM)&_doc_identifer_msg(MODULE920, MODULE888, _T("<saknas>"), 3, pRec->getFileIndex(), 0));

	}
}

