#if !defined(__PROJECTFRAME_H__)
#define __PROJECTFRAME_H__

#include "MDIPropertyFrame.h" // for CMyControlPopup


///////////////////////////////////////////////////////////////////////////////////////////
// CMDIProjectFrame frame
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CMDIProjectFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDIProjectFrame)

//private:
	CXTPDockingPaneManager m_paneManager;

	CXTPToolBar m_wndToolBar;

	CMyControlPopup *m_pToolsPopup;

	vecInt m_vecExcudedItemsOnTab0;
	vecInt m_vecExcudedItemsOnTab1;
	vecInt m_vecExcudedItemsOnTab2;

	BOOL m_bIsTBtnEnabledOpen;
	BOOL m_bIsTBtnEnabledCreate;
	BOOL m_bIsTBtnEnabledTools;

	static HWND m_TopWindow;

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	CString m_sMsg1;
	CString m_sMsgCap;
	CString m_sLangFN;

	HICON m_hIcon;

public:

	CMDIProjectFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIProjectFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void setExcludedToolItems(int tab_selected);

	void setEnableToolbar(BOOL enable_open,BOOL enable_create,BOOL enable_tools);

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg void OnPaint();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnClose();
	afx_msg void OnUpdateTBBTNImport(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTBBTNCreate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTBBTNTools(CCmdUI* pCmdUI);

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CProjectSelectListFrame frame

class CProjectSelectListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CProjectSelectListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:
	CString m_sToolTipFilter;
	CString m_sToolTipFilterOff;
	CString m_sToolTipPrintOut;
	CString m_sToolTipSearch;

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	BOOL m_bEnableTBBTNFilterOff;
	CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CProjectSelectListFrame();           // protected constructor used by dynamic creation
	virtual ~CProjectSelectListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	CDialogBar m_wndFieldChooser;   // Sample Field chooser window
	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#endif