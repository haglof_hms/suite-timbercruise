#pragma once
#include "Resource.h"
#include "LandMarkDB.h"

class CClassNamesReportRec : public CXTPReportRecord
{
	int m_nClassIndex;

	protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
		double m_fValue;
	public:
		CFloatItem(double fValue, LPCTSTR fmt_str = sz1dec)
			: CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
		}

		void setFloatItem(double value)
		{
			m_fValue = value;
			SetValue(value);
		}

		double getFloatItem(void)
		{
			return m_fValue;
		}
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
		int m_nValue;
	public:
		CIntItem(int nValue)
			: CXTPReportRecordItemNumber(nValue)
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		void setIntItem(int value)
		{
			m_nValue = value;
			SetValue(value);
		}

		int getIntItem(void)
		{
			return m_nValue;
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
		CString m_sText;
	public:
		CTextItem(CString sValue)
			: CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_sText = szText;
			SetValue(m_sText);
		}

		void setTextItem(LPCTSTR text)
		{
			m_sText = text;
			SetValue(m_sText);
		}

		CString getTextItem(void)
		{
			return m_sText;
		}
	};

public:

	CClassNamesReportRec(void)
	{
		m_nClassIndex = -1;
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
	}

	CClassNamesReportRec(CTransaction_TCClass &rec)
	{
		m_nClassIndex = rec.getClassIndex();
		AddItem(new CIntItem(rec.getPrintOrder()));	//new 091009
		AddItem(new CTextItem(rec.getClassName()));
	}

	int getClassIndex(void)
	{
		return m_nClassIndex;
	}

	void setClassIndex(int index)
	{
		m_nClassIndex = index;
	}

	double getColumnFloat(int item)
	{
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item, double value)
	{
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)
	{
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item, int value)
	{
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)
	{
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item, LPCTSTR text)
	{
		((CTextItem*)GetItem(item))->setTextItem(text);
	}


};


// CClassNamesView form view

class CClassNamesView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CClassNamesView)

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sDBErrorMsg;
	CString m_sErrorEmptyStr;
	CString m_sDeleteMsg;
	CString m_sDeleteMsg2;
	CString m_sMenuSort;
	CString m_sPrintOrderError;

	CXTPReportControl m_wndReport;
	vecTransactionTCClass m_vecClasses;
	
	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL checkClassName(CString classname);
	BOOL getClassNames(void);
	void addClass(void);
	void removeClass(void);
	void saveClassData(void);
	void Refresh(void);
protected:
	CClassNamesView();           // protected constructor used by dynamic creation
	virtual ~CClassNamesView();

	void setupReport(void);
	void populateReport(void);
	void setPrintOrder(void);

public:
	enum { IDD = IDD_FORMVIEW6 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnBtnDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemRClick(NMHDR *pNotifyStruct, LRESULT* /*result*/);
	DECLARE_MESSAGE_MAP()
};


