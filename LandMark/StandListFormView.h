#pragma once

#include "Resource.h"


///////////////////////////////////////////////////////////////////////////////////////////
// CStandSelectionListFrame frame

class CStandSelectionListFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CStandSelectionListFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:
	CString m_sToolTipFilter;
	CString m_sToolTipFilterOff;
	CString m_sToolTipAdd;

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	BOOL m_bEnableTBBTNFilterOff;
	CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CStandSelectionListFrame();           // protected constructor used by dynamic creation
	virtual ~CStandSelectionListFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

	void setFilterWndCap(LPCTSTR cap)
	{
		m_wndFilterEdit.SetWindowText(cap);
	}
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};




class CNoPropStandsReportRec : public CXTPReportRecord
{
	//int m_nTraktID;
	UINT m_nIndex;
protected:
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);

			CXTPReportControl *pCtrl = NULL;
			if (pItemArgs != NULL)
			{
				pCtrl = pItemArgs->pControl;
				if (pCtrl != NULL)
				{
					pCtrl->GetParent()->SendMessage(WM_COMMAND,pCtrl->GetDlgCtrlID(),0);
				}
			}
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
		int m_nValue;
	public:
		CIntItem(int nValue)
			: CXTPReportRecordItemNumber(nValue)
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		void setIntItem(int value)
		{
			m_nValue = value;
			SetValue(value);
		}

		int getIntItem(void)
		{
			return m_nValue;
		}
	};



public:

	CNoPropStandsReportRec(void)
	{
		m_nIndex = -1;									// FileIndex
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));					// TractID
		AddItem(new CFloatItem(0.0,sz1dec));			// TractAcres
		AddItem(new CTextItem(_T("")));					// Cruise Method
		AddItem(new CTextItem(_T("")));					// CruiseDate
		AddItem(new CTextItem(_T("")));					// TractLoc
		AddItem(new CTextItem(_T("")));					// TractOwner
		AddItem(new CTextItem(_T("")));					// Cruiser
		AddItem(new CIntItem(-1));						// Property id 
	}

	CNoPropStandsReportRec(CTransaction_TCStand &rec)
	{
		m_nIndex = rec.getFileIndex();								// FileIndex
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(rec.getTractID()));					// TractID
		AddItem(new CFloatItem(rec.getTractAcres(),sz1dec));		// TractAcres
		AddItem(new CTextItem(rec.getCruiseMethod()));				// Cruise Method
		AddItem(new CTextItem(rec.getCruiseDate()));				// CruiseDate
		AddItem(new CTextItem(rec.getTractLoc()));					// TractLoc
		AddItem(new CTextItem(rec.getTractOwner()));				// TractOwner
		AddItem(new CTextItem(rec.getCruiser()));					// Cruiser
		AddItem(new CIntItem(rec.getPropID()));						// Property id
	}
	
	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

	int getColumnInt(int item)
	{
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item, int value)
	{
		if (item > 0)
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	//int getTraktID(void)		{ return m_nTraktID; };

	int getFileIndex(void)
	{
		return m_nIndex;
	}

	int getIndex(void)
	{
		return m_nIndex;
	}

	BOOL getColumnCheck(int item)	{	return ((CCheckItem*)GetItem(item))->getChecked(); }

};


//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CContactsSelectListFrame; 070108 p�d

class CStandsReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CStandsReportFilterEditControl)
public:
	CStandsReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//////////////////////////////////////////////////////////////////////////////
// CStandSelListFormView form view

class CStandSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CStandSelListFormView)

protected:
	CStandSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CStandSelListFormView();

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	int m_nSelectedColumn;

	vecTransactionTCStand/*Trakt*/ m_vecStandsNoProperty;
	void getStandsNoProperty(void);
	BOOL setupReport(void);
	
	CXTPReportSubListControl m_wndSubList;
	CStandsReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	void LoadReportState(void);
	void SaveReportState(void);

	int m_nDBIndex;

	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

public:
	// Need to set this method public; 070126 p�d
	void populateReport(void);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

	void setFilterWindow(void);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnAddStandToProperty(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


