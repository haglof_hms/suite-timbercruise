#pragma once
#include "Resource.h"
#include "afxwin.h"
#include "LandMarkDB.h"

// CAssignMultiDlg dialog

class CAssignMultiDlg : public CDialog
{
	DECLARE_DYNAMIC(CAssignMultiDlg)

	CString m_sLangFN;
	CString m_sAbrevLangSet;
	CString m_sEmpty;

	CListBox m_listClass;

	void setLanguage(void);
	void setListCtrl(void);

	CLandMarkDB *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;
	
	vecTransactionTCClass m_vecClasses;

public:
	int classindex;
	CAssignMultiDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAssignMultiDlg();

// Dialog Data
	enum { IDD = IDD_ASSIGN_MULTI_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnBnClickedOk();
	
public:
	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;
};
