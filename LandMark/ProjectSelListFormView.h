#pragma once

#include "Resource.h"

#include "LandMarkDB.h"

/////////////////////////////////////////////////////////////////////////////////
// CProjectReportDataRec

class CProjectReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CProjectReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CProjectReportDataRec(UINT index,CTransaction_property data)	 /*TODO: change for project*/
	{
		CString sValue;
		m_nIndex = index;
		AddItem(new CTextItem((data.getObjectID())));
		AddItem(new CTextItem((data.getPropertyName())));	/*TODO: change for project*/
		AddItem(new CTextItem((data.getPropertyNum())));	/*TODO: change for project*/
		AddItem(new CTextItem((data.getCountyName())));
		AddItem(new CTextItem((data.getMunicipalName())));
		AddItem(new CTextItem((data.getParishName())));
		AddItem(new CTextItem((data.getBlock())));
		AddItem(new CTextItem((data.getUnit())));
		sValue.Format(_T("%.1f"),data.getAreal());
		AddItem(new CTextItem((sValue)));
		sValue.Format(_T("%.1f"),data.getArealMeasured());
		AddItem(new CTextItem((sValue)));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};


//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CContactsSelectListFrame; 070108 p�d

class CProjectReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CProjectReportFilterEditControl)
public:
	CProjectReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//////////////////////////////////////////////////////////////////////////////
// CProjectSelListFormView form view

class CProjectSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CProjectSelListFormView)
	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	int m_nSelectedColumn;

protected:
	CProjectSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CProjectSelListFormView();

	vecTransactionProperty m_vecProjectData;	/*TODO: change for project*/
	BOOL setupReport(void);
	void populateReport(void);
	void getProjects(void);

	CXTPReportSubListControl m_wndSubList;
	CProjectReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	void LoadReportState(void);
	void SaveReportState(void);

	int m_nDBIndex;

	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	void setFilterWindow(void);
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldChooser();
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnPrintPreview();
	afx_msg void OnRefresh();
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
