// StandsInPropertyFormView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "StandsInPropertyFormView.h"
#include "PropertyTabView.h"

// CStandsInPropertyFormView

IMPLEMENT_DYNCREATE(CStandsInPropertyFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CStandsInPropertyFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, IDC_PROP_STANDS_REPORT, OnReportItemClick)
	ON_NOTIFY(NM_KEYDOWN, IDC_PROP_STANDS_REPORT, OnReportKeyDown)
	ON_BN_CLICKED(ID_TOOLS_ADD_STAND_TO_PROP, &CStandsInPropertyFormView::OnBnClickedButton31)
	ON_BN_CLICKED(ID_TOOLS_DEL_STAND_FROM_PROP, &CStandsInPropertyFormView::OnBnClickedButton32)

END_MESSAGE_MAP()


CStandsInPropertyFormView::CStandsInPropertyFormView()
	: CXTResizeFormView(CStandsInPropertyFormView::IDD)
{
	m_bInitialized = FALSE;
}

CStandsInPropertyFormView::~CStandsInPropertyFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;
	SaveReportState();
}

void CStandsInPropertyFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP
}

BOOL CStandsInPropertyFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CStandsInPropertyFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	//SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport2();

		m_bInitialized = TRUE;

		LoadReportState();
	}

}

BOOL CStandsInPropertyFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CStandsInPropertyFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	
	if (m_wndReport2.GetSafeHwnd())
	{
		setResize(&m_wndReport2,1,2,cx - 2,cy - 4);
	}

}

void CStandsInPropertyFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
}

void CStandsInPropertyFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result)
{
}

BOOL CStandsInPropertyFormView::populateReport(void)
{
	CString sObjID,S;
	// Get Owners for property, from database; 090810 p�d
	getStandsForProperty(&m_nActivePropertyID);
	// populate report; 090810 p�d
	m_wndReport2.ResetContent();

	if (m_vecTraktForProperty.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktForProperty.size();i++)
		{
			CTransaction_TCStand rec = m_vecTraktForProperty[i];
			if (rec.getPropID() == m_nActivePropertyID)
			{
				/*TODO: isPropertyUsedInObject(rec.getTraktID(),m_nActivePropertyID,sObjID);
				*/
				m_wndReport2.AddRecord(new CPropStandsReportRec(rec));	
				
			}
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)
	m_wndReport2.Populate();
	m_wndReport2.UpdateWindow();

	return TRUE;
}


void CStandsInPropertyFormView::doRePopulate(void)
{
	CString sObjID,S;
	
	// Get Owners for property, from database; 090810 p�d
	if (m_pDB != NULL)
		m_pDB->getTractsForProperty(m_nActivePropertyID,m_vecTraktForProperty);
	

	// populate report; 090810 p�d
	m_wndReport2.ResetContent();

	if (m_vecTraktForProperty.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktForProperty.size();i++)
		{
			CTransaction_TCStand/*trakt*/ rec = m_vecTraktForProperty[i];
			if (rec.getPropID() == m_nActivePropertyID)
			{
				/*TODO: isPropertyUsedInObject(rec.getTraktID(),m_nActivePropertyID,sObjID);
				*/
				m_wndReport2.AddRecord(new CPropStandsReportRec(rec));	
				
			}
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)
	m_wndReport2.Populate();
	m_wndReport2.UpdateWindow();
}

BOOL CStandsInPropertyFormView::getStandsForProperty(int *prop_id)
{
	BOOL bReturn = FALSE;
	CTransaction_property recProp;
	CPropertyTabView *pView = (CPropertyTabView*)getFormViewByID(IDD_FORMVIEW11);
	if (pView != NULL)
	{
		CPropertyFormView* pView0 = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView0 != NULL)
		{
			recProp = pView0->getActiveProperty();
			// Get Owners for property, from database; 060317 p�d
			if (m_pDB != NULL)
			{
				m_pDB->getTractsForProperty(recProp.getID(),m_vecTraktForProperty);	
				
				*prop_id = recProp.getID(); 
				bReturn = TRUE;
			}	// if (m_pDB != NULL)
		}
	}

	return bReturn;
}


void CStandsInPropertyFormView::isPropertyUsedInObject(int trakt_id,int prop_id,CString& obj_id)
{
	CString sObjList;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			/*TODO: m_pDB->isPropertyUsed_list(trakt_id,prop_id,sObjList);
			*/
		}	// if (m_pDB != NULL)
	}
	obj_id = sObjList;
}

// CStandsInPropertyFormView diagnostics

#ifdef _DEBUG
void CStandsInPropertyFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CStandsInPropertyFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG



// CStandsInPropertyFormView message handlers

BOOL CStandsInPropertyFormView::setupReport2(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport2.GetSafeHwnd() == 0)
	{
		if (!m_wndReport2.Create(this,IDC_PROP_STANDS_REPORT,FALSE,FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport2.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport2.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sYes = xml.str(IDS_STRING250/*240*/);
				m_sNo = xml.str(IDS_STRING251/*241*/);

				m_sMsgCap = xml.str(IDS_STRING240/*213*/);
				m_sMsgDoRemoveStandCap = xml.str(IDS_STRING337/*3316*/);
				m_sMsgRemoveStandCap.Format(_T("%s\n%s\n\n"),xml.str(IDS_STRING338/*3317*/),xml.str(IDS_STRING339/*3318*/));

				//TractID
				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING140), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->AllowRemove(FALSE);	//don't allow removal off column

				//Tract Acres
				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING141), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				//Cruise Method
				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING161), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				//Cruise Date
				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING142), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				//Tract Loc.
				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING143), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetVisible(FALSE);

				//Tract Owner
				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_5, xml.str(IDS_STRING144), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetVisible(FALSE);

				//Cruiser
				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_6, xml.str(IDS_STRING145), 120));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				
				m_wndReport2.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport2.SetMultipleSelection( FALSE );
				m_wndReport2.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport2.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport2.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport2.AllowEdit(FALSE);
				m_wndReport2.FocusSubItems(TRUE);
				m_wndReport2.SetFocus();
			}
			xml.clean();
		}	// if (fileExists(sLangFN))
	}

	return TRUE;

}

void CStandsInPropertyFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_PROPSTANDS_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		m_wndReport2.SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;

}

void CStandsInPropertyFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	m_wndReport2.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_PROPSTANDS_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

}

void CStandsInPropertyFormView::OnBnClickedButton31()
{
	//Add stand to property
	showFormView(IDD_REPORTVIEW4,m_sLangFN,1);
	
}

void CStandsInPropertyFormView::OnBnClickedButton32()
{
	//remove stand from property
	CXTPReportRow *pRow = m_wndReport2.GetFocusedRow();
	if (pRow)
	{
		CPropStandsReportRec *pRec = (CPropStandsReportRec*)pRow->GetRecord();
		/*TODO:if (pRec->getCanBeChanged())*/
		{
			if (::MessageBox(this->GetSafeHwnd(),m_sMsgDoRemoveStandCap,m_sMsgCap,MB_ICONINFORMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
			{
				if (m_pDB != NULL)
					m_pDB->updRemovePropertyFromStand(pRec->getFileIndex());	//pRec->getTraktID());
					
				populateReport();
			}
		}
		//else
		//	::MessageBox(this->GetSafeHwnd(),m_sMsgRemoveStandCap,m_sMsgCap,MB_ICONSTOP | MB_OK);
			
			
	}	// if (pItemNotify->pRow)
}
