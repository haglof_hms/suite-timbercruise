// TCruisePathDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "TCruisePathDlg.h"


// CTCruisePathDlg dialog

IMPLEMENT_DYNAMIC(CTCruisePathDlg, CDialog)

CTCruisePathDlg::CTCruisePathDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTCruisePathDlg::IDD, pParent)
{

}

CTCruisePathDlg::~CTCruisePathDlg()
{
}

void CTCruisePathDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_BTN_BROWSE, m_wndBtnBrowse);


}


BEGIN_MESSAGE_MAP(CTCruisePathDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_BROWSE, &CTCruisePathDlg::OnBnClickedBtnBrowse)
	ON_BN_CLICKED(IDOK, &CTCruisePathDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CTCruisePathDlg::OnBnClickedCancel)
	ON_EN_CHANGE(IDC_EDIT1, &CTCruisePathDlg::OnEnChangeEdit1)
END_MESSAGE_MAP()


// CTCruisePathDlg message handlers

BOOL CTCruisePathDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CDialog::PreCreateWindow(cs))
		return FALSE;
	
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CTCruisePathDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	
	setLanguage();

	return TRUE;  
}

void CTCruisePathDlg::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING102));
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING101));
		}
		delete xml;
	}

	m_wndBtnBrowse.SetWindowText(_T("..."));
}

void CTCruisePathDlg::OnBnClickedBtnBrowse()
{
	CFileDialog *m_pOpenFile = new CFileDialog(TRUE);
	CString csBuf, csPath;
	
	if(!m_pOpenFile)
		return;

	m_pOpenFile->m_ofn.lpstrFilter = _T("Exe files (*.exe)\0*.exe\0");
	m_pOpenFile->m_ofn.Flags = OFN_EXPLORER | OFN_HIDEREADONLY;

	m_pOpenFile->GetOFN().lpstrFile = csBuf.GetBuffer(2048*(_MAX_PATH + 1) +1);

	if(m_pOpenFile->DoModal() == IDOK)
	{
		m_wndEdit1.SetWindowText(csBuf);
		m_wndEdit1.setItemStrData(csBuf);
	}

	csBuf.ReleaseBuffer();
	delete m_pOpenFile;
}

void CTCruisePathDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	m_wndEdit1.setItemStrData(m_wndEdit1.getText());
	OnOK();
}

void CTCruisePathDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}

void CTCruisePathDlg::OnEnChangeEdit1()
{
	
}
