// TractNameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "TractNameDlg.h"


// CTractNameDlg dialog

IMPLEMENT_DYNAMIC(CTractNameDlg, CDialog)

CTractNameDlg::CTractNameDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTractNameDlg::IDD, pParent)
{
	m_csName = _T("");
	m_csPath = _T("");
}

CTractNameDlg::~CTractNameDlg()
{
}

void CTractNameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_TRACTNAME_LBL, m_wndLbl);
	DDX_Control(pDX, IDC_TRACTNAME_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_TRACTNAME_EDIT, m_wndEdit);
}


BEGIN_MESSAGE_MAP(CTractNameDlg, CDialog)
END_MESSAGE_MAP()


// CTractNameDlg message handlers

BOOL CTractNameDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	
	setLanguage();

	m_wndEdit.SetLimitText(128);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTractNameDlg::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING201));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING202));
		}
		delete xml;
	}

	m_wndLbl.SetWindowText(m_csPath);

}
BOOL CTractNameDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CDialog::PreCreateWindow(cs))
		return FALSE;
	
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTractNameDlg::OnOK()
{
	UpdateData(TRUE);

	m_wndEdit.GetWindowText(m_csName);

	CDialog::OnOK();
}
