#pragma once

#include "Resource.h"
#include "LandMarkDB.h"

// CPathSettingsFormView form view

class CPathSettingsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPathSettingsFormView)

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sDBErrorMsg; 

	BOOL m_bInitialized;

	CString m_csExportPath;
	CString m_csImportPath;
	CString m_csProfilePath;
	CString m_csRawDataPath;
	CString m_csCustomPath;

	CString m_csLblDll;
	CString m_csLblDllInfo;
	CString m_csLblExport;
	CString m_csLblImport;
	CString m_csLblProfile;
	CString m_csLblRawData;
	CString m_csLblCustom;

	
	CLandMarkDB* m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;

	CXTPPropertyGridItem* m_pItemExportPath;
	CXTPPropertyGridItem* m_pItemImportPath;
	CXTPPropertyGridItem* m_pItemProfilePath;
	CXTPPropertyGridItem* m_pItemRawDataPath;
	CXTPPropertyGridItem* m_pItemCustomPath;

	CStatic m_wndPathHolder;
	CXTPPropertyGrid m_wndPropertyGrid;

protected:
	CPathSettingsFormView();           // protected constructor used by dynamic creation
	virtual ~CPathSettingsFormView();

	void setLanguage(void);

	BOOL getRegistryValues(void);

public:
	enum { IDD = IDD_FORMVIEW16 };	
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
public:
	afx_msg LRESULT OnValueChanged(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnInitialUpdate();
	afx_msg void OnSetFocus(CWnd* pOldWnd);
};


