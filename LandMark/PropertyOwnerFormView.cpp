// PropertyOwnerFormView.cpp : implementation file
//

#include "stdafx.h"
#include "PropertyOwnerFormView.h"
#include "ContactsTabView.h"
#include "ContactsFormView.h"

#include "PropertyTabView.h"

// CPropertyOwnerFormView

IMPLEMENT_DYNCREATE(CPropertyOwnerFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPropertyOwnerFormView, CXTResizeFormView)
	ON_WM_SIZE()
//	ON_WM_ERASEBKGND()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
//	ON_BN_CLICKED(IDC_BUTTON2_1, OnBnClickedButton1)
	ON_BN_CLICKED(ID_TOOLS_ADD_CONTACT, OnBnClickedButton1)
	ON_BN_CLICKED(ID_TOOLS_REMOVE_CONTACT, OnBnClickedButton2)
	
	ON_NOTIFY(NM_CLICK, IDC_PROP_OWNER_REPORT, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, IDC_PROP_OWNER_REPORT, OnReportItemDblClick)
	ON_NOTIFY(NM_KEYDOWN, IDC_PROP_OWNER_REPORT, OnReportKeyDown)
END_MESSAGE_MAP()

CPropertyOwnerFormView::CPropertyOwnerFormView()
	: CXTResizeFormView(CPropertyOwnerFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CPropertyOwnerFormView::~CPropertyOwnerFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;
	SaveReportState();
}

void CPropertyOwnerFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP
}

BOOL CPropertyOwnerFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CPropertyOwnerFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
  m_wndReport1.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void CPropertyOwnerFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	//SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport1();

		setLanguage();

		m_bInitialized = TRUE;

		LoadReportState();
	}

}

BOOL CPropertyOwnerFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CPropertyOwnerFormView::OnSetFocus(CWnd* pOldWnd)
{
/*
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pTabView)
	{
		CPropertyFormView *pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView)
		{
			BOOL bStartPrev = pView->getStartPrevStatus();
			BOOL bEndNext = pView->getEndNextStatus();
			// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,bStartPrev);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,bStartPrev);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,bEndNext);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,bEndNext);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
		}

	}
*/
	CXTResizeFormView::OnSetFocus(pOldWnd);
}

void CPropertyOwnerFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		CXTPReportColumn *pCol = pItemNotify->pColumn;
		if (pCol != NULL)
		{
			if (pCol->GetItemIndex() == COLUMN_2)
			{
				CXTPReportRow *pRow = pItemNotify->pRow;

				if (pRow != NULL)
				{
					if (pRow->GetTreeDepth() == 0)
					{
						XTP_REPORTRECORDITEM_ARGS itemArgs(&m_wndReport1, pItemNotify->pRow, pItemNotify->pColumn);
						m_wndReport1.EditItem(&itemArgs);
					}	// if (pRow->GetTreeDepth() == 0)
				}	// if (pRow != NULL)
			}
		}	// if (pCol != NULL)
	}	// if (pItemNotify != NULL)
}

void CPropertyOwnerFormView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pItem != NULL)
	{
		m_wndReport1.CollapseAll();
		showFormView(IDD_FORMVIEW8,m_sLangFN,0);
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3); 
		if (pTabView)
		{
			CPropOwnerReportRec *pRec = (CPropOwnerReportRec*)pItemNotify->pItem->GetRecord();
			if (pRec != NULL)
			{
				CContactsFormView *pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
				if (pView)
				{
					pView->doPopulateOnContactID(pRec->getContactID());
				}
			}
		}
	}
	
}

void CPropertyOwnerFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!m_wndReport1.GetFocusedRow())
		return;

	if (lpNMKey->nVKey == VK_SPACE || lpNMKey->nVKey == VK_F2)
	{
		CXTPReportColumn *pColumn = m_wndReport1.GetFocusedColumn();
		CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
		if (pColumn != NULL && pRow != NULL)
		{
			if (pColumn->GetIndex() == 2)
			{
				XTP_REPORTRECORDITEM_ARGS itemArgs(&m_wndReport1, pRow, pColumn);
				m_wndReport1.EditItem(&itemArgs);
			}	// if (pColumn->GetIndex() == 2)
		}	// if (pColumn != NULL && pRow != NULL)
	}	// if (lpNMKey->nVKey == VK_SPACE || lpNMKey->nVKey == VK_F2)
}


// CPropertyOwnerFormView diagnostics

#ifdef _DEBUG
void CPropertyOwnerFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CPropertyOwnerFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CPropertyOwnerFormView message handlers

void CPropertyOwnerFormView::setLanguage(void)
{

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sOKBtn = xml.str(IDS_STRING250/*240*/);
			m_sCancelBtn = xml.str(IDS_STRING251/*241*/);

			m_sErrCap = xml.str(IDS_STRING240/*213*/);
	
			m_sMsgCap = xml.str(IDS_STRING329/*273*/);
			m_sDeleteMsg = xml.str(IDS_STRING330/*274*/);

			m_sSaveMsg.Format(_T("%s\n\n%s\n"),
								xml.str(IDS_STRING331/*2770*/),
								xml.str(IDS_STRING242/*237*/));

			m_sDoneSavingMsg =	xml.str(IDS_STRING332/*2771*/);

			m_sName	= xml.str(IDS_STRING271/*222*/);
			m_sCompany	= xml.str(IDS_STRING284/*223*/);
			m_sOwnerShare	= xml.str(IDS_STRING327/*263*/);

			m_sAlreadyRegOwnerMsg.Format(_T("%s\n%s"),
								xml.str(IDS_STRING333/*2750*/),
								xml.str(IDS_STRING334/*2751*/));

			m_sMsgRemovePropOwner1 = xml.str(IDS_STRING335/*3330*/);
			m_sMsgRemovePropOwner2 = xml.str(IDS_STRING336/*3331*/);

			xml.clean();
		}
	}

}

BOOL CPropertyOwnerFormView::getPropOwners(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getPropOwners(m_vecPropOwnersData))
				bReturn = TRUE;
			
		}	// if (m_pDB != NULL)
	}
	return bReturn;
}

BOOL CPropertyOwnerFormView::getPropOwnersForProperty(void)
{
	BOOL bReturn = FALSE;
	CTransaction_property recProp;
	CPropertyTabView *pView = (CPropertyTabView*)getFormViewByID(IDD_FORMVIEW11);
	if (pView != NULL)
	{
		CPropertyFormView* pView0 = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView0 != NULL)
		{
			recProp = pView0->getActiveProperty();
			// Get Owners for property, from database; 060317 p�d
			if (m_pDB != NULL)
			{
				m_pDB->getPropOwnersForProperty(recProp.getID(),m_vecPropOwnersForPropertyData);	
				bReturn = TRUE;
				
			}	// if (m_pDB != NULL)
		}
	}
	
	return bReturn;
}

// PROTECTED METHODS
BOOL CPropertyOwnerFormView::setupReport1(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_PROP_OWNER_REPORT,FALSE,FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sAddress			= (xml.str(IDS_STRING273/*266*/));
				m_sPostAddress	= (xml.str(IDS_STRING275/*267*/));
				m_sPhoneWork.Format(_T("%s %s"), xml.str(IDS_STRING278),xml.str(IDS_STRING279/*268*/));
				m_sPhoneHome.Format(_T("%s %s"), xml.str(IDS_STRING278),xml.str(IDS_STRING280/*269*/));
				m_sMobile				= (xml.str(IDS_STRING281/*270*/));
				m_sFax					= (xml.str(IDS_STRING246/*271*/));

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING271/*222*/)), 150));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING272/*223*/)), 200));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING272/*290*/)), 50));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING327/*263*/)), 40));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport1.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();
				m_wndReport1.GetColumns()->Find(0)->SetTreeColumn(TRUE);
			}
			xml.clean();
		}	// if (fileExists(sLangFN))
	}

	return TRUE;

}

void CPropertyOwnerFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	if (m_wndReport1.GetSafeHwnd())
	{
		setResize(&m_wndReport1,1,2,cx - 2,cy - 4);
	}

}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPropertyOwnerFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	int nIndex = -1;
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pTabView)
	{
		CXTPTabManagerItem *pManager = pTabView->getTabControl()->getSelectedTabPage();
		nIndex = pManager->GetIndex();
	}

	switch (wParam)
	{
		case ID_DELETE_ITEM :
		{
			// Check which tab we are on; 080623 p�d
			if (nIndex == 1)
			{
				removePropOwner();
			}
			break;
		}	// case ID_NEW_ITEM :
	}	// switch (wParam)

	return 0L;
}

PROP_OWNERS_FOR_PROPERTY_DATA* CPropertyOwnerFormView::getPropOwnerForPropertyData(int prop_id,int contact_id)
{

	if (m_vecPropOwnersForPropertyData.size() > 0)
	{
		for (UINT i = 0;i < m_vecPropOwnersForPropertyData.size();i++)
		{
			dataPOPD = m_vecPropOwnersForPropertyData[i];
			if (dataPOPD.getPropID() == prop_id && dataPOPD.getContactData().getID() == contact_id)
			{
				return &dataPOPD;
			}
		}	// for (UINT i = 0;i < m_vecPropOwnersForPropertyData.size();i++)
	}	// if (m_vecPropOwnersForPropertyData.size() > 0)

	return NULL;
}

// Handle transaction on database species table; 060317 p�d

BOOL CPropertyOwnerFormView::populateReport(void)
{
	int nPropID;
	int nIsContact;
	CString sOwnerShare;
	CTransaction_contacts data;
	CPropOwnerReportRec *pRecord;
	getPropOwnersForProperty();
	// populate report; 060317 p�d
	m_wndReport1.ResetContent();

	if (m_vecPropOwnersForPropertyData.size() > 0)
	{
		for (UINT i = 0;i < m_vecPropOwnersForPropertyData.size();i++)
		{
			nPropID = m_vecPropOwnersForPropertyData[i].getPropID();
			nIsContact = m_vecPropOwnersForPropertyData[i].getIsContact();
			sOwnerShare	 = m_vecPropOwnersForPropertyData[i].getOwnerShare();
			data = m_vecPropOwnersForPropertyData[i].getContactData();
			pRecord = new CPropOwnerReportRec(i,nPropID,
																				 data.getID(),
																				 nIsContact,
																				 data.getName(),
																				 data.getCompany(),
																				 sOwnerShare,
																				 m_sLangFN,
																				 m_wndReport1.GetSafeHwnd());
			m_wndReport1.AddRecord(pRecord);
			CString sPostAddress;
			sPostAddress.Format(_T("%s %s"),
													data.getPostNum(),
													data.getPostAddress());
			pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sAddress + _T(" : ") + data.getAddress()));
			pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sPostAddress  + _T(" : ") + sPostAddress));
			pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sPhoneWork  + _T(" : ") + data.getPhoneWork()));
			pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sPhoneHome  + _T(" : ") + data.getPhoneHome()));
			pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sMobile  + _T(" : ") + data.getMobile()));
			pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sFax  + _T(" : ") + data.getFaxNumber()));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)
	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	return TRUE;
}

BOOL CPropertyOwnerFormView::removePropOwner(void)
{
	CTransaction_prop_owners_for_property_data *dataPOPD;
	CTransaction_prop_owners dataPOD;
	CString sMsg;
	int nIndex = -1;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
		if (pRow != NULL)
		{
			CPropOwnerReportRec* pRec = (CPropOwnerReportRec*)pRow->GetRecord();
			if (m_pDB != NULL && pRec != NULL)
			{
				dataPOPD = getPropOwnerForPropertyData(pRec->getPropID(),pRec->getContactID());
				if (dataPOPD != NULL)
				{
					sMsg.Format(_T("%s\n\n%s : %s\n%s : %s\n%s : %s\n\n%s"),
										m_sMsgCap,
										m_sName,
										dataPOPD->getContactData().getName(),
										m_sCompany,
										dataPOPD->getContactData().getCompany(),
										m_sOwnerShare,
										dataPOPD->getOwnerShare(),
										m_sDeleteMsg);

					if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					{
						dataPOD = CTransaction_prop_owners(pRec->getPropID(),pRec->getContactID(),_T(""),0,_T(""));
						// Delete this property owner
						m_pDB->removePropOwner(dataPOD);
						
						populateReport();
						bReturn = TRUE;
					}	// if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					
					CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
					if (pTabView)
					{
						CPropertyFormView *pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
						if (pView)
						{
							pView->refreshNavButtons();				
							pView = NULL;
						}	// if (pView)
						pTabView = NULL;
					}	// if (pTabView)
					

				}	// if (dataPOPD != NULL)
			}	// if (m_pDB != NULL)
		}
	}
	return bReturn;
}

BOOL CPropertyOwnerFormView::addPropOwner(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		m_wndReport1.AddRecord(new CPropOwnerReportRec());
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow)
			{
				pRow->SetSelected(TRUE);
			}
		}
		return TRUE;
	}
	return TRUE;
}

// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CPropertyOwnerFormView::isDataChanged(void)
{
	savePropOwner();
	m_wndReport1.setIsDirty(FALSE);

	return TRUE;
}

BOOL CPropertyOwnerFormView::savePropOwner(void)
{
	CTransaction_prop_owners rec;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Add records from Report to vector; 060317 p�d
			CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
			if (pRecs->GetCount() > 0)
			{

				for (int i = 0;i < pRecs->GetCount();i++)
				{
					CPropOwnerReportRec *pRec = (CPropOwnerReportRec *)pRecs->GetAt(i);

					rec = CTransaction_prop_owners(pRec->getPropID(),
						pRec->getContactID(),
						pRec->getColumnText(COLUMN_3),
						(pRec->getColumnCheck(COLUMN_2) ? 1 : 0),
						_T(""));

					// Do it like this.
					// First remove ALL owners for this property (ID).
					if (i == 0)	// We only need to do this once; 080609 p�d
						m_pDB->removePropOwner(rec.getPropID());


					// Then, add new/updates owners; 070125 p�d
					m_pDB->addPropOwner(rec);
					
				
				}	// for (int i = 0;i < pRecs->GetCount();i++)

			}	// if (pRecs->GetCount() > 0)
			m_wndReport1.setIsDirty(FALSE);
			// Tell user that data's been saved; 070126 p�d
	/*	Commented out 2007-11-05 P�D
			Don't tell user, just do it!
			::MessageBox(0,m_sDoneSavingMsg,m_sErrCap,MB_ICONSTOP | MB_OK);
	*/
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

BOOL CPropertyOwnerFormView::setPropOwner(CTransaction_contacts &data)
{
	CString S;
	CTransaction_property dataProp;
	CTransaction_prop_owners dataPropOwner;
	int nNumOfRecs;
	CPropOwnerReportRec *pRecord = NULL;
	if (m_bConnected)
	{
		CPropertyTabView *pView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
		if (pView)
		{
			//CPropertyFormView *pProp = (CPropertyFormView*)pView->getTabControl()->getTabPage(0);
			CPropertyFormView *pProp = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pProp != NULL)
			{
				dataProp = pProp->getActiveProperty();
				//if (dataProp != NULL)
				//{
				// Save property owner to database; 070126 p�d
				if (m_pDB != NULL)
				{
					dataPropOwner = CTransaction_prop_owners(dataProp.getID(),
						data.getID(),
						_T(""),
						0,
						_T(""));

					if (!m_pDB->addPropOwner(dataPropOwner))
					{
						CString sMsg;
						sMsg.Format(_T("%s\n\n%s"),data.getName(),m_sAlreadyRegOwnerMsg);
						::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONASTERISK | MB_OK);
						return FALSE;
					}
					else
					{
						// Only add owner, if we have a valid property data in TabView tabpage; 070125 p�d
						nNumOfRecs = m_wndReport1.GetRecords()->GetCount();
						pRecord = new CPropOwnerReportRec(nNumOfRecs+1,
							dataProp.getID(),
							data.getID(),
							0,
							data.getName(),
							data.getCompany(),
							_T(""),
							m_sLangFN,
							m_wndReport1.GetSafeHwnd());

						m_wndReport1.AddRecord(pRecord);
						CString sPostAddress;
						sPostAddress.Format(_T("%s %s"),
							data.getPostNum(),
							data.getPostAddress());
						pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sAddress + _T(" : ") + data.getAddress()));
						pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sPostAddress  + _T(" : ") + sPostAddress));
						pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sPhoneWork  + _T(" : ") + data.getPhoneWork()));
						pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sPhoneHome  + _T(" : ") + data.getPhoneHome()));
						pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sMobile  + _T(" : ") + data.getMobile()));
						pRecord->GetChilds()->Add(new CPropOwnerReportChildRec(m_sFax  + _T(" : ") + data.getFaxNumber()));

						m_wndReport1.Populate();
						m_wndReport1.UpdateWindow();
					}	// else

					// Reload property owners for property after 
					// a new proeprty owner's added; 070129 p�d
					getPropOwnersForProperty();

					}	// if (m_pDB != NULL)
				//}	// if (dataProp != NULL)
			}	// if (pManager)
		}	// if (pView)
		
	}

	return TRUE;
}

void CPropertyOwnerFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_PROPOWNERS_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		m_wndReport1.SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;

}

void CPropertyOwnerFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	m_wndReport1.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_PROPOWNERS_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

}
// Add Contact
void CPropertyOwnerFormView::OnBnClickedButton1()
{
	showFormView(IDD_REPORTVIEW1,m_sLangFN,1);
	
}
// Remove PropertyOwner from Property; 100127 p�d
void CPropertyOwnerFormView::OnBnClickedButton2()
{
	CString sMsg;
	CXTPReportRow *pRow = m_wndReport1.GetFocusedRow();
	CPropOwnerReportRec *pRec = NULL;
	if (pRow && m_pDB)
	{
		pRec = (CPropOwnerReportRec*)pRow->GetRecord();
		if (pRec)
		{
			sMsg.Format(L"%s : \'%s\', %s",m_sMsgRemovePropOwner1,pRec->getOwnerName(),m_sMsgRemovePropOwner2);
			if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				m_pDB->removePropOwner(pRec->getPropID(),pRec->getContactID());
				populateReport();
				
			}
		}
	}
}
