// PathSettingsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "PathSettingsFormView.h"
#include "MDISettingsFrame.h"
#include "CustomItems.h"


// CPathSettingsFormView

IMPLEMENT_DYNCREATE(CPathSettingsFormView, CXTResizeFormView)

CPathSettingsFormView::CPathSettingsFormView()
	: CXTResizeFormView(CPathSettingsFormView::IDD)
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_bInitialized = FALSE;

	m_csExportPath = _T("");
	m_csImportPath = _T("");
	m_csProfilePath = _T("");
	m_csRawDataPath = _T("");
	m_csCustomPath = _T("");

	m_csLblDll = _T("");
	m_csLblDllInfo = _T("");
	m_csLblExport = _T("");
	m_csLblImport = _T("");
	m_csLblProfile = _T("");
	m_csLblRawData = _T("");
	m_csLblCustom = _T("");
}

CPathSettingsFormView::~CPathSettingsFormView()
{
	if(m_pDB != NULL)
		delete m_pDB;
}

void CPathSettingsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_PATHHOLDER, m_wndPathHolder);
}

BEGIN_MESSAGE_MAP(CPathSettingsFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnValueChanged)
END_MESSAGE_MAP()


// CPathSettingsFormView diagnostics

#ifdef _DEBUG
void CPathSettingsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPathSettingsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CPathSettingsFormView message handlers

BOOL CPathSettingsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;
}

void CPathSettingsFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CPathSettingsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	if(!m_bInitialized)
	{
		setLanguage();

		// get the size of the placeholder, this will be used when creating the grid.
		CRect rc;
		m_wndPathHolder.GetWindowRect( &rc );
		ScreenToClient( &rc );

		if(m_wndPropertyGrid.Create(rc, this, IDC_PATHSETTINGS_GRID))
		{
			m_wndPropertyGrid.SetTheme(xtpGridThemeOffice2003);
			m_wndPropertyGrid.SetVariableItemsHeight(TRUE);

			getRegistryValues();

			//dll paths
			CXTPPropertyGridItem* pPaths = m_wndPropertyGrid.AddCategory(m_csLblDll);
			pPaths->SetDescription(m_csLblDllInfo);

			//add child items
			m_pItemExportPath = pPaths->AddChildItem(new CCustomItemFileBox(m_csLblExport, m_csExportPath));
			
			m_pItemImportPath = pPaths->AddChildItem(new CCustomItemFileBox(m_csLblImport, m_csImportPath));
			
			m_pItemProfilePath = pPaths->AddChildItem(new CCustomItemFileBox(m_csLblProfile, m_csProfilePath));
			
			m_pItemRawDataPath = pPaths->AddChildItem(new CCustomItemFileBox(m_csLblRawData, m_csRawDataPath));
			
			m_pItemCustomPath = pPaths->AddChildItem(new CCustomItemFileBox(m_csLblCustom, m_csCustomPath));
			
			pPaths->Expand();

		}

		// set the resize
		SetResize(IDC_PATHSETTINGS_GRID, SZ_TOP_LEFT, SZ_BOTTOM_RIGHT);
		
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

		m_bInitialized = TRUE;		
	}


	UpdateData(FALSE);
}

void CPathSettingsFormView::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_csLblDll = xml->str(IDS_STRING364);
			m_csLblDllInfo = xml->str(IDS_STRING365);
			m_csLblExport = xml->str(IDS_STRING366);
			m_csLblImport = xml->str(IDS_STRING367);
			m_csLblProfile = xml->str(IDS_STRING368);
			m_csLblRawData = xml->str(IDS_STRING369);
			m_csLblCustom = xml->str(IDS_STRING370);
			m_sDBErrorMsg = xml->str(IDS_STRING105);
		}
		delete xml;
	}
}

BOOL CPathSettingsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	if(pData->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData, pData->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


void CPathSettingsFormView::OnSetFocus(CWnd* pOldWnd)
{
	CXTResizeFormView::OnSetFocus(pOldWnd);

	// turn off all imagebuttons in the main window
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
}



LRESULT CPathSettingsFormView::OnValueChanged(WPARAM wParam, LPARAM lParam)
{
	int nGridAction = (int)wParam;
	CXTPPropertyGridItem* pItem = (CXTPPropertyGridItem*)lParam;
	ASSERT(pItem);

	HKEY hKeyCU = NULL;
	TCHAR szPath[BUFSIZ];
	DWORD dwPathSize = BUFSIZ;
	BOOL bSaveOk = FALSE;

	//open key
	if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_TCRUISE_OPTIONS_PATH_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyCU) == ERROR_SUCCESS)
	{
		bSaveOk = TRUE;
	}

	switch (nGridAction)
	{
		case XTP_PGN_ITEMVALUE_CHANGED:
		{
			if(pItem == m_pItemExportPath)
			{
				if(bSaveOk)
				{
					dwPathSize = _stprintf_s(szPath, m_pItemExportPath->GetValue());
					dwPathSize *= sizeof(TCHAR);
					if(dwPathSize >= 0)
						RegSetValueEx(hKeyCU,REG_TCRUISE_EXPORTDLL_ITEM, NULL,REG_SZ, (LPBYTE)szPath, dwPathSize);
				}
			}
			else if(pItem == m_pItemImportPath)
			{
				if(bSaveOk)
				{
					dwPathSize = _stprintf_s(szPath, m_pItemImportPath->GetValue());
					dwPathSize *= sizeof(TCHAR);
					if(dwPathSize >= 0)
						RegSetValueEx(hKeyCU,REG_TCRUISE_IMPORTDLL_ITEM, NULL,REG_SZ, (LPBYTE)szPath, dwPathSize);
				}
			}
			else if(pItem == m_pItemProfilePath)
			{
				if(bSaveOk)
				{
					dwPathSize = _stprintf_s(szPath, m_pItemProfilePath->GetValue());
					dwPathSize *= sizeof(TCHAR);
					if(dwPathSize >= 0)
						RegSetValueEx(hKeyCU,REG_TCRUISE_PROFILEDLL_ITEM, NULL,REG_SZ, (LPBYTE)szPath, dwPathSize);
				}
			}
			else if(pItem == m_pItemRawDataPath)
			{
				if(bSaveOk)
				{
					dwPathSize = _stprintf_s(szPath, m_pItemRawDataPath->GetValue());
					dwPathSize *= sizeof(TCHAR);
					if(dwPathSize >= 0)
						RegSetValueEx(hKeyCU,REG_TCRUISE_RAWDATADLL_ITEM, NULL,REG_SZ, (LPBYTE)szPath, dwPathSize);
				}
			}
			else if(pItem == m_pItemCustomPath)
			{
				if(bSaveOk)
				{
					dwPathSize = _stprintf_s(szPath, m_pItemCustomPath->GetValue());
					dwPathSize *= sizeof(TCHAR);
					if(dwPathSize >= 0)
						RegSetValueEx(hKeyCU,REG_TCRUISE_CUSTOMDLL_ITEM, NULL,REG_SZ, (LPBYTE)szPath, dwPathSize);
				}
			}
		}
		break;
	}

	if(hKeyCU != NULL)
		RegCloseKey(hKeyCU);

	return FALSE;
}

//getRegistryValues
BOOL CPathSettingsFormView::getRegistryValues()
{
	BOOL bRet = TRUE;

	HKEY hKeyCU = NULL;
	TCHAR szPath[BUFSIZ];
	DWORD dwPathSize = BUFSIZ;

	//open key
	if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_TCRUISE_OPTIONS_PATH_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyCU) == ERROR_SUCCESS)
	{
		//export dll
		szPath[0] = '\0';
		dwPathSize = BUFSIZ;
		if(RegQueryValueEx(hKeyCU, REG_TCRUISE_EXPORTDLL_ITEM, NULL, NULL, (LPBYTE)szPath, &dwPathSize) == ERROR_SUCCESS)
		{
			m_csExportPath = szPath;
		}

		//import dll
		szPath[0] = '\0';
		dwPathSize = BUFSIZ;
		if(RegQueryValueEx(hKeyCU, REG_TCRUISE_IMPORTDLL_ITEM, NULL, NULL, (LPBYTE)szPath, &dwPathSize) == ERROR_SUCCESS)
		{
			m_csImportPath = szPath;
		}

		//profile dll
		szPath[0] = '\0';
		dwPathSize = BUFSIZ;
		if(RegQueryValueEx(hKeyCU, REG_TCRUISE_PROFILEDLL_ITEM, NULL, NULL, (LPBYTE)szPath, &dwPathSize) == ERROR_SUCCESS)
		{
			m_csProfilePath = szPath;
		}

		//raw data dll
		szPath[0] = '\0';
		dwPathSize = BUFSIZ;
		if(RegQueryValueEx(hKeyCU, REG_TCRUISE_RAWDATADLL_ITEM, NULL, NULL, (LPBYTE)szPath, &dwPathSize) == ERROR_SUCCESS)
		{
			m_csRawDataPath = szPath;
		}

		//custom dll
		szPath[0] = '\0';
		dwPathSize = BUFSIZ;
		if(RegQueryValueEx(hKeyCU, REG_TCRUISE_CUSTOMDLL_ITEM, NULL, NULL, (LPBYTE)szPath, &dwPathSize) == ERROR_SUCCESS)
		{
			m_csCustomPath = szPath;
		}
	}

		if(hKeyCU != NULL)
		RegCloseKey(hKeyCU);

	return bRet;
}

