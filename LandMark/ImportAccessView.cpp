// ImportAccessView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "ImportAccessView.h"
#include "TractNameDlg.h"


// CImportAccessView

IMPLEMENT_DYNCREATE(CImportAccessView, CFormView)

CImportAccessView::CImportAccessView()
	: CFormView(CImportAccessView::IDD)
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_bStandsOk = FALSE;
	m_bStandsFailed = FALSE;
	m_bCruiseIsAuditType = FALSE;
}

CImportAccessView::~CImportAccessView()
{
	if(m_pDB != NULL)
	{
		delete m_pDB;		
	}
}

void CImportAccessView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LBL_OK, m_wndLblOk);
//	DDX_Control(pDX, IDC_LBL_FAILED, m_wndLblFailed);
	DDX_Control(pDX, IDC_EDIT_OK, m_wndEditStandOk);
//	DDX_Control(pDX, IDC_EDIT_FAILED, m_wndEditStandFailed);
}

BEGIN_MESSAGE_MAP(CImportAccessView, CFormView)
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BTN_IMPORT_OK, &CImportAccessView::OnBnClickedBtnImportOk)
END_MESSAGE_MAP()


// CImportAccessView diagnostics

#ifdef _DEBUG
void CImportAccessView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CImportAccessView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CImportAccessView message handlers

BOOL CImportAccessView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	if(pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memset(&m_dbConnectionData, 0x00, sizeof(DB_CONNECTION_DATA));
		memcpy(&m_dbConnectionData, pCopyDataStruct->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}

	return CFormView::OnCopyData(pWnd, pCopyDataStruct);
}



BOOL CImportAccessView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void CImportAccessView::OnDestroy()
{
	if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
	{
		m_wndProgressCtrl.DestroyWindow();
	}
	
	CFormView::OnDestroy();
}


LRESULT CImportAccessView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{

	return 0L;
}


void CImportAccessView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

	CString csTitle, csErrorMsg;
	CString buf_filename;
	

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sDBErrorMsg = xml->str(IDS_STRING105);
			csTitle = xml->str(IDS_STRING167);
			csErrorMsg = xml->str(IDS_STRING168);
			m_csMsg1 = xml->str(IDS_STRING169);
			m_csMsg2 = xml->str(IDS_STRING170);
			m_csMsg3 = xml->str(IDS_STRING171);
			m_csErrMsgStand = xml->str(IDS_STRING204);
			m_csProgress = xml->str(IDS_STRING216);
			m_csInitErrMsg = xml->str(IDS_STRING217);
			m_csStrLbl = xml->str(IDS_STRING220);
			m_csStrFailed = xml->str(IDS_STRING221);
			m_csStrOk = xml->str(IDS_STRING222);
			m_csStrInto = xml->str(IDS_STRING223);
		}
		delete xml;
	}

	if(m_pDB == NULL)	//check database connection
	{
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}
	else
	{
		//check if we gets a valid FileIndex
		if(m_pDB->getLastFileIndex() == -2)
		{
			AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
			return;
		}
	}

	if(!m_wndProgressCtrl.Create(CProgressDialog::IDD, this))
	{
		AfxMessageBox(m_csInitErrMsg);
	}


	CFileDialog *pOpenFileDlg = new CFileDialog(TRUE);

	pOpenFileDlg->GetOFN().lpstrTitle = csTitle;

	pOpenFileDlg->GetOFN().lpstrFile = buf_filename.GetBuffer(2048*(_MAX_PATH + 1) +1);
	pOpenFileDlg->GetOFN().nMaxFile = 2048;
	

	if(!pOpenFileDlg)
	{
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}

	pOpenFileDlg->m_ofn.lpstrFilter = _T("Access Database (*.mdb)\0*.mdb\0");
	pOpenFileDlg->m_ofn.Flags = OFN_ALLOWMULTISELECT|OFN_EXPLORER|OFN_HIDEREADONLY;

	if(pOpenFileDlg->DoModal() != IDOK)
	{
		buf_filename.ReleaseBuffer();
		delete pOpenFileDlg;
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}
	
	POSITION pos;
	CString csFile;

	m_caFilePaths.RemoveAll();
	m_caStandOk.RemoveAll();
	m_caStandFailed.RemoveAll();

	m_csLogStr = _T("");

	::UpdateWindow(AfxGetMainWnd()->GetSafeHwnd());

	AfxGetApp()->BeginWaitCursor();
	
	pos = pOpenFileDlg->GetStartPosition();
	while(pos)
	{
		csFile = pOpenFileDlg->GetNextPathName(pos);
		m_caFilePaths.Add(csFile);			
	}

	buf_filename.ReleaseBuffer();
	delete pOpenFileDlg;
	
	// Turn off autocommit - in case of a rollback
	m_pDB->setAutoCommitStatus(FALSE);

	for(int i=0; i<m_caFilePaths.GetSize(); i++)
	{
		m_bCruiseIsAuditType = FALSE;
		//convert data
		if(convertDataToSQL(m_caFilePaths.GetAt(i)) == FALSE)
		{
			m_bStandsFailed = TRUE;
			m_caStandFailed.Add(m_caFilePaths.GetAt(i));
			//break;
		}
		else
		{
			m_bStandsOk = TRUE;
		}
	}

	// Turn on autocommit 
	m_pDB->setAutoCommitStatus(TRUE);

	setupList();

	AfxGetApp()->EndWaitCursor();

}

void CImportAccessView::setupList()
{
	//send msg to update stand view
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
		(LPARAM)&_doc_identifer_msg(MODULE923, MODULE920, _T(""), 1, 0, 0));

	CString csTmp = _T(""), csStr = _T("");

	m_wndEditStandFailed.ShowWindow(SW_HIDE);
		m_wndLblFailed.ShowWindow(SW_HIDE);
		m_wndLblOk.SetWindowText(m_csStrLbl);
		m_wndEditStandOk.SetWindowText(m_csLogStr);

}


BOOL CImportAccessView::convertDataToSQL(LPCTSTR database)
{
	BOOL bRes = TRUE;
	CString csSQL, csTract = _T("");
	SAConnection con;
	SACommand cmd;
	int fileindex = 0;
	CString csMsg;
	CString csName;
	CString csStrTable;
	CString csTmp;
		
	if(m_pDB == NULL)	//check database connection
	{
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		return FALSE;
	}

	csSQL.Format(_T("Driver={Microsoft Access Driver (*.mdb)}; Dbq=%s"), database);
	
	try
	{
		
		//connect to access db
		con.Connect((SAString)csSQL, _T(""), _T(""), SA_ODBC_Client);


		//get next fileindex from SQL db
		m_nFileIndex = getNextFileIndex();
		
		csTract = _T("");
		//check if Tract allready exists in SQL db
		if(getData(con, cmd, _T("Stand")) > 0)	//??== TRUE)
		{
			while(cmd.FetchNext())
			{
				csTract = (CString)cmd.Field(2).asString();
			}

TractNameLoop:
			if(csTract == _T(""))
			{
				//needs to enter tract name
				CTractNameDlg *dlg = new CTractNameDlg;
				csName = _T("");
				dlg->m_csPath = database;

				if(dlg->DoModal() != IDOK)
				{
					delete dlg;
					con.Disconnect();
					return FALSE;
				}

				csName = dlg->m_csName;
				delete dlg;
				if(csName == _T(""))
				{
					con.Disconnect();
					return FALSE;
				}
				else
					csTract = csName;							

			}

			if((fileindex = m_pDB->tractExists(csTract)) > 0)
			{
				int res;
				//Tract exists, ask if to update
				csMsg.Format(_T("%s\r\n\r\n%s"),csTract, m_csMsg1);
				if((res = AfxMessageBox(csMsg, MB_ICONEXCLAMATION|MB_YESNOCANCEL)) == IDCANCEL)
				{
					//csMsg.Format(_T("%s\r\n\r\n%s"),csTract, m_csMsg2);
					//AfxMessageBox(csMsg);
					con.Disconnect();
					return TRUE;						
				}
				else if(res == IDNO)
				{
					csTract = _T("");
					goto TractNameLoop;

				}
				else
				{
					//if yes, delete from db and update with same fileindex
					if(m_pDB->removeDataFromDB(fileindex) == TRUE)
						m_nFileIndex = fileindex;
					else
					{
						csTmp.Format(_T("%s%s\r\n\r\n"), m_csStrFailed, database);
						m_csLogStr += csTmp;

						con.Disconnect();
						return FALSE;
					}
				}
			}//if((fileindex = m_pDB->tractExists(csTract)) > 0)
			/*			}//if(cmd.FetchNext())
			else
			{
			con.Disconnect();
			return FALSE;
			}*/
		}//if(getData(con, cmd, _T("Stand")) == TRUE)
		else
		{
			CString csMsg;
			csMsg.Format(_T("%s\r\n\r\n%s"), database, m_csErrMsgStand);
			AfxMessageBox(csMsg, MB_ICONERROR);
			con.Disconnect();

			csTmp.Format(_T("%s%s\r\n\r\n"), m_csStrFailed, database);
			m_csLogStr += csTmp;

			return FALSE;
		}

		AfxGetApp()->BeginWaitCursor();
		::UpdateWindow(AfxGetMainWnd()->GetSafeHwnd());

		int pos;
		bRes = FALSE;
		while(m_nFileIndex >= 0)
		{
			bRes = TRUE;
			pos = 0;
			if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
			{
				m_wndProgressCtrl.ShowWindow(SW_NORMAL);
				m_wndProgressCtrl.setProgressPos(0);
				m_wndProgressCtrl.SetWindowText(csTract);
				m_wndProgressCtrl.setStep(1);
			}

			for(int i=0; i<NUM_OF_DB_TABLES; i++)
			{
				// Process window messages
				doEvents();
				
				if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
				{
					csStrTable.Format(_T("%s (%d/%d)"), m_csProgress, i+1, NUM_OF_DB_TABLES);					
					m_wndProgressCtrl.setLbl1(csStrTable);
					m_wndProgressCtrl.setLbl2(DBTables[i].oldTable);
					m_wndProgressCtrl.setProgressPos(0);
				}

				if((pos = getData(con, cmd, DBTables[i].oldTable)) > 0)
				{
					if(m_wndProgressCtrl.GetSafeHwnd() != NULL)
					{
						m_wndProgressCtrl.setRange(pos);
					}

					if(setData(cmd, DBTables[i].newTable, csTract, m_wndProgressCtrl) == FALSE)
					{
						bRes = FALSE;
						break;
					}

					if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
					{
						m_wndProgressCtrl.setProgressPos(pos);
					}
				}

				if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
				{
					m_wndProgressCtrl.setFullBar();
				}
				
				::UpdateWindow(AfxGetMainWnd()->GetSafeHwnd());
			}	//for(int i=0; i<NUM_OF_DB_TABLES; i++)

			if(bRes == FALSE)
				break;

			doCommit();	//only commit if all ok
			break;
		}

		if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
		{
			m_wndProgressCtrl.ShowWindow(SW_HIDE);
		}

		AfxGetApp()->EndWaitCursor();

	}
	catch(SAException &x)
	{
		if (m_wndProgressCtrl.GetSafeHwnd() != NULL)
		{
			m_wndProgressCtrl.ShowWindow(SW_HIDE);
		}

		csMsg.Format(_T("%s\r\n\r\n%s"),csTract, x.ErrText());
		AfxMessageBox(csMsg);
		bRes = FALSE;
	}

	if(bRes == FALSE)
	{
		doRollback();
		csMsg.Format(_T("%s\r\n\r\n%s"),csTract, m_csMsg2);
		AfxMessageBox(csMsg);

		csTmp.Format(_T("%s%s\r\n\r\n"), m_csStrFailed, database);
		m_csLogStr += csTmp;
	}
	else
	{
		//csMsg.Format(_T("%s\r\n\r\n%s"),csTract, m_csMsg3);
		//AfxMessageBox(csMsg);
		m_caStandOk.Add(csTract);

		csTmp.Format(_T("%s%s %s%s\r\n\r\n"), m_csStrOk, database , m_csStrInto, csTract);
		m_csLogStr += csTmp;
	}

	con.Disconnect();

	return bRes;
}

int CImportAccessView::getNextFileIndex()
{
	int nRet;

	if(m_pDB != NULL)
		nRet = m_pDB->getLastFileIndex();
	else
	{
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		return -1;
	}
	
	if(nRet >= 0)
		nRet += 1;
	else
		nRet = 1;

	return nRet;
}



int CImportAccessView::getData(SAConnection &con, SACommand &cmd, LPCTSTR table)
{
	int ant = 1;
	CString csSQL, csQuest;
	CString csMsg;
	SACommand command;
	int ans = 0;
	CString csTable;

	csTable.Format(_T("%s"), table);

	//AfxMessageBox(csTable);

	//special for table AuditRemeasData, needs to calculate difference between tree values and insert into new table, new v6.0.4
	if(csTable == _T("AuditRemeasData") && m_bCruiseIsAuditType == TRUE)
	{
		csQuest.Format(_T("select count(*) from %s"), _T("Tree"));
		try
		{
			command.setConnection(&con);
			command.setCommandText((SAString)csQuest);
			command.Execute();

			ant = 1;
			while(command.FetchNext())
			{
				// Process window messages
				doEvents();
				
				ant = command.Field(1).asShort();
			}
			command.Close();	//??
		}
#ifdef SHOW_MY_EXTRA_MSG
		catch(SAException &x)
		{
			AfxMessageBox(x.ErrText());
			return -1;
		}
#else
		catch(SAException &)
		{
			return -1;
		}
#endif

		csSQL.Format(_T("select * from %s order by FileIndex, PlotIndex, TRecIndex"), _T("Tree"));	//get tree data
		try
		{
			cmd.setConnection(&con);
			cmd.setCommandText((SAString)csSQL);
			cmd.Execute();
		}
#ifdef SHOW_MY_EXTRA_MSG
		catch(SAException &x)
		{
			AfxMessageBox(x.ErrText());
			return -1;
		}
#else
		catch(SAException &)
		{
			return -1;
		}
#endif

		return ant;
	}//if(csTable == _T("AuditRemeasData") && m_bCruiseIsAuditType == TRUE)

	csSQL.Format(_T("select * from %s"), table);
	try
	{
		cmd.setConnection(&con);
		cmd.setCommandText((SAString)csSQL);
		cmd.Execute();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &x)
	{
		AfxMessageBox(x.ErrText());
		return -1;
	}
#else
	catch(SAException &)
	{
		return -1;
	}
#endif

	if(table == _T("Stand"))
	{
		//check for remeasure or audit cruise
		csQuest.Format(_T("select RemeasType from %s"), table);
		try
		{
			command.setConnection(&con);
			command.setCommandText((SAString)csQuest);
			command.Execute();

			ans = 0;
			while(command.FetchNext())
			{
				ans = command.Field(1).asShort();
				
				if(ans == 1 || ans ==2)
					m_bCruiseIsAuditType = TRUE;
			}

			command.Close();
		}
#ifdef SHOW_MY_EXTRA_MSG
		catch(SAException &x)
		{
			AfxMessageBox(x.ErrText());
			return -1;
		}
#else
		catch(SAException &)
		{
			return -1;
		}
#endif
	}//if(table == TBL_STAND)

	
	csQuest.Format(_T("select count(*) from %s"), table);
	try
	{
		command.setConnection(&con);
		command.setCommandText((SAString)csQuest);
		command.Execute();

		ant = 0;
		while(command.FetchNext())
			ant = command.Field(1).asShort();
		command.Close();	//??
			
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &x)
	{
		AfxMessageBox(x.ErrText());
		return -1;
	}
#else
	catch(SAException &)
	{
		return -1;
	}
#endif


	return ant;
}


BOOL CImportAccessView::setData(SACommand &cmd, LPCTSTR table, LPCTSTR csTract, CProgressDialog &dlg)
{
	if(m_pDB != NULL)
		return m_pDB->setData(cmd, table, m_nFileIndex, csTract, dlg);
	else
	{
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		return FALSE;
	}
}

void CImportAccessView::doCommit()
{
	if(m_pDB != NULL)
		return m_pDB->doCommit();
	else
	{
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		return;
	}
}

void CImportAccessView::doRollback()
{
	if(m_pDB != NULL)
		return m_pDB->doRollback();
	else
	{
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		return;
	}
}

void CImportAccessView::OnBnClickedBtnImportOk()
{
	
	 PostMessage(WM_COMMAND, ID_FILE_CLOSE);

}
