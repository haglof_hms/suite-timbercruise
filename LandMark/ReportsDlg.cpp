// ReportsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "ReportsDlg.h"

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <ole2.h>

//#include <shlobj.h>


// Implicitly link ole32.dll
#pragma comment( lib, "ole32.lib" )



// CReportsDlg dialog

IMPLEMENT_DYNAMIC(CReportsDlg, CDialog)

CReportsDlg::CReportsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CReportsDlg::IDD, pParent)
{
	m_sStand = _T("");

}

CReportsDlg::~CReportsDlg()
{
}

void CReportsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LIST_CTRL, m_wndListCtrl);
	DDX_Control(pDX, IDC_LBL, m_wndLbl);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
}


BEGIN_MESSAGE_MAP(CReportsDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CReportsDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CReportsDlg message handlers

BOOL CReportsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CDialog::PreCreateWindow(cs))
		return FALSE;
	
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CReportsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format("%s%s%s%s", getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	setListCtrl();
	setLanguage();

	return TRUE;
}

void CReportsDlg::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			SetWindowText(_T(xml->str(IDS_STRING146)));
			m_wndLbl.SetWindowText(_T(xml->str(IDS_STRING147)));
			m_wndBtnOK.SetWindowTextA(_T(xml->str(IDS_STRING103)));
			m_wndBtnCancel.SetWindowTextA(_T(xml->str(IDS_STRING104)));
		}
		delete xml;
	}
}

void CReportsDlg::setListCtrl()
{
	int nCnt = 0;
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_wndListCtrl.InsertColumn(0, _T(xml->str(IDS_STRING148)), LVCFMT_LEFT, 100);
			m_wndListCtrl.InsertColumn(1, _T(xml->str(IDS_STRING149)), LVCFMT_LEFT, 100);
			m_wndListCtrl.InsertColumn(2, _T(xml->str(IDS_STRING150)), LVCFMT_LEFT, 200);
		}
		delete xml;
	}
	else
		return;

	//Get the windows handle to the header control for the list control, then subclass the control.
	HWND hWndHeader = m_wndListCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_wndHeaderCtrl.SubclassWindow(hWndHeader);

	//theme
	m_wndHeaderCtrl.SetTheme(new CXTHeaderCtrlThemeOffice2003());

	m_wndListCtrl.ModifyExtendedStyle(0, LVS_EX_CHECKBOXES | LVS_EX_FULLROWSELECT);

	if(getFiles() == FALSE)
		return;

	if(vecFileData.size() > 0)
	{
		for(UINT i=0; i<vecFileData.size(); i++)
		{
			m_wndListCtrl.InsertItem(nCnt, _T(""), 0);
			m_wndListCtrl.SetItem(nCnt, 0, LVIF_TEXT, _T(vecFileData[i].getFileName()), 0, NULL, NULL, NULL);
			m_wndListCtrl.SetItem(nCnt, 1, LVIF_TEXT, _T(vecFileData[i].getFileTitle()), 0, NULL, NULL, NULL);
			m_wndListCtrl.SetItem(nCnt, 2, LVIF_TEXT, _T(vecFileData[i].getFileComments()), 0, NULL, NULL, NULL);
			nCnt++;
		}
	}

}

BOOL CReportsDlg::getFiles()
{
	BOOL bRet = FALSE;
	HKEY hKey;
	CHAR szDir[BUFSIZ];
	DWORD dwDirSize = BUFSIZ;
	CString csDir, csTitle, csComm;

	//get directory from registry
	if(RegOpenKeyEx(HKEY_CURRENT_USER, HMS_INST_DIR, NULL, KEY_QUERY_VALUE, &hKey) != ERROR_SUCCESS)
		return FALSE;

	if(RegQueryValueEx(hKey, HMS_INST_DIR_KEY, NULL, NULL, (LPBYTE)szDir, &dwDirSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);
	sprintf_s(szDir, "%s%s\\*.rpt",szDir,REPORTS_TC_DIR);
	csDir = szDir;

	BOOL bFiles;
	CFileFind cfFind;
	bFiles = cfFind.FindFile(csDir);

	vecFileData.clear();

	while(bFiles)
	{
		CString csPath;
		bFiles = cfFind.FindNextFile();

		csPath = cfFind.GetFilePath();
		
		csTitle = getTitle(csPath);
		csComm = getComments(csPath);
		vecFileData.push_back(CTransaction_Files(cfFind.GetFilePath(),cfFind.GetFileTitle(), csTitle, csComm));
	}

	if(vecFileData.size() > 0)
		bRet = TRUE;

	return bRet;
}


CString CReportsDlg::getTitle(LPCSTR path)
{
	
	CString csOut = _T("");

	WCHAR wPath[BUFSIZ];

	MultiByteToWideChar(CP_ACP, 0, path, (int)(strlen(path)+1), wPath, sizeof(wPath)/sizeof(wPath[0]));
	
	IPropertySetStorage *pPropSetStg = NULL;
	IPropertyStorage *pPropStg = NULL;
	PROPSPEC propspec; 
	PROPVARIANT propRead;
	HRESULT hr = S_OK;

	hr = ::StgOpenStorageEx(wPath,
		STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_READ,	
		STGFMT_ANY,
		0,
		NULL,
		NULL,
		IID_IPropertySetStorage,
		reinterpret_cast<void**>(&pPropSetStg) );

	if(hr != S_OK)
	{
		return _T("");
	}

	hr = pPropSetStg->Open( PropSetfmtid, 
                            STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_READ,
                            &pPropStg );

	if(hr != S_OK)
	{
		pPropSetStg->Release();
		return _T("");
	}

	propspec.ulKind = PRSPEC_PROPID;
	propspec.propid  = 0x00000002;	//title

	hr = pPropStg->ReadMultiple( 1, &propspec, &propRead );
	
	if(hr != S_OK)
	{
		pPropStg->Release();
		pPropSetStg->Release();
		return _T("");
	}

	//TODO Change this
	csOut = (WCHAR*)propRead.pwszVal;
	if(csOut.Find(_T("???")) >= 0)
		csOut.Format("%s", (WCHAR*)propRead.pwszVal);

	pPropStg->Release();
	pPropSetStg->Release();
	
	return csOut;
}


CString CReportsDlg::getComments(LPCSTR path)
{
	
	CString csOut = _T("");

	WCHAR wPath[BUFSIZ];

	MultiByteToWideChar(CP_ACP, 0, path, (int)(strlen(path)+1), wPath, sizeof(wPath)/sizeof(wPath[0]));
	
	IPropertySetStorage *pPropSetStg = NULL;
	IPropertyStorage *pPropStg = NULL;
	PROPSPEC propspec; 
	PROPVARIANT propRead;
	HRESULT hr = S_OK;

	hr = ::StgOpenStorageEx(wPath,
		STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_READ,	
		STGFMT_ANY,
		0,
		NULL,
		NULL,
		IID_IPropertySetStorage,
		reinterpret_cast<void**>(&pPropSetStg) );

	if(hr != S_OK)
	{
		return _T("");
	}

	hr = pPropSetStg->Open( PropSetfmtid, 
                            STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_READ,
                            &pPropStg );

	if(hr != S_OK)
	{
		pPropSetStg->Release();
		return _T("");
	}

	propspec.ulKind = PRSPEC_PROPID;
	propspec.propid  = 0x00000006;	//comment

	hr = pPropStg->ReadMultiple( 1, &propspec, &propRead );
	
	if(hr != S_OK)
	{
		pPropStg->Release();
		pPropSetStg->Release();
		return _T("");
	}

	//TODO Change this
	csOut = (WCHAR*)propRead.pwszVal;
	if(csOut.Find(_T("???")) >= 0)
		csOut.Format("%s", (WCHAR*)propRead.pwszVal);


	pPropStg->Release();
	pPropSetStg->Release();
	
	return csOut;
}

void CReportsDlg::OnBnClickedOk()
{
	CString csReport,csOrder;
	if(m_wndListCtrl.GetItemCount() > 0)
	{
		for(int i=0; i<m_wndListCtrl.GetItemCount(); i++)
		{
			if(m_wndListCtrl.GetCheck(i))
			{
				//launch report
				
				csReport.Format("%s", vecFileData[i].getFilePath());

				csOrder = m_sStand;		//TODO change this?

				// tell the report-suite to open the report
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
					(LPARAM)&_user_msg(333, "OpenSuiteEx", 
					"Reports2.dll",
					csReport,
					"",
					csOrder));
			}
		}
	}

	OnOK();
}
