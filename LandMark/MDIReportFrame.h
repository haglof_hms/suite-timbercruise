#pragma once


// CMDIReportFrame frame

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CMDIReportFrame : public CChildFrameBase	//CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDIReportFrame)

	CXTPDockingPaneManager m_paneManager;
	CString m_sAbrevLangSet;
	CString m_sLangFN;

protected:
	CString m_sToolTipFilter;
	CString m_sToolTipFilterOff;
	CString m_sToolTipRefresh;
	CString m_sToolTipImport;
	CString m_sToolTipPrintOut;
	CString m_sToolTipAssignGroups;
	CString m_sErrorMsg;

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager()
	{
		return &m_paneManager;
	}

	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	BOOL m_bEnableTBBTNFilterOff;

	CXTPToolBar m_wndToolBar;
	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;

public:
	CMDIReportFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIReportFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	CDialogBar m_wndFieldChooserDlg;
	CDialogBar m_wndFilterEdit;

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

	
protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnPaint();
	afx_msg void OnClose();
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
};


