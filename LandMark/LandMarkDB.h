#pragma once

#include "stdafx.h"
#include "ProgressDialog.h"


class CStandSelListReportRec;
class CAssignGroupsReportRec;
class CClassNamesReportRec;
class CExternalDocsReportDataRec;

class CLandMarkDB : public CDBBaseClass_SQLApi
{

public:
	CLandMarkDB(void);
	CLandMarkDB(SAClient_t client, LPCTSTR db_name = _T(""), LPCTSTR user_name = _T(""), LPCTSTR psw = _T(""));
	CLandMarkDB(DB_CONNECTION_DATA &db_connection);

	void setAutoCommitStatus(BOOL commit =TRUE);

	BOOL getStands(vecTransactionTCStand &vec);
	BOOL getGroups(vecTransactionTCGroups &vec);
	BOOL getGroupsFromOrderTable(vecTransactionTCGroups &vec);
	BOOL getSnapshots(int fileindex, vecTransactionTCSnapshot &vec);
	BOOL getSnapshotHeaders(int fileindex, vecTransactionTCSnapshotHeader &vec);
	BOOL standDataExists(int fileindex);
	BOOL removeDataFromDB(int fileindex);
	BOOL removeClassFromOrderHdr(int index);
	BOOL removeGroupFromOrder(int index);
	BOOL removeSnapshot(int id);
	BOOL saveDataToDB(CStandSelListReportRec* pRec);
	BOOL saveClassNameToTableOrder(CAssignGroupsReportRec* pRec);
	BOOL saveGroupToTableOrder(CAssignGroupsReportRec* pRec);
	BOOL saveClassNameToTableOrderHdr(CClassNamesReportRec *pRec);
	BOOL savePrintOrderToTableOrderHdr(CClassNamesReportRec *pRec);
	BOOL savePrintOrderToTableOrder(CAssignGroupsReportRec* pRec);
	BOOL saveToTableOrderHdr(CClassNamesReportRec *pRec);
	BOOL saveNewClassNameToTableOrderHdr(CClassNamesReportRec *pRec);
	BOOL getClassNames(vecTransactionTCClass &vec);
	BOOL getCompanyInfo(CTransaction_TCCompanyInfo &info);
	BOOL saveCompanyInfo(CTransaction_TCCompanyInfo info);

	BOOL saveImage(CString path, CTransaction_TCCompanyInfo info);
	BOOL removeImage(CTransaction_TCCompanyInfo info);
	BOOL loadImage(CString path, CTransaction_TCCompanyInfo info);

	int getClassIndexFromOrderHdr(CString classname);
	CString getClassNameFromOrderHdr(int index);
	int getNextClassIndex(void);

	CString getCruiseMethod(int method);
	int setCruiseMethod(LPCTSTR method);
	int getLastFileIndex(void);

	BOOL setData(SACommand &cmd, LPCTSTR table, int fileindex, LPCTSTR csTract, CProgressDialog &dlg);

	BOOL isCruiseAuditRemeas(int fileindex);
	BOOL setAuditRemeasData(SACommand &cmd, int fileindex, CProgressDialog &dlg);
	BOOL isStrNumber(CString str);
	BOOL saveType2ToAuditTable(AuditRemeasType &meas2, CString csSQL, int fields);
	BOOL saveToAuditTable(AuditRemeasType &meas2, AuditRemeasType &meas1, CString csSQL, int fields);
	void resetAuditStruct(AuditRemeasType &meas);
	BOOL getFieldData(SACommand &cmd, AuditRemeasType &meas, int fileindex, int fields);

	int tractExists(LPCTSTR tract);	//returns fileindex from SQL db if exists, else -1 
	BOOL checkGroupsAssigned(void);
	BOOL getExternalDocuments(LPCTSTR tbl_name,int link_id,vecTransaction_external_docs &);
	BOOL addExternalDocuments(CTransaction_external_docs rec);
	int getNextExternalDocIndex(void);
	BOOL saveExternalDocument(int id, LPCTSTR tbl_name,int link_id, int type,CExternalDocsReportDataRec *rec);
	BOOL saveNewExternalDocument(int id, LPCTSTR tbl_name,int link_id, int type,CExternalDocsReportDataRec *rec);
	BOOL delExternalDocument(int id,LPCTSTR tbl_name,int link_id);
	BOOL delExternalDocuments(LPCTSTR tbl_name,int link_id);
	BOOL standHasExternalDocs(LPCTSTR tbl_name,int link_id);
	BOOL standHasSnapshots(int stand_id);
	CString getTractName(int link_id);

	BOOL saveImage(CString path, CTransaction_contacts rec);
	BOOL removeImage(CTransaction_contacts rec);
	BOOL loadImage(CString path, CTransaction_contacts rec);

	BOOL getContacts(vecTransactionContacts &);
	BOOL getContacts(CString sql,vecTransactionContacts &);
	BOOL getContacts(CString category,CString sql,vecTransactionContacts &);
	BOOL addContact(CTransaction_contacts &);
	BOOL updContact(CTransaction_contacts &);
	BOOL removeContact(CTransaction_contacts &);
	int getLastContactID(void);
	BOOL resetContactIdentityField(void);
	int getNumOfRecordsInContacts(void);
	int getNumOfRecordsInContacts(LPCTSTR count_field,LPCTSTR count_value);
	BOOL contactExists(CTransaction_contacts &rec);

	BOOL getCategories(vecTransactionCategory &);
	BOOL addCategory(CTransaction_category &);
	BOOL updCategory(CTransaction_category &);
	BOOL removeCategory(CTransaction_category &);

	BOOL getCategoriesForContact(vecTransactionCategoriesForContacts &);
	BOOL addCategoriesForContact(vecTransactionCategoriesForContacts &);
	BOOL addCategoriesForContact(int contact_id,int category_id);
	BOOL removeCategoriesForContact(int contact_id);

	BOOL categoryExists(CTransaction_category &);

	BOOL propertyExists(CTransaction_property &rec);
	BOOL getProperties(vecTransactionProperty &);
	BOOL getProperties(CString sql,vecTransactionProperty &vec);
	BOOL addProperty(CTransaction_property &);
	BOOL updProperty(CTransaction_property &);
	BOOL runSQLQuestion(LPCTSTR sql);
	BOOL updPropertyObjID(int prop_id,LPCTSTR obj_id);
	BOOL removeProperty(CTransaction_property &);
	int getLastPropertyID(void);
	BOOL resetPropertyIdentityField(void);
	int getNumOfRecordsInProperty(void);
	int getNumOfRecordsInProperty(LPCTSTR count_field,LPCTSTR count_value);
	
	BOOL getPropOwners(vecTransactionPropOwners &);
	BOOL getPropOwnersForProperty(int,vecTransactionPropOwnersForPropertyData &);
	BOOL addPropOwner(CTransaction_prop_owners &);
	BOOL removePropOwner(CTransaction_prop_owners &);
	BOOL removePropOwner(int prop_id);
	BOOL removePropOwner(int prop_id,int contact_id);	// Added 100127 p�d
	BOOL isPropertyOwnerUsed(int contact_id);

	BOOL propOwnersExists(CTransaction_prop_owners &rec);
	BOOL propOwnersExists(int prop_id);

	BOOL getTractsForProperty(int propid, vecTransactionTCStand &vec);
	BOOL updRemovePropertyFromStand(int trakt_id);
	BOOL updConnectPropertyToStand(int trakt_id,int prop_id);
	BOOL getTraktNoProperty(vecTransactionTCStand &vec);

	BOOL getProjectTypes(vecTransactionProjectType &);
	BOOL addProjectType(CTransaction_ProjectType &);
	BOOL updProjectType(CTransaction_ProjectType &);
	BOOL removeProjectType(CTransaction_ProjectType &);
	BOOL ProjectTypeExists(CTransaction_ProjectType &);

};

