#pragma once

#include "Resource.h"
#include "LandMarkDB.h"

// CSelectCategoryDlg dialog

class CSelectCategoryDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CSelectCategoryDlg)

protected:
	CString	m_sLangAbrev;
	CString m_sLangFN;
	CString m_sSelectedCategories;

	CXTCheckListBox m_wndLB1;
	CXTResizeGroupBox m_wndGroup;
	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	void setLanguage(void);

	// Categories in TBL_CONTACTS; 061221 p�d
	vecTransactionCategory m_vecCategoryData;
	BOOL getCategories(void);

	void addCategories(void);

	// Categories for Contact in TBL_CATEGORY_CONTACTS; 061221 p�d
	vecTransactionCategoriesForContacts m_vecCatgoriesForContactData;
	BOOL getCategoriesForContact(void);
	vecTransactionCategoriesForContacts m_vecSelectedCatgoriesForContactData;

	BOOL isCategorySelected(CTransaction_category rec1);

	CTransaction_contacts m_contact;

	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	CSelectCategoryDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectCategoryDlg();

// Dialog Data
	enum { IDD = IDD_SELECT_CATEGORY_DLG };

	// Categories selected by user; 061221 p�d
	CString getSelectedCategoriesStr(void)
	{
		return m_sSelectedCategories;
	}
	// Get vector of Categories (ID) for this Contact; 061221 p�d
	vecTransactionCategoriesForContacts getSelectedCategoriesForContact(void)
	{
		return m_vecSelectedCatgoriesForContactData;
	}

	void setContact(CTransaction_contacts data)
	{
		m_contact = data;
	}
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	// Generated message map functions
	//{{AFX_MSG(CCheckListBoxDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
