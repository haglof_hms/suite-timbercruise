// ReportView.cpp : implementation file
//

#include "stdafx.h"
#include "ReportView.h"
#include "MDIReportFrame.h"
#include "LandMarkDB.h"
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <ole2.h>

// Implicitly link ole32.dll
#pragma comment( lib, "ole32.lib" )

//CReportFilterEditControl
IMPLEMENT_DYNCREATE(CReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CReportFilterEditControl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CString S;
	CMDIReportFrame* pWnd = (CMDIReportFrame*)getFormViewParentByID(IDD_FORMVIEW1);	
	if(pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != _T(""));
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar, nRepCnt, nFlags);
}


// CReportView

IMPLEMENT_DYNCREATE(CReportView, CXTPReportView)

CReportView::CReportView() : CXTPReportView()
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_sStand = _T("");
	m_csContactID = _T("");
}

CReportView::CReportView(LPCTSTR stand)
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_sStand = stand;
}

CReportView::~CReportView()
{
	if(m_pDB != NULL)
		delete m_pDB;
}

BEGIN_MESSAGE_MAP(CReportView, CXTPReportView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDBClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_COMMAND(ID_TBBTN_COLUMNS1, OnShowFieldChooser)
	ON_COMMAND(ID_TBBTN_FILTER1,OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_FILTER_OFF1, OnShowFieldFilterOff)
	ON_COMMAND(ID_TBBTN_IMPORT1, OnTBBTNImport)
	ON_COMMAND(ID_TBBTN_PRINTOUT1, OnTBBTNPrintOut)
	ON_COMMAND(ID_TBBTN_REFRESH1, OnTBBTNRefresh)
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_COMMAND(ID_TBBTN_ASSIGNGROUPS, OnTBBTNAssignGroups)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnBtnDown)
END_MESSAGE_MAP()


// CReportView drawing

void CReportView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}


// CReportView diagnostics

#ifdef _DEBUG
void CReportView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CReportView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CReportView message handlers

void CReportView::OnSize(UINT nType, int cx, int cy)
{
	CXTPReportView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CReportView::OnDestroy()
{
	SaveReportState();

	CXTPReportView::OnDestroy();
}

BOOL CReportView::OnCopyData(CWnd *pWnd, COPYDATASTRUCT *pCopyDataStruct)
{
	if(pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memset(&m_dbConnectionData, 0x00, sizeof(DB_CONNECTION_DATA));
		memcpy(&m_dbConnectionData, pCopyDataStruct->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}
	
	return CXTPReportView::OnCopyData(pWnd, pCopyDataStruct);
}


BOOL CReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTPReportView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CReportView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	
	CMDIReportFrame *pWnd = (CMDIReportFrame*)getFormViewParentByID(IDD_FORMVIEW1);	

	if(m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST, &pWnd->m_wndFieldChooserDlg);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if(m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if(m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if(m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL1, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14, FW_BOLD);
	}

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

	getReportData();

	setupReport();
	
	LoadReportState();

	getContactID();
}



BOOL CReportView::setupReport()
{
	CString csColText;
	int nNUmOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	//add scrollbars
	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing(FALSE);
	GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE);
	GetReportCtrl().EnableScrollBar(SB_VERT, TRUE);

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sGroupByThisField = xml->str(IDS_STRING127);
			m_sGroupByBox = xml->str(IDS_STRING128);
			m_sFieldChooser = xml->str(IDS_STRING129);
			m_sFilterOn = xml->str(IDS_STRING130);
			m_sLicMsgEnd = xml->str(IDS_STRING158);
			m_sLicMsgLeft = xml->str(IDS_STRING159);
			m_sDBErrorMsg = xml->str(IDS_STRING105);
			m_sAssignMsg = xml->str(IDS_STRING203);
		
			//setup columns
			if(GetReportCtrl().GetSafeHwnd() != NULL)
			{
				GetReportCtrl().ShowWindow(SW_NORMAL);
	 
				//filename
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_REPORT_NAME, xml->str(IDS_STRING148), 100));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT | DT_WORDBREAK);
				pCol->SetVisible(FALSE);	//don't show default

				//title
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_REPORT_TITLE, xml->str(IDS_STRING149), 100));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT | DT_WORDBREAK);
				pCol->AllowRemove(FALSE);		//don't allow removal off column

				//subject
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_REPORT_SUBJECT, xml->str(IDS_STRING153), 100));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT | DT_WORDBREAK);

				//comments
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_REPORT_COMMENTS, xml->str(IDS_STRING150), 100));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT | DT_WORDBREAK);

				//Keywords
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_REPORT_KEYWORD, xml->str(IDS_STRING154), 100));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT | DT_WORDBREAK);

				//author
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_REPORT_AUTHOR, xml->str(IDS_STRING155), 100));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT | DT_WORDBREAK);

				//created
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_REPORT_CREATED, xml->str(IDS_STRING156), 100));
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT | DT_WORDBREAK);

				GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
				GetReportCtrl().GetReportHeader()->AllowColumnReorder(TRUE);
				GetReportCtrl().GetReportHeader()->AllowColumnResize( TRUE);	
				GetReportCtrl().GetReportHeader()->AllowColumnSort( TRUE);
				GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
				GetReportCtrl().SetMultipleSelection( TRUE );
				GetReportCtrl().AllowEdit(FALSE);
				//GetReportCtrl().FocusSubItems(TRUE);	//use this if to use tab key between different items 

				GetReportCtrl().SetPaintManager(new CReportMultilinePaintManager());
				GetReportCtrl().EnableToolTips(FALSE);

				GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
				GetReportCtrl().GetPaintManager()->m_bUseColumnTextAlignment = TRUE;

				GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
				GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
				
				GetReportCtrl().GetRecords()->SetCaseSensitive(FALSE);

				populateReport();
			}//if(GetReportCtrl().GetSafeHwnd() != NULL)
		}//if(xml->Load(m_sLangFN))
		delete xml;
	}//if(fileExists(m_sLangFN))

	return TRUE;
}


void CReportView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if(!AfxGetApp()->GetProfileBinary(REG_WP_TIMBERCRUISE_SEL_REPORT_KEY, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar(&memFile, CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);
	}
	catch(COleException *pEx)
	{
		pEx->Delete();
	}
	catch(CArchiveException *pEx)
	{
		pEx->Delete();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	
	//Get selected column index into registry
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(REG_WP_TIMBERCRUISE_SEL_REPORT_KEY, _T("SelColIndex"), 0);

/*
	//Get filtertext for this report
	sFilterText = AfxGetApp()->GetProfileString(REG_WP_TIMBERCRUISE_SEL_REPORT_KEY, _T("FilterText"), _T(""));
	
	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
*/
}

void CReportView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar(&memFile, CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_WP_TIMBERCRUISE_SEL_REPORT_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString(REG_WP_TIMBERCRUISE_SEL_REPORT_KEY, _T("FilterText"), sFilterText);

	//Set selected column index into registry
	AfxGetApp()->WriteProfileInt(REG_WP_TIMBERCRUISE_SEL_REPORT_KEY, _T("SelColIndex"), m_nSelectedColumn);
}

void CReportView::OnReportItemClick(NMHDR *pNotifyStruct, LRESULT *)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	if(pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}
}

void CReportView::OnReportItemDBClick(NMHDR* pNotifyStruct, LRESULT* result)
{
	CString csReport = _T("") ,csOrder = _T(""), csTmp = _T("");

	CXTPReportSelectedRows* pRows = GetReportCtrl().GetSelectedRows();

	if(pRows != NULL)
	{
		POSITION pos = pRows->GetFirstSelectedRowPosition();

		if(m_pDB != NULL)
		{
			vecTransactionTCGroups vec;
			//update tc_GroupOrder table
			if(m_pDB->getGroups(vec) == FALSE)
			{
				AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
				return;
			}
			if(m_pDB->checkGroupsAssigned() == FALSE)
			{
				if(AfxMessageBox(m_sAssignMsg, MB_ICONEXCLAMATION|MB_YESNO) != IDYES)
					return;
			}
		}
		else
		{
			AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			return;
		}

		while(pos)
		{
			CXTPReportRow* pRow = pRows->GetNextSelectedRow(pos);
			if(pRow)
			{
				CSelListReportRec* pRec = (CSelListReportRec*)pRow->GetRecord();
				if(pRec)
				{
					//launch report

					csReport.Format(_T("%s"), pRec->getFilePath());//pRec->getRecord().getFilePath());

					if(m_sStand != _T(""))
						csOrder.Format(_T("Tract=%s"), m_sStand);

					if(m_csContactID != _T("") && m_csContactID != _T("-1"))
					{
						if(csOrder != _T(""))
							csOrder += _T(";");
						csTmp.Format(_T("Contact=%s"), m_csContactID);
						csOrder += csTmp;
					}


					// tell the report-suite to open the report
					AfxGetApp()->BeginWaitCursor();
					AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
						(LPARAM)&_user_msg(333, _T("OpenSuiteEx"), 
						_T("Reports2.dll"),
						csReport,
						_T(""),
						csOrder));
					AfxGetApp()->EndWaitCursor();
				}
			}
		}//while(pos)		
	}//if(pRows != NULL)
}

void CReportView::OnReportColumnRClick(NMHDR *pNotifyStruct, LRESULT *result)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	ASSERT(pItemNotify->pColumn);

	CPoint ptClick = pItemNotify->pt;
	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	//create menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS1, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX1, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER1, m_sFieldChooser);

	if(GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS1, MF_BYCOMMAND|MF_CHECKED);
	}

	if(GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX1, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn *pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	//track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	switch(nMenuResult)
	{
	case ID_GROUP_BYTHIS1:
		if(pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
		{
			pColumns->GetGroupsOrder()->Add(pColumn);
		}
		GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
		GetReportCtrl().Populate();
		break;
	case ID_SHOW_GROUPBOX1:
		GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
		break;
	case ID_SHOW_FIELDCHOOSER1:
		OnShowFieldChooser();
		break;
	}
}

void CReportView::OnShowFieldChooser()
{
	CMDIReportFrame *pWnd = (CMDIReportFrame*)getFormViewParentByID(IDD_FORMVIEW1);	
	if(pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooserDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg, bShow, FALSE);
	}

}

void CReportView::OnShowFieldFilter()
{
	CMDIReportFrame *pWnd = (CMDIReportFrame*)getFormViewParentByID(IDD_FORMVIEW1);	
	if(pWnd != NULL)
	{
		if(m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
		{
			CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
			CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
			int nColumn = pColumn->GetIndex();

			if(pCols && nColumn < pCols->GetCount())
			{
				for(int i=0; i<pCols->GetCount(); i++)
				{
					pCols->GetAt(i)->SetFiltrable(i == nColumn);
				}
			}//if(pCols && nColumn < pCols->GetCount())
			m_wndLbl.SetWindowText(m_sFilterOn + _T(" : "));
			m_wndLbl1.SetWindowText(pColumn->GetCaption());
		}//if(m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())

		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}//if(pWnd != NULL)
}

void CReportView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));

	CMDIReportFrame *pWnd = (CMDIReportFrame*)getFormViewParentByID(IDD_FORMVIEW1);	
	if(pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}
}

LRESULT CReportView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case ID_DELETE_ITEM:
		OnDeleteReport();
		break;
	case ID_DBNAVIG_LIST:
		OnTBBTNAssignGroups();
		break;
	case ID_SHOWVIEW_MSG:
		m_sStand = _T("");

		if(lParam > 0)
		{
			if(m_pDB)
			{
				m_sStand = m_pDB->getTractName((int)lParam);
			}
		}
		break;
	}	

	return 0L;
}

void CReportView::OnTBBTNImport()
{
	showFormView(IDD_FORMVIEW2, m_sLangFN, 0);
}

void CReportView::getReportData()
{
	HKEY hKey;
	TCHAR szDir[BUFSIZ];
	DWORD dwDirSize = BUFSIZ;
	CString csDir, csTitle, csComment, csSubject, csAuthor, csKeywords,csCreated;

	//get directory from registry
	//if(RegOpenKeyEx(HKEY_CURRENT_USER, HMS_INST_DIR, NULL, KEY_QUERY_VALUE, &hKey) != ERROR_SUCCESS)
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, HMS_INST_DIR, NULL, KEY_QUERY_VALUE, &hKey) != ERROR_SUCCESS)
		return;

	if(RegQueryValueEx(hKey, REPORTS_INST_DIR_KEY/*HMS_INST_DIR_KEY*/, NULL, NULL, (LPBYTE)szDir, &dwDirSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hKey);
		return;
	}

	RegCloseKey(hKey);
	//_stprintf_s(szDir, _T("%s%s\\*.rpt"),szDir,REPORTS_TC_DIR);
	_stprintf_s(szDir, _T("%s\\*.rpt"),szDir);
	csDir = szDir;

	//find files in Reports\TC folder
	BOOL bFiles;
	CFileFind cfFind;
	bFiles = cfFind.FindFile(csDir);

	m_vecReportData.clear();
	CString csPath;

	while(bFiles)
	{
		
		bFiles = cfFind.FindNextFile();

		csPath = cfFind.GetFilePath();
		csTitle = getFileInfo(csPath, 0x00000002);	//title
		csSubject = getFileInfo(csPath, 0x00000003);	//Subject
		csAuthor = getFileInfo(csPath, 0x00000004);		//author
		csKeywords = getFileInfo(csPath, 0x00000005);	//keywords
		csComment = getFileInfo(csPath, 0x00000006);	//comment
		csCreated = getFileInfo(csPath, 0x0000000D);	//created
		m_vecReportData.push_back(CTransaction_TCReports(cfFind.GetFileTitle(), cfFind.GetFilePath(), csTitle, csSubject, csAuthor, csKeywords, csComment, csCreated));
	}


}




CString CReportView::getFileInfo(LPCTSTR path, PROPID propid)
{
	
	CString csOut = _T("");

	//WCHAR wPath[BUFSIZ];

//	MultiByteToWideChar(CP_ACP, 0, path, (int)(strlen(path)+1), wPath, sizeof(wPath)/sizeof(wPath[0]));
	
	IPropertySetStorage *pPropSetStg = NULL;
	IPropertyStorage *pPropStg = NULL;
	PROPSPEC propspec; 
	PROPVARIANT propRead;
	HRESULT hr = S_OK;

	hr = ::StgOpenStorageEx(/*wPath*/path,
		STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_READ,	
		STGFMT_ANY,
		0,
		NULL,
		NULL,
		IID_IPropertySetStorage,
		reinterpret_cast<void**>(&pPropSetStg) );

	if(hr != S_OK)
	{
		return _T("");
	}

	hr = pPropSetStg->Open( PropSetfmtid, 
                            STGM_DIRECT|STGM_SHARE_EXCLUSIVE|STGM_READ,
                            &pPropStg );

	if(hr != S_OK)
	{
		pPropSetStg->Release();
		return _T("");
	}

	propspec.ulKind = PRSPEC_PROPID;
	propspec.propid  = propid;	

	hr = pPropStg->ReadMultiple( 1, &propspec, &propRead );
	
	if(hr != S_OK)
	{
		pPropStg->Release();
		pPropSetStg->Release();
		return _T("");
	}

	if(propid >= 0x0000000A && propid <= 0x0000000D)
	{
		FILETIME ft;
		SYSTEMTIME st;
		ft = propRead.filetime;
		FileTimeToSystemTime(&ft, &st);
		csOut.Format(_T("%02d/%02d/%4d"), st.wMonth, st.wDay, st.wYear);
	}
	else
	{
		//TODO: might need to change this
		int nWlen = (int)wcslen(propRead.pwszVal);
		int nLen = (int)strlen(propRead.pszVal);
		if(nLen > 1 && nWlen > 1)
			csOut.Format(_T("%S"), propRead.pszVal);
		else
			csOut.Format(_T("%s"), propRead.pwszVal);

		//check if string length is more then one character, else asume multiple nul in string
/*		if(strlen(propRead.pszVal) <= 2)
		{
			csOut = propRead.pwszVal;
			if(csOut.Find(_T("??")) >= 0)	//check just in case
			{
				csOut.Format(_T("%s"), propRead.pwszVal);
			}
		}
		else
		{
			csOut.Format(_T("%s"), propRead.pwszVal);
		}

		*/
	}


	pPropStg->Release();
	pPropSetStg->Release();
	
	return csOut;
}


void CReportView::populateReport()
{
	GetReportCtrl().GetRecords()->RemoveAll();
	
	for(UINT i=0; i<m_vecReportData.size(); i++)
	{
		CTransaction_TCReports data = m_vecReportData[i];
		GetReportCtrl().AddRecord(new CSelListReportRec(data));
	}

	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	GetReportCtrl().SetFocusedRow(0);

}


void CReportView::OnTBBTNPrintOut()
{
	CString csReport = _T("") ,csOrder = _T(""), csTmp=_T("");

	CXTPReportSelectedRows* pRows = GetReportCtrl().GetSelectedRows();

	if(pRows != NULL)
	{
		POSITION pos = pRows->GetFirstSelectedRowPosition();

		if(m_pDB != NULL)
		{
			vecTransactionTCGroups vec;
			//update tc_GroupOrder table
			if(m_pDB->getGroups(vec) == FALSE)
			{
				AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
				return;
			}
			if(m_pDB->checkGroupsAssigned() == FALSE)
			{
				if(AfxMessageBox(m_sAssignMsg, MB_ICONEXCLAMATION|MB_YESNO) != IDYES)
					return;
			}
		}
		else
		{
			AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			return;
		}

		while(pos)
		{
			CXTPReportRow* pRow = pRows->GetNextSelectedRow(pos);
			if(pRow)
			{
				CSelListReportRec* pRec = (CSelListReportRec*)pRow->GetRecord();
				if(pRec)
				{
					//launch report

					csReport.Format(_T("%s"), pRec->getFilePath());//pRec->getRecord().getFilePath());

					if(m_sStand != _T(""))
						csOrder.Format(_T("Tract=%s"), m_sStand);

					if(m_csContactID != _T("") && m_csContactID != _T("-1"))
					{
						if(csOrder != _T(""))
							csOrder += _T(";");
						csTmp.Format(_T("Contact=%s"), m_csContactID);
						csOrder += csTmp;
					}


					// tell the report-suite to open the report
					AfxGetApp()->BeginWaitCursor();
					AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,
						(LPARAM)&_user_msg(333, _T("OpenSuiteEx"), 
						_T("Reports2.dll"),
						csReport,
						_T(""),
						csOrder));
					AfxGetApp()->EndWaitCursor();
				}
			}
		}//while(pos)		
	}//if(pRows != NULL)
}

void CReportView::OnDeleteReport()
{
	CString csReport,csMsg;
	CXTPReportSelectedRows* pRows = GetReportCtrl().GetSelectedRows();

	if(fileExists(m_sLangFN))
	{
		RLFReader* xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csMsg = xml->str(IDS_STRING157);
		}
		delete xml;
	}

	if(pRows != NULL)
	{
		if(AfxMessageBox(csMsg, MB_ICONQUESTION | MB_YESNO) != IDYES)
			return;

		POSITION pos = pRows->GetFirstSelectedRowPosition();

		while(pos)
		{
			CXTPReportRow* pRow = pRows->GetNextSelectedRow(pos);
			if(pRow)
			{
				CSelListReportRec* pRec = (CSelListReportRec*)pRow->GetRecord();
				if(pRec)
				{
					//delete report
					csReport.Format(_T("%s"), pRec->getFilePath());//pRec->getRecord().getFilePath());
					CFile::Remove(csReport);
				}
			}
		}//while(pos)
	}//if(pRows != NULL)

	getReportData();
	populateReport();
}

void CReportView::OnTBBTNRefresh()
{
	getReportData();
	populateReport();
}


void CReportView::OnTBBTNAssignGroups()
{
	showFormView(IDD_FORMVIEW5, m_sLangFN, 0);
}

void CReportView::OnBtnDown(NMHDR *pNotifyStruct, LRESULT *)
{
	LPNMKEY lpNMKEY = (LPNMKEY)pNotifyStruct;

	if(!GetReportCtrl().GetFocusedRow())
		return;

	// Delete focused class
	if(lpNMKEY->nVKey == VK_DELETE)
	{
		OnDeleteReport();
	}

	
}


//get contactID from registry, save in m_csContactID
void CReportView::getContactID()
{
	HKEY hKeyCU = NULL;
	TCHAR szData[BUFSIZ];
	DWORD dwSize = BUFSIZ;
	
	//open key
	if(RegOpenKeyEx(HKEY_CURRENT_USER, REG_HMSTC_SETTINGS_KEY, NULL, KEY_QUERY_VALUE|KEY_SET_VALUE, &hKeyCU) == ERROR_SUCCESS)
	{
		if(RegQueryValueEx(hKeyCU, REG_REPORT_HEADER_ITEM, NULL, NULL, (LPBYTE)szData, &dwSize) == ERROR_SUCCESS)
		{
			m_csContactID.Format(_T("%s"),szData);
		}
	}

	if(hKeyCU != NULL)
		RegCloseKey(hKeyCU);
}

