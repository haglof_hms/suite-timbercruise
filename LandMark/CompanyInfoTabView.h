#pragma once
#include "MDIBaseFrame.h"
#include "CompanyInfoFormView.h"

// CCompanyInfoTabView view

class CCompanyInfoTabView : public CView
{
	DECLARE_DYNCREATE(CCompanyInfoTabView)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	BOOL m_bInitialized;

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

protected:
	CCompanyInfoTabView();           // protected constructor used by dynamic creation
	virtual ~CCompanyInfoTabView();

	CMyTabControl& getTabControl(void)
	{
		return m_wndTabControl;
	}

	void setLanguage(void);
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int tab_id, int nIcon);
	CMDIFrameDoc* GetDocument();
	CCompanyInfoFormView* getCompanyInfoFormView(void);

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual void OnInitialUpdate();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
};


