#pragma once

#include "stdafx.h"
#include "LandMarkDB.h"
#include "Resource.h"

// CReportView view

class CReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CReportFilterEditControl)
public:
	CReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	afx_msg void OnKeyUp(UINT, UINT, UINT);
	
	DECLARE_MESSAGE_MAP()
};

//CSelListReportRec
class CSelListReportRec : public CXTPReportRecord
{
	CString m_csFilepath;

protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
		double m_fValue;
	public:
		CFloatItem(double fValue, LPCTSTR fmt_str = sz1dec)
			: CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
		}

		void setFloatItem(double value)
		{
			m_fValue = value;
			SetValue(value);
		}

		double getFloatItem(void)
		{
			return m_fValue;
		}
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
		int m_nValue;
	public:
		CIntItem(int nValue)
			: CXTPReportRecordItemNumber(nValue)
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		void setIntItem(int value)
		{
			m_nValue = value;
			SetValue(value);
		}

		int getIntItem(void)
		{
			return m_nValue;
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
		CString m_sText;
	public:
		CTextItem(CString sValue)
			: CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChaged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_sText = szText;
			SetValue(m_sText);
		}

		void setTextItem(LPCTSTR text)
		{
			m_sText = text;
			SetValue(m_sText);
		}

		CString getTextItem(void)
		{
			return m_sText;
		}
	};

public:

	CSelListReportRec(void)
	{
		m_csFilepath = _T("");
		AddItem(new CTextItem(_T("")));					// 
		AddItem(new CTextItem(_T("")));					// 
		AddItem(new CTextItem(_T("")));					// 
		AddItem(new CTextItem(_T("")));					// 
		AddItem(new CTextItem(_T("")));					// 
		AddItem(new CTextItem(_T("")));					// 
		AddItem(new CTextItem(_T("")));					// 
	}

	CSelListReportRec(CTransaction_TCReports &rec)
	{
		m_csFilepath = rec.getFilePath();
		AddItem(new CTextItem(rec.getFileName()));				// 
		AddItem(new CTextItem(rec.getTitle()));					//
		AddItem(new CTextItem(rec.getSubject()));				//
		AddItem(new CTextItem(rec.getComment()));				//
		AddItem(new CTextItem(rec.getKeywords()));				//
		AddItem(new CTextItem(rec.getAuthor()));				//
		AddItem(new CTextItem(rec.getCreated()));				//
	}

	
	
	CString getFilePath(void)
	{
		return m_csFilepath;
	}

	double getColumnFloat(int item)
	{
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item, double value)
	{
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)
	{
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item, int value)
	{
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)
	{
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item, LPCTSTR text)
	{
		((CTextItem*)GetItem(item))->setTextItem(text);
	}
	};



//class CReportView
class CReportView : public CXTPReportView
{
	DECLARE_DYNCREATE(CReportView)

protected:
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;
	CString m_sFilterOn;
	CString m_sErrorMsg;
	CString m_sLicMsgEnd;
	CString m_sLicMsgLeft;
	CString m_sAssignMsg;
	CString m_sDBErrorMsg;

	CString m_sStand;
	CString m_csContactID;

	int m_nSelectedColumn;

	vecTransactionTCReports m_vecReportData;
	
	CLandMarkDB *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;
	

	CXTPReportSubListControl m_wndSubList;
	CReportFilterEditControl m_wndFilterEdit;

	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	//CXTPReportControl m_wndReport;

	void LoadReportState(void);
	void SaveReportState(void);
	void getContactID(void);

	CString getFileInfo(LPCTSTR path, PROPID propid);
	
public:
	CReportView();           // protected constructor used by dynamic creation
	CReportView(LPCTSTR stand);
	virtual ~CReportView();

	void setReportOrderStr(LPCTSTR str) { m_sStand = str;}
	void getReportData(void);

	BOOL setupReport(void);
	void populateReport(void);

public:
	enum {IDD = IDD_FORMVIEW1};
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	void OnDeleteReport(void);
public:
	virtual void OnInitialUpdate();

	

protected:
	DECLARE_MESSAGE_MAP()

	public:
	void OnDestroy(void);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnReportItemClick(NMHDR *pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR *pNotifyStruct, LRESULT* result);
	afx_msg void OnShowFieldChooser(void);
	afx_msg void OnShowFieldFilter(void);
	afx_msg void OnShowFieldFilterOff(void);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
	afx_msg void OnReportItemDBClick(NMHDR *pNotifuStruct, LRESULT* result);
	afx_msg void OnTBBTNPrintOut(void);
	afx_msg void OnTBBTNImport(void);
	afx_msg void OnTBBTNAssignGroups(void);
	afx_msg void OnTBBTNRefresh(void);
	afx_msg void OnBtnDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
};


