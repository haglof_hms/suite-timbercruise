// AssignGroupsView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "AssignGroupsView.h"
#include "Resource.h"
#include "MDIAssignGroupsFrame.h"
#include "AssignMultiDlg.h"

//CAssignGroupsFilterEditControl
IMPLEMENT_DYNCREATE(CAssignGroupsFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CAssignGroupsFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CAssignGroupsFilterEditControl::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CString S;
	CMDIAssignGroupsFrame *pWnd = (CMDIAssignGroupsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
	if(pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != _T(""));
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar, nRepCnt, nFlags);
}



// CAssignGroupsView

IMPLEMENT_DYNCREATE(CAssignGroupsView, CXTPReportView)

CAssignGroupsView::CAssignGroupsView() : CXTPReportView()
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_nSelectedColumn = -1;
}

CAssignGroupsView::~CAssignGroupsView()
{
	if(m_pDB != NULL)
		delete m_pDB;
}

BEGIN_MESSAGE_MAP(CAssignGroupsView, CXTPReportView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_COMMAND(ID_TBBTN_COLUMNS3, OnShowFieldChooser)
	ON_COMMAND(ID_TBBTN_FILTER2, OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_FILTER_OFF2, OnShowFieldFilterOff)
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_COMMAND(ID_TBBTN_REFRESH2, OnTBBTNRefresh)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_WM_COPYDATA()
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnValueChanged)
	ON_COMMAND(ID_TBBTN_CLASSES, OnTBBTNClasses)
	ON_COMMAND(ID_TBBTN_ASSIGN_MULTI, OnTBBTNAssignMulti)
	ON_NOTIFY(NM_RCLICK, XTP_ID_REPORT_CONTROL, OnReportItemRClick)
END_MESSAGE_MAP()


// CAssignGroupsView drawing

void CAssignGroupsView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}


// CAssignGroupsView diagnostics

#ifdef _DEBUG
void CAssignGroupsView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CAssignGroupsView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CAssignGroupsView message handlers

BOOL CAssignGroupsView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTPReportView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CAssignGroupsView::OnSize(UINT nType, int cx, int cy)
{
	CXTPReportView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CAssignGroupsView::OnDestroy()
{
	SaveReportState();

	CXTPReportView::OnDestroy();
}

void CAssignGroupsView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	
	CMDIAssignGroupsFrame *pWnd = (CMDIAssignGroupsFrame*)getFormViewParentByID(IDD_FORMVIEW5);

	if(m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST, &pWnd->m_wndFieldChooserDlg);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if(m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if(m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if(m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL1, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14, FW_BOLD);
	}

	setLanguage();
	
	if(getGroups() == FALSE)
	{
		CMDIAssignGroupsFrame *pFrame = (CMDIAssignGroupsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
		if(pFrame)
			pFrame->m_bDBConnOk = FALSE;
		GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
		return;
	}

	setupReport();

	LoadReportState();
}

void CAssignGroupsView::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sDBErrorMsg = xml->str(IDS_STRING105);
			m_sGroupByThisField = xml->str(IDS_STRING127);
			m_sGroupByBox = xml->str(IDS_STRING128);
			m_sFieldChooser = xml->str(IDS_STRING129);
			m_sFilterOn = xml->str(IDS_STRING130);
			m_sPrintOrderError = xml->str(IDS_STRING231);
			m_sMenuSort = xml->str(IDS_STRING230);
			
		}
		delete xml;
	}
}

BOOL CAssignGroupsView::setupReport()
{
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	//add scrollbars
	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing(FALSE);
	GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE);
	GetReportCtrl().EnableScrollBar(SB_VERT, TRUE);

	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			//setup columns
			if(GetReportCtrl().GetSafeHwnd() != NULL)
			{
				GetReportCtrl().ShowWindow(SW_NORMAL);


				//printorder
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_ASSIGNGROUPS_PRINTORDER, xml->str(IDS_STRING174), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				//pCol->SetVisible(FALSE);	//visible from version 6.0.6


				//groupname
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_ASSIGNGROUPS_GROUPNAME, xml->str(IDS_STRING175), 200));
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->AllowRemove(FALSE);

				//class (supergroup)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_ASSIGNGROUPS_CLASSNAME, xml->str(IDS_STRING176), 200));
				if(m_pDB != NULL)
				{
					m_pDB->getClassNames(m_vecClasses);
					for(UINT i=0; i<m_vecClasses.size(); i++)
					{
						pCol->GetEditOptions()->AddConstraint(m_vecClasses[i].getClassName(), i);
					}
				}
				pCol->GetEditOptions()->AddComboButton();
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetHeaderAlignment(DT_LEFT);
				pCol->SetAlignment(DT_LEFT);
				pCol->AllowRemove(FALSE);
				
				GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
				GetReportCtrl().GetReportHeader()->AllowColumnReorder(TRUE);
				GetReportCtrl().GetReportHeader()->AllowColumnResize( TRUE);	
				GetReportCtrl().GetReportHeader()->AllowColumnSort( TRUE);
				GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
				GetReportCtrl().AllowEdit(TRUE);
				GetReportCtrl().FocusSubItems(TRUE);	//use this if to use tab key between different items 

				GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
				GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
				
				GetReportCtrl().GetRecords()->SetCaseSensitive(FALSE);

				populateReport();
			}//if(GetReportCtrl().GetSafeHwnd() != NULL)
		}//if(xml->Load(m_sLangFN))
		delete xml;
	}//if(fileExists(m_sLangFN))

	return TRUE;
}

void CAssignGroupsView::populateReport()
{
	GetReportCtrl().GetRecords()->RemoveAll();

	GetReportCtrl().BeginUpdate();
	
	for(UINT i=0; i<m_vecGroups.size(); i++)
	{
		CTransaction_TCGroups data = m_vecGroups[i];
		GetReportCtrl().AddRecord(new CAssignGroupsReportRec(data));
	}

	GetReportCtrl().EndUpdate();
	
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	//GetReportCtrl().SetFocusedRow(0);
}

void CAssignGroupsView::OnShowFieldChooser()
{
	CMDIAssignGroupsFrame *pWnd = (CMDIAssignGroupsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
	if(pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooserDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg, bShow, FALSE);
	}
}

void CAssignGroupsView::OnShowFieldFilter()
{
	CMDIAssignGroupsFrame *pWnd = (CMDIAssignGroupsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
	if(pWnd != NULL)
	{
		if(m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
		{
			CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
			CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
			int nColumn = pColumn->GetIndex();

			if(pCols && nColumn < pCols->GetCount())
			{
				for(int i=0; i<pCols->GetCount(); i++)
				{
					pCols->GetAt(i)->SetFiltrable(i == nColumn);
				}
			}

			m_wndLbl.SetWindowText(m_sFilterOn + _T(" : "));
			m_wndLbl1.SetWindowText(pColumn->GetCaption());
		}

		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}
}

void CAssignGroupsView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));

	CMDIAssignGroupsFrame *pWnd = (CMDIAssignGroupsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
	if(pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}
}

LRESULT CAssignGroupsView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	CString csFrom;

	switch(wParam)
	{
	case ID_SAVE_ITEM:
		saveDataToDB();
		break;
	case ID_DELETE_ITEM:
		deleteGroupFromDB();
		break;
	case ID_DBNAVIG_LIST:
		OnTBBTNClasses();
		break;
	case ID_WPARAM_VALUE_FROM + 0x02:
		{
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			if(sizeof(*msg) == sizeof(_doc_identifer_msg))
			{
				csFrom.Format(_T("%s"),msg->getSendFrom());
				if(csFrom == MODULE926)
				{
					if(msg->getValue1() == AGV_REMOVE_CLASS)
					{
						removeClassFromRecord((LPARAM)msg->getValue2());
					}
					else if(msg->getValue1() == AGV_UPDATE_CLASS_NAME)
					{
						updateClassNameInRecord((LPARAM)msg->getValue2());
					}
					else if(msg->getValue1() == AGV_UPDATE_ITEM)
					{
						updateComboList((LPARAM)msg->getValue2());
					}
				}
			}
		}
		break;
	}

	return 0L;
}

void CAssignGroupsView::OnTBBTNRefresh()
{
	AfxGetApp()->BeginWaitCursor();
	getGroups();
	populateReport();
	AfxGetApp()->EndWaitCursor();
}

void CAssignGroupsView::OnReportItemClick(NMHDR *pNotifyStruct, LRESULT *)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	
	if(pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}
}

void CAssignGroupsView::OnReportColumnRClick(NMHDR *pNotifyStruct, LRESULT *result)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	ASSERT(pItemNotify->pColumn);

	CPoint ptClick = pItemNotify->pt;
	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	//create menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1,(LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS2, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX2, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER2, m_sFieldChooser);

	if(GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS2, MF_BYCOMMAND|MF_CHECKED);
	}

	if(GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX2, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns *pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn *pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	//track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTALIGN|TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	switch(nMenuResult)
	{
	case ID_GROUP_BYTHIS2:
		if(pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
		{
			pColumns->GetGroupsOrder()->Add(pColumn);
		}
		GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
		GetReportCtrl().Populate();
		break;
	case ID_SHOW_GROUPBOX2:
		GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
		break;
	case ID_SHOW_FIELDCHOOSER2:
		OnShowFieldChooser();
		break;
	}
}

BOOL CAssignGroupsView::OnCopyData(CWnd *pWnd, COPYDATASTRUCT *pCopyDataStruct)
{
	if(pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memset(&m_dbConnectionData, 0x00, sizeof(DB_CONNECTION_DATA));
		memcpy(&m_dbConnectionData, pCopyDataStruct->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}
	
	return CXTPReportView::OnCopyData(pWnd, pCopyDataStruct);
}

void CAssignGroupsView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if(!AfxGetApp()->GetProfileBinary(REG_WP_TIMBERCRUISE_SEL_GROUPS_KEY, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar(&memFile, CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);
	}
	catch(COleException *pEx)
	{
		pEx->Delete();
	}
	catch(CArchiveException *pEx)
	{
		pEx->Delete();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;

	//Get selected column index
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(REG_WP_TIMBERCRUISE_SEL_GROUPS_KEY, _T("SelColIndex"), 0);

	/*
	//Get filtertext for this report
	sFilterText = AfxGetApp()->GetProfileString(REG_WP_TIMBERCRUISE_SEL_GROUPS_KEY, _T("FilterText"), _T(""));

	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
	*/
}

void CAssignGroupsView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar(&memFile, CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();
	
	AfxGetApp()->WriteProfileBinary(REG_WP_TIMBERCRUISE_SEL_GROUPS_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString(REG_WP_TIMBERCRUISE_SEL_GROUPS_KEY,_T("FilterText"), sFilterText);

	AfxGetApp()->WriteProfileInt(REG_WP_TIMBERCRUISE_SEL_GROUPS_KEY, _T("SelColIndex"), m_nSelectedColumn);
}

BOOL CAssignGroupsView::getGroups()
{
	if(m_pDB != NULL)
	{
		if((m_bConnected = m_pDB->getGroups(m_vecGroups)) == TRUE)
			return TRUE;
	}

	AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
	return FALSE;

}

void CAssignGroupsView::OnValueChanged(NMHDR *pNotifyStruct, LRESULT *)
{
	int index;
	BOOL bSave;
	int tmp, actrow = 0,act_printorder;
	CXTPReportRecords *pRecs = NULL;
	CAssignGroupsReportRec *pRecord = NULL;
	CXTPReportRow *pRow = NULL;
	

	if(GetReportCtrl().GetFocusedRow() == NULL)
		return;

	XTP_NM_REPORTRECORDITEM *pNotifyItem = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	
	if(pNotifyItem->pColumn->GetItemIndex() == COLUMN_ASSIGNGROUPS_CLASSNAME)
	{
		if(m_pDB != NULL)
		{
			//save class name and index to database
			CAssignGroupsReportRec *pRec = (CAssignGroupsReportRec*)GetReportCtrl().GetFocusedRow()->GetRecord();
			if(pRec)
			{
				//get class index from GroupOrderHdr table and save to GroupOrder table
				index = m_pDB->getClassIndexFromOrderHdr(pRec->getColumnText(2));
				if(index == -1)
				{
					//error occured, set to empty row
					pRec->setClassIndex(0);
					pRec->setColumnText(COLUMN_ASSIGNGROUPS_CLASSNAME,_T(""));
				}
				else
					pRec->setClassIndex(index);
				m_pDB->saveClassNameToTableOrder(pRec);
			}
		}
		else
			AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
	}
	else if(pNotifyItem->pColumn->GetItemIndex() == COLUMN_ASSIGNGROUPS_PRINTORDER)	
	{
		//Check that two groups don't get same print order
		CAssignGroupsReportRec *pRec = (CAssignGroupsReportRec*)GetReportCtrl().GetFocusedRow()->GetRecord();

		if(!pRec)
			return;
		
		act_printorder = pRec->getColumnInt(COLUMN_ASSIGNGROUPS_PRINTORDER);

		if(act_printorder <= 0)
		{

			AfxMessageBox(m_sPrintOrderError, MB_ICONEXCLAMATION);
			Refresh();	//get old value
			//TODO: set current row
			return;
		}

		pRecs = GetReportCtrl().GetRecords();
		if(!pRecs)
			return;

		bSave = FALSE;
		for(int i=0; i<pRecs->GetCount(); i++)
		{
			pRecord = (CAssignGroupsReportRec*)pRecs->GetAt(i);
			if(pRecord)
			{
				if(pRecord->getIndex() != pRec->getIndex() &&
					(tmp = pRecord->getColumnInt(COLUMN_ASSIGNGROUPS_PRINTORDER)) == pRec->getColumnInt(COLUMN_ASSIGNGROUPS_PRINTORDER))
				{
					pRecord->setColumnInt(COLUMN_ASSIGNGROUPS_PRINTORDER, ++tmp);
					pRec = pRecord;
					i = -1;
					bSave = TRUE;
				}
			}
		}

		if(bSave == TRUE)
		{
			//save recordset to db
			saveDataToDB();
		}
		else
		{
			//only current record needs to be saved
			if(m_pDB != NULL)
			{
				m_pDB->savePrintOrderToTableOrder(pRec);
			}
		}

		//repopulate
		Refresh();

		//set focus on correct row
		CXTPReportRows *pRows = GetReportCtrl().GetRows();
		if(!pRows)
			return;

		for(int i=0; i<pRows->GetCount(); i++)
		{
			pRow = pRows->GetAt(i);
			if(!pRow)
				return;

			pRec = (CAssignGroupsReportRec*)pRow->GetRecord();
			if(pRec)
			{
				if(pRec->getColumnInt(COLUMN_ASSIGNGROUPS_PRINTORDER) == act_printorder)
				{
					actrow = i;
					break;
				}
			}

		}

		pRow = pRows->GetAt(actrow);
		if(pRow)
			GetReportCtrl().SetFocusedRow(pRow);
	}
}

void CAssignGroupsView::saveDataToDB()
{
	CXTPReportRecords *pRecords = GetReportCtrl().GetRecords();
	CAssignGroupsReportRec *pRec = NULL;

	if(m_pDB != NULL)
	{
		for(int i=0; i< pRecords->GetCount(); i++)
		{
			pRec = (CAssignGroupsReportRec*)pRecords->GetAt(i);
			if(pRec)
				m_pDB->saveGroupToTableOrder(pRec);
		}
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
}

void CAssignGroupsView::deleteGroupFromDB()
{
	CString csStr,csTmp, csCaption,csStrOk;
	
	//get focused row
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	if(!pRow)
		return;

	//ask if to delete
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csStr = xml->str(IDS_STRING199);
			csStrOk = xml->str(IDS_STRING200);
		}
		delete xml;
	}

	CAssignGroupsReportRec *pRec = (CAssignGroupsReportRec*)GetReportCtrl().GetFocusedRow()->GetRecord();

	csCaption = csStr + _T("!");
	csTmp.Format(_T(": %s?"),pRec->getColumnText(COLUMN_ASSIGNGROUPS_GROUPNAME));
	csStr += csTmp;

	if(pRec)
	{
		if(MessageBox(csStr,csCaption, MB_ICONQUESTION | MB_YESNO/*CANCEL*/) == IDYES)
		{
			//remove data from db
			if(m_pDB != NULL)
			{
				if(m_pDB->removeGroupFromOrder(pRec->getIndex()))
				{
					AfxMessageBox(csStrOk);
					Refresh();
				}
			}
			else
				AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
		}
	}
}

void CAssignGroupsView::Refresh()
{
	if(m_pDB != NULL)
	{
		AfxGetApp()->BeginWaitCursor();
		m_pDB->getGroupsFromOrderTable(m_vecGroups);
		populateReport();
		AfxGetApp()->EndWaitCursor();
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
}

void CAssignGroupsView::OnTBBTNClasses()
{
	showFormView(IDD_FORMVIEW6, m_sLangFN, 0);

	
}

void CAssignGroupsView::updateComboList(LPARAM lParam)
{
	CXTPReportColumn *pCol = NULL;

	if(lParam == AGV_UPDATE_COMBO_LIST)
	{
		if(m_pDB != NULL)
		{
			m_pDB->getClassNames(m_vecClasses);

			pCol = GetReportCtrl().GetColumns()->GetAt(COLUMN_ASSIGNGROUPS_CLASSNAME);
			pCol->GetEditOptions()->GetConstraints()->RemoveAll();
			for(UINT i=0; i<m_vecClasses.size(); i++)
			{
				pCol->GetEditOptions()->AddConstraint(m_vecClasses[i].getClassName(), i);
			}
		}
		else
			AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
	}
}

void CAssignGroupsView::removeClassFromRecord(LPARAM lParam)
{
	int index = (int)lParam;
	BOOL bSave = FALSE;
	CXTPReportRecords *pRecords = GetReportCtrl().GetRecords();
	CAssignGroupsReportRec* pRec = NULL;

	if(m_pDB != NULL)
	{
		for(int i=0; i< pRecords->GetCount(); i++)
		{
			pRec = (CAssignGroupsReportRec*)pRecords->GetAt(i);
			if(pRec)
			{
				if(pRec->getClassIndex() == index)
				{
					pRec->setClassIndex(0);
					pRec->setColumnText(COLUMN_ASSIGNGROUPS_CLASSNAME, _T(""));
					m_pDB->saveClassNameToTableOrder(pRec);
					bSave = TRUE;
				}
			}
		}
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);


	if(bSave)
	{
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();
	}

	updateComboList((LPARAM)AGV_UPDATE_COMBO_LIST);
}

void CAssignGroupsView::updateClassNameInRecord(LPARAM lParam)
{
	int index = (int)lParam;
	BOOL bSave = FALSE;
	CString csNewName = _T("");
	CXTPReportRecords *pRecords = GetReportCtrl().GetRecords();
	CAssignGroupsReportRec* pRec = NULL;

	if(m_pDB != NULL)
	{
		m_pDB->getClassNames(m_vecClasses);
		for(UINT i=0; i<m_vecClasses.size(); i++)
		{
			if(m_vecClasses[i].getClassIndex() == index)
			{
				csNewName = m_vecClasses[i].getClassName();
				break;
			}
		}
		
		for(int i=0; i< pRecords->GetCount(); i++)
		{
			pRec = (CAssignGroupsReportRec*)pRecords->GetAt(i);
			if(pRec)
			{
				if(pRec->getClassIndex() == index)
				{

					pRec->setColumnText(COLUMN_ASSIGNGROUPS_CLASSNAME, csNewName);
					m_pDB->saveClassNameToTableOrder(pRec);
					bSave = TRUE;
				}
			}
		}
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);

	if(bSave)
	{
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();
	}

	updateComboList((LPARAM)AGV_UPDATE_COMBO_LIST);
}

void CAssignGroupsView::OnTBBTNAssignMulti()
{
	int index; 
	CAssignMultiDlg *dlg = new CAssignMultiDlg;

	if(dlg->DoModal() != IDOK)
	{
		delete dlg;
		return;
	}

	index = dlg->classindex;

	delete dlg;

	CXTPReportSelectedRows *pRows = GetReportCtrl().GetSelectedRows();
	
	if(pRows)
	{
		POSITION pos = pRows->GetFirstSelectedRowPosition();
		while(pos)
		{
			CXTPReportRow *pRow = pRows->GetNextSelectedRow(pos);

			if(pRow)
			{
				CAssignGroupsReportRec *pRec = (CAssignGroupsReportRec*)pRow->GetRecord();

				if(pRec)
				{
					if(m_pDB != NULL)
					{
						if(index == -1)
						{
							//error occured, set to empty row
							pRec->setClassIndex(0);
							pRec->setColumnText(COLUMN_ASSIGNGROUPS_CLASSNAME,_T(""));
						}
						else
						{
							pRec->setClassIndex(index);
							pRec->setColumnText(COLUMN_ASSIGNGROUPS_CLASSNAME, m_pDB->getClassNameFromOrderHdr(index));
						}

						//save
						m_pDB->saveClassNameToTableOrder(pRec);
					}
					else
						AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
				}
			}
		}
	}

	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CAssignGroupsView::OnReportItemRClick(NMHDR *pNotifyStruct, LRESULT *)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	ASSERT(pItemNotify->pColumn);

	CPoint ptClick = pItemNotify->pt;
	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	//create menu
	menu.AppendMenuW(MF_SEPARATOR,  (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenuW(MF_STRING, ID_SETGROUPS_PRINTORDER, m_sMenuSort);

	//get case
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY| TPM_RETURNCMD|TPM_LEFTALIGN|TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this,NULL);
	
	switch(nMenuResult)
	{
	case ID_SETGROUPS_PRINTORDER:
		setPrintOrder();
		break;
	default:
		break;
	}
}

void CAssignGroupsView::setPrintOrder()
{
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if(!pRows)
		return;

	CXTPReportRow *pRow = NULL;
	CAssignGroupsReportRec *pRec = NULL;
	
	int index = 1;
	for(int i=0; i<pRows->GetCount(); i++)
	{
		pRow = pRows->GetAt(i);
		if(!pRow)
			return;

		pRec = (CAssignGroupsReportRec*)pRow->GetRecord();
		if(pRec)
		{
			pRec->setColumnInt(COLUMN_ASSIGNGROUPS_PRINTORDER, index++);
			if(m_pDB)
				m_pDB->savePrintOrderToTableOrder(pRec);	
		}
	}

	Refresh();	//update report
}

