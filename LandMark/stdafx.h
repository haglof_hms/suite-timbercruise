// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500 // Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
//#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <atlimage.h>


//for showing extra error messages 
//#define SHOW_MY_EXTRA_MSG

#ifdef SHOW_MY_EXTRA_MSG
#undef SHOW_MY_EXTRA_MSG
#endif

//TODO: for contacts and properties tables
//#define USE_TC_CONTACT

#ifdef USE_TC_CONTACT
#undef USE_TC_CONTACT
#endif



//TODO: for projects tables, need to define USE_TC_CONTACT for this to work
//#define USE_TC_PROJECTS

#ifdef USE_TC_PROJECTS
#undef USE_TC_PROJECTS
#endif


//includes
#include <SQLAPI.h>
#include <pad_hms_miscfunc.h>
#include <ResLangFileReader.h>
#include <pad_transaction_classes.h>
#include <DBBaseClass_ADODirect.h>	// ... in DBTransaction_lib
#include <DBBaseClass_SQLApi.h>
#include <xmllite.h>
#include "DatabaseTables.h"
#include "PPMessageBox.h"

//functions
BOOL License(void);
BOOL hitTest_X(int hit_x,int limit_x,int w_x);
CString getTCruiseDir(void);
CString getTCruiseDirFromUser(void);
CString getToolBarResFN();
CString getMyPathToUserAppData(void);
CString getMyPathToCommonAppData(void);
CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp);
CView *getFormViewByID(int idd);
CWnd *getFormViewParentByID(int idd);
void setupForDBConnection(HWND hWndRec, HWND hWndSend);
BOOL createTable(CString db_name);
BOOL alterTable(CString db_name);
void DoRegistryChecks();
void setToolbarBtn(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show = TRUE);
void enableTabPage(int idx,BOOL enable);
void activeTabPage(int idx);
BOOL checkExportDll(void);

static BOOL bShowLicenseMsgBox = TRUE;
extern HINSTANCE hInst;

//const strings
const LPCTSTR PROGRAM_NAME			  				= _T("TimberCruise");
const LPCTSTR SHELLPROGRAM_NAME						= _T("HMSShell");
const LPCTSTR HMS_INST_DIR							= _T("SOFTWARE\\HaglofManagmentSystem\\Settings");
const LPCTSTR HMS_INST_DIR_KEY						= _T("HMSDir");
const LPCTSTR REPORTS_INST_DIR_KEY					= _T("TC_Reports");
const LPCTSTR REPORTS_TC_DIR						= _T("Reports\\TC");
const LPCTSTR HMS_DATABASE_KEY						= _T("SOFTWARE\\HaglofManagmentSystem\\Database");
const LPCTSTR HMS_DATABASE_CONNECTED				= _T("IsConnected");
const LPCTSTR TEMP_IMAGE_FN							=_T("6624F9E3-ADC4-4903-B91B-9DB95FE724F7.bin");

const LPCTSTR REG_HMS_KEY					= _T("HaglofManagmentSystem");
const LPCTSTR REG_PPMSGBOX_VALUE			= _T("ShowTCExportDLL");
const LPCTSTR REG_PPMSGBOX_KEY				= _T("Software\\HaglofManagmentSystem\\HMSShell\\PPMessageBox");


// TCruise specific items
const LPCTSTR TCRUISE_EXE_NAME						= _T("TCruise.exe");
const LPCTSTR TCRUISE_WINDOW_TITLE					= _T(" - Heuristic Solutions Timber Cruise Program");
const LPCTSTR REG_TIMBERCRUISE_WP					= _T("Timber Cruise\\Placement\\TimberCruiseFrame");
const LPCTSTR REG_TIMBERCRUISEREPORT_WP				= _T("Timber Cruise\\Placement\\ReportFrame");
const LPCTSTR REG_TIMBERCRUISEASSIGNGROUPS_WP		= _T("Timber Cruise\\Placement\\GroupOrderFrame");
const LPCTSTR REG_TIMBERCRUISECLASSNAMESFRAME_WP	= _T("Timber Cruise\\Placement\\ClassFrame");
const LPCTSTR REG_TIMBERCRUISECOMPANY_WP			= _T("Timber Cruise\\Placement\\CompanyInfo");
const LPCTSTR REG_TIMBERCRUISE_EXTERNAL_DOCS_WP		= _T("Timber Cruise\\Placement\\ExternalDocs");
const LPCTSTR REG_TIMBERCRUISEIMPORTACCESS_WP		= _T("Timber Cruise\\Placement\\ImportAccessFrame");
const LPCTSTR REG_TIMBERCRUISESETTINGS_WP			= _T("Timber Cruise\\Placement\\SettingsFrame");


const LPCTSTR REG_TCRUISE_PATH_KEY					= _T("Software\\Heuristic Solutions Apps\\Heuristic Solutions TCruise");
const LPCTSTR REG_TCRUISE_OPTIONS_PATH_KEY			= _T("Software\\Heuristic Solutions Apps\\Heuristic Solutions TCruise\\Options");
const LPCTSTR REG_TCRUISE_INSTALLDIR_ITEM			= _T("InstallDir");
const LPCTSTR REG_TCRUISE_EXPORTDLL_ITEM			= _T("ExportDll");
const LPCTSTR REG_TCRUISE_IMPORTDLL_ITEM			= _T("ImportDll");
const LPCTSTR REG_TCRUISE_PROFILEDLL_ITEM			= _T("ProfileDll");
const LPCTSTR REG_TCRUISE_RAWDATADLL_ITEM			= _T("RawDataExportDll");
const LPCTSTR REG_TCRUISE_CUSTOMDLL_ITEM			= _T("CustomProcDll");
const LPCTSTR REG_CRYSTAL_LOV_PATH_KEY				= _T("Software\\Business Objects\\Suite 12.0\\Crystal Reports\\DatabaseOptions\\LOV");
const LPCTSTR REG_CRYSTAL_MAXROW_ITEM				= _T("MaxRowsetRecords");
const LPCTSTR TCRUISE_DEF_DIR						= _T("C:\\Program Files\\Heuristic Solutions Applications\\Timber Cruise");
const LPCTSTR REG_WP_TIMBERCRUISE_SEL_STAND_KEY		= _T("Timber Cruise\\SelectStandList\\Report");
const LPCTSTR REG_WP_TIMBERCRUISE_SEL_REPORT_KEY	= _T("Timber Cruise\\SelectReportList\\Report");
const LPCTSTR REG_WP_TIMBERCRUISE_SEL_GROUPS_KEY	= _T("Timber Cruise\\SelectGroupsList\\Report");
const LPCTSTR REG_WP_TIMBERCRUISE_EXTERNALDOCS_KEY = _T("Timber Cruise\\ExternalDocsList\\Report");
const LPCTSTR REG_HMSTC_SETTINGS_KEY				= _T("software\\HaglofManagmentSystem\\Timber Cruise\\Settings");
const LPCTSTR REG_REPORT_HEADER_ITEM				=  _T("ReportHeaderID");


const LPCTSTR REG_WP_CATEGORYFRAME_KEY				= _T("Timber Cruise\\CategoryFrame\\Placement");	
const LPCTSTR REG_WP_CONTACTSFRAME_KEY				= _T("Timber Cruise\\ContactsFrame\\Placement");	
const LPCTSTR REG_WP_CONTACTS_SELLEIST_FRAME_KEY	= _T("Timber Cruise\\ContactsSelListFrame\\Placement");	
const LPCTSTR REG_WP_CONTACTS_SELLEIST_REPORT_KEY	= _T("Timber Cruise\\ContactsSelListFrame\\Report");	
const LPCTSTR REG_WP_PROPERTYFRAME_KEY				= _T("Timber Cruise\\PropertyFrame\\Placement");	
const LPCTSTR REG_WP_PROPERTY_REPORT_KEY			= _T("Timber Cruise\\PropertyListFrame\\Report");	
const LPCTSTR REG_WP_PROPERTY_SELLIST_FRAME_KEY		= _T("Timber Cruise\\PropertySelListFrame\\Placement");	
const LPCTSTR REG_WP_PROPERTY_SELLIST_REPORT_KEY	= _T("Timber Cruise\\PropertySelListFrame\\Report");	// 070105 p�d
const LPCTSTR REG_WP_PROPOWNERS_REPORT_KEY			= _T("Timber Cruise\\PropOwnersListFrame\\Report");	// 070118 p�d
const LPCTSTR REG_WP_PROPSTANDS_REPORT_KEY			= _T("Timber Cruise\\PropStandsListFrame\\Report");	// 090810 p�d
const LPCTSTR REG_WP_STANDS_SELLIST_FRAME_KEY		= _T("Timber Cruise\\StandSelListFrame\\Placement");
const LPCTSTR REG_WP_STANDS_SELLIST_REPORT_KEY		= _T("Timber Cruise\\StandSelListFrame\\Report");
/*const LPCTSTR REG_WP_CONTACTS_POSTNR_REPORT_KEY	= _T("Timber Cruise\\ContactsPostNrFrame\\Report");	// 070105 p�d
const LPCTSTR REG_WP_CONTACTS_REPORT_FRAME_KEY		= _T("Timber Cruise\\ContactsReportFrame\\Placement");	// 070115 p�d
const LPCTSTR REG_WP_CONTACTS_REPORT_STATE_KEY	= _T("Timber Cruise\\ContactsReportStateFrame\\Report");	// 070105 p�d
const LPCTSTR REG_WP_CONTACTS_SEARCH_REPL_FRAME_KEY	= _T("Timber Cruise\\SearchReplaceFrame\\Placement");	// 070104 p�d
*/
const LPCTSTR REG_WP_PROJECTFRAME_KEY				= _T("Timber Cruise\\ProjectFrame\\Placement");	
const LPCTSTR REG_WP_PROJECT_REPORT_KEY				= _T("Timber Cruise\\ProjectListFrame\\Report");	
const LPCTSTR REG_WP_PROJECT_SELLIST_FRAME_KEY		= _T("Timber Cruise\\ProjectSelListFrame\\Placement");	
const LPCTSTR REG_WP_PROJECT_SELLIST_REPORT_KEY		= _T("Timber Cruise\\ProjectSelListFrame\\Report");	// 070105 p�d
const LPCTSTR REG_WP_PROJECTTYPEFRAME_KEY			= _T("Timber Cruise\\ProjectTypeFrame\\Placement");

const LPCTSTR LICENSE_NAME							= _T("License");				// Used for Languagefile for License; 080901 p�d
const LPCTSTR LICENSE_FILE_NAME						= _T("License.dll");		// Used on setting up filename and path; 080901 p�d
const LPTSTR TIMBERCRUISE_LIC_ID					= _T("H2300");	
const LPTSTR TIMBERCRUISE_LIC_HEADER				= _T("TimberCruise");


const LPCTSTR TOOLBAR_RES_DLL						= _T("HMSIcons.icl");
#define RSTR_TB_IMPORT						19
#define RSTR_TB_ADD							0
#define RSTR_TB_DEL							28
#define RSTR_TB_FILTER						39
#define RSTR_TB_FILTER_OFF					8
#define RSTR_TB_COLUMNS						47
#define RSTR_TB_PRINT						36
#define RSTR_TB_REFRESH						43
#define RSTR_TB_TEXTDOC						40
#define RSTR_TB_LIST						26
#define RSTR_TB_GEM							13
#define RSTR_TB_EDIT						7
#define RSTR_TB_GIS							74
#define RSTR_TB_SEARCH						37
#define RSTR_TB_TOOLS						47


#define AGV_UPDATE_COMBO_LIST					1990
#define AGV_UPDATE_CLASS_NAME					1991
#define AGV_REMOVE_CLASS						1992
#define AGV_UPDATE_ITEM							1993

const int MIN_X_SIZE_TIMBERCRUISE				= 400;
const int MIN_Y_SIZE_TIMBERCRUISE				= 400;
const int MIN_X_SIZE_IMPORTACCESS				= 300;
const int MIN_Y_SIZE_IMPORTACCESS				= 100;
const int MIN_X_SIZE_CLASSNAMES					= 100;
const int MIN_Y_SIZE_CLASSNAMES					= 200;
const int MIN_X_SIZE_CONTACT					= 200;
const int MIN_Y_SIZE_CONTACT					= 200;
const int MIN_X_SIZE_EXTERNAL_DOCS				= 585;
const int MIN_Y_SIZE_EXTERNAL_DOCS				= 400;
const int MIN_X_SIZE_SNAPSHOTS					= 585;
const int MIN_Y_SIZE_SNAPSHOTS					= 400;

const int MIN_X_SIZE_CATEGORY					= 450;
const int MIN_Y_SIZE_CATEGORY					= 250;
const int MIN_X_SIZE_PROJECTTYPE				= 450;
const int MIN_Y_SIZE_PROJECTTYPE				= 250;
const int MIN_X_SIZE_CONTACTS					= 585;
const int MIN_Y_SIZE_CONTACTS					= 400;
const int MIN_X_SIZE_CONTACTS_SELLIST			= 585;
const int MIN_Y_SIZE_CONTACTS_SELLIST			= 350;
const int MIN_X_SIZE_PROPERTY					= 585;
const int MIN_Y_SIZE_PROPERTY					= 400;
const int MIN_X_SIZE_PROPERTY_SELLIST			= 585;
const int MIN_Y_SIZE_PROPERTY_SELLIST			= 350;

const int MIN_X_SIZE_PROJECT					= 585;
const int MIN_Y_SIZE_PROJECT					= 400;
const int MIN_X_SIZE_PROJECT_SELLIST			= 585;
const int MIN_Y_SIZE_PROJECT_SELLIST			= 350;

const int MIN_X_SIZE_SETTINGS					= 585;
const int MIN_Y_SIZE_SETTINGS					= 350;

// ID's for popupmenu in ReportControl; 070109 p�d
#define ID_GROUP_BYTHIS							0x8200
#define ID_SHOW_GROUPBOX						0x8201
#define ID_SHOW_FIELDCHOOSER					0x8202
#define ID_TABCONTROL							0x8203
#define ID_TABCONTROL1							0x8204
#define ID_TAB_CLICKED							0x8205
#define ID_GROUP_BYTHIS1						0x8206
#define ID_SHOW_GROUPBOX1						0x8207
#define ID_SHOW_FIELDCHOOSER1					0x8208
#define ID_GROUP_BYTHIS2						0x8209
#define ID_SHOW_GROUPBOX2						0x8210
#define ID_SHOW_FIELDCHOOSER2					0x8211
#define ID_TABCONTROL2							0x8212
#define ID_SET_PRINTORDER						0x8213

#define ID_TBBTN_COLUMNS						0x8214
#define ID_SHOWVIEW_MSG							0x8215
#define ID_TBBTN1								0x8216
#define ID_TBBTN_COLUMNS1						0x8217
#define ID_TBBTN2								0x8218
#define ID_TBBTN_COLUMNS3						0x8219
#define ID_TBBTN3								0x8220
#define IDC_REPORT								0x8221

#define ID_SETGROUPS_PRINTORDER					0x8222
#define IDC_CATEGORY_REPORT						0x8223
#define IDC_PROP_OWNER_REPORT					0x8224

#define ID_TOOLS_ADD_CONTACT					0x8225
#define ID_TOOLS_REMOVE_CONTACT					0x8226
#define ID_TOOLS_ADD_STAND_TO_PROP				0x8227
#define ID_TOOLS_DEL_STAND_FROM_PROP			0x8228

#define IDC_PROP_STANDS_REPORT					0x8229
#define ID_TOOLS_ADD_STAND_TO_PROJECT			0x8230
#define ID_TOOLS_DEL_STAND_FROM_PROJECT			0x8231
#define IDC_PROJECT_STANDS_REPORT				0x8232
#define IDC_PROJECTTYPE_REPORT					0x8233

#define IDC_PATHSETTINGS_GRID					0x8234
#define IDC_HEADERSETTINGS_GRID					0x8235
#define ID_TABCONTROL3							0x8236
#define ID_REPORT_SEARCH_CONTACT				0x8237

const LPCTSTR sz0dec							= _T("%.0f");
const LPCTSTR sz1dec							= _T("%.1f");
const LPCTSTR sz2dec							= _T("%.2f");



//strings in language file
#define IDS_STRING100	100
#define IDS_STRING101	101
#define IDS_STRING102	102
#define IDS_STRING103	103
#define IDS_STRING104	104
#define IDS_STRING105	105

#define IDS_STRING121	121
#define IDS_STRING122	122
#define IDS_STRING123	123
#define IDS_STRING124	124
#define IDS_STRING125	125
#define IDS_STRING126	126
#define IDS_STRING127	127
#define IDS_STRING128	128
#define IDS_STRING129	129
#define IDS_STRING130	130
#define IDS_STRING131	131
#define IDS_STRING132	132
#define IDS_STRING133	133
#define IDS_STRING134	134
#define IDS_STRING135	135
#define IDS_STRING136	136
#define IDS_STRING137	137
#define IDS_STRING138	138
#define IDS_STRING139	139
#define IDS_STRING140	140
#define IDS_STRING141	141
#define IDS_STRING142	142
#define IDS_STRING143	143
#define IDS_STRING144	144
#define IDS_STRING145	145
#define IDS_STRING146	146
#define IDS_STRING147	147
#define IDS_STRING148	148
#define IDS_STRING149	149
#define IDS_STRING150	150
#define IDS_STRING151	151
#define IDS_STRING152	152
#define IDS_STRING153	153
#define IDS_STRING154	154
#define IDS_STRING155	155
#define IDS_STRING156	156
#define IDS_STRING157	157
#define IDS_STRING158	158
#define IDS_STRING159	159
#define IDS_STRING160	160
#define IDS_STRING161	161
#define IDS_STRING162	162
#define IDS_STRING163	163
#define IDS_STRING164	164
#define IDS_STRING165	165
#define IDS_STRING166	166
#define IDS_STRING167	167
#define IDS_STRING168	168
#define IDS_STRING169	169
#define IDS_STRING170	170
#define IDS_STRING171	171
#define IDS_STRING172	172
#define IDS_STRING173	173
#define IDS_STRING174	174
#define IDS_STRING175	175
#define IDS_STRING176	176
#define IDS_STRING177	177
#define IDS_STRING178	178
#define IDS_STRING179	179
#define IDS_STRING180	180
#define IDS_STRING181	181
#define IDS_STRING182	182
#define IDS_STRING183	183
#define IDS_STRING184	184
#define IDS_STRING185	185
#define IDS_STRING186	186
#define IDS_STRING187	187
#define IDS_STRING188	188
#define IDS_STRING189	189
#define IDS_STRING190	190
#define IDS_STRING191	191
#define IDS_STRING192	192
#define IDS_STRING193	193
#define IDS_STRING194	194
#define IDS_STRING195	195
#define IDS_STRING196	196
#define IDS_STRING197	197
#define IDS_STRING198	198
#define IDS_STRING199	199
#define IDS_STRING200	200
#define IDS_STRING201	201
#define IDS_STRING202	202
#define IDS_STRING203	203
#define IDS_STRING204	204
#define IDS_STRING205	205
#define IDS_STRING206	206
#define IDS_STRING207	207
#define IDS_STRING208	208
#define IDS_STRING209	209
#define IDS_STRING210	210
#define IDS_STRING211	211
#define IDS_STRING212	212
#define IDS_STRING213	213
#define IDS_STRING214	214
#define IDS_STRING215	215
#define IDS_STRING216	216
#define IDS_STRING217	217
#define IDS_STRING218	218
#define IDS_STRING219	219
#define IDS_STRING220	220
#define IDS_STRING221	221
#define IDS_STRING222	222
#define IDS_STRING223	223
#define IDS_STRING224	224
#define IDS_STRING225	225
#define IDS_STRING226	226
#define IDS_STRING227	227
#define IDS_STRING228	228	
#define IDS_STRING229	229
#define IDS_STRING230	230	
#define IDS_STRING231	231

//contacts and property strings starts here
#define IDS_STRING240	240
#define IDS_STRING241	241
#define IDS_STRING242	242
#define IDS_STRING243	243
#define IDS_STRING244	244
#define IDS_STRING245	245
#define IDS_STRING246	246
#define IDS_STRING247	247
#define IDS_STRING248	248
#define IDS_STRING249	249
#define IDS_STRING250	250
#define IDS_STRING251	251
#define IDS_STRING252	252
#define IDS_STRING253	253
#define IDS_STRING254	254
#define IDS_STRING255	255
#define IDS_STRING256	256
#define IDS_STRING257	257
#define IDS_STRING258	258
#define IDS_STRING259	259
#define IDS_STRING260	260
#define IDS_STRING261	261
#define IDS_STRING262	262
#define IDS_STRING263	263
#define IDS_STRING264	264
#define IDS_STRING265	265
#define IDS_STRING266	266
#define IDS_STRING267	267
#define IDS_STRING268	268
#define IDS_STRING269	269
#define IDS_STRING270	270
#define IDS_STRING271	271
#define IDS_STRING272	272
#define IDS_STRING273	273
#define IDS_STRING274	274
#define IDS_STRING275	275
#define IDS_STRING276	276
#define IDS_STRING277	277
#define IDS_STRING278	278
#define IDS_STRING279	279
#define IDS_STRING280	280
#define IDS_STRING281	281
#define IDS_STRING282	282
#define IDS_STRING283	283
#define IDS_STRING284	284
#define IDS_STRING285	285
#define IDS_STRING286	286
#define IDS_STRING287	287
#define IDS_STRING288	288
#define IDS_STRING289	289
#define IDS_STRING290	290
#define IDS_STRING291	291
#define IDS_STRING292	292
#define IDS_STRING293	293
#define IDS_STRING294	294
#define IDS_STRING295	295
#define IDS_STRING296	296
#define IDS_STRING297	297
#define IDS_STRING298	298
#define IDS_STRING299	299
#define IDS_STRING300	300
#define IDS_STRING301	301
#define IDS_STRING302	302
#define IDS_STRING303	303
#define IDS_STRING304	304
#define IDS_STRING305	305
#define IDS_STRING306	306
#define IDS_STRING307	307
#define IDS_STRING308	308
#define IDS_STRING309	309
#define IDS_STRING310	310
#define IDS_STRING311	311
#define IDS_STRING312	312
#define IDS_STRING313	313
#define IDS_STRING314	314
#define IDS_STRING315	315
#define IDS_STRING316	316
#define IDS_STRING317	317
#define IDS_STRING318	318
#define IDS_STRING319	319
#define IDS_STRING320	320
#define IDS_STRING321	321
#define IDS_STRING322	322
#define IDS_STRING323	323
#define IDS_STRING324	324
#define IDS_STRING325	325
#define IDS_STRING326	326
#define IDS_STRING327	327
#define IDS_STRING328	328
#define IDS_STRING329	329
#define IDS_STRING330	330
#define IDS_STRING331	331
#define IDS_STRING332	332
#define IDS_STRING333	333
#define IDS_STRING334	334
#define IDS_STRING335	335
#define IDS_STRING336	336
#define IDS_STRING337	337
#define IDS_STRING338	338
#define IDS_STRING339	339
#define IDS_STRING340	340
#define IDS_STRING341	341
#define IDS_STRING342	342
#define IDS_STRING343	343
#define IDS_STRING344	344
#define IDS_STRING345	345
#define IDS_STRING346	346
#define IDS_STRING347	347
#define IDS_STRING348	348
#define IDS_STRING349	349
#define IDS_STRING350	350
#define IDS_STRING351	351
#define IDS_STRING352	352
#define IDS_STRING353	353
#define IDS_STRING354	354
#define IDS_STRING355	355
#define IDS_STRING356	356
#define IDS_STRING357	357
#define IDS_STRING358	358
#define IDS_STRING359	359
#define IDS_STRING360	360
#define IDS_STRING361	361
#define IDS_STRING362	362
#define IDS_STRING363	363
#define IDS_STRING364	364
#define IDS_STRING365	365
#define IDS_STRING366	366
#define IDS_STRING367	367
#define IDS_STRING368	368
#define IDS_STRING369	369
#define IDS_STRING370	370
#define IDS_STRING371	371
#define IDS_STRING372	372
#define IDS_STRING373	373
#define IDS_STRING374	374
#define IDS_STRING375	375
#define IDS_STRING376	376
#define IDS_STRING377	377
#define IDS_STRING378	378
#define IDS_STRING379	379


#define IDS_STRING927	927
#define IDS_STRING940	940
#define IDS_STRING941	941

const LPCTSTR MODULE888 = _T("Module888");
const LPCTSTR MODULE920 = _T("Module920");
const LPCTSTR MODULE921 = _T("Module921");
const LPCTSTR MODULE922	= _T("Module922");
const LPCTSTR MODULE923	= _T("Module923");
const LPCTSTR MODULE924 = _T("Module924");
const LPCTSTR MODULE925 = _T("Module925");
const LPCTSTR MODULE926	= _T("Module926");
const LPCTSTR MODULE927 = _T("Module927");
const LPCTSTR MODULE928 = _T("Module928");
const LPCTSTR MODULE929 = _T("ReportView929");

const LPCTSTR MODULE931 = _T("Module931");
const LPCTSTR MODULE932 = _T("ReportView932");

const LPCTSTR MODULE936 = _T("Module936");
const LPCTSTR MODULE937 = _T("ReportView937");
const LPCTSTR MODULE942 = _T("Module942");

enum 
{
	COLUMN_TRACTID,
	COLUMN_TRACTACRES,
	COLUMN_CRUISEMETHOD,
	COLUMN_CRUISEDATE,
	COLUMN_TRACTLOC,
	COLUMN_TRACTOWNER,
	COLUMN_CRUISER,
	COLUMN_EXTDOC_ATTACHED,
	COLUMN_SNAPSHOTS
};

enum 
{
	COLUMN_REPORT_NAME,
	COLUMN_REPORT_TITLE,
	COLUMN_REPORT_SUBJECT,
	COLUMN_REPORT_COMMENTS,
	COLUMN_REPORT_KEYWORD,
	COLUMN_REPORT_AUTHOR,
	COLUMN_REPORT_CREATED
};

enum
{
	COLUMN_ASSIGNGROUPS_PRINTORDER,
	COLUMN_ASSIGNGROUPS_GROUPNAME,
	COLUMN_ASSIGNGROUPS_CLASSNAME
};

enum
{
	COLUMN_CLASS_PRINTORDER,
	COLUMN_CLASS_CLASSNAME
};

enum
{
	COLUMN_EXTDOC_ICON,
	COLUMN_EXTDOC_URL,
	COLUMN_EXTDOC_NOTE,
	COLUMN_EXTDOC_DONEBY,
	COLUMN_EXTDOC_DATE
};

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6,
	COLUMN_7,
	COLUMN_8,
	COLUMN_9,
	COLUMN_10,
	COLUMN_11,
	COLUMN_12,
	COLUMN_13,
	COLUMN_14,
	COLUMN_15,
	COLUMN_16,
	COLUMN_17,
	COLUMN_18
};

typedef enum { NEW_ITEM, UPD_ITEM } enumACTION;
typedef enum originForSearch { ORIGIN_NONE,ORIGIN_PROPERTY,ORIGIN_CONTACT } ORIGIN_FOR_SEARCH;

typedef enum focusedEdit { ED_NONE,
ED_PROPNUM,							// Fastighet
ED_PROPNAME,							// Fastighet
ED_BLOCK,								// Fastighet
ED_UNIT,									// Fastighet
ED_AREAL1,								// Fastighet
ED_AREAL2,								// Fastighet
ED_OBJID,								// Fastighet
ED_CTACT_ORGNR,					// Kontakter
ED_CTACT_NAME,						// Kontakter
ED_CTACT_COMPANY,				// Kontakter
ED_CTACT_ADDRESS,				// Kontakter
ED_CTACT_POSTNUM,				// Kontakter
ED_CTACT_POST_ADDRESS,		// Kontakter
ED_CTACT_COUNTY,					// Kontakter
ED_CTACT_COUNTRY,				// Kontakter
ED_CTACT_VAT_NUMBER,			// Kontakter
ED_CTACT_PHONE_WORK,			// Kontakter
ED_CTACT_PHONE_HOME,			// Kontakter
ED_CTACT_FAX_NUMBER,			// Kontakter
ED_CTACT_MOBILE,					// Kontakter
ED_CTACT_EMAIL,					// Kontakter
ED_CTACT_WEBSITE					// Kontakter
} FOCUSED_EDIT;

//////////////////////////////////////////////////////////////////////////
// This class is a customization of standard Report Paint Manager, 
// which allows drawing record items text with word wrapping.
// You can test this customization sample with "Multiline Sample" menu option.
// It is implemented using DT_WORDBREAK mode, and uses 2 methods overriding.
class CReportMultilinePaintManager : public CXTPReportPaintManager
{
public:
	CReportMultilinePaintManager();
	virtual ~CReportMultilinePaintManager();

	// Draws Item Caption with word wrapping.
	void DrawItemCaption(XTP_REPORTRECORDITEM_DRAWARGS* pDrawArgs, XTP_REPORTRECORDITEM_METRICS* pMetrics);

	// Customized calculation of the row height in word wrapping mode, 
	// which is required in other report drawing methods.
	int GetRowHeight(CDC* pDC, CXTPReportRow* pRow, int nWidth);

};


//class CTransaction_TCStand
class CTransaction_TCStand
{
	int m_nFileIndex;
	CString m_sTractID;
	double m_fTractAcres;
	CString m_sCruiseMethod;
	CString m_sCruiseDate;
	CString m_sTractLoc;
	CString m_sTractOwner;
	CString m_sCruiser;
	int m_nPropID;

public:
	CTransaction_TCStand(void);

	CTransaction_TCStand(int fileindex, LPCTSTR tractid, double tractacres, LPCTSTR cruisemethod, LPCTSTR cruisedate, LPCTSTR tractloc, LPCTSTR tractowner, LPCTSTR cruiser, int propid);

	CTransaction_TCStand(const CTransaction_TCStand &s);

	int getFileIndex(void) {return m_nFileIndex;}
	CString getTractID(void) {return m_sTractID;}
	double getTractAcres(void) {return m_fTractAcres;}
	CString getCruiseMethod(void) {return m_sCruiseMethod;}
	CString getCruiseDate(void) {return m_sCruiseDate;}
	CString getTractLoc(void) {return m_sTractLoc;}
	CString getTractOwner(void) {return m_sTractOwner;}
	CString getCruiser(void) {return m_sCruiser;}
	int getPropID(void) { return m_nPropID;}

};

typedef std::vector<CTransaction_TCStand> vecTransactionTCStand;

//class CTransaction_TCFiles
class CTransaction_TCFiles
{
	CString m_sFilePath;
	CString m_sFileName;
	CString m_sFileTitle;
	CString m_sFileComments;

public:
	CTransaction_TCFiles(void);
	CTransaction_TCFiles(LPCTSTR path, LPCTSTR name, LPCTSTR title, LPCTSTR comm);
	CTransaction_TCFiles(const CTransaction_TCFiles &f);

	CString getFilePath(void) { return m_sFilePath;}
	CString getFileName(void) { return m_sFileName;}
	CString getFileTitle(void) { return m_sFileTitle;}
	CString getFileComments(void) { return m_sFileComments;}
};

typedef std::vector<CTransaction_TCFiles> vecTransactionTCFiles;


class CTransaction_TCReports
{
	CString m_sFilePath;
	CString m_sFileName;
	CString m_sSubject;
	CString m_sTitle;
	CString m_sComment;
	CString m_sAuthor;
	CString m_sKeywords;
	CString m_sCreated;
public:
	CTransaction_TCReports(void);
	CTransaction_TCReports(LPCTSTR filename, LPCTSTR filepath, LPCTSTR title, LPCTSTR subject, LPCTSTR author, LPCTSTR keywords, LPCTSTR comment, LPCTSTR created);
	CTransaction_TCReports(const CTransaction_TCReports &r);

	CString getFilePath(void) { return m_sFilePath;}
	CString getFileName(void) { return m_sFileName;}
	CString getTitle(void) { return m_sTitle;}
	CString getSubject(void) { return m_sSubject;}
	CString getComment(void) { return m_sComment;}
	CString getAuthor(void) { return m_sAuthor;}
	CString getKeywords(void) { return m_sKeywords;}
	CString getCreated(void) { return m_sCreated;}
};

typedef std::vector<CTransaction_TCReports> vecTransactionTCReports;

class CTransaction_TCGroups
{
	int m_nID;
	int m_nPrintOrder;
	CString m_csGroupName;
	int m_nClassIndex;
	CString m_csClassName;

public:
	CTransaction_TCGroups(void);
	CTransaction_TCGroups(int id, int order, LPCTSTR groupname, int classindex, LPCTSTR classname);
	CTransaction_TCGroups(const CTransaction_TCGroups &g);

	int getID(void) { return m_nID;}
	int getPrintOrder(void) { return m_nPrintOrder;}
	CString getGroupName(void) { return m_csGroupName;}
	int getClassIndex(void) { return m_nClassIndex;}
	CString getClassName(void) { return m_csClassName;}
};

typedef std::vector<CTransaction_TCGroups> vecTransactionTCGroups;

class CTransaction_TCClass
{
	int m_nIndex;
	CString m_csClassName;
	int m_nPrintOrder;	//new 091009

public:
	CTransaction_TCClass(void);
	CTransaction_TCClass(int index, int printorder, LPCTSTR classname);
	CTransaction_TCClass(const CTransaction_TCClass &c);

	int getClassIndex(void) { return m_nIndex;}
	CString getClassName(void) { return m_csClassName;}
	int getPrintOrder(void) { return m_nPrintOrder;}
};

typedef std::vector<CTransaction_TCClass> vecTransactionTCClass;

class CTransaction_TCSnapshot
{
	char *m_pBuffer;
	int m_nLen;
	CString m_strName;
	int m_nId;

public:
	CTransaction_TCSnapshot(char *buf, int len, CString name, int id);
	CTransaction_TCSnapshot(const CTransaction_TCSnapshot &src);
	~CTransaction_TCSnapshot();

	char* getBuffer() const;
	int getBufferLength() const;
	CString getName() const;
	int getId() const;
};

typedef std::vector<CTransaction_TCSnapshot> vecTransactionTCSnapshot;

class CTransaction_TCSnapshotHeader
{
	CString m_strName;
	int m_nId;

public:
	CTransaction_TCSnapshotHeader(CString name, int id);
	~CTransaction_TCSnapshotHeader();

	CString getName() const;
	int getId() const;
};

typedef std::vector<CTransaction_TCSnapshotHeader> vecTransactionTCSnapshotHeader;

class CTransaction_TCCompanyInfo
{
	int m_nId;
	CString m_sCompany;
	CString m_sAddress;
	CString m_sAddress2;
	CString m_sCity;
	CString m_sZipCode;
	CString m_sState;
	CString m_sCountry;
	CString m_sPhone;
	CString m_sFax;
	CString m_sEmail;
	CString m_sWebsite;

public:
	CTransaction_TCCompanyInfo(void);
	CTransaction_TCCompanyInfo(int, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR);
	CTransaction_TCCompanyInfo(const CTransaction_TCCompanyInfo &i);

	void clear(void);

	int getId(void) { return m_nId;}
	CString getCompany(void) { return m_sCompany;}
	CString getAddress(void) { return m_sAddress;}
	CString getAddress2(void) { return m_sAddress2;}
	CString getCity(void) { return m_sCity;}
	CString getZipCode(void) { return m_sZipCode;}
	CString getState(void) { return m_sState;}
	CString getCountry(void) { return m_sCountry;}
	CString getPhone(void) { return m_sPhone;}
	CString getFax(void) { return m_sFax;}
	CString getEmail(void) { return m_sEmail;}
	CString getWebsite(void) { return m_sWebsite;}

	void setId(int id) { m_nId = id;}
	void setCompany(CString str) {m_sCompany = str;}
	void setAddress(CString str) {m_sAddress = str;}
	void setAddress2(CString str) {m_sAddress2 = str;}
	void setCity(CString str) {m_sCity = str;}
	void setZipCode(CString str) {m_sZipCode = str;}
	void setState(CString str) {m_sState = str;}
	void setCountry(CString str) {m_sCountry = str;}
	void setPhone(CString str) {m_sPhone = str;}
	void setFax(CString str) {m_sFax = str;}
	void setEmail(CString str) {m_sEmail = str;}
	void setWebsite(CString str) {m_sWebsite = str;}
};


const FMTID PropSetfmtid ={
/* F29F85E0-4FF9-1068-AB91-08002B27B3D9 */
        0xf29f85e0,
        0x4ff9,
        0x1068,
        {0xab, 0x91, 0x08, 0x00, 0x2b, 0x27, 0xb3, 0xd9 }
        };


class CMySQLServer_ADODirect : public CSQLServer_ADODirect
{
public:
	CMySQLServer_ADODirect(void) : CSQLServer_ADODirect() {}

	void setDefaultGroupOrder();
	void setDefaultCompanyInfo();
	BOOL getServerVersion();
};



typedef struct _search_replace_info
{
	int m_nID;
	ORIGIN_FOR_SEARCH m_origin;
	CString m_sText;
	int m_nNumber;
	double m_fNumber;
	FOCUSED_EDIT m_focusedEdit;
	bool m_bIsNumeric;
} SEARCH_REPLACE_INFO;

class CTransaction_ProjectType
{
	int m_nID;
	CString m_sProjectType;
	CString m_sNotes;
	CString m_sCreated;
public:
	// Default constructor
	CTransaction_ProjectType(void);

	CTransaction_ProjectType(int id,LPCTSTR ProjectType,
															 LPCTSTR notes,
															 LPCTSTR created);
	
	// Copy constructor
	CTransaction_ProjectType(const CTransaction_ProjectType &);

	inline BOOL operator ==(CTransaction_ProjectType &c) const
	{
		return (m_nID == c.m_nID  &&
						m_sProjectType == c.m_sProjectType &&
						m_sNotes == c.m_sNotes);
	}


	int getID(void);
	CString getProjectType(void);
	CString getNotes(void);
	CString getCreated(void);
};
typedef std::vector<CTransaction_ProjectType> vecTransactionProjectType;



