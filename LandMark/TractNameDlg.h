#pragma once

#include "Resource.h"

// CTractNameDlg dialog

class CTractNameDlg : public CDialog
{
	DECLARE_DYNAMIC(CTractNameDlg)

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CStatic m_wndLbl;
	CStatic m_wndLbl2;
	CEdit m_wndEdit;

	void setLanguage(void);


public:

	CString m_csName;
	CString m_csPath;

	CTractNameDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTractNameDlg();

// Dialog Data
	enum { IDD = IDD_TRACTNAME_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnOK();
};
