#pragma once

#include "Resource.h"
#include "LandMarkDB.h"
#include "picturectrl.h"

// CCompanyInfoFormView form view

class CCompanyInfoFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CCompanyInfoFormView)

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sTempImageFN;
	CString m_sDBErrorMsg; 

	CString m_csErrMsgOpen;

	BOOL m_bInitialized;

	CStatic m_wndLbl1;
	CStatic m_wndLbl2;
	CStatic m_wndLbl3;
	CStatic m_wndLbl4;
	CStatic m_wndLbl5;
	CStatic m_wndLbl6;
	CStatic m_wndLbl7;
	CStatic m_wndLbl8;
	CStatic m_wndLbl9;
	CStatic m_wndLbl10;
	CStatic m_wndLbl11;
	CStatic m_wndLblMaxPic;

	CEdit m_wndEdit1;
	CEdit m_wndEdit2;
	CEdit m_wndEdit3;
	CEdit m_wndEdit4;
	CEdit m_wndEdit5;
	CEdit m_wndEdit6;
	CEdit m_wndEdit7;
	CEdit m_wndEdit8;
	CEdit m_wndEdit9;
	CEdit m_wndEdit10;
	CEdit m_wndEdit11;

	CButton m_wndBtnPicture;
	CButton m_wndBtnRemovePic;

	BOOL m_bSavePic;
	CString m_csImagePath;

	CPictureCtrl m_wndPicCtrl;

	CTransaction_TCCompanyInfo info;

	CLandMarkDB* m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;

	void saveCompanyInfo(void);

protected:
	CCompanyInfoFormView();           // protected constructor used by dynamic creation
	virtual ~CCompanyInfoFormView();

	void setLanguage(void);
	void setTextData(void);
	void setImage(void);

public:
	enum { IDD = IDD_FORMVIEW4 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnInitialUpdate();
	afx_msg void OnBnClickedBtnPicture();
	afx_msg void OnBnClickedBtnRemovePic();
	afx_msg void OnDestroy();
};


