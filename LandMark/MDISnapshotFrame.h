#pragma once


// CMDISnapshotFrame frame
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CMDISnapshotFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDISnapshotFrame)

protected:
	CMDISnapshotFrame();           // protected constructor used by dynamic creation
	virtual ~CMDISnapshotFrame();

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	HICON m_hIcon;

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);

	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
};
