// MDISnapshotFrame.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "MDISnapshotFrame.h"
#include "SnapshotView.h"
#include "Resource.h"

// CMDISnapshotFrame

IMPLEMENT_DYNCREATE(CMDISnapshotFrame, CChildFrameBase)

CMDISnapshotFrame::CMDISnapshotFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDISnapshotFrame::~CMDISnapshotFrame()
{
}

BEGIN_MESSAGE_MAP(CMDISnapshotFrame, CChildFrameBase)
	ON_WM_CREATE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()

// CMDISnapshotFrame message handlers

BOOL CMDISnapshotFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTPFrameWndBase<CMDIChildWnd>::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CMDISnapshotFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	return 0;
}

LRESULT CMDISnapshotFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE,wParam,lParam);

		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

void CMDISnapshotFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SNAPSHOTS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SNAPSHOTS;

	CXTPFrameWndBase<CMDIChildWnd>::OnGetMinMaxInfo(lpMMI);
}

void CMDISnapshotFrame::OnSetFocus(CWnd* pOldWnd)
{
	CSnapshotView *pView = (CSnapshotView*)GetActiveView();
	if (pView)
	{
		pView->UpdateToolbarButtons();
	}

	CXTPFrameWndBase<CMDIChildWnd>::OnSetFocus(pOldWnd);
}
