#pragma once


// CImportReportView view

class CImportReportView : public CView
{
	DECLARE_DYNCREATE(CImportReportView)

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sErrorMsg;

protected:
	CImportReportView();           // protected constructor used by dynamic creation
	virtual ~CImportReportView();

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	//virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual void OnInitialUpdate();
};


