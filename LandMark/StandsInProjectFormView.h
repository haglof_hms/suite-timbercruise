#pragma once

#include "stdafx.h"
#include "Resource.h"
#include "LandMarkDB.h"

/////////////////////////////////////////////////////////////////////////////
// CProjectStandsReportRec

class CProjectStandsReportRec : public CXTPReportRecord
{
	UINT m_nIndex;					

protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
		double m_fValue;
	public:
		CFloatItem(double fValue, LPCTSTR fmt_str = sz1dec)
			: CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
		}

		void setFloatItem(double value)
		{
			m_fValue = value;
			SetValue(value);
		}

		double getFloatItem(void)
		{
			return m_fValue;
		}
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
		int m_nValue;
	public:
		CIntItem(int nValue)
			: CXTPReportRecordItemNumber(nValue)
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		void setIntItem(int value)
		{
			m_nValue = value;
			SetValue(value);
		}

		int getIntItem(void)
		{
			return m_nValue;
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
		CString m_sText;
	public:
		CTextItem(CString sValue)
			: CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_sText = szText;
			SetValue(m_sText);
		}

		void setTextItem(LPCTSTR text)
		{
			m_sText = text;
			SetValue(m_sText);
		}

		CString getTextItem(void)
		{
			return m_sText;
		}
	};

	class CIconItem : public CXTPReportRecordItem
	{
		//private:
	public:
		CIconItem(int icon_id) : CXTPReportRecordItem()
		{
			SetIconIndex(icon_id);
		}

		void setIconId(int id)
		{
			SetIconIndex(id);
		}

		int getIconIndex()
		{
			return GetIconIndex();
		}
	};

public:

	CProjectStandsReportRec(void)
	{
		m_nIndex = -1;									// FileIndex
		AddItem(new CTextItem(_T("")));					// TractID
		AddItem(new CFloatItem(0.0,sz1dec));			// TractAcres
		AddItem(new CTextItem(_T("")));					// Cruise Method
		AddItem(new CTextItem(_T("")));					// CruiseDate
		AddItem(new CTextItem(_T("")));					// TractLoc
		AddItem(new CTextItem(_T("")));					// TractOwner
		AddItem(new CTextItem(_T("")));					// Cruiser
		AddItem(new CIntItem(-1));						// Project id 
	}

	CProjectStandsReportRec(CTransaction_TCStand &rec)
	{
		m_nIndex = rec.getFileIndex();								// FileIndex
		AddItem(new CTextItem(rec.getTractID()));					// TractID
		AddItem(new CFloatItem(rec.getTractAcres(),sz1dec));		// TractAcres
		AddItem(new CTextItem(rec.getCruiseMethod()));				// Cruise Method
		AddItem(new CTextItem(rec.getCruiseDate()));				// CruiseDate
		AddItem(new CTextItem(rec.getTractLoc()));					// TractLoc
		AddItem(new CTextItem(rec.getTractOwner()));				// TractOwner
		AddItem(new CTextItem(rec.getCruiser()));					// Cruiser
		AddItem(new CIntItem(rec.getPropID()));						// Project id	/*TODO: change to project*/
	}


	int getFileIndex(void)
	{
		return m_nIndex;
	}

	int getIndex(void)
	{
		return m_nIndex;
	}

	double getColumnFloat(int item)
	{
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item, double value)
	{
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)
	{
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item, int value)
	{
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)
	{
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item, LPCTSTR text)
	{
		((CTextItem*)GetItem(item))->setTextItem(text);
	}
};


// CStandsInProjectFormView form view

class CStandsInProjectFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CStandsInProjectFormView)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sYes;
	CString m_sNo;

	CString m_sMsgCap;
	CString m_sMsgDoRemoveStandCap;
	CString m_sMsgRemoveStandCap;

	int m_nActiveProjectID;

	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;


	vecTransactionTCStand/*Trakt*/ m_vecTraktForProject;
	BOOL getStandsForProject(int *prop_id);
	void isProjectUsedInObject(int trakt_id,int prop_id,CString& obj_id);

	BOOL populateReport(void);

protected:
	CStandsInProjectFormView();           // protected constructor used by dynamic creation
	virtual ~CStandsInProjectFormView();

	// My data members
	CMyReportCtrl m_wndReport2;

	BOOL setupReport2(void);
	void LoadReportState(void);
	void SaveReportState(void);

public:
	enum { IDD = IDD_FORMVIEW12 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

		void doRePopulate(void);

		BOOL isTraktInProject(void) 
		{ 
			return (m_wndReport2.GetRows()->GetCount() > 0);
		}
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
//	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
//	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton31();
	afx_msg void OnBnClickedButton32();
};


