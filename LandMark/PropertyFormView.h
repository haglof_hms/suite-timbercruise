#pragma once

#include "Resource.h"

#include "LandMarkDB.h"

// CPropertyFormView form view

class CPropertyFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPropertyFormView)

private:
	BOOL m_bInitialized;
	BOOL m_bSetFocusOnInitDone;
	BOOL m_bIsDataEnabled;
	CString	m_sLangAbrev;
	CString m_sLangFN;
	CString m_sErrCap;
	CString m_sSaveMsg;
	CString m_sDoneSavingMsg;
	CString m_sDeleteMsg;
	CString m_sMsgCap;
	CString m_sMsgCap1;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sCounty;
	CString m_sMunicipal;
	CString m_sParish;
	CString m_sName;
	CString m_sOwnerTabCaption;
	CString m_sFrameCaption;
	CString m_sDataMissinMsg;
	CString m_sPropertyActiveMsg;
	CString m_sNoPropertiesMsg;
	CString m_sNoResultInSearch;

	CString m_sSQLProps;

	BOOL m_bIsDirty;

	// Status for navigation buttons; 070125 p�d
	BOOL m_bNavButtonStartPrev;
	BOOL m_bNavButtonEndNext;

	enumACTION m_enumAction;

	void setLanguage(void);

	FOCUSED_EDIT m_enumFocusedEdit;

	BOOL getProperties(void);
	vecTransactionProperty m_vecPropertyData;
	CTransaction_property m_enteredPropertyData;
	CTransaction_property m_activePropertyData;
	BOOL doIsPropertyUsed(CTransaction_property &);

	CTransaction_county_municipal_parish m_recSelectedCMP;
	
	int m_nDBIndex;
	void populateData(int idx, BOOL bSetBars = TRUE);
	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void setEnableData(BOOL enable);
	void setEnableOwnerTab(BOOL enable);
	void setActiveTab(int tab);


	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL checkFocus(CWnd *pWnd);
protected:
	CPropertyFormView();           // protected constructor used by dynamic creation
	virtual ~CPropertyFormView();

	CXTResizeGroupBox m_wndGroup;
	CXTResizeGroupBox m_wndGroup2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLbl11;

	CMyExtEdit m_wndEdit1;	//Location
	CMyExtEdit m_wndEdit2;	//State
	CMyExtEdit m_wndEdit3;	//County
	CMyExtEdit m_wndEdit4;	//Property
	CMyExtEdit m_wndEdit5;	// *not used*
	CMyExtEdit m_wndEdit6;	// *not used*
	CMyExtEdit m_wndEdit7;	// *not used*
	CMyExtEdit m_wndEdit8;	// *not used*
	CMyExtEdit m_wndEdit9;	//Property number
	CMyExtEdit m_wndEdit10;	// *not used*

	CXTButton m_wndCountyBtn;
	CXTButton m_wndMunicipalBtn;
	CXTButton m_wndParishBtn;
	CButton m_wndRemoveBtn;

	// My methods
	BOOL getEnteredData(void);

	BOOL addProperty(void);

	void resetIsDirty(void);
	BOOL getIsDirty(void);

	void clearAll();

	void setSearchToolbarBtn(BOOL enable);
public:
	// Need to be PUBLIC
	BOOL isDataChanged(void);
	BOOL saveProperty(void);
	void setCountyMunicpalAndParish(CTransaction_county_municipal_parish);
	BOOL doPopulateNext(void);
	BOOL doPopulatePrev(void);
	void doPopulate(int,bool set_by_index = true, BOOL bSetBars = TRUE);
	void doRePopulate(void);
	void refreshNavButtons(void);
	void refreshProperties(void);
	
	void doSetSearchBtn(BOOL enable);
	// Set public, removeProperty is called from
	// CPropertyTabView. This is also the case
	// with removePropOwner in CMDIPropOwnerFormView; 070126 p�d
	BOOL removeProperty(void);	

	BOOL getStartPrevStatus(void)	{	return m_bNavButtonStartPrev;	}

	BOOL getEndNextStatus(void)		{	return m_bNavButtonEndNext;	}
	
	int getDBIndex(void)					{	return m_nDBIndex;	}

	void doSetNavigationBar(void);

	BOOL doRePopulateFromSearch(LPCTSTR sql,bool goto_first);

	CTransaction_property &getActiveProperty(void)	{ return m_activePropertyData; }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW11 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnImport();
	afx_msg void OnSearchReplace();

	/*TODO: afx_msg void OnChangeObjID();
	*/
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton55();
	afx_msg void OnEnSetfocusEdit51();
	afx_msg void OnEnSetfocusEdit52();
	afx_msg void OnEnSetfocusEdit53();
	afx_msg void OnEnSetfocusEdit517();
	afx_msg void OnEnSetfocusEdit54();
	afx_msg void OnEnSetfocusEdit55();
	afx_msg void OnEnSetfocusEdit56();
	afx_msg void OnEnSetfocusEdit57();
	afx_msg void OnEnSetfocusEdit58();
	afx_msg void OnEnSetfocusEdit59();
};


