// CompanyInfoFormView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "CompanyInfoFormView.h"
#include "MDICompanyInfoFrame.h"


// CCompanyInfoFormView

IMPLEMENT_DYNCREATE(CCompanyInfoFormView, CXTResizeFormView)

CCompanyInfoFormView::CCompanyInfoFormView()
	: CXTResizeFormView(CCompanyInfoFormView::IDD)
{
	m_pDB = NULL;
	m_bConnected = FALSE;
	m_bInitialized = FALSE;
	m_bSavePic = FALSE;
	m_csImagePath = _T("");
}

CCompanyInfoFormView::~CCompanyInfoFormView()
{
	if(m_pDB != NULL)
		delete m_pDB;
}

void CCompanyInfoFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_STATIC1, m_wndLbl1);
	DDX_Control(pDX, IDC_STATIC2, m_wndLbl2);
	DDX_Control(pDX, IDC_STATIC3, m_wndLbl3);
	DDX_Control(pDX, IDC_STATIC4, m_wndLbl4);
	DDX_Control(pDX, IDC_STATIC5, m_wndLbl5);
	DDX_Control(pDX, IDC_STATIC6, m_wndLbl6);
	DDX_Control(pDX, IDC_STATIC7, m_wndLbl7);
	DDX_Control(pDX, IDC_STATIC8, m_wndLbl8);
	DDX_Control(pDX, IDC_STATIC9, m_wndLbl9);
	DDX_Control(pDX, IDC_STATIC10, m_wndLbl10);
	DDX_Control(pDX, IDC_STATIC11, m_wndLbl11);
	DDX_Control(pDX, IDC_MAX_PIC, m_wndLblMaxPic);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit10);
	DDX_Control(pDX, IDC_EDIT11, m_wndEdit11);

	DDX_Control(pDX, IDC_BTN_PICTURE, m_wndBtnPicture);
	DDX_Control(pDX, IDC_BTN_REMOVE_PIC, m_wndBtnRemovePic);

	DDX_Control(pDX, IDC_STATIC_PIC, m_wndPicCtrl);
}

BEGIN_MESSAGE_MAP(CCompanyInfoFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BTN_PICTURE, &CCompanyInfoFormView::OnBnClickedBtnPicture)
	ON_BN_CLICKED(IDC_BTN_REMOVE_PIC, &CCompanyInfoFormView::OnBnClickedBtnRemovePic)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CCompanyInfoFormView diagnostics

#ifdef _DEBUG
void CCompanyInfoFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CCompanyInfoFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CCompanyInfoFormView message handlers

BOOL CCompanyInfoFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;
}

void CCompanyInfoFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CCompanyInfoFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);
	m_sTempImageFN.Format(_T("%s\\%s"), getMyDocumentsDir(),TEMP_IMAGE_FN);

	if(!m_bInitialized)
	{
		setLanguage();
		
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

		m_bInitialized = TRUE;

		if(m_pDB != NULL)
		{
			if((m_pDB->getCompanyInfo(info) == TRUE))
			{
				setTextData();
				if(m_pDB->loadImage(m_sTempImageFN, info) == TRUE)
					setImage();
			}
			else
			{
				AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
				CMDICompanyInfoFrame *pFrame = (CMDICompanyInfoFrame*)getFormViewParentByID(IDD_FORMVIEW4);
				if(pFrame)
					pFrame->m_bDBConnOk = FALSE;
				GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
				return;

			}
		}
		else
		{
			AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
			CMDICompanyInfoFrame *pFrame = (CMDICompanyInfoFrame*)getFormViewParentByID(IDD_FORMVIEW4);
			if(pFrame)
				pFrame->m_bDBConnOk = FALSE;
			GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
			return;
		}
	}

	m_wndEdit1.SetLimitText(50);	//company
	m_wndEdit2.SetLimitText(50);	//address
	m_wndEdit3.SetLimitText(50);	//city
	m_wndEdit4.SetLimitText(20);	//zip
	m_wndEdit5.SetLimitText(50);	//state
	m_wndEdit6.SetLimitText(30);	//phone
	m_wndEdit7.SetLimitText(30);	//fax
	m_wndEdit8.SetLimitText(50);	//email
	m_wndEdit9.SetLimitText(50);	//website
	m_wndEdit10.SetLimitText(50);	//address2
	m_wndEdit11.SetLimitText(50);	//country
}

void CCompanyInfoFormView::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING185));	//company
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING186));	//address
			m_wndLbl3.SetWindowText(xml->str(IDS_STRING187));	//city
			m_wndLbl4.SetWindowText(xml->str(IDS_STRING188));	//zip
			m_wndLbl5.SetWindowText(xml->str(IDS_STRING189));	//state
			m_wndLbl6.SetWindowText(xml->str(IDS_STRING190));	//phone
			m_wndLbl7.SetWindowText(xml->str(IDS_STRING191));	//fax
			m_wndLbl8.SetWindowText(xml->str(IDS_STRING192));	//email
			m_wndLbl9.SetWindowText(xml->str(IDS_STRING193));	//website
			m_wndLbl10.SetWindowText(xml->str(IDS_STRING195));	//address2
			m_wndLbl11.SetWindowText(xml->str(IDS_STRING196));	//country

			m_wndLblMaxPic.SetWindowText(xml->str(IDS_STRING198));

			m_wndBtnPicture.SetWindowText(xml->str(IDS_STRING194));
			m_wndBtnRemovePic.SetWindowText(xml->str(IDS_STRING197));

			m_csErrMsgOpen = xml->str(IDS_STRING168);
			m_sDBErrorMsg = xml->str(IDS_STRING105);
		}
		delete xml;
	}
}

void CCompanyInfoFormView::setTextData()
{
		m_wndEdit1.SetWindowText(info.getCompany());
		m_wndEdit2.SetWindowText(info.getAddress());
		m_wndEdit10.SetWindowText(info.getAddress2());
		m_wndEdit3.SetWindowText(info.getCity());
		m_wndEdit4.SetWindowText(info.getZipCode());
		m_wndEdit5.SetWindowText(info.getState());
		m_wndEdit11.SetWindowText(info.getCountry());
		m_wndEdit6.SetWindowText(info.getPhone());
		m_wndEdit7.SetWindowText(info.getFax());
		m_wndEdit8.SetWindowText(info.getEmail());
		m_wndEdit9.SetWindowText(info.getWebsite());
}

void CCompanyInfoFormView::setImage()
{
	//load image
	m_wndPicCtrl.Load(m_sTempImageFN);
	
	//delete temp file
	CFile::Remove(m_sTempImageFN);
}

BOOL CCompanyInfoFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	if(pData->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData, pData->lpData, sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if(m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

LRESULT CCompanyInfoFormView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case ID_DELETE_ITEM:
		info.clear();
		setTextData();
		m_wndEdit1.SetModify(TRUE);	//otherwise data won't be saved to db in OnDestroy
		break;
	case ID_SAVE_ITEM:
		saveCompanyInfo();
		break;
	}

	return 0L;
}

void CCompanyInfoFormView::OnBnClickedBtnPicture()
{
	CFileDialog *pOpenFileDlg = new CFileDialog(TRUE);
	CString csPath;

	pOpenFileDlg->m_ofn.lpstrFilter = _T("Pictures (*.bmp; *.jpg)\0*.bmp;*.jpg\0\0"); 
	pOpenFileDlg->m_ofn.Flags = OFN_EXPLORER|OFN_HIDEREADONLY;

	if(pOpenFileDlg->DoModal() != IDOK)
	{
		delete pOpenFileDlg;
		return;
	}
	
	csPath = pOpenFileDlg->GetPathName();

	delete pOpenFileDlg;

	if(m_wndPicCtrl.Load(csPath) == FALSE)
	{
		AfxMessageBox(m_csErrMsgOpen);
		m_bSavePic = FALSE;
		m_csImagePath = _T("");
	}
	else
	{
		m_bSavePic = TRUE;
		m_csImagePath = csPath;
	}

}

void CCompanyInfoFormView::OnBnClickedBtnRemovePic()
{
	m_wndPicCtrl.FreeData();

	if(m_pDB != NULL)
	{
		m_pDB->removeImage(info);
		m_bSavePic = FALSE;
		m_csImagePath = _T("");
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
	
	Invalidate();
	UpdateWindow();
	m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);
}

void CCompanyInfoFormView::saveCompanyInfo()
{
	CString csTmp = _T("");

	m_wndEdit1.GetWindowText(csTmp);
	info.setCompany(csTmp);
	m_wndEdit2.GetWindowText(csTmp);
	info.setAddress(csTmp);
	m_wndEdit10.GetWindowText(csTmp);
	info.setAddress2(csTmp);
	m_wndEdit3.GetWindowText(csTmp);
	info.setCity(csTmp);
	m_wndEdit4.GetWindowText(csTmp);
	info.setZipCode(csTmp);
	m_wndEdit5.GetWindowText(csTmp);
	info.setState(csTmp);
	m_wndEdit11.GetWindowText(csTmp);
	info.setCountry(csTmp);
	m_wndEdit6.GetWindowText(csTmp);
	info.setPhone(csTmp);
	m_wndEdit7.GetWindowText(csTmp);
	info.setFax(csTmp);
	m_wndEdit8.GetWindowText(csTmp);
	info.setEmail(csTmp);
	m_wndEdit9.GetWindowText(csTmp);
	info.setWebsite(csTmp);

	if(m_pDB != NULL)
	{
		m_pDB->saveCompanyInfo(info);
		if(m_bSavePic == TRUE && m_csImagePath != _T(""))
		{
			m_pDB->saveImage(m_csImagePath, info);
			m_bSavePic = FALSE;
		}
	}
	else
		AfxMessageBox(m_sDBErrorMsg, MB_ICONEXCLAMATION);
}

void CCompanyInfoFormView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();

	//save info if something has been changed
	if(m_bInitialized == TRUE)
	{
		if(m_wndEdit1.GetModify() || m_wndEdit2.GetModify() || m_wndEdit3.GetModify() || m_wndEdit4.GetModify() || m_wndEdit5.GetModify() || 
			m_wndEdit6.GetModify() || m_wndEdit7.GetModify() || m_wndEdit8.GetModify() || m_wndEdit9.GetModify() || 
			m_wndEdit10.GetModify() || m_wndEdit11.GetModify() || m_bSavePic)
			saveCompanyInfo();
	}
}
