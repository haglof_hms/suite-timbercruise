#include "stdafx.h"
#include "Resource.h"
#include "MDIContactsFrame.h"
#include "ContactsFormView.h"
#include "ContactsTabView.h"
#include "CategoryFormView.h"

/////////////////////////////////////////////////////////////////////////////
// CMDIContactsFrame

IMPLEMENT_DYNCREATE(CMDIContactsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIContactsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIContactsFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_OPEN, OnUpdateTBBTNImport)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_CREATE, OnUpdateTBBTNCreate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIContactsFrame::CMDIContactsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bIsTBtnEnabledOpen = TRUE;
	m_bIsTBtnEnabledCreate = TRUE;

}

CMDIContactsFrame::~CMDIContactsFrame()
{
}

void CMDIContactsFrame::OnDestroy(void)
{

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTSFRAME_KEY);
	SavePlacement(this, csBuf);

}

int CMDIContactsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResFN();

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR5);
	m_wndToolBar.SetPosition(xtpBarTop);

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING240/*213*/);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING241/*2360*/),
						xml.str(IDS_STRING242/*237*/));

			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();

					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR5)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RSTR_TB_IMPORT,xml.str(IDS_STRING243/*312*/),FALSE);	//don't show
						setToolbarBtn(sTBResFN,p->GetAt(1),RSTR_TB_SEARCH,xml.str(IDS_STRING244/*310*/),FALSE);	//don't show
						setToolbarBtn(sTBResFN,p->GetAt(2),RSTR_TB_TOOLS,_T(""),FALSE);	//don't show

					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
		}	// if (xml.Load(m_sLangFN))
		xml.clean();
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDIContactsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIContactsFrame diagnostics

#ifdef _DEBUG
void CMDIContactsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIContactsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIContactsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CONTACTS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CONTACTS;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIContactsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIContactsFrame::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	
	CContactsFormView *pView = (CContactsFormView *)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}	// if (pView)

}

// load the placement in OnShowWindow()
void CMDIContactsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTSFRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIContactsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIContactsFrame::OnClose(void)
{

	CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW8);
	if (pTabView != NULL)
	{
		CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView != NULL)
		{
			pView->saveContact(0);
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)
	
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIContactsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW8);
		if (pTabView != NULL)
		{
			CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pView != NULL)
			{
				pView->isDataChanged();	//check if to save current contact before open list
			}	// if (pView != NULL)
		}	// if (pTabView != NULL)
		showFormView(IDD_REPORTVIEW1,m_sLangFN,0);		
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			if (msg->getValue1() == 113)
			{
				CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW8);
				if (pTabView != NULL)
				{
					CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView != NULL)
					{
						pView->refreshContacts();
						pView = NULL;
					}		
					pTabView = NULL;
				}
			}	// if (msg->getValue1() == 113)
		}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
	}		
	else
	{

		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(WM_USER_MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	
	return 0L;
}


// MY METHODS
void CMDIContactsFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}


void CMDIContactsFrame::OnUpdateTBBTNImport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledOpen);
}


void CMDIContactsFrame::OnUpdateTBBTNCreate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledCreate);
}

// CMDIContactsFrame message handlers

void CMDIContactsFrame::setEnableToolbar(BOOL enable_open,BOOL enable_create)
{
	m_bIsTBtnEnabledOpen = enable_open;
	m_bIsTBtnEnabledCreate = enable_create;
}



/////////////////////////////////////////////////////////////////////////////
// CMDICategoryFrame

IMPLEMENT_DYNCREATE(CMDICategoryFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDICategoryFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDICategoryFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDICategoryFrame::CMDICategoryFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDICategoryFrame::~CMDICategoryFrame()
{
}

void CMDICategoryFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CATEGORYFRAME_KEY);
	SavePlacement(this, csBuf);
}

int CMDICategoryFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING214),
						xml.str(IDS_STRING215));
		}	// if (xml.Load(m_sLangFN))
		xml.clean();
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDICategoryFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDICategoryFrame diagnostics

#ifdef _DEBUG
void CMDICategoryFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDICategoryFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDICategoryFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CATEGORY;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CATEGORY;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDICategoryFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDICategoryFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CCategoryFormView *pView = (CCategoryFormView *)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}	// if (pView)

}

// load the placement in OnShowWindow()
void CMDICategoryFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CATEGORYFRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDICategoryFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDICategoryFrame::OnClose(void)
{
	CCategoryFormView *pView = (CCategoryFormView *)GetActiveView();
	if (pView)
	{
//		if (pView->getIsDirty())
//		{
// Commented out (PL); 070402 p�d
//			if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//			{
				pView->saveCategory();
//			}	// if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//		}	// if (pView->getIsDirty())
	}	// if (pView)
	
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDICategoryFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}


// MY METHODS


// CMDICategoryFrame message handlers
