#include "stdafx.h"
#include "MDIProjectFrame.h"
#include "Resource.h"
#include "ProjectTabView.h"


/////////////////////////////////////////////////////////////////////////////
// CMDIProjectFrame

HWND CMDIProjectFrame::m_TopWindow = NULL;

IMPLEMENT_DYNCREATE(CMDIProjectFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIProjectFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIProjectFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_OPEN, OnUpdateTBBTNImport)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_CREATE, OnUpdateTBBTNCreate)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_TOOLS, OnUpdateTBBTNTools)

	ON_XTP_CREATECONTROL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIProjectFrame::CMDIProjectFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_pToolsPopup = NULL;
	m_bIsTBtnEnabledOpen = TRUE;
	m_bIsTBtnEnabledCreate = TRUE;
	m_bIsTBtnEnabledTools = TRUE;
}

CMDIProjectFrame::~CMDIProjectFrame()
{
}

void CMDIProjectFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROJECTFRAME_KEY);
	SavePlacement(this, csBuf);
}

int CMDIProjectFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResFN();

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR5);
	m_wndToolBar.SetPosition(xtpBarTop);


	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING240/*213*/);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING252/*2360*/),
						xml.str(IDS_STRING242/*237*/));
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();

					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR5)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RSTR_TB_IMPORT,xml.str(IDS_STRING243/*312*/),FALSE);	//don't show
						setToolbarBtn(sTBResFN,p->GetAt(1),RSTR_TB_SEARCH,xml.str(IDS_STRING244/*310*/),FALSE);	//don't show
						setToolbarBtn(sTBResFN,p->GetAt(2),RSTR_TB_TOOLS,xml.str(IDS_STRING293/*318*/));	//
					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDIProjectFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	m_TopWindow = m_hWnd;

	return TRUE;
}

// CMDIProjectFrame diagnostics

#ifdef _DEBUG
void CMDIProjectFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIProjectFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIProjectFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROJECT;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROJECT;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIProjectFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIProjectFrame::OnSetFocus(CWnd* wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
	CProjectTabView *pTabView = (CProjectTabView *)getFormViewByID(IDD_FORMVIEW13);
	if (pTabView != NULL)
	{
		CProjectFormView* pView = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView != NULL)
		{
			pView->doSetNavigationBar();
			pView = NULL;
		}		
		pTabView = NULL;
	}

	CMDIChildWnd::OnSetFocus(wnd);
}

// load the placement in OnShowWindow()
void CMDIProjectFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROJECTFRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIProjectFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIProjectFrame::OnClose(void)
{
	CProjectTabView *pView = (CProjectTabView *)getFormViewByID(IDD_FORMVIEW13);
	if (pView)
	{
		/*TODO: CProjectOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CProjectOwnerFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(1)->GetHandle()));
		if (pView1)
		{
			pView1->isDataChanged();
		}*/
		
		CProjectFormView* pView0 = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView0)
		{
			pView0->isDataChanged();
		}		
	}

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

 // Check if the top-most window is closing
 if (m_hWnd==m_TopWindow)
  m_TopWindow = NULL;

	CMDIChildWnd::OnClose();
}

void CMDIProjectFrame::OnUpdateTBBTNImport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledOpen);
}

void CMDIProjectFrame::OnUpdateTBBTNCreate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledCreate);
}

void CMDIProjectFrame::OnUpdateTBBTNTools(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledTools);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIProjectFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		CProjectTabView *pTabView = (CProjectTabView *)getFormViewByID(IDD_FORMVIEW13);
		if (pTabView != NULL)
		{
			CProjectFormView* pView = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pView != NULL)
			{
				pView->isDataChanged();
				pView = NULL;
			}		
			pTabView = NULL;
		}
		showFormView(IDD_REPORTVIEW5,m_sLangFN,0);		
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		CString S;
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			if (msg->getValue1() == 122)
			{
				CProjectTabView *pTabView = (CProjectTabView *)getFormViewByID(IDD_FORMVIEW13);
				if (pTabView != NULL)
				{
					CProjectFormView* pView = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView != NULL)
					{
						pView->refreshProjects();
						pView = NULL;
					}		
					pTabView = NULL;
				}
			}	// if (msg->getValue1() == 122)
			/*TODO: if (msg->getValue1() == 10)	// From GIS; 090810 p�d
			{
				CProjectTabView *pTabView = (CProjectTabView *)getFormViewByID(IDD_FORMVIEW13);
				if (pTabView != NULL)
				{
					CProjectFormView* pView = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView != NULL)
					{
						pView->doPopulate(msg->getValue2(),false);
						pView = NULL;
					}		
				}
			}	// if (msg->getValue1() == 10)
*/
		}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
	}		
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(WM_USER_MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}



// MY METHODS
void CMDIProjectFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

int CMDIProjectFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_TBBTN_TOOLS)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_vecExcudedItemsOnTab0.clear();
				m_vecExcudedItemsOnTab1.clear();
				m_vecExcudedItemsOnTab2.clear();

				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_ADD_STAND_TO_PROJECT);
				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_DEL_STAND_FROM_PROJECT);
				//m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_ADD_CONTACT);
				//m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_REMOVE_CONTACT);

				//m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_CHANGE_OBJID);
				//m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_ADD_STAND_TO_PROJECT);
				//m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_DEL_STAND_FROM_PROJECT);

				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_ADD_STAND_TO_PROJECT);
				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_DEL_STAND_FROM_PROJECT);
				//m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_CHANGE_OBJID);
				//m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_ADD_CONTACT);
				//m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_REMOVE_CONTACT);

				m_pToolsPopup = new CMyControlPopup();
				m_pToolsPopup->SetStyle(xtpButtonIcon);
				m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab0);
				
				//m_pToolsPopup->addMenuIDAndText(ID_TOOLS_CHANGE_OBJID,xml.str(IDS_STRING294));///*320));
				//m_pToolsPopup->addMenuIDAndText();	// Add separator
				//m_pToolsPopup->addMenuIDAndText(ID_TOOLS_ADD_CONTACT,xml.str(IDS_STRING295/*311*/));
				//m_pToolsPopup->addMenuIDAndText(ID_TOOLS_REMOVE_CONTACT,xml.str(IDS_STRING328/*322*/));	
				//m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_ADD_STAND_TO_PROJECT,xml.str(IDS_STRING346));
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_DEL_STAND_FROM_PROJECT,xml.str(IDS_STRING347));

				lpCreateControl->pControl = m_pToolsPopup;
				xml.clean();		
			}
		}
		return TRUE;
	}
	return FALSE;
}


void CMDIProjectFrame::setExcludedToolItems(int tab_selected)
{
	switch (tab_selected)
	{
		case 0 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab0);
		break;
		case 1 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab1);
		break;
		case 2 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab2);
		break;
	};
}

void CMDIProjectFrame::setEnableToolbar(BOOL enable_open,BOOL enable_create,BOOL enable_tools)
{
	m_bIsTBtnEnabledOpen = enable_open;
	m_bIsTBtnEnabledCreate = enable_create;
	m_bIsTBtnEnabledTools = enable_tools;
}

// CMDIProjectFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CProjectSelectListFrame

IMPLEMENT_DYNCREATE(CProjectSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CProjectSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CProjectSelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_FILTER_OFF, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CProjectSelectListFrame::CProjectSelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CProjectSelectListFrame::~CProjectSelectListFrame()
{
}

void CProjectSelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROJECT_SELLIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CProjectSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooser.Create(this, IDD_FIELD_CHOOSER,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS))
		return -1;      // fail to create

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
		return -1;      // fail to create

	// docking for field chooser
	m_wndFieldChooser.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooser, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooser, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	// Set to 0 = No Docking; 090224 p�d
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CProjectSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROJECT_SELLIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CProjectSelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CProjectSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CProjectSelectListFrame diagnostics

#ifdef _DEBUG
void CProjectSelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CProjectSelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CProjectSelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROJECT_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROJECT_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CProjectSelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CProjectSelectListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CProjectSelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CProjectSelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while(pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE, wParam, lParam);

		}
	}

	return 0L;
}

void CProjectSelectListFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CProjectSelectListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sToolTipFilter = xml.str(IDS_STRING121/*300*/);
			m_sToolTipFilterOff = xml.str(IDS_STRING122/*301*/);
			m_sToolTipPrintOut = xml.str(IDS_STRING123/*302*/);
			m_sToolTipSearch = xml.str(IDS_STRING244/*310*/);

			m_wndFieldChooser.SetWindowText((xml.str(IDS_STRING268/*242*/)));
			m_wndFilterEdit.SetWindowText((xml.str(IDS_STRING269/*243*/)));
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CProjectSelectListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR6)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RSTR_TB_FILTER,m_sToolTipFilter);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RSTR_TB_FILTER_OFF,m_sToolTipFilterOff);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RSTR_TB_PRINT,m_sToolTipPrintOut);	//
					setToolbarBtn(sTBResFN,p->GetAt(3),RSTR_TB_IMPORT,_T(""),FALSE);	//don't show
					setToolbarBtn(sTBResFN,p->GetAt(4),RSTR_TB_IMPORT,_T(""),FALSE);	//don't show

				}	// if (nBarID == IDR_TOOLBAR6)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CProjectSelectListFrame message handlers


