// MDIExternalDocsFrame.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "MDIExternalDocsFrame.h"
#include "Resource.h"
#include "ExternalDocsView.h"


// CMDIExternalDocsFrame

IMPLEMENT_DYNCREATE(CMDIExternalDocsFrame, CChildFrameBase)

CMDIExternalDocsFrame::CMDIExternalDocsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIExternalDocsFrame::~CMDIExternalDocsFrame()
{
}


BEGIN_MESSAGE_MAP(CMDIExternalDocsFrame, CChildFrameBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
END_MESSAGE_MAP()


// CMDIExternalDocsFrame message handlers

BOOL CMDIExternalDocsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CXTPFrameWndBase<CMDIChildWnd>::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CMDIExternalDocsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTPFrameWndBase<CMDIChildWnd>::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	//create toolbar
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR4);
	m_wndToolBar.SetPosition(xtpBarTop);

	m_sAbrevLangSet.Format(_T("%s"), getLangSet());
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	setLanguage();

	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString csResFN = getToolBarResFN();
	CXTPToolBar *pToolBar = &m_wndToolBar;

	if(pToolBar && pToolBar->IsBuiltIn())
	{
		if(pToolBar->GetType() != xtpBarTypeMenuBar)
		{
			UINT nBarID = pToolBar->GetBarID();
			pToolBar->LoadToolBar(nBarID, FALSE);
			CXTPControls *p = pToolBar->GetControls();
			if(p && nBarID == IDR_TOOLBAR4)
			{
				//hResModule = LoadLibraryEx(sTBResFN, NULL, DONT_RESOLVE_DLL_REFERENCES|LOAD_LIBRARY_AS_DATAFILE);
				//if(hResModule)
				{
					pCtrl = p->GetAt(0);
					pCtrl->SetTooltip(m_sToolTipAdd);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_ADD);
					//hIcon = LoadIcon(hResModule, RSTR_TB_ADD);
					if(hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(1);
					pCtrl->SetTooltip(m_sToolTipRem);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_DEL);
					//hIcon = LoadIcon(hResModule, RSTR_TB_DEL);
					if(hIcon) pCtrl->SetCustomIcon(hIcon);
				}
			}//if(p && nBarID == IDR_TOOLBAR4)
		}//if(pToolBar->GetType() != xtpBarTypeMenuBar)
	}//if(pToolBar && pToolBar->IsBuiltIn())

	m_bFirstOpen = TRUE;

	return 0;
}

void CMDIExternalDocsFrame::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sMsgCap = xml->str(IDS_STRING205);
			m_sToolTipAdd = xml->str(IDS_STRING208);
			m_sToolTipRem = xml->str(IDS_STRING209);
		}

		delete xml;
	}
}


void CMDIExternalDocsFrame::OnSize(UINT nType, int cx, int cy)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnSize(nType, cx, cy);
}

void CMDIExternalDocsFrame::OnDestroy()
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_TIMBERCRUISE_EXTERNAL_DOCS_WP);
	SavePlacement(this, csBuf);
}

void CMDIExternalDocsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_EXTERNAL_DOCS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_EXTERNAL_DOCS;

	CXTPFrameWndBase<CMDIChildWnd>::OnGetMinMaxInfo(lpMMI);
}

void CMDIExternalDocsFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);

}

void CMDIExternalDocsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_TIMBERCRUISE_EXTERNAL_DOCS_WP);
		LoadPlacement(this, csBuf);
  }

}

void CMDIExternalDocsFrame::OnSetFocus(CWnd* pOldWnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CMDIExternalDocsFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CExternalDocsView *pRepView = (CExternalDocsView*)getFormViewByID(IDD_FORMVIEW7);
	if (pRepView != NULL)
	{
		pRepView->saveExternalDocs();

		pRepView = NULL;

	}

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}

void CMDIExternalDocsFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if(m_wndToolBar.GetSafeHwnd() != NULL)
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);
		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}

	CMDIChildWnd::OnPaint();
	// Do not call CXTPFrameWndBase<CMDIChildWnd>::OnPaint() for painting messages
}

LRESULT CMDIExternalDocsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument* pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while(pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE, wParam, lParam);
		}
	}

	return 0L;
}