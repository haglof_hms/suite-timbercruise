#pragma once

#ifndef _LANDMARK_H_
#define _LANDMARK_H_

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include <vector>

extern "C" AFX_EXT_API void InitSuite(CStringArray *, vecINDEX_TABLE &, vecINFO_TABLE &);

extern "C" AFX_EXT_API void OpenSuite(int idx, LPCTSTR func, CWnd *, vecINDEX_TABLE &, int *ret);

// Create tables, function called at init of suite and when new database (company) is created
extern "C" AFX_EXT_API void DoDatabaseTables(LPCTSTR);

//Alter tables, function called at init of suite and when database is changed
extern "C" AFX_EXT_API void DoAlterTables(LPCTSTR);

#endif
