
#include "stdafx.h"

#include "ProjectFormView.h"
/*TODO: #include "ProjectOwnerFormView.h"*/
#include "StandsInProjectFormView.h"
#include "ProjectTabView.h"


///////////////////////////////////////////////////////////////
// CProjectTabView 

IMPLEMENT_DYNCREATE(CProjectTabView, CView)

BEGIN_MESSAGE_MAP(CProjectTabView, CView)
	//{{AFX_MSG_MAP(CPricelistsView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL1, OnSelectedChanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CProjectTabView::CProjectTabView()
{
	m_bInitialized = FALSE;
}

CProjectTabView::~CProjectTabView()
{
}

BOOL CProjectTabView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if (!CView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;

}

void CProjectTabView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	if (m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,1,30,rect.right-1,rect.bottom-32);
	}
}

void CProjectTabView::OnDestroy()
{
	m_font.DeleteObject();
	CView::OnDestroy();
}

void CProjectTabView::OnClose()
{
	CView::OnClose();
}

int CProjectTabView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Tab control
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, CRect(0, 0, 0, 0), this, ID_TABCONTROL1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = FALSE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = FALSE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
//	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );
//	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS, NULL, 0, CSize(16, 16), xtpImageNormal);
	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	RLFReader xml;
	if (xml.Load(m_sLangFN))
	{
		AddView(RUNTIME_CLASS(CProjectFormView), xml.str(IDS_STRING308/*2480*/),0, -1);
		/*TODO: AddView(RUNTIME_CLASS(CProjectOwnerFormView), xml.str(IDS_STRING309/*2481),1, -1);*/
		AddView(RUNTIME_CLASS(CStandsInProjectFormView), xml.str(IDS_STRING310/*2482*/),2, -1);
		
		xml.clean();
	}
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{
		CFrameWnd* pFrame = GetParentFrame();
		CProjectFormView* pView = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(0)->GetHandle()));
		ASSERT_KINDOF(CProjectFormView, pView);
		// Sets the Active view.
		// OBS! This is used on function GetActiveView() in 
		//	e.g. CMDIContactsFrame::OnClose(); 070111 p�d
		pFrame->SetActiveView(pView);
	}	// if (m_wndTabControl.getNumOfTabPages() > 0)

	return 0;
}

void CProjectTabView::OnInitialUpdate( )
{
	CView::OnInitialUpdate();

	if (!m_bInitialized)
	{
		setLanguage();

		LOGFONT lf;
    memset(&lf,0,sizeof(lf));
    lf.lfHeight = 16;
		lf.lfWeight = FW_BOLD;
    _tcscpy(lf.lfFaceName,_T("Lucida Sans Unicode"));
    if (!m_font.CreateFontIndirect(&lf))
    {
       MessageBox(_T("Unable to set font"),MB_OK);
    }

		m_bInitialized = TRUE;
	}	// if (!m_bInitialized)
}

// Catch message sent from (WM_USER_MSG_SUITE)
LRESULT CProjectTabView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	CString S;
	CFrameWnd* pFrame = GetParentFrame();
	CProjectFormView* pView0 = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(0)->GetHandle()));
	/*TODO: CProjectOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CProjectOwnerFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(1)->GetHandle()));
	*/
	CStandsInProjectFormView* pView1 = DYNAMIC_DOWNCAST(CStandsInProjectFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(1)->GetHandle()));

	switch (wParam)
	{
		// CStandsInProjectFormView
		case 2 :
		{

			// Check if data has changed on ProjectFormView before
			// changing tab; 070118 p�d
			if (pView0)
			{
				pView0->isDataChanged();
			}
			if (pView1)
			{
				/*TODO: pView1->isDataChanged();
				*/
			}
			/*TODO: if (pView2)
			{
				pView2->SetFocus();
				pView2->populateReport();
				pFrame->SetActiveView(pView2);
			}
			*/
			break;
		}
		// CProjectFormView
		case 1 :
		{

			// Check if data has changed on CMDIPropOwnerFormView before
			// changing tab; 070118 p�d
			if (pView1)
			{
				/*TODO: pView1->isDataChanged();
				*/
			}
			
			if (pView0)
			{
				pView0->SetFocus();
				pFrame->SetActiveView(pView0);
			}

			break;
		}
		// CMDIPropOwnerFormView
		case 0 :
		{

			// Check if data has changed on ProjectFormView before
			// changing tab; 070118 p�d
			if (pView0)
			{
				pView0->isDataChanged();
			}
			
			if (pView1)
			{
				pView1->SetFocus();
				pView1->populateReport();
				pFrame->SetActiveView(pView1);
			}
			
			break;
		}

		case ID_SAVE_ITEM :
		{
			CXTPTabManagerItem *pManager = m_wndTabControl.getSelectedTabPage();
			if (pManager)
			{
				if (pManager->GetIndex() == 0)
				{
					pView0->isDataChanged();
				}
				
				if (pManager->GetIndex() == 1)
				{
					/*TODO: pView1->isDataChanged();
					*/
				}
				
				
			}
			break;
		}	// case ID_SAVE_ITEM :

	}	// switch (wParam)

	return 0L;
}

void CProjectTabView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		CMDIProjectFrame* pFrame = (CMDIProjectFrame*)GetParentFrame();
		CView* pView = DYNAMIC_DOWNCAST(CView, CWnd::FromHandle(m_wndTabControl.GetSelectedItem()->GetHandle()));
		ASSERT_KINDOF(CView, pView);
		pFrame->SetActiveView(pView);
		int nIndex = pItem->GetIndex();
		if (nIndex > -1)
		{
			if(!pFrame)
				return;

			pFrame->setEnableToolbar(nIndex == 0,nIndex == 0,TRUE);
			
			if (nIndex == 0)
			{
				CProjectFormView* pView0 = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(0)->GetHandle()));
				if (pView0 != NULL)
				{
					// If data's changed, do an update; 071214 p�d
					pView0->isDataChanged();
					pView0->doSetSearchBtn(nIndex == 0);
				}
			}	// if (nIndex == 0)
			else if (nIndex == 2)
			{
				/*TODO: CProjectOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CProjectOwnerFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(1)->GetHandle()));
				if (pView1 != NULL)
				{
					pView1->populateReport();
				}*/
			
			}	// if (nIndex == 1)
			else if (nIndex == 1)
			{
				CStandsInProjectFormView* pView2 = DYNAMIC_DOWNCAST(CStandsInProjectFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(1)->GetHandle()));
				if (pView2 != NULL)
				{
					pView2->populateReport();
				}
				
			}	// if (nIndex == 1)

			if (pFrame != NULL)
			{
				pFrame->setExcludedToolItems(nIndex);
			}
			
		}	// if (nIndex > -1)
	}	// if (pItem != NULL)
}


#ifdef _DEBUG
void CProjectTabView::AssertValid() const
{
	CView::AssertValid();
}

void CProjectTabView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMDIFrameDoc* CProjectTabView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMDIFrameDoc)));
	return (CMDIFrameDoc*)m_pDocument;
}

#endif

void CProjectTabView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	RECT rect;
	GetClientRect(&rect);

	pDC->SelectObject(&m_font);
	pDC->SelectObject(GetStockObject(HOLLOW_BRUSH));
	pDC->SetBkMode( TRANSPARENT );
	pDC->FillSolidRect(0,0,rect.right,30,INFOBK);
	pDC->RoundRect(1,1,rect.right-2,28,1,1);

	pDC->TextOutW(5,5,m_sMsg);

	if (!m_bInitialized)
	{
		m_bInitialized = TRUE;
	}	// 	if (!m_bInitialized)
}


CProjectFormView *CProjectTabView::getProjectFormView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CProjectFormView* pView = DYNAMIC_DOWNCAST(CProjectFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CProjectFormView, pView);
		return pView;
	}
	return NULL;
}

/*TODO: CProjectOwnerFormView *CProjectTabView::getPropOwnerFormView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(1);
	if (m_tabManager)
	{
		CProjectOwnerFormView* pView = DYNAMIC_DOWNCAST(CProjectOwnerFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CProjectOwnerFormView, pView);
		return pView;
	}
	return NULL;
}
*/

CStandsInProjectFormView *CProjectTabView::getProjectStandsFormView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(1);
	if (m_tabManager)
	{
		CStandsInProjectFormView* pView = DYNAMIC_DOWNCAST(CStandsInProjectFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CStandsInProjectFormView, pView);
		return pView;
	}
	return NULL;
}


BOOL CProjectTabView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int tab_id, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();
	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}

	m_tabManager =	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	m_tabManager->SetData(tab_id);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CProjectTabView::setLanguage()
{
/*
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			if (( m_tabManager = m_wndTabControl.getTabPage(0)) )
			{
				m_tabManager->SetCaption((xml.str(IDS_STRING2480)));
			}	// if (m_tabManager)
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))
*/
}