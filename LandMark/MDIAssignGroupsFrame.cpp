// MDIAssignGroupsFrame.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "MDIAssignGroupsFrame.h"
#include "Resource.h"


// CMDIAssignGroupsFrame

IMPLEMENT_DYNCREATE(CMDIAssignGroupsFrame, CChildFrameBase)

CMDIAssignGroupsFrame::CMDIAssignGroupsFrame()
{
	m_bFirstOpen = FALSE;
	m_bEnableTBBTNFilterOff = FALSE;
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bDBConnOk = TRUE;
}

CMDIAssignGroupsFrame::~CMDIAssignGroupsFrame()
{
}


BEGIN_MESSAGE_MAP(CMDIAssignGroupsFrame, CChildFrameBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_DESTROY()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_FILTER_OFF2, OnUpdateTBBTNFilterOff)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CMDIAssignGroupsFrame message handlers

BOOL CMDIAssignGroupsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTPFrameWndBase<CMDIChildWnd>::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CMDIAssignGroupsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTPFrameWndBase<CMDIChildWnd>::OnCreate(lpCreateStruct) == -1)
		return -1;

	if(m_hIcon)
	{
		SetIcon(m_hIcon, TRUE);
		SetIcon(m_hIcon, FALSE);
	}

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	//create toolbar
	m_wndToolBar.CreateToolBar(WS_TABSTOP | WS_VISIBLE | WS_CHILD | CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR3);

	EnableDocking(CBRS_ALIGN_ANY);

	//initialize dialog bar m_wndFieldChooser
	if(!m_wndFieldChooserDlg.Create(this, IDD_FIELD_CHOOSER,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS3))
		return -1;

	//initialize dialog bar m_wndFilterEdit
	if(!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN3))
		return -1;

	//docking for field chooser
	m_wndFieldChooserDlg.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooserDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN)/3));

	//docking for filter edit
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN)/3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;

	return 0;
}

void CMDIAssignGroupsFrame::OnSize(UINT nType, int cx, int cy)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CMDIAssignGroupsFrame::OnDestroy()
{
	//CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	// save window placement
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_TIMBERCRUISEASSIGNGROUPS_WP);
	SavePlacement(this, csBuf);

	m_bFirstOpen = TRUE;

}

void CMDIAssignGroupsFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}


LRESULT CMDIAssignGroupsFrame::OnMessageFromShell(WPARAM wParam, LPARAM lParam)
{
	CDocument* pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while(pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE, wParam, lParam);
		}
	}

	return 0L;
}

void CMDIAssignGroupsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_TIMBERCRUISE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_TIMBERCRUISE;

	CXTPFrameWndBase<CMDIChildWnd>::OnGetMinMaxInfo(lpMMI);
}

void CMDIAssignGroupsFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	::SendMessage(GetMDIFrame()->m_hWndMDIClient, WM_MDISETMENU, 0, 0);

	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE|RDW_FRAME|RDW_NOCHILDREN);
}

void CMDIAssignGroupsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	//load placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_TIMBERCRUISEASSIGNGROUPS_WP);
		LoadPlacement(this, csBuf);
	}
}

void CMDIAssignGroupsFrame::OnSetFocus(CWnd* pOldWnd)
{
	//CXTPFrameWndBase<CMDIChildWnd>::OnSetFocus(pOldWnd);

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	if(m_bDBConnOk == TRUE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);


	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);

}

void CMDIAssignGroupsFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if(m_wndToolBar.GetSafeHwnd() != NULL)
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);
		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}

	CMDIChildWnd::OnPaint();
	// Do not call CXTPFrameWndBase<CMDIChildWnd>::OnPaint() for painting messages
}

void CMDIAssignGroupsFrame::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_sToolTipFilter = xml->str(IDS_STRING121);
			m_sToolTipFilterOff = xml->str(IDS_STRING122);
			m_sToolTipRefresh = xml->str(IDS_STRING124);
			m_sToolTipClasses = xml->str(IDS_STRING180);
			m_sToolTipMulti = xml->str(IDS_STRING225);

			m_wndFieldChooserDlg.SetWindowText(xml->str(IDS_STRING126));
			m_wndFilterEdit.SetWindowText(xml->str(IDS_STRING121));
		}
		delete xml;
	}
}

void CMDIAssignGroupsFrame::setupToolBarIcons()
{
	//set tooltips and icons
	CXTPControl *pCtrl = NULL;
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CString csResFN = getToolBarResFN();

	if(fileExists(csResFN))
	{
		CXTPToolBar *pToolBar = &m_wndToolBar;
		if(pToolBar->IsBuiltIn())
		{
			UINT nBarID = pToolBar->GetBarID();
			pToolBar->LoadToolBar(nBarID, FALSE);
			CXTPControls *p = pToolBar->GetControls();

			if(nBarID == IDR_TOOLBAR3)
			{
				//hResModule = LoadLibraryEx(csResFN, NULL, DONT_RESOLVE_DLL_REFERENCES|LOAD_LIBRARY_AS_DATAFILE);
				//if(hResModule)
				{
					pCtrl = p->GetAt(0);
					pCtrl->SetTooltip(m_sToolTipFilter);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_FILTER);
					//hIcon = LoadIcon(hResModule, RSTR_TB_FILTER);
					if(hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(1);
					pCtrl->SetTooltip(m_sToolTipFilterOff);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_FILTER_OFF);
					//hIcon = LoadIcon(hResModule, RSTR_TB_FILTER_OFF);
					if(hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(2);
					pCtrl->SetTooltip(m_sToolTipRefresh);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_REFRESH);
					//hIcon = LoadIcon(hResModule, RSTR_TB_REFRESH);
					if(hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(3);
					pCtrl->SetTooltip(m_sToolTipClasses);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_LIST);
					//hIcon = LoadIcon(hResModule, RSTR_TB_LIST);
					if(hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(4);
					pCtrl->SetTooltip(m_sToolTipMulti);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_EDIT);
					//hIcon = LoadIcon(hResModule, RSTR_TB_EDIT);
					if(hIcon) pCtrl->SetCustomIcon(hIcon);

					FreeLibrary(hResModule);
				}
			}
		}
	}
}

void CMDIAssignGroupsFrame::OnUpdateTBBTNFilterOff(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

