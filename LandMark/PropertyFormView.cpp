


// PropertyFormView.cpp : implementation file
//

#include "stdafx.h"
#include "MDIPropertyFrame.h"
#include "PropertyFormView.h"
#include "StandsInPropertyFormView.h"

//#include ".\propertyformview.h"

//#include "CMPFormView.h"

#include "PropertyTabView.h"

//#include "RemovePropertiesSelDlg.h"
//#include "ChangeObjIDDlg.h"

// CPropertyFormView

IMPLEMENT_DYNCREATE(CPropertyFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPropertyFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_KEYUP()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BUTTON5_1, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON5_3, OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON5_4, OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5_5, &CPropertyFormView::OnBnClickedButton55)

	ON_COMMAND(ID_TBBTN_OPEN, OnImport)
	ON_COMMAND(ID_TBBTN_CREATE, OnSearchReplace)	// 090820 p�d

	/*TODO: ON_COMMAND(ID_TOOLS_CHANGE_OBJID, OnChangeObjID)*/
	ON_EN_SETFOCUS(IDC_EDIT5_1, &CPropertyFormView::OnEnSetfocusEdit51)
	ON_EN_SETFOCUS(IDC_EDIT5_2, &CPropertyFormView::OnEnSetfocusEdit52)
	ON_EN_SETFOCUS(IDC_EDIT5_3, &CPropertyFormView::OnEnSetfocusEdit53)
	ON_EN_SETFOCUS(IDC_EDIT5_17, &CPropertyFormView::OnEnSetfocusEdit517)
	ON_EN_SETFOCUS(IDC_EDIT5_4, &CPropertyFormView::OnEnSetfocusEdit54)
	ON_EN_SETFOCUS(IDC_EDIT5_5, &CPropertyFormView::OnEnSetfocusEdit55)
	ON_EN_SETFOCUS(IDC_EDIT5_6, &CPropertyFormView::OnEnSetfocusEdit56)
	ON_EN_SETFOCUS(IDC_EDIT5_7, &CPropertyFormView::OnEnSetfocusEdit57)
	ON_EN_SETFOCUS(IDC_EDIT5_8, &CPropertyFormView::OnEnSetfocusEdit58)
	ON_EN_SETFOCUS(IDC_EDIT5_9, &CPropertyFormView::OnEnSetfocusEdit59)
END_MESSAGE_MAP()

CPropertyFormView::CPropertyFormView()
	: CXTResizeFormView(CPropertyFormView::IDD)
{
	m_bInitialized = FALSE;
	m_bIsDataEnabled = FALSE;
	m_pDB = NULL;
	m_bSetFocusOnInitDone = FALSE;
}

CPropertyFormView::~CPropertyFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CPropertyFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_PROP_GROUP, m_wndGroup);
	DDX_Control(pDX, IDC_GROUP5_2, m_wndGroup2);

	DDX_Control(pDX, IDC_LBL5_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL5_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL5_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL5_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5_5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL5_6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL5_7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL5_8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL5_9, m_wndLbl9);
	DDX_Control(pDX, IDC_LBL5_10, m_wndLbl10);
	DDX_Control(pDX, IDC_LBL5_11, m_wndLbl11);

	DDX_Control(pDX, IDC_EDIT5_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT5_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT5_3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT5_4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5_5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT5_6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT5_7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT5_8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT5_17, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT5_9, m_wndEdit10);

	DDX_Control(pDX, IDC_BUTTON5_1, m_wndCountyBtn);
	DDX_Control(pDX, IDC_BUTTON5_3, m_wndMunicipalBtn);
	DDX_Control(pDX, IDC_BUTTON5_4, m_wndParishBtn);
	DDX_Control(pDX, IDC_BUTTON5_5, m_wndRemoveBtn);

	//}}AFX_DATA_MAP

}

BOOL CPropertyFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CPropertyFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndEdit1.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit3.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit4.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit5.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit6.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit7.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit8.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit9.SetDisabledColor(BLACK,COL3DFACE);
		m_wndEdit10.SetDisabledColor(BLACK,COL3DFACE);

		m_wndEdit7.SetAsNumeric();
		m_wndEdit8.SetAsNumeric();

		setLanguage();

		m_sSQLProps.Format(_T("select * from %s"),TBL_PROPERTIES);
		getProperties();
		m_nDBIndex = (int)m_vecPropertyData.size() - 1;
		populateData(m_nDBIndex);

		m_wndCountyBtn.SetBitmap(CSize(18,14),IDB_BITMAP2);
		m_wndCountyBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_wndMunicipalBtn.SetBitmap(CSize(18,14),IDB_BITMAP2);
		m_wndMunicipalBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_wndParishBtn.SetBitmap(CSize(18,14),IDB_BITMAP2);
		m_wndParishBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_bIsDirty = FALSE;
		m_bInitialized = TRUE;
	}	// if (! m_bInitialized )
}

void CPropertyFormView::OnSetFocus(CWnd *pWnd)
{
	setEnableOwnerTab(m_vecPropertyData.size() > 0);

	CXTResizeFormView::OnSetFocus(pWnd);
}

// Handle key strokes and send message to
// HMSShell, �which the sell interptret and sends back
// a message like ID_NEW_ITEM etc; 070103 p�d
void CPropertyFormView::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
//	BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
//	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, nChar,bControlKey);
}

BOOL CPropertyFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CLandMarkDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CPropertyFormView diagnostics

#ifdef _DEBUG
void CPropertyFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CPropertyFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// PRIVATE
void CPropertyFormView::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			m_wndLbl1.SetWindowText((xml.str(IDS_STRING299/*253*/)));
			m_wndLbl2.SetWindowText((xml.str(IDS_STRING300/*254*/)));
			m_wndLbl3.SetWindowText((xml.str(IDS_STRING301/*255*/)));
			m_wndLbl4.SetWindowText((xml.str(IDS_STRING303/*256*/)));
			m_wndLbl5.SetWindowText((xml.str(IDS_STRING304/*257*/)));
			m_wndLbl6.SetWindowText((xml.str(IDS_STRING305/*258*/)));
			m_wndLbl7.SetWindowText((xml.str(IDS_STRING306/*259*/)));
			m_wndLbl8.SetWindowText((xml.str(IDS_STRING307/*260*/)));
			m_wndLbl9.SetWindowText((xml.str(IDS_STRING302/*2560*/)));
			m_wndLbl11.SetWindowText(xml.str(IDS_STRING298/*2701*/));

			m_wndLbl10.SetWindowText(xml.str(IDS_STRING311/*3300*/));
			m_wndRemoveBtn.SetWindowText(xml.str(IDS_STRING312/*3301*/));

			m_sOwnerTabCaption = (xml.str(IDS_STRING313/*2481*/));
			
			m_sCounty = (xml.str(IDS_STRING299/*253*/));
			m_sMunicipal = (xml.str(IDS_STRING300/*254*/));
			m_sParish	= (xml.str(IDS_STRING301/*255*/));
			m_sName = (xml.str(IDS_STRING303/*256*/));

			m_sOKBtn = (xml.str(IDS_STRING250/*240*/));
			m_sCancelBtn = (xml.str(IDS_STRING251/*241*/));

			m_sErrCap = (xml.str(IDS_STRING240/*213*/));
			m_sSaveMsg.Format(_T("%s\n\n%s\n"),
								(xml.str(IDS_STRING314/*2760*/)),
								(xml.str(IDS_STRING242/*237*/)));

			m_sDoneSavingMsg =	(xml.str(IDS_STRING315/*2761*/));

			m_sMsgCap1 = (xml.str(IDS_STRING240/*213*/));
			m_sMsgCap = (xml.str(IDS_STRING316/*261*/));
			m_sDeleteMsg = (xml.str(IDS_STRING317/*262*/));

			m_sFrameCaption = (xml.str(IDS_STRING303/*256*/));

			m_sDataMissinMsg.Format(_T("%s\n\n%s"),
				(xml.str(IDS_STRING318/*2810*/)),
				(xml.str(IDS_STRING319/*2811*/)));

			m_sPropertyActiveMsg.Format(_T("%s\n%s\n\n%s\n%s\n"),
				(xml.str(IDS_STRING320/*3000*/)),
				(xml.str(IDS_STRING321/*3001*/)),
				(xml.str(IDS_STRING322/*3002*/)),
				(xml.str(IDS_STRING323/*3003*/)));
			
			m_sNoPropertiesMsg.Format(_T("%s\n\n%s"),
				xml.str(IDS_STRING324/*3110*/),
				xml.str(IDS_STRING325/*3111*/));

			m_sNoResultInSearch = xml.str(IDS_STRING326/*3221*/);
		}
		xml.clean();
	}
}

void CPropertyFormView::populateData(int idx, BOOL bSetBars)
{
	CString sTmp;

	if (m_vecPropertyData.size() > 0 && 
		  idx >= 0 && 
			idx < (int)m_vecPropertyData.size())
	{
		m_enumAction = UPD_ITEM;

		m_activePropertyData = m_vecPropertyData[idx];

		m_wndEdit1.SetWindowText(m_activePropertyData.getCountyName());
		m_wndEdit1.setIdentifer(m_activePropertyData.getCountyCode());
		m_wndEdit2.SetWindowText(m_activePropertyData.getMunicipalName());
		m_wndEdit2.setIdentifer(m_activePropertyData.getMunicipalCode());
		m_wndEdit3.SetWindowText(m_activePropertyData.getParishName());
		m_wndEdit3.setIdentifer(m_activePropertyData.getParishCode());
		m_wndEdit9.SetWindowText(m_activePropertyData.getPropertyNum());
		m_wndEdit10.SetWindowText(m_activePropertyData.getObjectID());
		m_wndEdit4.SetWindowText(m_activePropertyData.getPropertyName());
		m_wndEdit5.SetWindowText(m_activePropertyData.getBlock());
		m_wndEdit6.SetWindowText(m_activePropertyData.getUnit());
		m_wndEdit7.setFloat(m_activePropertyData.getAreal(),1);
		m_wndEdit8.setFloat(m_activePropertyData.getArealMeasured(),1);

		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
		if (pTabView)
		{
			sTmp.Format(_T("%s; %s %s"),
								m_sFrameCaption,
								m_wndEdit4.getText(),	//property name
								m_wndEdit9.getText()	//property number
								);	
			pTabView->setMsg(sTmp);
			

			CXTPTabManagerItem *pManager = pTabView->getTabControl()->getTabPage(1);
			if (pManager)
			{
				pManager->SetCaption(m_sOwnerTabCaption);
				pManager->SetData(DWORD_PTR(&m_activePropertyData));
				
				// Also update owners for selected property; 070126 p�d
				CPropertyOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CPropertyOwnerFormView, 
					CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle()));
				{
					if (pView1 != NULL)
					{
						// Check if data's been changed on PropertyOwners for property; 070126 p�d
						//pView1->isDataChanged();
						// Display propertyowner(s) for this property; 070126 p�d
						pView1->populateReport();
					}	// if (pView1 != NULL)
				}	// CMDIPropOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CMDIPropOwnerFormView, 
				
				// Also update stands for selected property; 070126 p�d
				CStandsInPropertyFormView* pView2 = DYNAMIC_DOWNCAST(CStandsInPropertyFormView, 
					CWnd::FromHandle(pTabView->getTabControl()->getTabPage(2)->GetHandle()));
				{
					if (pView2 != NULL)
					{
						pView2->populateReport();
					}	// if (pView1 != NULL)
				}	// CMDIPropOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CMDIPropOwnerFormView, 
				
			}	// if (pManager)
		}	// if (pTabView)

		setEnableData(TRUE);
		setEnableOwnerTab(TRUE);

		if(bSetBars)
		{
			if (m_vecPropertyData.size() == 1)
			{
				setNavigationButtons(FALSE,FALSE);
			}
			else if (m_vecPropertyData.size() > 1)
			{
				setNavigationButtons(idx > 0,idx < (int)m_vecPropertyData.size() - 1);
			}
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
		}

	}
	else
	{
		// Not used; 090219 p�d
		//::MessageBox(this->GetSafeHwnd(),(m_sNoPropertiesMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
		setNavigationButtons(FALSE,FALSE);
		setEnableData(FALSE);
		setEnableOwnerTab(FALSE);
		setSearchToolbarBtn(FALSE);
		m_nDBIndex = -1;
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		if (m_enumAction == NEW_ITEM)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}
		else
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}

	}
}

// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
BOOL CPropertyFormView::isDataChanged(void)
{
	if (saveProperty())
	{
		resetIsDirty();
	}
	return FALSE;
}

BOOL CPropertyFormView::getIsDirty(void)
{
	if (m_bIsDirty)
		return m_bIsDirty;

	if (m_wndEdit1.isDirty() ||
			m_wndEdit2.isDirty() ||
			m_wndEdit3.isDirty() ||
			m_wndEdit4.isDirty() ||
			m_wndEdit5.isDirty() ||
			m_wndEdit6.isDirty() ||
			m_wndEdit7.isDirty() ||
			m_wndEdit8.isDirty() ||
			m_wndEdit9.isDirty() ||
			m_wndEdit10.isDirty()) 
	{
		return TRUE;
	}
	return FALSE;
}

void CPropertyFormView::resetIsDirty(void)
{

	m_wndEdit1.resetIsDirty();
	m_wndEdit2.resetIsDirty();
	m_wndEdit3.resetIsDirty();
	m_wndEdit4.resetIsDirty();
	m_wndEdit5.resetIsDirty();
	m_wndEdit6.resetIsDirty();
	m_wndEdit7.resetIsDirty();
	m_wndEdit8.resetIsDirty();
	m_wndEdit9.resetIsDirty();
	m_wndEdit10.resetIsDirty();
}

void CPropertyFormView::clearAll()
{

	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
	m_wndEdit7.SetWindowText(_T(""));
	m_wndEdit8.SetWindowText(_T(""));
	m_wndEdit9.SetWindowText(_T(""));
	m_wndEdit10.SetWindowText(_T(""));

	CPropertyTabView *pView = (CPropertyTabView*)getFormViewByID(IDD_FORMVIEW11);
	if (pView != NULL)
	{
		CString sTmp;
		sTmp.Format(_T("%s:"),m_sFrameCaption);

		pView->setMsg((sTmp));
		CPropertyOwnerFormView *pViewOwner = pView->getPropOwnerFormView();
		if (pViewOwner != NULL)
		{
			pViewOwner->getReportCtrl().ClearReport();
			pViewOwner->getReportCtrl().Populate();
			pViewOwner->getReportCtrl().UpdateWindow();
		}
		
	}

	m_wndEdit4.SetFocus();	//property name

}

void CPropertyFormView::doSetSearchBtn(BOOL enable)
{
	setSearchToolbarBtn(enable);
}

// CPropertyFormView message handlers

BOOL CPropertyFormView::getProperties(void)
{
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getProperties(m_sSQLProps,m_vecPropertyData);			
		}	// if (pDB != NULL)
	}
	return (m_vecPropertyData.size() > 0);
}

BOOL CPropertyFormView::doRePopulateFromSearch(LPCTSTR sql,bool goto_first)
{
	BOOL bFound = FALSE;
	m_sSQLProps = sql;
	getProperties();
	if (m_vecPropertyData.size() > 0)
	{

		// We'll try to find index of active property and set m_nDBIndex to point 
		// to this item; 091008 p�d
		for (UINT i = 0;i < m_vecPropertyData.size();i++)
		{
			if (m_activePropertyData.getID() == m_vecPropertyData[i].getID())
			{
				m_nDBIndex = i;	
				bFound = TRUE;
				break;
			}	// if (m_activePropertyData.getID() == m_vecPropertyData[i].getID())
		}	// for (UINT i = 0;i < m_vecPropertyData.size();i++)

		// If we couldn't find the item, set to first or last, depending on
		// "goto_first"; 091008 p�d
		if (!bFound)
		{
			// Go to last entry; 090820 p�d
			m_nDBIndex = (m_vecPropertyData.size() - 1);
		}

		populateData(m_nDBIndex);
		doSetNavigationBar();
	}
	else
		::MessageBox(this->GetSafeHwnd(),m_sNoResultInSearch,m_sMsgCap1,MB_ICONASTERISK | MB_OK);

	return FALSE;
}

void CPropertyFormView::doSetNavigationBar(void)
{
	// If there's more than one item in list, set Navigationbar; 090217 p�d
	// If only one item, disable Navigationbar; 090217 p�d
	if (m_vecPropertyData.size() > 1)
	{
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < ((int)m_vecPropertyData.size()-1));
	}
	else
	{
		setNavigationButtons(FALSE,FALSE);
	}
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,(m_vecPropertyData.size() >= 1) || (m_enumAction == NEW_ITEM));
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,(m_vecPropertyData.size() >= 1));
}


// PROTECTED METHODS

void CPropertyFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPropertyFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_WPARAM_VALUE_FROM + 0x02 :
		{
			_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
			if (sizeof(*msg) == sizeof(_doc_identifer_msg))
			{
				if (msg->getValue1() == 10)
				{
					// Try to match the Property from GIS to properties in Forrest; 090128 p�d
					if (m_vecPropertyData.size() > 0)
					{
						for (UINT i = 0;i < m_vecPropertyData.size();i++)
						{
							if (m_vecPropertyData[i].getID() == msg->getValue2())
							{
								m_nDBIndex = i;
								populateData(m_nDBIndex);
								break;
							}
						}
					}
				}	// if (msg->getValue1() == 10)
			}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
			break;
		}
		case ID_NEW_ITEM :
		{
			saveProperty();		// Save before adding a new one; 090511 p�d
			// Set as new item; 081219 p�d
			m_activePropertyData = CTransaction_property();
			addProperty();
			setEnableData(TRUE);
			setEnableOwnerTab(FALSE);
			setActiveTab(0);
			break;
		}	// case ID_NEW_ITEM :
		case ID_DELETE_ITEM :
		{
			setActiveTab(0);
			removeProperty();	
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (saveProperty())
			{
				m_nDBIndex = 0;
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (saveProperty())
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			// Just save data; 080609 p�d
			if (saveProperty())
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((int)m_vecPropertyData.size() - 1))
					m_nDBIndex = (int)m_vecPropertyData.size() - 1;
				populateData(m_nDBIndex);
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			// Just save data; 080609 p�d
			if (saveProperty())
			{
				m_nDBIndex = (int)m_vecPropertyData.size()-1;
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_DBNAVIG_END :
	}	// switch (wParam)

	return 0L;
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CPropertyFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{

	m_bNavButtonStartPrev = start_prev;
	m_bNavButtonEndNext = end_next;
/*
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
*/
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

}


void CPropertyFormView::setEnableData(BOOL enable)
{
	m_wndEdit1.EnableWindow(enable);
	m_wndEdit1.SetReadOnly(!enable);
	m_wndEdit2.EnableWindow(enable);
	m_wndEdit2.SetReadOnly(!enable);
	m_wndEdit3.EnableWindow(enable);
	m_wndEdit3.SetReadOnly(!enable);
	m_wndEdit4.EnableWindow(enable);
	m_wndEdit4.SetReadOnly(!enable);
	m_wndEdit5.EnableWindow(enable);
	m_wndEdit5.SetReadOnly(!enable);
	m_wndEdit6.EnableWindow(enable);
	m_wndEdit6.SetReadOnly(!enable);
	m_wndEdit7.EnableWindow(enable);
	m_wndEdit7.SetReadOnly(!enable);
	m_wndEdit8.EnableWindow(enable);
	m_wndEdit8.SetReadOnly(!enable);
	m_wndEdit9.EnableWindow(enable);
	m_wndEdit9.SetReadOnly(!enable);
	m_wndEdit10.EnableWindow(enable);
	m_wndEdit10.SetReadOnly(!enable);

	m_wndCountyBtn.EnableWindow(enable);
	m_wndMunicipalBtn.EnableWindow(enable);
	m_wndParishBtn.EnableWindow(enable);

	m_bIsDataEnabled = enable;


}

void CPropertyFormView::setEnableOwnerTab(BOOL enable)
{
	enableTabPage(1,enable);
	enableTabPage(2,enable);
	
}

void CPropertyFormView::setActiveTab(int tab)
{
	activeTabPage(tab);
	
}

// Handle transaction on database species table; 060317 p�d

BOOL CPropertyFormView::getEnteredData(void)
{
	int nID = -1;

	BOOL bIsOK = (!m_wndEdit4.getText().IsEmpty());

	if (!bIsOK)
		return FALSE;


	return TRUE;

}

BOOL CPropertyFormView::addProperty(void)
{
	clearAll();
	m_bIsDirty = TRUE;
	m_enumAction = NEW_ITEM;
	return TRUE;
}

BOOL CPropertyFormView::saveProperty(void)
{
	/*TODO: check that not all fields empty*/
	if(m_wndEdit4.getText().IsEmpty())
	{
		//AfxMessageBox(_T("Property name missing!"));
		return FALSE;
	}
	

	if (!m_bIsDataEnabled) return FALSE;
	// Datbase info data members
	BOOL bReturn = FALSE;
	int nNumOf;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Check number of entries in the contacts table.
			// If there's no entries, reset the identity field to start
			// from 1; 070102 p�d

			if (m_activePropertyData.getID() == -1)
			{
				nNumOf = m_pDB->getNumOfRecordsInProperty();
				if (nNumOf < 1)
				{
					m_pDB->resetPropertyIdentityField();
					
				}
				
			}	// if (m_enumAction = NEW_ITEM)
/*
			if (nNumOf > 0)
			{
				if (!getEnteredData())
				{
					::MessageBox(this->GetSafeHwnd(),(m_sDataMissinMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);
					return FALSE;
				}
			}	// if (nNumOf > 0)
*/
			m_enteredPropertyData = CTransaction_property(m_activePropertyData.getID(),
																									m_wndEdit1.getIdentifer(),
																									m_wndEdit2.getIdentifer(),
																									m_wndEdit3.getIdentifer(),
																									m_wndEdit1.getText(),
																									m_wndEdit2.getText(),
																									m_wndEdit3.getText(),
																									m_wndEdit9.getText(),
																									m_wndEdit4.getText(),
																									m_wndEdit5.getText(),
																									m_wndEdit6.getText(),
																									m_wndEdit7.getFloat(),
																									m_wndEdit8.getFloat(),
																									getUserName(),
																									_T(""),
																									m_wndEdit10.getText(),
																									_T(""),_T(""));

			if (m_pDB->addProperty(m_enteredPropertyData))
			{
				// Reload Properties; 081219 p�d
				getProperties();
				m_nDBIndex = (m_vecPropertyData.size()-1);
				populateData(m_nDBIndex);
			}
			else if (m_pDB->updProperty(m_enteredPropertyData))
			{
				// Reload Properties; 081219 p�d
				getProperties();
				populateData(m_nDBIndex);
			}
			
			bReturn = TRUE;

		}	// if (m_pDB != NULL)
		m_bIsDirty = FALSE;
		resetIsDirty();
		// Check who has focus, to determin if we should enable tool-butttons; 091008 p�d
		setSearchToolbarBtn(TRUE);
	}

	m_enumAction = UPD_ITEM;

	if(bReturn)
	{
		//send msg to update Property select list view
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
				(LPARAM)&_doc_identifer_msg(MODULE931, MODULE932, _T(""), 1, 0, 0));
	}

	return bReturn;
}

BOOL CPropertyFormView::removeProperty(void)
{
	CXTPReportRow *pRow = NULL;
	CTransaction_property data;
	CString sMsg;
	int nIndex = -1;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_vecPropertyData.size() > 0)
		{
			if (m_pDB != NULL)
			{
				data = m_vecPropertyData[m_nDBIndex];
				if (!doIsPropertyUsed(data))
				{
					//user needs to remove all owners and\or tracts from property first 
					sMsg.Format(_T("%s\n\n%s: %s %s\n%s: %s\n%s: %s\n%s: %s\n\n%s"),
									m_sMsgCap,
									m_sName,
									data.getPropertyName(),
									data.getPropertyNum(),
									//data.getBlock(),
									//data.getUnit(),
									m_sCounty,
									data.getCountyName(),
									m_sMunicipal,
									data.getMunicipalName(),
									m_sParish,
									data.getParishName(),
									m_sDeleteMsg);

					if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					{
						bReturn = m_pDB->removeProperty(data);
						
					}	// if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				}	// if (!doIsPropertyUsed(data))
				else
					::MessageBox(this->GetSafeHwnd(),m_sPropertyActiveMsg,m_sErrCap,MB_ICONINFORMATION | MB_OK);
			}	// if (m_pDB != NULL)
		}	// if (m_vecMachineData.size() > 0)

		if (bReturn)
		{
			// Reload information
			getProperties();

			if (m_vecPropertyData.size() == 0)
			{
				setNavigationButtons(FALSE,FALSE);
				clearAll();
				setEnableData(FALSE);
				setEnableOwnerTab(FALSE);

			}
			else if (m_vecPropertyData.size() == 1)
			{
				setNavigationButtons(FALSE,FALSE);
				setEnableData(TRUE);
				setEnableOwnerTab(TRUE);

			}
			else if (m_vecPropertyData.size() > 1)
			{
				setNavigationButtons(m_nDBIndex > 0,
  													 m_nDBIndex < ((int)m_vecPropertyData.size()-1));
				setEnableData(TRUE);
				setEnableOwnerTab(TRUE);
			}

			// After a delete, set to last item; 060103 p�d
			m_nDBIndex = (int)m_vecPropertyData.size() - 1;
			populateData(m_nDBIndex);
//			m_enumAction = UPD_ITEM;
			m_bIsDirty = FALSE;


			//send msg to update Property select list view
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
				(LPARAM)&_doc_identifer_msg(MODULE931, MODULE932, _T(""), 1, 0, 0));

		}	// if (bReturn)

		return TRUE;
	}
	return FALSE;
}

BOOL CPropertyFormView::doIsPropertyUsed(CTransaction_property &rec)
{
	BOOL bRet1 = FALSE;
	BOOL bRet2 = FALSE;
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW11);
	if (pTabView)
	{
		if (CStandsInPropertyFormView *pView1 = DYNAMIC_DOWNCAST(CStandsInPropertyFormView,CWnd::FromHandle(pTabView->getTabControl()->getTabPage(2)->GetHandle())))
		{
			if (pView1) bRet1 = pView1->isTraktInProperty();
		}	// if (CStandsInPropertyFormView*pView = DYNAMIC_DOWNCAST(CStandsInPropertyFormView,CWnd::FromHandle(pTabView->getTabControl()->getTabPage(2)->GetHandle())))

		if (CPropertyOwnerFormView *pView2 = DYNAMIC_DOWNCAST(CPropertyOwnerFormView,CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle())))
		{
			if (pView2) bRet2 = pView2->isOwnerInProperty();
		}	// if (CPropertyOwnerFormView *pView2 = DYNAMIC_DOWNCAST(CMDIPropOwnerFormView,CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle())))
	
	}	// if (pTabView)

	if (bRet1 || bRet2) return TRUE;
	else return FALSE;
}

void CPropertyFormView::setCountyMunicpalAndParish(CTransaction_county_municipal_parish data)
{
	CString sValue;
	m_recSelectedCMP = data;
	m_wndEdit1.SetWindowText(m_recSelectedCMP.getCountyName());
	sValue.Format(_T("%d"),m_recSelectedCMP.getCountyID());
	m_wndEdit1.setIdentifer(sValue);
	
	m_wndEdit2.SetWindowText(m_recSelectedCMP.getMunicipalName());
	sValue.Format(_T("%d"),m_recSelectedCMP.getMunicipalID());
	m_wndEdit2.setIdentifer(sValue);
	
	m_wndEdit3.SetWindowText(m_recSelectedCMP.getParishName());
	sValue.Format(_T("%d"),m_recSelectedCMP.getParishID());
	m_wndEdit3.setIdentifer(sValue);
	
	m_wndEdit1.setIsDirty();
	m_wndEdit2.setIsDirty();
	m_wndEdit3.setIsDirty();

}

BOOL CPropertyFormView::doPopulateNext()
{
	m_nDBIndex++;
	if (m_nDBIndex > ((int)m_vecPropertyData.size() - 1))
		m_nDBIndex = (int)m_vecPropertyData.size() - 1;
	populateData(m_nDBIndex);

	return (m_nDBIndex < ((int)m_vecPropertyData.size() - 1));
}

BOOL CPropertyFormView::doPopulatePrev()
{
	m_nDBIndex--;
	if (m_nDBIndex < 0)	m_nDBIndex = 0;
	populateData(m_nDBIndex);

	return (m_nDBIndex > 0);
}

void CPropertyFormView::doPopulate(int index,bool set_by_index, BOOL bSetBars)
{
	// Do a check, if user has changed
	// data for active contact and if so
	// ask user to save; 070111 p�d
	if(bSetBars)
	isDataChanged();

	if (set_by_index)
	{
		m_nDBIndex = index;
		populateData(m_nDBIndex, bSetBars);
	}
	else
	{
		// Find index of argument 'index' in m_vecPropertyData; 090810 p�d
		if (m_vecPropertyData.size() > 0)
		{
			// Set default to	laste entry, if we can't match index to ID; 090810 p�d
			m_nDBIndex = (int)m_vecPropertyData.size() - 1;
			for (UINT i = 0;i < m_vecPropertyData.size();i++)
			{
				if (m_vecPropertyData[i].getID() == index)
				{
					m_nDBIndex = i;
					populateData(m_nDBIndex, bSetBars);
					break;
				}	// if (m_vecPropertyData[i].getID() == index)
			}	// for (UINT i = 0;i < m_vecPropertyData.size();i++)
		}	// if (m_vecPropertyData.size() > 0)
	}

}

void CPropertyFormView::doRePopulate(void)
{
	populateData(m_nDBIndex);
}


void CPropertyFormView::OnImport()
{
	AfxMessageBox(_T("TODO: Import data"));
	/*TODO: showFormView(IDD_FORMVIEW7,m_sLangFN,122);
	*/
}

void CPropertyFormView::OnSearchReplace()
{
	SEARCH_REPLACE_INFO info;
	info.m_nID = m_activePropertyData.getID();
	info.m_origin = ORIGIN_PROPERTY;
	// Setup SEARCH_REPLACE_INFO info depending on
	// which edit items selected; 090820 p�d
	info.m_focusedEdit = m_enumFocusedEdit; 
	info.m_nNumber = 0;
	info.m_fNumber = 0.0;
	info.m_bIsNumeric = FALSE;
	// Numeric edit on these: 090820 p�d
	if (m_enumFocusedEdit == ED_PROPNUM)
	{
		info.m_sText = m_wndEdit9.getText();
	}
	else if (m_enumFocusedEdit == ED_PROPNAME)
	{
		info.m_sText = m_wndEdit4.getText();
	}
	else if (m_enumFocusedEdit == ED_BLOCK)
	{
		info.m_sText = m_wndEdit5.getText();
	}
	else if (m_enumFocusedEdit == ED_UNIT)
	{
		info.m_sText = m_wndEdit6.getText();
	}
	else if (m_enumFocusedEdit == ED_AREAL1)
	{
		info.m_bIsNumeric = TRUE;
		info.m_sText = _T("");
		info.m_nNumber = 0;
		info.m_fNumber = m_wndEdit7.getFloat();
	}
	else if (m_enumFocusedEdit == ED_AREAL2)
	{
		info.m_bIsNumeric = TRUE;
		info.m_sText = _T("");
		info.m_nNumber = 0;
		info.m_fNumber = m_wndEdit8.getFloat();
	}
	else if (m_enumFocusedEdit == ED_OBJID)
	{
		info.m_sText = m_wndEdit10.getText();
	}
	/*TODO: showFormView(IDD_FORMVIEW10,m_sLangFN,(LPARAM)&info);
	*/
	AfxMessageBox(_T("TODO: Search and Replace"));
}

void CPropertyFormView::refreshProperties(void)
{
	// Reload information
	getProperties();

	if (m_vecPropertyData.size() == 1)
	{
		setNavigationButtons(FALSE,FALSE);
	}
	else if (m_vecPropertyData.size() > 1)
	{
		setNavigationButtons(m_nDBIndex > 0,
												 m_nDBIndex < ((int)m_vecPropertyData.size()-1));
	}

	// After a delete, set to last item; 060103 p�d
	m_nDBIndex = (int)m_vecPropertyData.size() - 1;

	populateData(m_nDBIndex);

}

void CPropertyFormView::refreshNavButtons(void)
{
	if (m_vecPropertyData.empty())
	{
		setNavigationButtons(FALSE,FALSE);
		clearAll();
	}
	else
	{
		setNavigationButtons(m_nDBIndex > 0,
												 m_nDBIndex < ((int)m_vecPropertyData.size()-1));
	}
}



void CPropertyFormView::OnBnClickedButton1()
{
	/*TODO: showFormView(IDD_REPORTVIEW2,m_sLangFN);
	CCMPFormView *pView = (CCMPFormView *)getFormViewByID(IDD_REPORTVIEW2); 
	if (pView)
	{
		pView->setFilterText(m_wndEdit1.getText(),3);
	}
	*/
}

void CPropertyFormView::OnBnClickedButton3()
{
	/*TODO: showFormView(IDD_REPORTVIEW2,m_sLangFN);
	CCMPFormView *pView = (CCMPFormView *)getFormViewByID(IDD_REPORTVIEW2); 
	if (pView)
	{
		pView->setFilterText(m_wndEdit2.getText(),4);
	}
	*/
}

void CPropertyFormView::OnBnClickedButton4()
{
	/*TODO: showFormView(IDD_REPORTVIEW2,m_sLangFN);
	CCMPFormView *pView = (CCMPFormView *)getFormViewByID(IDD_REPORTVIEW2); 
	if (pView)
	{
		pView->setFilterText(m_wndEdit3.getText(),5);
	}
	*/
}

// Added 2009-05-12 P�D
// Delete properties (munltierase): 090512 p�d
void CPropertyFormView::OnBnClickedButton55()
{
	/*TODO: CRemovePropertiesSelDlg *pDlg = new CRemovePropertiesSelDlg();
	if (pDlg != NULL)
	{
		pDlg->setDBConnection(m_pDB);
		if (pDlg->DoModal() == IDOK)
		{
		}	// if (pDlg->DoModal() == IDOK)

		delete pDlg;
	}	// if (pDlg != NULL)
	*/
}

// Added 2009-08-12 P�D
/*TODO: 
void CPropertyFormView::OnChangeObjID()
{
	CChangeObjIDDlg *pDlg = new CChangeObjIDDlg();
	if (pDlg != NULL)
	{
		pDlg->setDBConnection(m_pDB);
		if (pDlg->DoModal() == IDOK)
		{
			getProperties();
			populateData(m_nDBIndex);
		}
		delete pDlg;
	}
}
*/

BOOL CPropertyFormView::checkFocus(CWnd *pWnd)
{
	CWnd *pWndFocus = GetFocus();

	return (pWnd->GetSafeHwnd() == pWndFocus->GetSafeHwnd());
}


void CPropertyFormView::setSearchToolbarBtn(BOOL enable)
{
	CPropertyTabView *pView = (CPropertyTabView*)getFormViewByID(IDD_FORMVIEW11);
	if (pView != NULL)
	{
		CMDIPropertyFrame *pPropFrame = (CMDIPropertyFrame *)pView->GetParent();
		if (pPropFrame)
			pPropFrame->setEnableToolbar(TRUE,
			enable && m_enumFocusedEdit != ED_NONE  && m_enumFocusedEdit != ED_AREAL1 && m_enumFocusedEdit != ED_AREAL2,
			m_vecPropertyData.size() > 0);

	}
}

void CPropertyFormView::OnEnSetfocusEdit51()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit52()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit53()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit517()
{
	m_enumFocusedEdit = ED_PROPNUM;
	if (m_vecPropertyData.size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit54()
{
	m_enumFocusedEdit = ED_PROPNAME;
	if (m_vecPropertyData.size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit55()
{
	m_enumFocusedEdit = ED_BLOCK;
	if (m_vecPropertyData.size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit56()
{
	m_enumFocusedEdit = ED_UNIT;
	if (m_vecPropertyData.size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit57()
{
	m_enumFocusedEdit = ED_AREAL1;
	setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit58()
{
	m_enumFocusedEdit = ED_AREAL2;
	setSearchToolbarBtn(FALSE);
}

void CPropertyFormView::OnEnSetfocusEdit59()
{
	m_enumFocusedEdit = ED_OBJID;
	if (m_vecPropertyData.size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}
