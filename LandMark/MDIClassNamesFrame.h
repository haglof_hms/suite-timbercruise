#pragma once


// CMDIClassNamesFrame frame
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CMDIClassNamesFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIClassNamesFrame)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CXTPDockingPaneManager m_paneManager;

	protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	HICON m_hIcon;
public:
	BOOL m_bDBConnOk;

protected:
	CMDIClassNamesFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIClassNamesFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;


protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnClose();
};


