#if !defined(__PROJECTTABVIEW_H__)
#define __PROJECTTABVIEW_H__

#include "ProjectFormView.h"
//#include "MDIPropOwnerFormView.h"
//#include "ProjectOwnerFormView.h"
#include "StandsInProjectFormView.h"

#include "MDIBaseFrame.h"
#include "MDIProjectFrame.h"

///////////////////////////////////////////////////////////////
// CProjectTabView 
class CProjectTabView : public CView
{
	DECLARE_DYNCREATE(CProjectTabView)

//private:
	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sMsg;

	CFont m_font;
public:
	CProjectTabView();           // protected constructor used by dynamic creation
	virtual ~CProjectTabView();

	CProjectFormView *getProjectFormView(void);
	//CProjectOwnerFormView *getPropOwnerFormView(void);
	CStandsInProjectFormView *getProjectStandsFormView(void);
	

	CMyTabControl *getTabControl(void)
	{
		return &m_wndTabControl;
	}

	void setMsg(LPCTSTR msg)
	{
		m_sMsg = msg;
		Invalidate();
	}

	void enablePage(int idx,BOOL enable)
	{
		if (idx < m_wndTabControl.getNumOfTabPages())
		{
			m_tabManager = m_wndTabControl.getTabPage(idx);
			if (m_tabManager)
			{
				m_tabManager->SetEnabled(enable);
			}	// if (m_tabManager)
		}	// if (idx < m_wndTabControl.getNumOfTabPages())
	}

	void activePage(int idx)
	{
		if (idx < m_wndTabControl.getNumOfTabPages())
		{
			m_tabManager = m_wndTabControl.getTabPage(idx);
			if (m_tabManager)
			{
				m_tabManager->Select();
			}	// if (m_tabManager)
		}	// if (idx < m_wndTabControl.getNumOfTabPages())
	}

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabbedViewView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate( );
	protected:
	//}}AFX_VIRTUAL
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int tab_id, int nIcon);

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	CMDIFrameDoc* GetDocument();

	void setLanguage(void);
protected:

	//{{AFX_MSG(CProjectTabView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

#ifndef _DEBUG  // debug version in TabbedViewView.cpp
inline CMDIFrameDoc* CProjectTabView::GetDocument()
	{ return (CMDIFrameDoc*)m_pDocument; }
#endif


#endif
