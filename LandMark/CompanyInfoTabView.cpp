// CompanyInfoTabView.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "CompanyInfoTabView.h"
#include "CompanyInfoFormView.h"


// CCompanyInfoTabView

IMPLEMENT_DYNCREATE(CCompanyInfoTabView, CView)

CCompanyInfoTabView::CCompanyInfoTabView()
{
	m_bInitialized = FALSE;
}

CCompanyInfoTabView::~CCompanyInfoTabView()
{
}

BEGIN_MESSAGE_MAP(CCompanyInfoTabView, CView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL2, OnSelectedChanged)
END_MESSAGE_MAP()


// CCompanyInfoTabView drawing

void CCompanyInfoTabView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}


// CCompanyInfoTabView diagnostics

#ifdef _DEBUG
void CCompanyInfoTabView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CCompanyInfoTabView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CCompanyInfoTabView message handlers

BOOL CCompanyInfoTabView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CCompanyInfoTabView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	if(!m_bInitialized)
	{
		m_sAbrevLangSet = getLangSet();
		m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);
		setLanguage();
		m_bInitialized = TRUE;
	}
}

void CCompanyInfoTabView::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			if(m_tabManager)
			{
				m_tabManager->SetCaption(xml->str(IDS_STRING184));
			}
		}
		delete xml;
	}

}

LRESULT CCompanyInfoTabView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	return 0L;
}

int CCompanyInfoTabView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Tab control
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP, CRect(0,0,0,0), this, ID_TABCONTROL2);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = FALSE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = FALSE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	AddView(RUNTIME_CLASS(CCompanyInfoFormView),_T(""), 1, -1);

	//set active view, need this if to use GetActiveView in MDICompanyInfoFrame::OnClose
	if(m_wndTabControl.getNumOfTabPages() > 0)
	{
		CFrameWnd* pFrame = GetParentFrame();
		CCompanyInfoFormView* pView = DYNAMIC_DOWNCAST(CCompanyInfoFormView, CWnd::FromHandle(m_wndTabControl.getTabPage(0)->GetHandle()));
		ASSERT_KINDOF(CCompanyInfoFormView, pView);
		pFrame->SetActiveView(pView);
	}

	return 0;
}

void CCompanyInfoTabView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);

	if (m_wndTabControl.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndTabControl,1,1,rect.right-1,rect.bottom-1);
	}
}

CMDIFrameDoc* CCompanyInfoTabView::GetDocument()
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMDIFrameDoc)));
	return (CMDIFrameDoc*)m_pDocument;
}

BOOL CCompanyInfoTabView::AddView(CRuntimeClass *pViewClass, LPCTSTR lpszTitle, int tab_id, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc = GetDocument();
	contextT.m_pNewViewClass = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();
	CWnd* pWnd;

	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if(pWnd == NULL)
			AfxThrowMemoryException();
	}
	CATCH_ALL(e)
	{
		TRACE0("Out of memory creating a view.\n");
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();
	CRect rect(0,0,0,0);

	if(!pWnd->Create(NULL, NULL, dwStyle, rect, &m_wndTabControl, AFX_IDW_PANE_FIRST + nTab, &contextT))
	{
		TRACE0("Couldn't create client tab for view.\n");
		// pWnd will be cleaned up by PostNcDestroy
		return FALSE;
	}

	m_tabManager = m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	m_tabManager->SetData(tab_id);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);
	
	return TRUE;
}

CCompanyInfoFormView* CCompanyInfoTabView::getCompanyInfoFormView()
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if(m_tabManager)
	{
		CCompanyInfoFormView* pView = DYNAMIC_DOWNCAST(CCompanyInfoFormView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CCompanyinfoFormView, pView);
		return pView;
	}

	return NULL;
}

void CCompanyInfoTabView::OnSelectedChanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	pResult = 0;

	CFrameWnd* pFrame = GetParentFrame();
	if(pFrame && m_wndTabControl.getNumOfTabPages() > 0)
	{
		CView *pView = DYNAMIC_DOWNCAST(CView, CWnd::FromHandle(m_wndTabControl.GetSelectedItem()->GetHandle()));
		ASSERT_KINDOF(CView, pView);

		pFrame->SetActiveView(pView);
	}
}
