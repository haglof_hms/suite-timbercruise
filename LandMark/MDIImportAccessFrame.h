#pragma once


// CMDIImportAccessFrame frame
#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>


class CMDIImportAccessFrame : public CChildFrameBase	//CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDIImportAccessFrame)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	HICON m_hIcon;
protected:
	CMDIImportAccessFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIImportAccessFrame();

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnClose();
	
};


