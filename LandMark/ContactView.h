#pragma once


// CContactView view

class CContactView : public CView
{
	DECLARE_DYNCREATE(CContactView)

protected:
	CContactView();           // protected constructor used by dynamic creation
	virtual ~CContactView();

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
};


