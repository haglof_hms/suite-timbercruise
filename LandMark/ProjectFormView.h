#pragma once

#include "Resource.h"

#include "LandMarkDB.h"

// CProjectFormView form view

class CProjectFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CProjectFormView)

private:
	BOOL m_bInitialized;
	BOOL m_bSetFocusOnInitDone;
	BOOL m_bIsDataEnabled;
	CString	m_sLangAbrev;
	CString m_sLangFN;
	CString m_sErrCap;
	CString m_sSaveMsg;
	CString m_sDoneSavingMsg;
	CString m_sDeleteMsg;
	CString m_sMsgCap;
	CString m_sMsgCap1;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sName;
	CString m_sFrameCaption;
	CString m_sDataMissinMsg;
	CString m_sProjectActiveMsg;
	CString m_sNoProjectsMsg;
	CString m_sNoResultInSearch;
	CString m_sStandTabCaption;

	CString m_sSQLProjects;

	BOOL m_bIsDirty;

	// Status for navigation buttons; 070125 p�d
	BOOL m_bNavButtonStartPrev;
	BOOL m_bNavButtonEndNext;

	enumACTION m_enumAction;

	void setLanguage(void);

	FOCUSED_EDIT m_enumFocusedEdit;

	BOOL getProjects(void);
	vecTransactionProperty m_vecProjectData;	/*TODO: change to project*/
	CTransaction_property m_enteredProjectData;	/*TODO: change for project*/
	CTransaction_property m_activeProjectData;	/*TODO: change for project*/
	BOOL doIsProjectUsed(CTransaction_property &);	/*TODO: change for project*/

	CTransaction_county_municipal_parish m_recSelectedCMP;
	
	int m_nDBIndex;
	void populateData(int idx, BOOL bSetBars = TRUE);
	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void setEnableData(BOOL enable);
	void setEnableOwnerTab(BOOL enable);
	void setActiveTab(int tab);


	CLandMarkDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL checkFocus(CWnd *pWnd);
protected:
	CProjectFormView();           // protected constructor used by dynamic creation
	virtual ~CProjectFormView();

	CXTResizeGroupBox m_wndGroup;
	
	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl9;
	
	CMyExtEdit m_wndEdit1;	//Project Information
	CMyExtEdit m_wndEdit2;	//Project Type
	CMyExtEdit m_wndEdit4;	//Project Name
	CMyExtEdit m_wndEdit9;	//Project Date
	
	// My methods
	BOOL getEnteredData(void);

	BOOL addProject(void);

	void resetIsDirty(void);
	BOOL getIsDirty(void);

	void clearAll();

	void setSearchToolbarBtn(BOOL enable);
public:
	// Need to be PUBLIC
	BOOL isDataChanged(void);
	BOOL saveProject(void);
	//void setCountyMunicpalAndParish(CTransaction_county_municipal_parish);
	BOOL doPopulateNext(void);
	BOOL doPopulatePrev(void);
	void doPopulate(int,bool set_by_index = true, BOOL bSetBars = TRUE);
	void doRePopulate(void);
	void refreshNavButtons(void);
	void refreshProjects(void);
	
	void doSetSearchBtn(BOOL enable);
	// Set public, removeProject is called from
	// CProjectTabView. This is also the case
	// with removePropOwner in CMDIPropOwnerFormView; 070126 p�d
	BOOL removeProject(void);	

	BOOL getStartPrevStatus(void)	{	return m_bNavButtonStartPrev;	}

	BOOL getEndNextStatus(void)		{	return m_bNavButtonEndNext;	}
	
	int getDBIndex(void)					{	return m_nDBIndex;	}

	void doSetNavigationBar(void);

	BOOL doRePopulateFromSearch(LPCTSTR sql,bool goto_first);

	CTransaction_property &getActiveProject(void)	{ return m_activeProjectData; }	/*TODO: change for project*/

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW13 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnImport();
	afx_msg void OnSearchReplace();

	/*TODO: afx_msg void OnChangeObjID();
	*/
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnSetfocusEdit61();
	afx_msg void OnEnSetfocusEdit62();
	afx_msg void OnEnSetfocusEdit617();
	afx_msg void OnEnSetfocusEdit64();
	};


