#include "StdAfx.h"
#include "MyDBBaseClass_SQLApi.h"

/////////////////////////////////////////////////////////////////////////////////////////
// CMyDBBaseClass_SQLApi

// PUBLIC CONSTRUCTORS
CMyDBBaseClass_SQLApi::CMyDBBaseClass_SQLApi(void)
{
	m_saClient	= SA_Client_NotSpecified;
	m_sDataBase	= _T("");
	m_sUserName	= _T("");
	m_sPassword	= _T("");
	m_enumConnType = CONNECTED_TYPE_NOT_CONNECTED;
	m_nConnTypeNumber = -1;
}

CMyDBBaseClass_SQLApi::CMyDBBaseClass_SQLApi(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
{
	m_saClient	= client;
	m_sDataBase	= db_name;
	m_sUserName	= user_name;
	m_sPassword	= psw;
	m_enumConnType = CONNECTED_TYPE_BY_LOGIN;
	m_nConnTypeNumber = -1;
}

CMyDBBaseClass_SQLApi::CMyDBBaseClass_SQLApi(DB_CONNECTION_DATA &db_connection,int type_of_conn)
{
	m_saClient	= db_connection.conn->Client();
	m_nConnTypeNumber = type_of_conn;
	try
	{
		if (type_of_conn == 1)
		{
			m_dbConnectionData = db_connection;
			if (m_dbConnectionData.conn->isConnected())
			{
				m_saCommand.setConnection(m_dbConnectionData.conn);
				m_enumConnType = CONNECTED_TYPE_FROM_MAIN_PROGRAM;
			}
		}
		else if (type_of_conn == 2)
		{
			m_dbConnectionData = db_connection;
			if (m_dbConnectionData.admin_conn->isConnected())
			{
				m_saCommand.setConnection(m_dbConnectionData.admin_conn);
				m_enumConnType = CONNECTED_TYPE_FROM_MAIN_PROGRAM;
			}
		}
	}
	catch(SAException &e)
	{
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
	}
}

// PUBLIC DESTRUCTOR
CMyDBBaseClass_SQLApi::~CMyDBBaseClass_SQLApi(void)
{

	// Disconnect from database; 060105 p�d
	if (m_saConnection.isConnected())
	{
		m_saConnection.Disconnect();
	}

}

// Public methods
DB_CONNECTION_DATA &CMyDBBaseClass_SQLApi::getDBConnection(void)
{
	return m_dbConnectionData;
}

void CMyDBBaseClass_SQLApi::doCommit(void)
{
	if (m_enumConnType == CONNECTED_TYPE_FROM_MAIN_PROGRAM)
	{
		if (m_nConnTypeNumber == 1)
		{
			m_dbConnectionData.conn->Commit();
		}
		else if (m_nConnTypeNumber == 2)
		{
			m_dbConnectionData.admin_conn->Commit();
		}
	}
	else if (m_enumConnType == CONNECTED_TYPE_BY_LOGIN)
		m_saConnection.Commit();
}

void CMyDBBaseClass_SQLApi::doRollback(void)
{
	if (m_enumConnType == CONNECTED_TYPE_FROM_MAIN_PROGRAM)
	{
		if (m_nConnTypeNumber == 1)
		{
			m_dbConnectionData.conn->Rollback();
		}
		else if (m_nConnTypeNumber == 2)
		{
			m_dbConnectionData.admin_conn->Rollback();
		}
	}
	else if (m_enumConnType == CONNECTED_TYPE_BY_LOGIN)
		m_saConnection.Rollback();
}

void CMyDBBaseClass_SQLApi::doSetAutoCommitOff()
{
	m_saConnection.setAutoCommit(SA_AutoCommitOff);
}


BOOL CMyDBBaseClass_SQLApi::connectToDataBase(void)
{
	try
	{
		m_saConnection.Connect(getDataBase(),getUserName(),getPassword(),getSAClient());
		
		if (m_saConnection.isConnected())
		{
			m_saCommand.setConnection(&m_saConnection);
		}
	}
	catch(SAException &)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CMyDBBaseClass_SQLApi::connectToDataBase(SAConnection *con)
{
	try
	{
		if (m_saConnection.isConnected())
		{
			m_saCommand.setConnection(&m_saConnection);
		}
	}
	catch(SAException &)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CMyDBBaseClass_SQLApi::exists(LPCTSTR sql)
{
	BOOL bExists = FALSE;
	int nCount = 0;
	try
	{

		if (m_nConnTypeNumber == 1)
			m_saCommand.setConnection(getDBConnection().conn);
		else if (m_nConnTypeNumber == 2)
			m_saCommand.setConnection(getDBConnection().admin_conn);

		m_saCommand.setCommandText((SAString)sql);
		m_saCommand.Execute();
		
		while (m_saCommand.FetchNext())	nCount++;
		bExists = (nCount > 0);
		if (m_nConnTypeNumber == 1)
			getDBConnection().conn->Commit();
		else if (m_nConnTypeNumber == 2)
			getDBConnection().admin_conn->Commit();
	}
	catch(SAException &)
	{
/*
		CString S;
		if (e.ErrClass() == SA_DBMS_API_Error)
		{
			S.Format(_T("%s %d"),e.ErrText().GetWideChars(),e.ErrNativeCode());
			// print error message
			AfxMessageBox(S);
		}
*/
		if (m_nConnTypeNumber == 1)
			getDBConnection().conn->Rollback();
		else if (m_nConnTypeNumber == 2)
			getDBConnection().admin_conn->Rollback();
		return TRUE;	// Return TRUE on error = Empty; 060111 p�d
	}

	return bExists;
}

BOOL CMyDBBaseClass_SQLApi::db_exists(LPCTSTR db_name)
{
	BOOL bFound		= FALSE;
	try
	{
		// Check which database server's used; 060313 p�d
		if (m_saClient == SA_MySQL_Client)
		{
			m_saCommand.setCommandText((SAString)_T("SHOW DATABASES;"));
		}
		else if (m_saClient == SA_SQLServer_Client)
		{
			m_saCommand.setCommandText((SAString)_T("SELECT * FROM MASTER.DBO.SYSDATABASES;"));
		}
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			if (_tcscmp(db_name,m_saCommand.Field(1).asString().GetWideChars()) == 0)
			{
				bFound = TRUE;
				break;
			}	// if (_tcscmp(db_name,m_saCommand.Field(1).asString().GetWideChars()) == 0)
		}	// while(m_saCommand.FetchNext())
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;	// Return TRUE on error = Empty; 060111 p�d
	}

	return bFound;
}

// Check if a tables exists in a db; 060313 p�d
BOOL CMyDBBaseClass_SQLApi::table_exists(LPCTSTR db_name,LPCTSTR table_name)
{
	BOOL bIsOK = FALSE;
	CString sSQL;
	try
	{
		// Check which database server's used; 060313 p�d
		if (m_saClient == SA_MySQL_Client)
		{
			sSQL.Format(_T("select * from `%s`.`%s`;"),db_name,table_name);
		}
		else if (m_saClient == SA_SQLServer_Client)
		{
			sSQL.Format(_T("use %s select name from sysobjects where name='%s';"),db_name,table_name);
		}
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		m_saConnection.Commit();
		bIsOK = TRUE;
	}
	catch(SAException &)
	{
		m_saConnection.Rollback();
		bIsOK = FALSE;
	}

	return bIsOK;
}

long CMyDBBaseClass_SQLApi::num_of_records(LPCTSTR  table_name)
{
	CString sSQL;
	long nNumOf = -1;
	try
	{
		sSQL.Format(_T("select count(*) from %s"),table_name);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			nNumOf =	m_saCommand.Field(1).asLong();
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		m_saConnection.Rollback();
		return nNumOf;
	}
	return nNumOf;
}

int CMyDBBaseClass_SQLApi::last_identity(LPCTSTR table_name,LPCTSTR column_name)
{
	CString sSQL,S;
	CString sIdentity;
	int nIdentity = 0;
	try
	{
		if (getConnTypeNumber() == 1)	// SQL Server
		{
			sSQL.Format(_T("select ident_current(\'%s\') as Ident"),table_name);
		}
		else if (getConnTypeNumber() == 2)	// MySQL
		{
			sSQL.Format(_T("select max(%s) as Ident from %s"),column_name,table_name);
		}
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			if (getConnTypeNumber() == 1)	// SQL Server
			{
				nIdentity =	m_saCommand.Field(_T("Ident")).asShort();
			}
			else if (getConnTypeNumber() == 2)	// MySQL
			{
				nIdentity =	m_saCommand.Field(_T("Ident")).asShort();
			}
		}
		m_saConnection.Commit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		m_saConnection.Rollback();
		return nIdentity;
	}
	return nIdentity;
}


SAClient_t CMyDBBaseClass_SQLApi::getSAClient(void)
{
	return m_saClient;
}

SAString CMyDBBaseClass_SQLApi::getDataBase(void)
{
	return m_sDataBase;
}

SAString CMyDBBaseClass_SQLApi::getUserName(void)
{
	return m_sUserName;
}

SAString CMyDBBaseClass_SQLApi::getPassword(void)
{
	return m_sPassword;
}

SAString CMyDBBaseClass_SQLApi::convertSADateTime(SADateTime &dt)
{
	TCHAR szTmp[32];
	_stprintf_s(szTmp,_T("%d-%02d-%02d %02d:%02d"),
		dt.GetYear(),
		dt.GetMonth(),
		dt.GetDay(),
		dt.GetHour(),
		dt.GetMinute());
	return szTmp;
}


int CMyDBBaseClass_SQLApi::getConnTypeNumber(void)
{
	if (m_saClient == SA_SQLServer_Client) return 1;
	else if (m_saClient == SA_MySQL_Client) return 2;
	else if (m_saClient == SA_Oracle_Client) return 3;
	else return 0;
}

