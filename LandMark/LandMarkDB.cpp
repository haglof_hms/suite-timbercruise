#include "stdafx.h"
#include "LandMarkDB.h"
#include "LandMarkView.h"
#include "AssignGroupsView.h"
#include "ClassNamesView.h"
#include "ExternalDocsView.h"

CLandMarkDB::CLandMarkDB(void)
: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
	
}

CLandMarkDB::CLandMarkDB(SAClient_t client, LPCTSTR db_name, LPCTSTR user_name, LPCTSTR psw)
: CDBBaseClass_SQLApi(client, db_name, user_name, psw)
{
	
}

CLandMarkDB::CLandMarkDB(DB_CONNECTION_DATA &db_connection)
: CDBBaseClass_SQLApi(db_connection, 1)
{
	
}

void CLandMarkDB::setAutoCommitStatus(BOOL commit /*=TRUE*/)
{
	try
	{
		if(commit == TRUE)
			m_saCommand.Connection()->setAutoCommit(SA_AutoCommitOn);
		else
			m_saCommand.Connection()->setAutoCommit(SA_AutoCommitOff);
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
	}
}

BOOL CLandMarkDB::getStands(vecTransactionTCStand &vec)
{
	CString csSQL;

	try
	{
		vec.clear();

		csSQL.Format(_T("SELECT * FROM %s ORDER BY FileIndex"), TBL_STAND);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_TCStand(m_saCommand.Field(1).asShort(),
				m_saCommand.Field(2).asString().GetWideChars(),
				m_saCommand.Field(3).asDouble(),
				getCruiseMethod(m_saCommand.Field(5).asShort()),
				m_saCommand.Field(8).asString().GetWideChars(),
				m_saCommand.Field(9).asString().GetWideChars(),
				m_saCommand.Field(10).asString().GetWideChars(),
				m_saCommand.Field(11).asString().GetWideChars()
#ifdef USE_TC_CONTACT
				,m_saCommand.Field(SAString(_T("PropID"))).asShort()
#else
				,-1
#endif
				));
		}
		doCommit();
	}
	
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(e.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}
#endif
		

	return TRUE;
}

BOOL CLandMarkDB::getClassNames(vecTransactionTCClass &vec)
{
	CString csSQL;

	try
	{
		vec.clear();
	
		csSQL.Format(_T("SELECT * FROM %s ORDER BY ClassIndex"),TBL_GROUP_ORDER_HDR);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_TCClass(m_saCommand.Field(1).asShort(),
				m_saCommand.Field(3).asShort(),
				m_saCommand.Field(2).asString().GetWideChars()));
		}
		doCommit();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(x.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}
#endif

	return TRUE;
}

BOOL CLandMarkDB::getCompanyInfo(CTransaction_TCCompanyInfo &info)
{
	CString csSQL;

	try
	{
		info.clear();

		csSQL.Format(_T("SELECT * FROM %s"), TBL_COMPANY_INFO);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			info.setId(m_saCommand.Field(1).asShort());
			info.setCompany(m_saCommand.Field(2).asString().GetWideChars());
			info.setAddress(m_saCommand.Field(3).asString().GetWideChars());
			info.setAddress2(m_saCommand.Field(4).asString().GetWideChars());
			info.setCity(m_saCommand.Field(5).asString().GetWideChars());
			info.setZipCode(m_saCommand.Field(6).asString().GetWideChars());
			info.setState(m_saCommand.Field(7).asString().GetWideChars());
			info.setCountry(m_saCommand.Field(8).asString().GetWideChars());
			info.setPhone(m_saCommand.Field(9).asString().GetWideChars());
			info.setFax(m_saCommand.Field(10).asString().GetWideChars());
			info.setEmail(m_saCommand.Field(11).asString().GetWideChars());
			info.setWebsite(m_saCommand.Field(12).asString().GetWideChars());
		}
		doCommit();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(x.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}
#endif

	return TRUE;
}

BOOL CLandMarkDB::saveCompanyInfo(CTransaction_TCCompanyInfo info)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET Company = '%s', Address = '%s', Address2 = '%s', City = '%s', ZipCode = '%s', State = '%s', Country = '%s', PhoneNumber = '%s', FaxNumber = '%s', Email = '%s', Website = '%s' WHERE Id = %d"),
			TBL_COMPANY_INFO, info.getCompany(), info.getAddress(), info.getAddress2(), info.getCity(), info.getZipCode(), info.getState(), info.getCountry(), info.getPhone(), info.getFax(), info.getEmail(), info.getWebsite(), info.getId());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

unsigned int FromFileWriter(SAPieceType_t &ePieceType, void *pBuf, unsigned int nLen, void *pAddlData);
SAString ReadWholeFile(/*const char **/ LPCTSTR sFilename);
void IntoFileReader(SAPieceType_t ePieceType, void *pBuf, unsigned int nLen, unsigned int nBlobSize, void *pAddlData);


SAString ReadWholeFile(/*const char **/ LPCTSTR sFilename)
{
	SAString s;
	//char sBuf[32*1024];
	TCHAR sBuf[32*1024];
	FILE *pFile = _tfopen(sFilename, _T("rb"));

	if(!pFile)
		SAException::throwUserException(-1, 
		(SAString)_T("Error opening file '%s'\n"), sFilename);
	do
	{
		unsigned int nRead = (unsigned int)fread(sBuf, 1*sizeof(TCHAR), sizeof(sBuf)/sizeof(TCHAR), pFile);
		s += SAString(sBuf, nRead);
	}
	while(!feof(pFile));
	
	fclose(pFile);
	
	return s;
}

static FILE *pFile = NULL;
static int nTotalBound;
static int nTotalRead;

unsigned int FromFileWriter(SAPieceType_t &ePieceType,	void *pBuf, unsigned int nLen, void *pAddlData)
{
	if(ePieceType == SA_FirstPiece)
	{
		//const char *sFilename = (const char *)pAddlData;
		LPCTSTR sFilename = (LPCTSTR)pAddlData;
		pFile = _tfopen(sFilename, _T("rb"));
		if(!pFile)
			SAException::throwUserException(-1, (SAString)_T("Can not open file '%s'"), sFilename);
		
		nTotalBound = 0;
	}
	
	unsigned int nRead = (unsigned int)fread(pBuf, 1, nLen, pFile);		//TODO: might need to change this if using Unicode and TCHAR
	nTotalBound += nRead;
	
	if(feof(pFile))
	{
		ePieceType = SA_LastPiece;
		fclose(pFile);
		pFile = NULL;
	}
	
	return nRead;
}

void IntoFileReader(SAPieceType_t ePieceType, void *pBuf, unsigned int nLen, unsigned int nBlobSize, void *pAddlData)
{
	if(ePieceType == SA_FirstPiece || ePieceType == SA_OnePiece)
	{
		nTotalRead = 0;
		//const char *sFilename = (const char *)pAddlData;
		LPCTSTR sFilename = (LPCTSTR)pAddlData;
		pFile = _tfopen(sFilename, _T("wb"));
		if(!pFile)
			SAException::throwUserException(-1, (SAString)_T("Can not open file '%s' for writing"), sFilename);
	}

	CString csMsg;

	fwrite(pBuf, 1, nLen, pFile);
	nTotalRead += nLen;

    if(ePieceType == SA_LastPiece || ePieceType == SA_OnePiece)
	{
		fclose(pFile);
		pFile = NULL;
	}
}



BOOL CLandMarkDB::saveImage(CString path, CTransaction_TCCompanyInfo info)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET PictureType = :1, Picture = :2 WHERE Id = %d"), TBL_COMPANY_INFO, info.getId());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Param(1).setAsShort() = 0;
		//m_saCommand.Param(2).setAsBLob() = ReadWholeFile(path);
		m_saCommand.Param(2).setAsBLob(FromFileWriter, 10*1024, (void*)(LPCTSTR)path);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::removeImage(CTransaction_TCCompanyInfo info)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET Picture = NULL WHERE Id = %d"), TBL_COMPANY_INFO, info.getId());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::loadImage(CString path, CTransaction_TCCompanyInfo info)
{
	CString csSQL;
	BOOL bRet = TRUE;

	try
	{
		csSQL.Format(_T("SELECT Picture FROM %s WHERE Id = %d"), TBL_COMPANY_INFO, info.getId());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		m_saCommand.Field(1).setLongOrLobReaderMode(SA_LongOrLobReaderManual);
		while(m_saCommand.FetchNext())
		{
			m_saCommand.Field(1).ReadLongOrLob(
				IntoFileReader,	// our callback to read BLob content into file
				/*10*/32*1024,		// our desired piece size
				(void*)/*(const char*)*/(LPCTSTR)path	// additional data, filename in our example
				);
		}
		doCommit();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}
#endif

	return bRet;
}


BOOL CLandMarkDB::getGroups(vecTransactionTCGroups &vec)
{
	CString csSQL, csName;
	int index = 0;
	vecTransactionTCGroups m_vec1, m_vec2;
	BOOL bUse, bChanged = FALSE;

	//get data from tc_GroupOrder table
	try
	{
		index = 0;
		m_vec1.clear();
		csSQL.Format(_T("SELECT * FROM %s"), TBL_GROUP_ORDER);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			m_vec1.push_back(CTransaction_TCGroups(m_saCommand.Field(1).asShort(),
				m_saCommand.Field(2).asShort(),
				m_saCommand.Field(3).asString().GetWideChars(),
				m_saCommand.Field(4).asShort(),
				m_saCommand.Field(5).asString().GetWideChars()
				));
			if(m_saCommand.Field(2).asShort() > index)
				index = m_saCommand.Field(2).asShort();	//get highest printorder number
		}
		doCommit();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(x.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}
#endif

	//next printorder index
	index++;

	//get data from tc_Groups table
	try
	{
		m_vec2.clear();
		csSQL.Format(_T("SELECT * FROM %s"), TBL_GROUPS);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			csName = m_saCommand.Field(3).asString().GetWideChars();

			//only use user defined group names
			if(csName.Find(_T("Group ")) != 0)
			{
				m_vec2.push_back(CTransaction_TCGroups(index, index, csName, 0, _T("")));
				index++;
			}			
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}

	vec.clear();
	index = 1;
	//if m_vec1[i] is in m_vec2 then use m_vec1[i]
	for(UINT i=0; i<m_vec1.size(); i++)
	{
		csName = m_vec1[i].getGroupName();
		for(UINT j=0; j<m_vec2.size(); j++)
		{
			if(csName.CompareNoCase(m_vec2[j].getGroupName()) == 0)
			{
				vec.push_back(CTransaction_TCGroups(index, /*index*/m_vec1[i].getPrintOrder(), m_vec1[i].getGroupName(), m_vec1[i].getClassIndex(), m_vec1[i].getClassName()));
				index++;
				break;
			}
		}
	}

	if(vec.size() != m_vec1.size())
		bChanged = TRUE;

	//if m_vec2[i] is not in vec then use m_vec2[i]
	for(UINT i=0; i<m_vec2.size(); i++)
	{
		bUse = TRUE;
		csName = m_vec2[i].getGroupName();
		for(UINT j=0; j<vec.size(); j++)
		{
			if(csName.CompareNoCase(vec[j].getGroupName()) == 0)
			{
				bUse = FALSE;
				break;
			}
		}

		if(bUse)
		{
			vec.push_back(CTransaction_TCGroups(index, /*index*/m_vec2[i].getPrintOrder(), m_vec2[i].getGroupName(), m_vec2[i].getClassIndex(), m_vec2[i].getClassName()));
			index++;
			bChanged = TRUE;
		}
	}

	
	if(bChanged)
	{
		//delete data from GroupOrder table and write vec
		try
		{
			csSQL.Format(_T("DELETE FROM %s"), TBL_GROUP_ORDER);
			m_saCommand.setCommandText((SAString)csSQL);
			m_saCommand.Execute();
			doCommit();
		}
		catch(SAException &x)
		{
			doRollback();
			AfxMessageBox(x.ErrText().GetWideChars());
			return FALSE;
		}

		for(UINT i=0; i<vec.size(); i++)
		{
			try
			{
				csSQL.Format(_T("INSERT INTO %s(ID,PrintOrder,GroupName,ClassIndex,ClassName) values\
							 (:1,:2,:3,:4,:5)"), TBL_GROUP_ORDER);
				m_saCommand.setCommandText((SAString)csSQL);

				m_saCommand.Param(1).setAsShort() = vec[i].getID();
				m_saCommand.Param(2).setAsShort() = vec[i].getPrintOrder();
				m_saCommand.Param(3).setAsString() = (SAString)vec[i].getGroupName();
				m_saCommand.Param(4).setAsShort() = vec[i].getClassIndex();
				m_saCommand.Param(5).setAsString() = (SAString)vec[i].getClassName();
				m_saCommand.Execute();
				doCommit();
			}
			catch(SAException &x)
			{
				doRollback();
				AfxMessageBox(x.ErrText().GetWideChars());
				return FALSE;
			}	
		}

	}//if(bChanged)


	return TRUE;
}



BOOL CLandMarkDB::getGroupsFromOrderTable(vecTransactionTCGroups &vec)
{
	CString csSQL;
	
	//get data from tc_GroupOrder table
	try
	{
		vec.clear();
		csSQL.Format(_T("SELECT * FROM %s"), TBL_GROUP_ORDER);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_TCGroups(m_saCommand.Field(1).asShort(),
				m_saCommand.Field(2).asShort(),
				m_saCommand.Field(3).asString().GetWideChars(),
				m_saCommand.Field(4).asShort(),
				m_saCommand.Field(5).asString().GetWideChars()
				));
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::getSnapshots(int fileindex, vecTransactionTCSnapshot &vec)
{
	CString csSQL;
	
	try
	{
		vec.clear();
		csSQL.Format(_T("SELECT img, name, id FROM gis_snapshots WHERE widgetid = %d AND type = 4 ORDER BY id"), fileindex); // Type 4 is TCruise stand
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			SAString str = m_saCommand.Field(1).asBLob();
			int len = m_saCommand.Field(1).asBLob().GetBinaryLength();

			char *buf = new char[len];
			memcpy(buf, m_saCommand.Field(1).asBLob().GetBinaryBuffer(0), len);
			int id = m_saCommand.Field(3).asLong();
			vec.push_back(CTransaction_TCSnapshot(buf, len, CString(m_saCommand.Field(2).asString()), m_saCommand.Field(3).asLong()));
			delete[] buf;
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::getSnapshotHeaders(int fileindex, vecTransactionTCSnapshotHeader &vec)
{
	CString csSQL;
	
	try
	{
		vec.clear();
		csSQL.Format(_T("SELECT name, id FROM gis_snapshots WHERE widgetid = %d AND type = 4 ORDER BY id"), fileindex); // Type 4 is TCruise stand
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_TCSnapshotHeader(CString(m_saCommand.Field(1).asString()), m_saCommand.Field(2).asLong()));
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;

}

BOOL CLandMarkDB::standDataExists(int fileindex)
{
	CString csSQL;
	csSQL.Format(_T("SELECT * FROM %s WHERE FileIndex = %d"),
		TBL_STAND, fileindex);
	return exists(csSQL);
}

BOOL CLandMarkDB::removeDataFromDB(int fileindex)
{
	CString csSQL;
	BOOL bRet = FALSE;

	if(standDataExists(fileindex))
	{
		try
		{
			for(int i=0; i<NUM_OF_DB_TABLES; i++)
			{
				switch(i)
				{
				case 0:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_SUM_DBH, fileindex);
					break;
				case 1:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_PLOT_VOLUME, fileindex);
					break;
				case 2:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_PLOT_GRDVOLS, fileindex);
					break;
				case 3:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_TREE_GRDVOL, fileindex);
					break;
				case 4:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_STRATA, fileindex);
					break;
				case 5:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_MERCH_SPECS, fileindex);
					break;
				case 6:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_PROD_NMS, fileindex);
					break;
				case 7:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_GRADE_NMS, fileindex);
					break;
				case 8:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_PRICES, fileindex);
					break;
				case 9:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_GROUPS, fileindex);
					break;
				case 10:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_SPECIES, fileindex);
					break;
				case 11:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_SITE_INDEX, fileindex);
					break;
				case 12:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_JOINEDPLOTDATA, fileindex);
				case 13:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_AUDITREMEASDATA, fileindex);
					break;
				case 14:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_TREE, fileindex);
					break;
				case 15:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_PLOT, fileindex);
					break;
				case 16:
					csSQL.Format(_T("DELETE FROM %s WHERE FileIndex = %d"),
						TBL_STAND, fileindex);
					break;
				}

				m_saCommand.setCommandText((SAString)csSQL);
				m_saCommand.Execute();
			}
			doCommit();
			bRet=TRUE;
		}
		catch(SAException &e)
		{
			doRollback();
			AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
			bRet = FALSE;
		}
	}

	return bRet;
}

BOOL CLandMarkDB::removeClassFromOrderHdr(int index)
{
	CString csSQL;

	if(index == 0)	//don't delete first row in db, empty row
		return TRUE;

	try
	{
		csSQL.Format(_T("DELETE FROM %s WHERE ClassIndex = %d"),
			TBL_GROUP_ORDER_HDR, index);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}
	return TRUE;
}


BOOL CLandMarkDB::removeGroupFromOrder(int index)
{
	CString csSQL;

	if(index < 0)	
		return TRUE;

	try
	{
		csSQL.Format(_T("DELETE FROM %s WHERE ID = %d"),
			TBL_GROUP_ORDER, index);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}
	return TRUE;
}

BOOL CLandMarkDB::removeSnapshot(int id)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("DELETE FROM gis_snapshots WHERE id = %d"), id);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}
	return TRUE;
}

BOOL CLandMarkDB::saveDataToDB(CStandSelListReportRec* pRec)
{
	BOOL bRet = FALSE;

	CString csSQL;

	if(standDataExists(pRec->getFileIndex()))
	{
		csSQL.Format(_T("update %s set CruiseDate = '%s', TractLoc = '%s', TractOwner = '%s', Cruiser = '%s' where FileIndex = %d"),
			TBL_STAND, pRec->getColumnText(COLUMN_CRUISEDATE), pRec->getColumnText(COLUMN_TRACTLOC), pRec->getColumnText(COLUMN_TRACTOWNER), pRec->getColumnText(COLUMN_CRUISER), pRec->getFileIndex());
		try
		{
			m_saCommand.setCommandText((SAString)csSQL);
			m_saCommand.Execute();
			doCommit();
			bRet=TRUE;
		}		

		catch(SAException &e)
		{
			doRollback();
			AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
			bRet = FALSE;
		}
	}

	return bRet;
}

BOOL CLandMarkDB::saveClassNameToTableOrder(CAssignGroupsReportRec *pRec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET ClassIndex = %d,ClassName = '%s' WHERE ID = %d"),
			TBL_GROUP_ORDER, pRec->getClassIndex(), pRec->getColumnText(COLUMN_ASSIGNGROUPS_CLASSNAME),pRec->getIndex());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::saveGroupToTableOrder(CAssignGroupsReportRec *pRec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET PrintOrder = %d, GroupName = '%s', ClassIndex = %d,ClassName = '%s' WHERE ID = %d"),
			TBL_GROUP_ORDER, pRec->getColumnInt(COLUMN_ASSIGNGROUPS_PRINTORDER), pRec->getColumnText(COLUMN_ASSIGNGROUPS_GROUPNAME), pRec->getClassIndex(), pRec->getColumnText(COLUMN_ASSIGNGROUPS_CLASSNAME),pRec->getIndex());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::saveClassNameToTableOrderHdr(CClassNamesReportRec *pRec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET ClassName = '%s' WHERE ClassIndex = %d"),
			TBL_GROUP_ORDER_HDR, pRec->getColumnText(COLUMN_CLASS_CLASSNAME),pRec->getClassIndex());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::savePrintOrderToTableOrderHdr(CClassNamesReportRec *pRec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET PrintOrder = %d WHERE ClassIndex = %d"),
			TBL_GROUP_ORDER_HDR, pRec->getColumnInt(COLUMN_CLASS_PRINTORDER),pRec->getClassIndex());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::savePrintOrderToTableOrder(CAssignGroupsReportRec *pRec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET PrintOrder = %d WHERE ID = %d"),
			TBL_GROUP_ORDER, pRec->getColumnInt(COLUMN_ASSIGNGROUPS_PRINTORDER),pRec->getIndex());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::saveToTableOrderHdr(CClassNamesReportRec *pRec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("UPDATE %s SET ClassName = '%s', PrintOrder = %d WHERE ClassIndex = %d"),
			TBL_GROUP_ORDER_HDR, pRec->getColumnText(COLUMN_CLASS_CLASSNAME), pRec->getColumnInt(COLUMN_CLASS_PRINTORDER),
			pRec->getClassIndex()
			);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::saveNewClassNameToTableOrderHdr(CClassNamesReportRec *pRec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("INSERT INTO %s  VALUES(%d, '%s', %d)"),
			TBL_GROUP_ORDER_HDR, pRec->getClassIndex(), pRec->getColumnText(COLUMN_CLASS_CLASSNAME), pRec->getColumnInt(COLUMN_CLASS_PRINTORDER));
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

int CLandMarkDB::getClassIndexFromOrderHdr(CString classname)
{
	int index = -1;
	CString csSQL;
	try
	{
		csSQL.Format(_T("SELECT * FROM %s ORDER BY ClassIndex"), TBL_GROUP_ORDER_HDR);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			if(classname.Compare(m_saCommand.Field(2).asString().GetWideChars()) == 0)
			{
				index = m_saCommand.Field(1).asShort();
			}
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return -1;
	}

	return index;
}

CString CLandMarkDB::getClassNameFromOrderHdr(int index)
{
	CString csRet = _T("");
	CString csSQL;
	try
	{
		csSQL.Format(_T("SELECT * FROM %s ORDER BY ClassIndex"), TBL_GROUP_ORDER_HDR);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			if(index == m_saCommand.Field(1).asShort())
			{
				csRet = m_saCommand.Field(2).asString().GetWideChars();
			}
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return _T("");
	}

	return csRet;
}

int CLandMarkDB::getNextClassIndex()
{
	CString csSQL;
	int ans = -1;
	int tmp = -1;
		
	try
	{
		csSQL.Format(_T("SELECT * FROM %s"), TBL_GROUP_ORDER_HDR);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			ans = m_saCommand.Field(1).asShort();
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return -1;
	}

	ans++;	//update to next index
			
	return ans;
}

CString CLandMarkDB::getCruiseMethod(int method)
{
	CString csLangFN;
	csLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	CString csPlot, csPoint;
	CString csDPoint, csStump, csUnkown;

	csPlot=_T("");
	csPoint = _T("");
	csDPoint = _T("");
	csStump = _T("");
	csUnkown = _T("");

	if(fileExists(csLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(csLangFN))
		{
			csPlot = xml->str(IDS_STRING162);
			csPoint = xml->str(IDS_STRING163);
			csDPoint = xml->str(IDS_STRING164);
			csStump = xml->str(IDS_STRING165);
			csUnkown = xml->str(IDS_STRING166);
		}
		delete xml;
	}

	switch(method)
	{
	case 0:	//Plot
		return csPlot;
	case 1:	//Point
		return csPoint;
	case 2:	//Double Point
		return csDPoint;
	case 3:	//Stump
		return csStump;
	default:	//Unkown
		return csUnkown;
	}
}


//TODO: if cruise method is editable
int CLandMarkDB::setCruiseMethod(LPCTSTR method)
{
	int nRet = 0;

	CString csLangFN;
	csLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	CString csPlot, csPoint;
	CString csDPoint, csStump, csUnkown;

	csPlot=_T("");
	csPoint = _T("");
	csDPoint = _T("");
	csStump = _T("");
	csUnkown = _T("");

	if(fileExists(csLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(csLangFN))
		{
			csPlot = xml->str(IDS_STRING162);
			csPoint = xml->str(IDS_STRING163);
			csDPoint = xml->str(IDS_STRING164);
			csStump = xml->str(IDS_STRING165);
			csUnkown = xml->str(IDS_STRING166);
		}
		delete xml;
	}

	if(method == csPoint)
		nRet = 1;
	else if(method == csDPoint)
		nRet = 2;
	else if(method == csStump)
		nRet = 3;
	else
		nRet = 0;

	return nRet;
}


int CLandMarkDB::getLastFileIndex()
{
	int nRet = -1;
	CString csSQL;

	csSQL.Format(_T("SELECT * FROM %s WHERE FileIndex = (SELECT MAX(FileIndex) FROM %s);"), TBL_STAND, TBL_STAND);
	try
	{
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			nRet = m_saCommand.Field(1).asShort();
		}
		doCommit();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(e.ErrText().GetWideChars());
		return -2;
	}
#else
	catch(SAException &)
	{
		doRollback();
		return -2;
	}
#endif

	return nRet;
}



BOOL CLandMarkDB::setData(SACommand &cmd, LPCTSTR table, int fileindex, LPCTSTR csTract, CProgressDialog &dlg)
{
	CString csSQL, csTmp, csName;
	int nFields, nStart = 2;
	CString csTable;
	csTable.Format(_T("%s"), table);

	if(csTable == TBL_AUDITREMEASDATA)
	{
		if(isCruiseAuditRemeas(fileindex))
			return setAuditRemeasData(cmd, fileindex, dlg);
		else
			return TRUE;
	}

	csSQL.Format(_T("INSERT INTO %s ("), table);

	//get num of fields in record
	nFields = cmd.FieldCount();

	//check that first param is FileIndex
	if(cmd.Field(1).Name().Compare((SAString)_T("FileIndex")) != 0)
	{
		if(csTable == TBL_JOINEDPLOTDATA)
		{
			;	//FileIndex not included in Access JoinedPlotData table 
		}
		else
		{
			CString csLangFN, csMsg;
			csLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
			if(fileExists(csLangFN))
			{
				RLFReader *xml = new RLFReader;
				if(xml->Load(csLangFN))
				{
					csMsg = xml->str(IDS_STRING172);
				}
				delete xml;
			}
			AfxMessageBox(csMsg);
			return FALSE;
		}
	}

	if(csTable == TBL_JOINEDPLOTDATA)
		csSQL += _T("FileIndex,");
	
	for(int i=1; i<=nFields; i++)
	{
		csName = (CString)cmd.Field(i).Name();

		if(csTable == TBL_TREE && csName == _T("Top"))
			csName = _T("Top_");
		if(csTable == TBL_TREE && csName == _T("Count"))
			csName = _T("Count_");
		if(csTable == TBL_PLOT_GRDVOLS && csName == _T("IN"))
			csName = _T("IN_");
		if(csTable == TBL_PRICES && csName == _T("Case"))
			csName = _T("Case_");

		if(i == nFields)
			csTmp.Format(_T("%s"), csName);
		else
			csTmp.Format(_T("%s,"), csName);
		csSQL += csTmp;
	}

	csSQL += _T(") values(");

	if(csTable == TBL_JOINEDPLOTDATA)
		nFields++;	//add for FileIndex

	for(int i=1; i<=nFields; i++)
	{
		if(i != nFields)
			csTmp.Format(_T(":%d,"),i);
		else
			csTmp.Format(_T(":%d"),nFields);
		csSQL += csTmp;
	}

	csSQL += _T(")");

	SAString saStr;
	MSG Msg;

	try
	{
		m_saCommand.setCommandText((SAString)csSQL);
		while(cmd.FetchNext())
		{
			// Process window messages
			doEvents();
				
			m_saCommand.Param(1).setAsShort() = fileindex;	
			
			if(csTable == TBL_JOINEDPLOTDATA)
				nStart = 1;
			else
				nStart = 0;

			for(int i=2; i<=nFields; i++)
			{
				saStr = cmd.Field(i - nStart).asString();
				if(saStr.IsEmpty())
				{
					//check that tract name not empty
					if(csTable == TBL_STAND && i == 2 && csTract != _T(""))
					{
						m_saCommand.Param(i).setAsString() = (SAString)csTract;
					}//if(table == TBL_STAND && i == 2)
					else
					{
						m_saCommand.Param(i).setAsNull();
					}
				}
				else if(csTable == TBL_STAND && i == 2 && csTract != _T(""))
				{	//update to new tract name
					m_saCommand.Param(i).setAsString() = (SAString)csTract;
				}
				else
				{
					m_saCommand.Param(i).setAsString() = saStr;
				}
			}//for(int i=2; i<=nFields; i++)

			m_saCommand.Execute();

			

			if(dlg.GetSafeHwnd() != NULL)
			{
				dlg.stepIt();
				dlg.updateWindow();
				::UpdateWindow(AfxGetMainWnd()->GetSafeHwnd());
				::PeekMessage(&Msg, 0, 0, 0, PM_NOREMOVE);
			}
		}//while(cmd.FetchNext())
		//doCommit();
		//don't commit here! Only commit after all tables has been filled with data
	}
	catch(SAException &x)
	{
		//doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

int CLandMarkDB::tractExists(LPCTSTR tract)
{
	int index = -1;
	CString csSQL;

	csSQL.Format(_T("SELECT * FROM %s WHERE TractID = '%s';"), TBL_STAND, tract);

	try
	{
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			if(m_saCommand.Field(1).asShort() > 0)
				index = m_saCommand.Field(1).asShort();
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return -1;
	}

	return index;
}

BOOL CLandMarkDB::isCruiseAuditRemeas(int fileindex)
{
	int index = 0;
	CString csSQL;
	BOOL bRet = FALSE;

	csSQL.Format(_T("SELECT RemeasType FROM %s WHERE FileIndex = %d;"), TBL_STAND, fileindex);

	try
	{
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		index = 0;
		while(m_saCommand.FetchNext())
		{
			index = m_saCommand.Field(1).asShort();
			if(index == 1 || index == 2)
				bRet = TRUE;
		}
		m_saCommand.Close();
		//don't commit here
	}
	catch(SAException &x)
	{
		AfxMessageBox(x.ErrText().GetWideChars());
		return FALSE;
	}

	return bRet;
}

BOOL CLandMarkDB::setAuditRemeasData(SACommand &cmd, int fileindex, CProgressDialog &dlg)
{
	CString csSQL, csTmp, csName, csSQL_all;
	int nFields, type;

	AuditRemeasType Meas2;
	AuditRemeasType Meas1;

	BOOL bRet = TRUE;

	csSQL.Format(_T("INSERT INTO %s ("), TBL_AUDITREMEASDATA);
	
	//get num of fields in record
	nFields = cmd.FieldCount();

	//check that first param is FileIndex
	if(cmd.Field(1).Name().Compare((SAString)_T("FileIndex")) != 0)
	{
			CString csLangFN, csMsg;
			csLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
			if(fileExists(csLangFN))
			{
				RLFReader *xml = new RLFReader;
				if(xml->Load(csLangFN))
				{
					csMsg = xml->str(IDS_STRING172);
				}
				delete xml;
			}
			AfxMessageBox(csMsg);
			return FALSE;
	}

	for(int i=1; i<=nFields; i++)
	{
		csName = (CString)cmd.Field(i).Name();

		if(csName == _T("Top"))
			csName = _T("Top_");
		if(csName == _T("Count"))
			csName = _T("Count_");
		
		if(i == nFields)
			csTmp.Format(_T("%s"), csName);
		else
			csTmp.Format(_T("%s,"), csName);
		csSQL += csTmp;
	}

	csSQL += _T(") values(");

	for(int i=1; i<=nFields; i++)
	{
		if(i != nFields)
			csTmp.Format(_T(":%d,"),i);
		else
			csTmp.Format(_T(":%d"),nFields);
		csSQL += csTmp;
	}

	csSQL += _T(")");

	csSQL_all.Format(_T("%s"), SAVE_TBL_AUDITREMEASDATA);
	for(int i=1; i<=NUM_OF_AUDITREMEAS_COLS; i++)
	{
		if(i != NUM_OF_AUDITREMEAS_COLS)
			csTmp.Format(_T(":%d,"),i);
		else
			csTmp.Format(_T(":%d"),i);
		csSQL_all += csTmp;
	}
	csSQL_all += _T(")");

	SAString saStr;
	MSG Msg;

	try
	{
		
		while(cmd.FetchNext())
		{
			if(cmd.Field(5).asShort() == 0)
			{
Type_is_0:
				m_saCommand.setCommandText((SAString)csSQL);
				m_saCommand.Param(1).setAsShort() = fileindex;	

				for(int i=2; i<=nFields; i++)
				{
					saStr = cmd.Field(i).asString();
					if(saStr.IsEmpty())
					{
						m_saCommand.Param(i).setAsNull();
					}
					else
					{
						if(cmd.Field(i).FieldType() == SA_dtString)
						{
							if((((CString)cmd.Field(i).Name()).Find(_T("CC")) == 0) && isStrNumber(saStr.GetWideChars()))
								m_saCommand.Param(i).setAsString() = saStr;
							else
								m_saCommand.Param(i).setAsString() = _T("1");
						}
						else
							m_saCommand.Param(i).setAsString() = saStr;
					}
				}//for(int i=2; i<=nFields; i++)

				m_saCommand.Execute();
			}//if(cmd.Field(5).asShort() == 0)
			else if(cmd.Field(5).asShort() == 2)
			{
Type_is_2:
				if(getFieldData(cmd, Meas2, fileindex, nFields) == FALSE)
					goto Type_is_return;//return FALSE;
			
				//get next row
				if(!cmd.FetchNext())
				{
					//no more trees, save current tree
					if(saveType2ToAuditTable(Meas2, csSQL_all, nFields) == FALSE)
						goto Type_is_return;//return FALSE;
					break;
				}

				//check type on next tree
				if((type = cmd.Field(5).asShort()) == 2)
				{
					if(saveType2ToAuditTable(Meas2, csSQL_all, nFields) == FALSE)
						goto Type_is_return;//return FALSE;
					goto Type_is_2;	//read next tree into Meas2 struct
				}
				else if(type == 0)
				{
					if(saveType2ToAuditTable(Meas2, csSQL_all, nFields) == FALSE)
						goto Type_is_return;//return FALSE;
					goto Type_is_0;	//save next tree in db
				}
				else if(type == 1)
				{
					if(getFieldData(cmd, Meas1, fileindex, nFields) == FALSE)
						goto Type_is_return;//return FALSE;

					//calculate difference and save in db
					if(saveToAuditTable(Meas2, Meas1, csSQL_all, nFields) == FALSE)
						goto Type_is_return;//return FALSE;

				}
				else
				{
					//unkown type, save current tree
					if(saveType2ToAuditTable(Meas2, csSQL_all, nFields) == FALSE)
						goto Type_is_return;//return FALSE;
				}
			}
			else if(cmd.Field(5).asShort() == 1)
			{
				//should not get here, skip?
				continue;
			}
			else
			{
				//unknown type
				continue;	//?
			}

			if(dlg.GetSafeHwnd() != NULL)
			{
				dlg.stepIt();
				dlg.updateWindow();
				::UpdateWindow(AfxGetMainWnd()->GetSafeHwnd());
				::PeekMessage(&Msg, 0, 0, 0, PM_NOREMOVE);
			}
		}//while(cmd.FetchNext())

		goto Type_is_continue;

Type_is_return:
		while(cmd.FetchNext());
		bRet = FALSE;
Type_is_continue:
		bRet = bRet;	//dummy
	}
	catch(SAException &x)
	{
		AfxMessageBox(x.ErrText().GetWideChars());
		bRet = FALSE;
	}


	return bRet;


}

BOOL CLandMarkDB::isStrNumber(CString str)
{
	BOOL bRet = FALSE;

	CString res = str.SpanIncluding(_T("0123456789.-"));
	if(str == res && str.GetLength() == res.GetLength())
		bRet = TRUE;

	return bRet;
}


BOOL CLandMarkDB::saveType2ToAuditTable(AuditRemeasType &meas2, CString csSQL, int fields)
{
try
	{
		m_saCommand.setCommandText((SAString)csSQL);

		m_saCommand.Param(1).setAsShort() = meas2.FileIndex;
		m_saCommand.Param(2).setAsShort() = meas2.PlotIndex;
		m_saCommand.Param(3).setAsShort() = meas2.TRecIndex;
		m_saCommand.Param(4).setAsDouble() = meas2.TRecType;
		m_saCommand.Param(5).setAsDouble() = meas2.Measurement;		//2
		meas2.SpeciesCode == _T("")? m_saCommand.Param(6).setAsNull() : m_saCommand.Param(6).setAsDouble() = 1;
		meas2.SpeciesName == _T("")? m_saCommand.Param(7).setAsNull() : m_saCommand.Param(7).setAsDouble() = 1;
		meas2.GroupName == _T("")? m_saCommand.Param(8).setAsNull() : m_saCommand.Param(8).setAsDouble() = 1;
		m_saCommand.Param(9).setAsDouble() = meas2.Dbh;
		m_saCommand.Param(10).setAsDouble() = meas2.ProductNumb;

		meas2.ProductName == _T("")? m_saCommand.Param(11).setAsNull() : m_saCommand.Param(11).setAsDouble() = 1;
		m_saCommand.Param(12).setAsDouble() = meas2.TQIPrd;
		m_saCommand.Param(13).setAsDouble() = meas2.Count_;
		m_saCommand.Param(14).setAsDouble() = meas2.GSF;
		m_saCommand.Param(15).setAsDouble() = meas2.PACF;
		m_saCommand.Param(16).setAsDouble() = meas2.DefStumpHt;
		m_saCommand.Param(17).setAsDouble() = meas2.HmObs;
		m_saCommand.Param(18).setAsDouble() = meas2.HsObs;
		m_saCommand.Param(19).setAsDouble() = meas2.HpObs;
		m_saCommand.Param(20).setAsDouble() = meas2.HmEst;

		m_saCommand.Param(21).setAsDouble() = meas2.HsEst;
		m_saCommand.Param(22).setAsDouble() = meas2.HpEst;
		m_saCommand.Param(23).setAsDouble() = meas2.Top_;
		m_saCommand.Param(24).setAsDouble() = meas2.Defect;
		m_saCommand.Param(25).setAsDouble() = meas2.FcObs;
		m_saCommand.Param(26).setAsDouble() = meas2.FcEst;
		m_saCommand.Param(27).setAsDouble() = meas2.Age;
		m_saCommand.Param(28).setAsDouble() = meas2.Ht;
		m_saCommand.Param(29).setAsDouble() = meas2.RadialGr;
		m_saCommand.Param(30).setAsDouble() = meas2.SngBark;

		m_saCommand.Param(31).setAsDouble() = meas2.ReproPlot;
		m_saCommand.Param(32).setAsDouble() = meas2.OffPlot;
		m_saCommand.Param(33).setAsDouble() = meas2.SubMerch;
		m_saCommand.Param(34).setAsDouble() = meas2.PWOnly;
		m_saCommand.Param(35).setAsDouble() = meas2.TClass;
		meas2.TClassName == _T("")? m_saCommand.Param(36).setAsNull() : m_saCommand.Param(36).setAsDouble() = 1;
		m_saCommand.Param(37).setAsDouble() = meas2.TClass2;
		meas2.TClassName2 == _T("")? m_saCommand.Param(38).setAsNull() : m_saCommand.Param(38).setAsDouble() = 1;
		m_saCommand.Param(39).setAsDouble() = meas2.IsGradeTree;

		if(meas2.IsGradeTree > 0)
		{
			m_saCommand.Param(40).setAsDouble() = meas2.GrdStumpHt;

			m_saCommand.Param(41).setAsDouble() = meas2.StopTop;
			m_saCommand.Param(42).setAsDouble() = meas2.TopType;
			meas2.TopTypeV == _T("")? m_saCommand.Param(43).setAsNull() : m_saCommand.Param(43).setAsDouble() = 1;
			m_saCommand.Param(44).setAsDouble() = meas2.SGL1;
			m_saCommand.Param(45).setAsDouble() = meas2.GRD1;
			meas2.GRDV1 == _T("")? m_saCommand.Param(46).setAsNull() : m_saCommand.Param(46).setAsDouble() = 1;
			m_saCommand.Param(47).setAsDouble() = meas2.DEF1;
			m_saCommand.Param(48).setAsDouble() = meas2.SGL2;
			m_saCommand.Param(49).setAsDouble() = meas2.GRD2;
			meas2.GRDV2 == _T("")? m_saCommand.Param(46).setAsNull() : m_saCommand.Param(46).setAsDouble() = 1;

			m_saCommand.Param(51).setAsDouble() = meas2.DEF2;
			m_saCommand.Param(52).setAsDouble() = meas2.SGL3;
			m_saCommand.Param(53).setAsDouble() = meas2.GRD3;
			meas2.GRDV3 == _T("")? m_saCommand.Param(46).setAsNull() : m_saCommand.Param(46).setAsDouble() = 1;
			m_saCommand.Param(55).setAsDouble() = meas2.DEF3;
			m_saCommand.Param(56).setAsDouble() = meas2.SGL4;
			m_saCommand.Param(57).setAsDouble() = meas2.GRD4;
			meas2.GRDV4 == _T("")? m_saCommand.Param(46).setAsNull() : m_saCommand.Param(46).setAsDouble() = 1;
			m_saCommand.Param(59).setAsDouble() = meas2.DEF4;
			m_saCommand.Param(60).setAsDouble() = meas2.SGL5;

			m_saCommand.Param(61).setAsDouble() = meas2.GRD5;
			meas2.GRDV5 == _T("")? m_saCommand.Param(46).setAsNull() : m_saCommand.Param(46).setAsDouble() = 1;
			m_saCommand.Param(63).setAsDouble() = meas2.DEF5;
			m_saCommand.Param(64).setAsDouble() = meas2.SGL6;
			m_saCommand.Param(65).setAsDouble() = meas2.GRD6;
			meas2.GRDV6 == _T("")? m_saCommand.Param(46).setAsNull() : m_saCommand.Param(46).setAsDouble() = 1;
			m_saCommand.Param(67).setAsDouble() = meas2.DEF6;
			m_saCommand.Param(68).setAsDouble() = meas2.SGL7;
			m_saCommand.Param(69).setAsDouble() = meas2.GRD7;
			meas2.GRDV7 == _T("")? m_saCommand.Param(46).setAsNull() : m_saCommand.Param(46).setAsDouble() = 1;

			m_saCommand.Param(71).setAsDouble() = meas2.DEF7;
		}

		m_saCommand.Param(72).setAsDouble() = meas2.LogTrim;
		m_saCommand.Param(73).setAsDouble() = meas2.BA;
		m_saCommand.Param(74).setAsDouble() = meas2.PW_CFVOB;
		m_saCommand.Param(75).setAsDouble() = meas2.PW_CFVIB;
		m_saCommand.Param(76).setAsDouble() = meas2.PW_GreenTons;
		m_saCommand.Param(77).setAsDouble() = meas2.PW_WtCords;
		m_saCommand.Param(78).setAsDouble() = meas2.SW_CFVOB;
		m_saCommand.Param(79).setAsDouble() = meas2.SW_CFVIB;
		m_saCommand.Param(80).setAsDouble() = meas2.SW_GreenTons;

		m_saCommand.Param(81).setAsDouble() = meas2.SW_WtCords;
		m_saCommand.Param(82).setAsDouble() = meas2.IntQuarter;
		m_saCommand.Param(83).setAsDouble() = meas2.Doyle;
		m_saCommand.Param(84).setAsDouble() = meas2.Scribner;
		m_saCommand.Param(85).setAsDouble() = meas2.PW_Dollars ;
		m_saCommand.Param(86).setAsDouble() = meas2.SW_Dollars;

		for(int i=0; i<NUM_OF_CC; i++)
		{
			if(meas2.CC[i] == _T(""))
				m_saCommand.Param(87+i).setAsNull();
			else if(isStrNumber(meas2.CC[i]))
				m_saCommand.Param(87+i).setAsDouble() = _tstof(meas2.CC[i]);
			else
				m_saCommand.Param(87+i).setAsDouble() = 1;
		}

		m_saCommand.Execute();
	}
	catch(SAException &x)
	{
		AfxMessageBox(x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::saveToAuditTable(AuditRemeasType &meas2, AuditRemeasType &meas1, CString csSQL, int fields)
{
	if(meas2.PlotIndex != meas1.PlotIndex)
	{
		//should have same plot index, skip?
		return TRUE;
	}

	if(meas1.TRecIndex != (meas2.TRecIndex + 1))
	{
		//not right tree index number, skip?
		return TRUE;
	}
	
	try
	{
		m_saCommand.setCommandText((SAString)csSQL);

		m_saCommand.Param(1).setAsShort() = meas2.FileIndex;
		m_saCommand.Param(2).setAsShort() = meas2.PlotIndex;
		m_saCommand.Param(3).setAsShort() = meas2.TRecIndex;
		m_saCommand.Param(4).setAsDouble() = meas2.TRecType - meas1.TRecType;
		m_saCommand.Param(5).setAsDouble() = meas1.Measurement;	//1
		m_saCommand.Param(6).setAsDouble() = meas2.SpeciesCode == meas1.SpeciesCode? 0:1;
		m_saCommand.Param(7).setAsDouble() = meas2.SpeciesName == meas1.SpeciesName? 0:1;
		m_saCommand.Param(8).setAsDouble() = meas2.GroupName == meas1.GroupName? 0:1;
		m_saCommand.Param(9).setAsDouble() = meas2.Dbh - meas1.Dbh;
		m_saCommand.Param(10).setAsDouble() = meas2.ProductNumb - meas1.ProductNumb;

		m_saCommand.Param(11).setAsDouble() = meas2.ProductName == meas1.ProductName? 0:1;
		m_saCommand.Param(12).setAsDouble() = meas2.TQIPrd - meas1.TQIPrd;
		m_saCommand.Param(13).setAsDouble() = meas2.Count_ - meas1.Count_;
		m_saCommand.Param(14).setAsDouble() = meas2.GSF - meas1.GSF;
		m_saCommand.Param(15).setAsDouble() = meas2.PACF - meas1.PACF;
		m_saCommand.Param(16).setAsDouble() = meas2.DefStumpHt - meas1.DefStumpHt;
		m_saCommand.Param(17).setAsDouble() = meas2.HmObs - meas1.HmObs;
		m_saCommand.Param(18).setAsDouble() = meas2.HsObs - meas1.HsObs;
		m_saCommand.Param(19).setAsDouble() = meas2.HpObs - meas1.HpObs;
		m_saCommand.Param(20).setAsDouble() = meas2.HmEst - meas1.HmEst;

		m_saCommand.Param(21).setAsDouble() = meas2.HsEst - meas1.HsEst;
		m_saCommand.Param(22).setAsDouble() = meas2.HpEst - meas1.HpEst;
		m_saCommand.Param(23).setAsDouble() = meas2.Top_ - meas1.Top_;
		m_saCommand.Param(24).setAsDouble() = meas2.Defect - meas1.Defect;
		m_saCommand.Param(25).setAsDouble() = meas2.FcObs - meas1.FcObs;
		m_saCommand.Param(26).setAsDouble() = meas2.FcEst - meas1.FcEst;
		m_saCommand.Param(27).setAsDouble() = meas2.Age - meas1.Age;
		m_saCommand.Param(28).setAsDouble() = meas2.Ht - meas1.Ht;
		m_saCommand.Param(29).setAsDouble() = meas2.RadialGr - meas1.RadialGr;
		m_saCommand.Param(30).setAsDouble() = meas2.SngBark - meas1.SngBark;

		m_saCommand.Param(31).setAsDouble() = meas2.ReproPlot - meas1.ReproPlot;
		m_saCommand.Param(32).setAsDouble() = meas2.OffPlot - meas1.OffPlot;
		m_saCommand.Param(33).setAsDouble() = meas2.SubMerch - meas1.SubMerch;
		m_saCommand.Param(34).setAsDouble() = meas2.PWOnly - meas1.PWOnly;
		m_saCommand.Param(35).setAsDouble() = meas2.TClass - meas1.TClass;
		m_saCommand.Param(36).setAsDouble() = meas2.TClassName == meas1.TClassName? 0:1;
		m_saCommand.Param(37).setAsDouble() = meas2.TClass2 - meas1.TClass2;
		m_saCommand.Param(38).setAsDouble() = meas2.TClassName2 == meas1.TClassName2? 0:1;
		m_saCommand.Param(39).setAsDouble() = meas2.IsGradeTree - meas1.IsGradeTree;
		m_saCommand.Param(40).setAsDouble() = meas2.GrdStumpHt - meas1.GrdStumpHt;

		m_saCommand.Param(41).setAsDouble() = meas2.StopTop - meas1.StopTop;
		m_saCommand.Param(42).setAsDouble() = meas2.TopType - meas1.TopType;
		m_saCommand.Param(43).setAsDouble() = meas2.TopTypeV == meas1.TopTypeV? 0:1;
		m_saCommand.Param(44).setAsDouble() = meas2.SGL1 - meas1.SGL1;
		m_saCommand.Param(45).setAsDouble() = meas2.GRD1 - meas1.GRD1;
		m_saCommand.Param(46).setAsDouble() = meas2.GRDV1 == meas1.GRDV1? 0:1;
		m_saCommand.Param(47).setAsDouble() = meas2.DEF1 - meas1.DEF1;
		m_saCommand.Param(48).setAsDouble() = meas2.SGL2 - meas1.SGL2;
		m_saCommand.Param(49).setAsDouble() = meas2.GRD2 - meas1.GRD2;
		m_saCommand.Param(50).setAsDouble() = meas2.GRDV2 == meas1.GRDV2? 0:1;

		m_saCommand.Param(51).setAsDouble() = meas2.DEF2 - meas1.DEF2;
		m_saCommand.Param(52).setAsDouble() = meas2.SGL3 - meas1.SGL3;
		m_saCommand.Param(53).setAsDouble() = meas2.GRD3 - meas1.GRD3;
		m_saCommand.Param(54).setAsDouble() = meas2.GRDV3 == meas1.GRDV3? 0:1;
		m_saCommand.Param(55).setAsDouble() = meas2.DEF3 - meas1.DEF3;
		m_saCommand.Param(56).setAsDouble() = meas2.SGL4 - meas1.SGL4;
		m_saCommand.Param(57).setAsDouble() = meas2.GRD4 - meas1.GRD4;
		m_saCommand.Param(58).setAsDouble() = meas2.GRDV4 == meas1.GRDV4? 0:1;
		m_saCommand.Param(59).setAsDouble() = meas2.DEF4 - meas1.DEF4;
		m_saCommand.Param(60).setAsDouble() = meas2.SGL5 - meas1.SGL5;

		m_saCommand.Param(61).setAsDouble() = meas2.GRD5 - meas1.GRD5;
		m_saCommand.Param(62).setAsDouble() = meas2.GRDV5 == meas1.GRDV5? 0:1;
		m_saCommand.Param(63).setAsDouble() = meas2.DEF5 - meas1.DEF5;
		m_saCommand.Param(64).setAsDouble() = meas2.SGL6 - meas1.SGL6;
		m_saCommand.Param(65).setAsDouble() = meas2.GRD6 - meas1.GRD6;
		m_saCommand.Param(66).setAsDouble() = meas2.GRDV6 == meas1.GRDV6? 0:1;
		m_saCommand.Param(67).setAsDouble() = meas2.DEF6 - meas1.DEF6;
		m_saCommand.Param(68).setAsDouble() = meas2.SGL7 - meas1.SGL7;
		m_saCommand.Param(69).setAsDouble() = meas2.GRD7 - meas1.GRD7;
		m_saCommand.Param(70).setAsDouble() = meas2.GRDV7 == meas1.GRDV7? 0:1;

		m_saCommand.Param(71).setAsDouble() = meas2.DEF7 - meas1.DEF7;
		m_saCommand.Param(72).setAsDouble() = meas2.LogTrim - meas1.LogTrim;
		m_saCommand.Param(73).setAsDouble() = meas2.BA - meas1.BA;
		m_saCommand.Param(74).setAsDouble() = meas2.PW_CFVOB - meas1.PW_CFVOB;
		m_saCommand.Param(75).setAsDouble() = meas2.PW_CFVIB - meas1.PW_CFVIB;
		m_saCommand.Param(76).setAsDouble() = meas2.PW_GreenTons - meas1.PW_GreenTons;
		m_saCommand.Param(77).setAsDouble() = meas2.PW_WtCords - meas1.PW_WtCords;
		m_saCommand.Param(78).setAsDouble() = meas2.SW_CFVOB - meas1.SW_CFVOB;
		m_saCommand.Param(79).setAsDouble() = meas2.SW_CFVIB - meas1.SW_CFVIB;
		m_saCommand.Param(80).setAsDouble() = meas2.SW_GreenTons - meas1.SW_GreenTons;

		m_saCommand.Param(81).setAsDouble() = meas2.SW_WtCords - meas1.SW_WtCords;
		m_saCommand.Param(82).setAsDouble() = meas2.IntQuarter - meas1.IntQuarter;
		m_saCommand.Param(83).setAsDouble() = meas2.Doyle - meas1.Doyle;
		m_saCommand.Param(84).setAsDouble() = meas2.Scribner - meas1.Scribner;
		m_saCommand.Param(85).setAsDouble() = meas2.PW_Dollars - meas1.PW_Dollars;
		m_saCommand.Param(86).setAsDouble() = meas2.SW_Dollars - meas1.SW_Dollars;

		for(int i=0; i<NUM_OF_CC; i++)
		{
			if(isStrNumber(meas2.CC[i]) && isStrNumber(meas1.CC[i]))
				m_saCommand.Param(87+i).setAsDouble() = _tstof(meas2.CC[i]) - _tstof(meas1.CC[i]);
			else
				m_saCommand.Param(87+i).setAsDouble() = meas2.CC[i] == meas1.CC[i]? 0:1;
		}

		m_saCommand.Execute();
	}
	catch(SAException &x)
	{
		AfxMessageBox(x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

void CLandMarkDB::resetAuditStruct(AuditRemeasType &meas)
{
	meas.FileIndex = 0;
	
	meas.PlotIndex = 0;
	meas.TRecIndex = 0;
	meas.TRecType = 0;
	meas.Measurement = 0;
	meas.SpeciesCode = _T("");
	meas.SpeciesName = _T("");
	meas.GroupName = _T("");
	meas.Dbh = 0.0;
	meas.ProductNumb = 0;
	meas.ProductName = _T("");
	meas.TQIPrd = 0;
	meas.Count_ = 0;
	meas.GSF = 0.0;
	meas.PACF = 0.0;
	meas.DefStumpHt = 0.0;
	meas.HmObs = 0.0;
	meas.HsObs = 0.0;
	meas.HpObs = 0.0;
	meas.HmEst = 0.0;
	meas.HsEst = 0.0;
	meas.HpEst = 0.0;
	meas.Top_ = 0.0;
	meas.Defect = 0;
	meas.FcObs = 0.0;
	meas.FcEst = 0.0;
	meas.Age = 0.0;
	meas.Ht = 0.0;
	meas.RadialGr = 0.0;
	meas.SngBark = 0.0;
	meas.ReproPlot = 0;
	meas.OffPlot = 0;
	meas.SubMerch = 0;
	meas.PWOnly = 0;
	meas.TClass = 0;
	meas.TClassName = _T("");
	meas.TClass2 = 0;
	meas.TClassName2 = _T("");
	meas.IsGradeTree = 0;
	meas.GrdStumpHt = 0.0;
	meas.StopTop = 0.0;
	meas.TopType = 0;
	meas.TopTypeV = _T("");
	meas.SGL1 = 0;
	meas.GRD1 = 0;
	meas.GRDV1 = _T("");
	meas.DEF1 = 0;
	meas.SGL2 =0;
	meas.GRD2 = 0;
	meas.GRDV2 = _T("");
	meas.DEF2 = 0;
	meas.SGL3 = 0;
	meas.GRD3 = 0;
	meas.GRDV3 = _T("");
	meas.DEF3 = 0;
	meas.SGL4 = 0;
	meas.GRD4 = 0;
	meas.GRDV4 = _T("");
	meas.DEF4 = 0;
	meas.SGL5 = 0;
	meas.GRD5 = 0;
	meas.GRDV5 = _T("");
	meas.DEF5 = 0;
	meas.SGL6 = 0;
	meas.GRD6 = 0;
	meas.GRDV6 = _T("");
	meas.DEF6 = 0;
	meas.SGL7 = 0;
	meas.GRD7 = 0;
	meas.GRDV7 = _T("");
	meas.DEF7 = 0;
	meas.LogTrim = 0.0;
	meas.BA = 0.0;
	meas.PW_CFVOB = 0.0;
	meas.PW_CFVIB = 0.0;
	meas.PW_GreenTons = 0.0;
	meas.PW_WtCords = 0.0;
	meas.SW_CFVOB = 0.0;
	meas.SW_CFVIB = 0.0;
	meas.SW_GreenTons = 0.0;
	meas.SW_WtCords = 0.0;
	meas.IntQuarter = 0.0;
	meas.Doyle = 0.0;
	meas.Scribner = 0.0;
	meas.PW_Dollars = 0.0;
	meas.SW_Dollars = 0.0;

	for(int i=0; i<NUM_OF_CC; i++)
		meas.CC[i] = _T("");
}


BOOL CLandMarkDB::getFieldData(SACommand &cmd, AuditRemeasType &meas, int fileindex, int fields)
{
	int num_cc = 0;
	CString csSQL;

	resetAuditStruct(meas);

	int num_of = 0;
	CString csField;

	num_of = cmd.FieldCount();

	meas.FileIndex = fileindex;

	for(int i=1; i<=num_of; i++)
	{
		csField = (CString)cmd.Field(i).Name();

		if(csField == _T("FileIndex"))
			;
		else if(csField == _T("PlotIndex"))
			meas.PlotIndex = cmd.Field((SAString)_T("PlotIndex")).asShort();
		else if(csField == _T("TRecIndex"))
			meas.TRecIndex = cmd.Field((SAString)_T("TRecIndex")).asShort();
		else if(csField == _T("TRecType"))
			meas.TRecType = cmd.Field((SAString)_T("TRecType")).asShort();
		else if(csField == _T("Measurement"))
			meas.Measurement = cmd.Field((SAString)_T("Measurement")).asShort();
		else if(csField == _T("SpeciesCode"))
			meas.SpeciesCode = cmd.Field((SAString)_T("SpeciesCode")).asString();
		else if(csField == _T("SpeciesName"))
			meas.SpeciesName = cmd.Field((SAString)_T("SpeciesName")).asString();
		else if(csField == _T("GroupName"))
			meas.GroupName = cmd.Field((SAString)_T("GroupName")).asString();
		else if(csField == _T("Dbh"))
			meas.Dbh = cmd.Field((SAString)_T("Dbh")).asDouble();
		else if(csField == _T("ProductNumb"))
			meas.ProductNumb = cmd.Field((SAString)_T("ProductNumb")).asShort();
		else if(csField == _T("ProductName"))
			meas.ProductName = cmd.Field((SAString)_T("ProductName")).asString();
		else if(csField == _T("TQIPrd"))
			meas.TQIPrd = cmd.Field((SAString)_T("TQIPrd")).asShort();
		else if(csField == _T("Count"))
			meas.Count_ = cmd.Field((SAString)_T("Count")).asShort();
		else if(csField == _T("GSF"))
			meas.GSF = cmd.Field((SAString)_T("GSF")).asDouble();
		else if(csField == _T("PACF"))
			meas.PACF = cmd.Field((SAString)_T("PACF")).asDouble();
		else if(csField == _T("DefStumpHt"))
			meas.DefStumpHt = cmd.Field((SAString)_T("DefStumpHt")).asDouble();
		else if(csField == _T("HmObs"))
			meas.HmObs = cmd.Field((SAString)_T("HmObs")).asDouble();
		else if(csField == _T("HsObs"))
			meas.HsObs = cmd.Field((SAString)_T("HsObs")).asDouble();
		else if(csField == _T("HpObs"))
			meas.HpObs = cmd.Field((SAString)_T("HpObs")).asDouble();
		else if(csField == _T("HmEst"))
			meas.HmEst = cmd.Field((SAString)_T("HmEst")).asDouble();
		else if(csField == _T("HsEst"))
			meas.HsEst = cmd.Field((SAString)_T("HsEst")).asDouble();
		else if(csField == _T("HpEst"))
			meas.HpEst = cmd.Field((SAString)_T("HpEst")).asDouble();
		else if(csField == _T("Top"))
			meas.Top_ = cmd.Field((SAString)_T("Top")).asDouble();
		else if(csField == _T("Defect"))
			meas.Defect = cmd.Field((SAString)_T("Defect")).asShort();
		else if(csField == _T("FcObs"))
			meas.FcObs = cmd.Field((SAString)_T("FcObs")).asDouble();
		else if(csField == _T("FcEst"))
			meas.FcEst = cmd.Field((SAString)_T("FcEst")).asDouble();
		else if(csField == _T("Age"))
			meas.Age = cmd.Field((SAString)_T("Age")).asDouble();
		else if(csField == _T("Ht"))
			meas.Ht = cmd.Field((SAString)_T("Ht")).asDouble();
		else if(csField == _T("RadialGr"))
			meas.RadialGr = cmd.Field((SAString)_T("RadialGr")).asDouble();
		else if(csField == _T("SngBark"))
			meas.SngBark = cmd.Field((SAString)_T("SngBark")).asDouble();
		else if(csField == _T("ReproPlot"))
			meas.ReproPlot = cmd.Field((SAString)_T("ReproPlot")).asShort();
		else if(csField == _T("OffPlot"))
			meas.OffPlot = cmd.Field((SAString)_T("OffPlot")).asShort();
		else if(csField == _T("SubMerch"))
			meas.SubMerch = cmd.Field((SAString)_T("SubMerch")).asShort();
		else if(csField == _T("PWOnly"))
			meas.PWOnly = cmd.Field((SAString)_T("PWOnly")).asShort();
		else if(csField == _T("TClass"))
			meas.TClass = cmd.Field((SAString)_T("TClass")).asShort();
		else if(csField == _T("TClassName"))
			meas.TClassName = cmd.Field((SAString)_T("TClassName")).asString();
		else if(csField == _T("TClass2"))
			meas.TClass2 = cmd.Field((SAString)_T("TClass2")).asShort();
		else if(csField == _T("TClassName2"))
			meas.TClassName2 = cmd.Field((SAString)_T("TClassName2")).asString();
		else if(csField == _T("IsGradeTree"))
			meas.IsGradeTree = cmd.Field((SAString)_T("IsGradeTree")).asShort();
		else if(csField == _T("GrdStumpHt"))
			meas.GrdStumpHt = cmd.Field((SAString)_T("GrdStumpHt")).asDouble();
		else if(csField == _T("StopTop"))
			meas.StopTop = cmd.Field((SAString)_T("StopTop")).asDouble();
		else if(csField == _T("TopType"))
			meas.TopType = cmd.Field((SAString)_T("TopType")).asShort();
		else if(csField == _T("TopTypeV"))
			meas.TopTypeV = cmd.Field((SAString)_T("TopTypeV")).asString();
		else if(csField == _T("SGL1"))
			meas.SGL1 = cmd.Field((SAString)_T("SGL1")).asShort();
		else if(csField == _T("GRD1"))
			meas.GRD1 = cmd.Field((SAString)_T("GRD1")).asShort();
		else if(csField == _T("GRDV1"))
			meas.GRDV1 = cmd.Field((SAString)_T("GRDV1")).asString();
		else if(csField == _T("DEF1"))
			meas.DEF1 = cmd.Field((SAString)_T("DEF1")).asShort();
		else if(csField == _T("SGL2"))
			meas.SGL2 = cmd.Field((SAString)_T("SGL2")).asShort();
		else if(csField == _T("GRD2"))
			meas.GRD2 = cmd.Field((SAString)_T("GRD2")).asShort();
		else if(csField == _T("GRDV2"))
			meas.GRDV2 = cmd.Field((SAString)_T("GRDV2")).asString();
		else if(csField == _T("DEF2"))
			meas.DEF2 = cmd.Field((SAString)_T("DEF2")).asShort();
		else if(csField == _T("SGL3"))
			meas.SGL3 = cmd.Field((SAString)_T("SGL3")).asShort();
		else if(csField == _T("GRD3"))
			meas.GRD3 = cmd.Field((SAString)_T("GRD3")).asShort();
		else if(csField == _T("GRDV3"))
			meas.GRDV3 = cmd.Field((SAString)_T("GRDV3")).asString();
		else if(csField == _T("DEF3"))
			meas.DEF3 = cmd.Field((SAString)_T("DEF3")).asShort();
		else if(csField == _T("SGL4"))
			meas.SGL4 = cmd.Field((SAString)_T("SGL4")).asShort();
		else if(csField == _T("GRD4"))
			meas.GRD4 = cmd.Field((SAString)_T("GRD4")).asShort();
		else if(csField == _T("GRDV4"))
			meas.GRDV4 = cmd.Field((SAString)_T("GRDV4")).asString();
		else if(csField == _T("DEF4"))
			meas.DEF4 = cmd.Field((SAString)_T("DEF4")).asShort();
		else if(csField == _T("SGL5"))
			meas.SGL5 = cmd.Field((SAString)_T("SGL5")).asShort();
		else if(csField == _T("GRD5"))
			meas.GRD5 = cmd.Field((SAString)_T("GRD5")).asShort();
		else if(csField == _T("GRDV5"))
			meas.GRDV5 = cmd.Field((SAString)_T("GRDV5")).asString();
		else if(csField == _T("DEF5"))
			meas.DEF5 = cmd.Field((SAString)_T("DEF5")).asShort();
		else if(csField == _T("SGL6"))
			meas.SGL6 = cmd.Field((SAString)_T("SGL6")).asShort();
		else if(csField == _T("GRD6"))
			meas.GRD6 = cmd.Field((SAString)_T("GRD6")).asShort();
		else if(csField == _T("GRDV6"))
			meas.GRDV6 = cmd.Field((SAString)_T("GRDV6")).asString();
		else if(csField == _T("DEF6"))
			meas.DEF6 = cmd.Field((SAString)_T("DEF6")).asShort();
		else if(csField == _T("SGL7"))
			meas.SGL7 = cmd.Field((SAString)_T("SGL7")).asShort();
		else if(csField == _T("GRD7"))
			meas.GRD7 = cmd.Field((SAString)_T("GRD7")).asShort();
		else if(csField == _T("GRDV7"))
			meas.GRDV7 = cmd.Field((SAString)_T("GRDV7")).asString();
		else if(csField == _T("DEF7"))
			meas.DEF7 = cmd.Field((SAString)_T("DEF7")).asShort();
		else if(csField == _T("LogTrim"))
			meas.LogTrim = cmd.Field((SAString)_T("LogTrim")).asDouble();
		else if(csField == _T("BA"))
			meas.BA = cmd.Field((SAString)_T("BA")).asDouble();
		else if(csField == _T("PW_CFVOB"))
			meas.PW_CFVOB = cmd.Field((SAString)_T("PW_CFVOB")).asDouble();
		else if(csField == _T("PW_CFVIB"))
			meas.PW_CFVIB = cmd.Field((SAString)_T("PW_CFVIB")).asDouble();
		else if(csField == _T("PW_GreenTons"))
			meas.PW_GreenTons = cmd.Field((SAString)_T("PW_GreenTons")).asDouble();
		else if(csField == _T("PW_WtCords"))
			meas.PW_WtCords = cmd.Field((SAString)_T("PW_WtCords")).asDouble();
		else if(csField == _T("SW_CFVOB"))
			meas.SW_CFVOB = cmd.Field((SAString)_T("SW_CFVOB")).asDouble();
		else if(csField == _T("SW_CFVIB"))
			meas.SW_CFVIB = cmd.Field((SAString)_T("SW_CFVIB")).asDouble();
		else if(csField == _T("SW_GreenTons"))
			meas.SW_GreenTons = cmd.Field((SAString)_T("SW_GreenTons")).asDouble();
		else if(csField == _T("SW_WtCords"))
			meas.SW_WtCords = cmd.Field((SAString)_T("SW_WtCords")).asDouble();
		else if(csField == _T("IntQuarter"))
			meas.IntQuarter = cmd.Field((SAString)_T("IntQuarter")).asDouble();
		else if(csField == _T("Doyle"))
			meas.Doyle = cmd.Field((SAString)_T("Doyle")).asDouble();
		else if(csField == _T("Scribner"))
			meas.Scribner = cmd.Field((SAString)_T("Scribner")).asDouble();
		else if(csField == _T("PW_Dollars"))
			meas.PW_Dollars = cmd.Field((SAString)_T("PW_Dollars")).asDouble();
		else if(csField == _T("SW_Dollars"))
			meas.SW_Dollars = cmd.Field((SAString)_T("SW_Dollars")).asDouble();
		else if(csField.Find(_T("CC")) == 0)
			num_cc++;
	}

	if(num_cc > NUM_OF_CC)
		num_cc = NUM_OF_CC;

	for(int i=0; i<num_cc; i++)
	{
		csSQL.Format(_T("CC%02d"),i+1);
		meas.CC[i] = cmd.Field((SAString)csSQL).asString();
	}

	return TRUE;
}


BOOL CLandMarkDB::checkGroupsAssigned()
{
	BOOL bRet = TRUE;
	CString csSQL;
	csSQL.Format(_T("SELECT * FROM %s"), TBL_GROUP_ORDER);
	try
	{
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			if(m_saCommand.Field(4).asShort() == 0)	//group not assigned to class
				bRet = FALSE;
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return bRet;
}


BOOL CLandMarkDB::getExternalDocuments(LPCTSTR tbl_name,int link_id,vecTransaction_external_docs &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where extdoc_link_tbl=:1 and extdoc_link_id=:2"),TBL_EXTERNAL_DOCS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= tbl_name;
		m_saCommand.Param(2).setAsShort()		= link_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_external_docs(m_saCommand.Field(1).asShort(),
				m_saCommand.Field(2).asString().GetWideChars(),
				m_saCommand.Field(3).asShort(),
				m_saCommand.Field(4).asShort(),
				m_saCommand.Field(5).asString().GetWideChars(),
				m_saCommand.Field(6).asString().GetWideChars(),
				m_saCommand.Field(7).asString().GetWideChars(),
				m_saCommand.Field(8).asString().GetWideChars()));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::addExternalDocuments(CTransaction_external_docs rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("insert into %s (extdoc_link_tbl,extdoc_link_id,extdoc_link_type,extdoc_url,extdoc_note,extdoc_created_by,extdoc_created) values(:1,:2,:3,:4,:5,:6,:7)")
			,TBL_EXTERNAL_DOCS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getExtDocLinkTbl_pk();
		m_saCommand.Param(2).setAsShort()		= rec.getExtDocLinkID();
		m_saCommand.Param(3).setAsShort()		= rec.getExtDocLinkType();
		m_saCommand.Param(4).setAsString()	= rec.getExtDocURL();
		m_saCommand.Param(5).setAsString()	= rec.getExtDocNote();
		m_saCommand.Param(6).setAsString()	= rec.getExtDocDoneBy();
		m_saCommand.Param(7).setAsString()	= rec.getExtDocCreated();

		m_saCommand.Prepare();
		m_saCommand.Execute();

		doCommit();
		bReturn = TRUE;
	}	
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

int CLandMarkDB::getNextExternalDocIndex()
{
	CString csSQL;
	int ans = 0;
		
	try
	{
		csSQL.Format(_T("SELECT * FROM %s"), TBL_EXTERNAL_DOCS);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			ans = m_saCommand.Field(1).asShort();
		}
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return -1;
	}

	ans++;	//update to next index
			
	return ans;


}

BOOL CLandMarkDB::saveExternalDocument(int id, LPCTSTR tbl_name,int link_id, int type, CExternalDocsReportDataRec *rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("UPDATE %s SET extdoc_link_tbl = :1, extdoc_link_id = :2, extdoc_link_type = :3, extdoc_url = :4, extdoc_note = :5, extdoc_created_by = :6, extdoc_created = :7 WHERE extdoc_id = %d")
			,TBL_EXTERNAL_DOCS,rec->getExtDocId());

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= tbl_name;
		m_saCommand.Param(2).setAsShort()		= link_id;
		m_saCommand.Param(3).setAsShort()		= type;
		m_saCommand.Param(4).setAsString()	= rec->getColumnText(COLUMN_EXTDOC_URL);
		m_saCommand.Param(5).setAsString()	= rec->getColumnText(COLUMN_EXTDOC_NOTE);
		m_saCommand.Param(6).setAsString()	= rec->getColumnText(COLUMN_EXTDOC_DONEBY);
		m_saCommand.Param(7).setAsString()	= rec->getColumnText(COLUMN_EXTDOC_DATE);

		m_saCommand.Prepare();
		m_saCommand.Execute();

		doCommit();
		bReturn = TRUE;
	}	// if (!spcExist(rec))
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::saveNewExternalDocument(int id, LPCTSTR tbl_name,int link_id, int type, CExternalDocsReportDataRec *rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("insert into %s (extdoc_id,extdoc_link_tbl,extdoc_link_id,extdoc_link_type,extdoc_url,extdoc_note,extdoc_created_by,extdoc_created) values(:1,:2,:3,:4,:5,:6,:7,:8)")
			,TBL_EXTERNAL_DOCS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort() = rec->getExtDocId();
		m_saCommand.Param(2).setAsString()	= tbl_name;
		m_saCommand.Param(3).setAsShort()		= link_id;
		m_saCommand.Param(4).setAsShort()		= type;
		m_saCommand.Param(5).setAsString()	= rec->getColumnText(COLUMN_EXTDOC_URL);
		m_saCommand.Param(6).setAsString()	= rec->getColumnText(COLUMN_EXTDOC_NOTE);
		m_saCommand.Param(7).setAsString()	= rec->getColumnText(COLUMN_EXTDOC_DONEBY);
		m_saCommand.Param(8).setAsString()	= rec->getColumnText(COLUMN_EXTDOC_DATE);
		

		m_saCommand.Prepare();
		m_saCommand.Execute();

		doCommit();
		bReturn = TRUE;
	}	
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		doRollback();
		return FALSE;
	}

	return bReturn;
}



BOOL CLandMarkDB::delExternalDocument(int id,LPCTSTR tbl_name,int link_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where extdoc_id=:1 and extdoc_link_tbl=:2 and extdoc_link_id=:3"),TBL_EXTERNAL_DOCS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()		= id;
		m_saCommand.Param(2).setAsString()	= tbl_name;
		m_saCommand.Param(3).setAsShort()		= link_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		// print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::delExternalDocuments(LPCTSTR tbl_name,int link_id)
{
CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where extdoc_link_tbl=:1 and extdoc_link_id=:2"),TBL_EXTERNAL_DOCS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= tbl_name;
		m_saCommand.Param(2).setAsShort()		= link_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		doRollback();
		// print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::standHasExternalDocs(LPCTSTR tbl_name,int link_id)
{
	CString sSQL;
	BOOL bRet = FALSE;
	try
	{
		sSQL.Format(_T("select * from %s where extdoc_link_tbl=:1 and extdoc_link_id=:2"),TBL_EXTERNAL_DOCS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= tbl_name;
		m_saCommand.Param(2).setAsShort()		= link_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			//external documents exists, return TRUE
			bRet = TRUE;
		}
		doCommit();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &e)
	{
		doRollback();
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException )
	{
		doRollback();
		 return FALSE;
	}
#endif

	return bRet;
}

BOOL CLandMarkDB::standHasSnapshots(int stand_id)
{
	CString sSQL;
	BOOL bRet = FALSE;
	try
	{
		sSQL = _T("select top 1 1 from gis_snapshots where widgetid = :1 and type = 4"),stand_id; // Type 4 is TCruise tract
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= stand_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			//external documents exists, return TRUE
			bRet = TRUE;
		}
		doCommit();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &e)
	{
		doRollback();
		 // print error message
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException )
	{
		doRollback();
		 return FALSE;
	}
#endif

	return bRet;
}

CString CLandMarkDB::getTractName(int link_id)
{
	CString csSQL,csOut = _T("");

	try
	{
		csSQL.Format(_T("select * from %s where FileIndex = %d"), TBL_STAND, link_id);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			csOut = m_saCommand.Field(2).asString().GetWideChars();
		}
		doCommit();
	}
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(e.ErrText().GetWideChars());
	}
#else
	catch(SAException )
	{
		doRollback();
	}
#endif

	return csOut;
}




BOOL CLandMarkDB::saveImage(CString path, CTransaction_contacts rec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("update %s set picture=:1 where id = %d"), TBL_CONTACTS, rec.getID());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Param(1).setAsBLob() = ReadWholeFile(path);
		//m_saCommand.Param(2).setAsBLob(FromFileWriter, 10*1024, (void*)path);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::removeImage(CTransaction_contacts rec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("update %s set picture = NULL where id = %d"), TBL_CONTACTS, rec.getID());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::loadImage(CString path, CTransaction_contacts rec)
{
	CString csSQL,S;
	BOOL bRet = TRUE;
	short nIsNullCounter = 0;

	try
	{
		csSQL.Format(_T("select picture from %s where id=%d"), TBL_CONTACTS, rec.getID());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		m_saCommand.Field(1).setLongOrLobReaderMode(SA_LongOrLobReaderManual);
		while(m_saCommand.FetchNext())
		{
				if (m_saCommand.Field(1).isNull()) 
				{
					bRet = FALSE;
				}
				else
				{
					m_saCommand.Field(1).ReadLongOrLob(IntoFileReader,	// our callback to read BLob content into file
																						 32*1024,		// our desired piece size
																						 (void*)/*(const char*)*/(LPCTSTR)path	// additional data, filename in our example
																						 );
					nIsNullCounter++;
				}
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nIsNullCounter > 0);
}


BOOL CLandMarkDB::getContacts(vecTransactionContacts &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asShort(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
				m_saCommand.Field(_T("connect_id")).asShort(),
				m_saCommand.Field(_T("type_of")).asShort(),_T(""),_T(""),_T(""),_T(""),_T("")));	
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::getContacts(CString sql,vecTransactionContacts &vec)
{
	CString sSQL(sql);
	try
	{
		vec.clear();
		
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asShort(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
				m_saCommand.Field(_T("connect_id")).asShort(),
				m_saCommand.Field(_T("type_of")).asShort(),_T(""),_T(""),_T(""),_T(""),_T("")));	
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::getContacts(CString category, CString sql,vecTransactionContacts &vec)
{
	CString sSQL;
	int nCategoryID = -1;
	try
	{
		vec.clear();

		// Get id of selcted category; 080630 p�d
		sSQL.Format(_T("select distinct a.id from %s a,%s b	where a.id=b.category_id and a.category='%s'"),
						TBL_CATEGORIES,TBL_CATEGORY_FOR_CONTACTS,category);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nCategoryID = m_saCommand.Field(1).asShort();
		}

		// Get selected Contacts; 080630 p�d
		// Get id of selcted category; 080630 p�d
		if (sql.IsEmpty())
		{
			sSQL.Format(_T("select a.* from %s a where exists (select b.contact_id from %s b where b.category_id=:1 and a.id=b.contact_id)"),
							TBL_CONTACTS,TBL_CATEGORY_FOR_CONTACTS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()		= nCategoryID;
		}
		else
		{
			sSQL.Format(_T("select a.* from %s a where exists (select b.contact_id from %s b where b.category_id=:1 and a.id=b.contact_id and %s)"),
							TBL_CONTACTS,TBL_CATEGORY_FOR_CONTACTS,sql);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()		= nCategoryID;
		}
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{	
			vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asShort(),
				(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
				(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
				(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
				m_saCommand.Field(_T("connect_id")).asShort(),
				m_saCommand.Field(_T("type_of")).asShort(),_T(""),_T(""),_T(""),_T(""),_T("")));
	  }
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CLandMarkDB::addContact(CTransaction_contacts &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!contactExists(rec))
		{
			sSQL.Format(_T("insert into %s (pnr_orgnr,name_of,company,address,post_num,post_address,county,country,phone_work,phone_home,fax_number,mobile,e_mail,web_site,notes,created_by,\
										 vat_number,connect_id,type_of) \
										 values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19)"),
									TBL_CONTACTS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getPNR_ORGNR().Left(50);
			m_saCommand.Param(2).setAsString()	= rec.getName().Left(50);
			m_saCommand.Param(3).setAsString()	= rec.getCompany().Left(50);
			m_saCommand.Param(4).setAsString()	= rec.getAddress().Left(50);
			m_saCommand.Param(5).setAsString()	= rec.getPostNum().Left(10);
			m_saCommand.Param(6).setAsString()	= rec.getPostAddress().Left(60);
			m_saCommand.Param(7).setAsString()	= rec.getCounty().Left(40);
			m_saCommand.Param(8).setAsString()	= rec.getCountry().Left(40);
			m_saCommand.Param(9).setAsString()	= rec.getPhoneWork().Left(25);
			m_saCommand.Param(10).setAsString()	= rec.getPhoneHome().Left(25);
			m_saCommand.Param(11).setAsString()	= rec.getFaxNumber().Left(25);
			m_saCommand.Param(12).setAsString() = rec.getMobile().Left(30);
			m_saCommand.Param(13).setAsString() = rec.getEMail().Left(50);
			m_saCommand.Param(14).setAsString() = rec.getWebSite().Left(50);
			m_saCommand.Param(15).setAsLongChar()	= rec.getNotes();
			m_saCommand.Param(16).setAsString() = getUserName();
			m_saCommand.Param(17).setAsString() = rec.getVATNumber();
			m_saCommand.Param(18).setAsShort()	= rec.getConnectID();
			m_saCommand.Param(19).setAsShort()	= rec.getTypeOf();

			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))

	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::updContact(CTransaction_contacts &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (contactExists(rec))
		{

			sSQL.Format(_T("update %s set pnr_orgnr=:1,name_of=:2,company=:3,address=:4,post_num=:5,post_address=:6,county=:7,country=:8, \
										 phone_work=:9,phone_home=:10,fax_number=:11,mobile=:12,e_mail=:13,web_site=:14,notes=:15,created_by=:16,vat_number=:17,connect_id=:18,type_of=:19 where id=:20"),
									TBL_CONTACTS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getPNR_ORGNR().Left(50);
			m_saCommand.Param(2).setAsString()	= rec.getName().Left(50);
			m_saCommand.Param(3).setAsString()	= rec.getCompany().Left(50);
			m_saCommand.Param(4).setAsString()	= rec.getAddress().Left(50);
			m_saCommand.Param(5).setAsString()	= rec.getPostNum().Left(10);
			m_saCommand.Param(6).setAsString()	= rec.getPostAddress().Left(60);
			m_saCommand.Param(7).setAsString()	= rec.getCounty().Left(40);
			m_saCommand.Param(8).setAsString()	= rec.getCountry().Left(40);
			m_saCommand.Param(9).setAsString()	= rec.getPhoneWork().Left(25);
			m_saCommand.Param(10).setAsString()	= rec.getPhoneHome().Left(25);
			m_saCommand.Param(11).setAsString()	= rec.getFaxNumber().Left(25);
			m_saCommand.Param(12).setAsString() = rec.getMobile().Left(30);
			m_saCommand.Param(13).setAsString() = rec.getEMail().Left(50);
			m_saCommand.Param(14).setAsString() = rec.getWebSite().Left(50);
			m_saCommand.Param(15).setAsLongChar()	= rec.getNotes();
			m_saCommand.Param(16).setAsString() = getUserName();
			m_saCommand.Param(17).setAsString() = rec.getVATNumber();
			m_saCommand.Param(18).setAsShort()	= rec.getConnectID();
			m_saCommand.Param(19).setAsShort()	= rec.getTypeOf();

			m_saCommand.Param(20).setAsShort()	= rec.getID();
			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::removeContact(CTransaction_contacts &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (contactExists(rec))
		{
			sSQL.Format(_T("delete from %s where id=:1"),TBL_CONTACTS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

// Gets the last id created in the IDENTITY field; 070102 p�d
int CLandMarkDB::getLastContactID(void)
{
	int nIdentity;
	CString sSQL;
	try
	{
		sSQL.Format(_T("select ident_current('%s')"),TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		nIdentity = -1;
		while(m_saCommand.FetchNext())
		{
			nIdentity = m_saCommand.Field(1).asShort();
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return nIdentity;
}

// OBS! THIS FUNCTION IS USED ONLY ON AN EMPTY TABLE; 070102 p�d
// Resets the IDENTITY field to 0, i.e. first id = 1; 070102 p�d
BOOL CLandMarkDB::resetContactIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 1)"),TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

int CLandMarkDB::getNumOfRecordsInContacts(void)
{
	return num_of_records(TBL_CONTACTS);
}

int CLandMarkDB::getNumOfRecordsInContacts(LPCTSTR count_field,LPCTSTR count_value)
{
	CString sSQL,sValue(count_value);
	int nReturn = 0;
	try
	{
		sSQL.Format(_T("select count(id) as 'result' from %s where %s LIKE '%s'"),
			TBL_CONTACTS,count_field,count_value);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nReturn = m_saCommand.Field(_T("result")).asShort();
		}
	
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return -1;
	}

	return nReturn;
}


BOOL CLandMarkDB::contactExists(CTransaction_contacts &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_CONTACTS,rec.getID());
	return exists(sSQL);
}

//////////////////////////////////////////////////////////////////////////////////////////
// Handle Categories

// PUBLIC
BOOL CLandMarkDB::getCategories(vecTransactionCategory &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),TBL_CATEGORIES);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_category(m_saCommand.Field(1).asShort(),
																					(LPCTSTR)(m_saCommand.Field(2).asString()),
																					(LPCTSTR)(m_saCommand.Field(3).asString()),
																					_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::addCategory(CTransaction_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!categoryExists(rec))
		{
			sSQL.Format(_T("insert into %s (category,notes) values(:1,:2)"),
									TBL_CATEGORIES);

			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsString()	= rec.getCategory();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();

			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::updCategory(CTransaction_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (categoryExists(rec))
		{

			sSQL.Format(_T("update %s set category=:1,notes=:2 where id=:3"),TBL_CATEGORIES);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsString() = rec.getCategory();
			m_saCommand.Param(2).setAsString() = rec.getNotes();
			m_saCommand.Param(3).setAsShort()  = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::removeCategory(CTransaction_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (categoryExists(rec))
		{
			sSQL.Format(_T("delete from %s where id=:1"),TBL_CATEGORIES);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsShort() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

// Handle Categories for Contacts

BOOL CLandMarkDB::getCategoriesForContact(vecTransactionCategoriesForContacts &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_CATEGORY_FOR_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_categories_for_contacts(m_saCommand.Field(1).asShort(),
																												 m_saCommand.Field(2).asShort(),
																												(LPCTSTR)(m_saCommand.Field(3).asString() )) );
		}

		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::addCategoriesForContact(vecTransactionCategoriesForContacts &list)
{
	CTransaction_categories_for_contacts data;
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (list.size() > 0)
		{
			// First remove categories set for this contact; 061221 p�d
			removeCategoriesForContact(list[0].getContactID());

			for (UINT i = 0;i < list.size();i++)
			{
				data = list[i];			
				sSQL.Format(_T("insert into %s (contact_id,category_id) values(:1,:2)"),
									TBL_CATEGORY_FOR_CONTACTS);

				m_saCommand.setCommandText((SAString)sSQL);
				m_saCommand.Param(1).setAsShort()	= data.getContactID();
				m_saCommand.Param(2).setAsShort()	= data.getCategoryID();

				m_saCommand.Prepare();
				m_saCommand.Execute();
			}
			doCommit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::addCategoriesForContact(int contact_id,int category_id)
{
	CTransaction_categories_for_contacts data;
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("insert into %s (contact_id,category_id) values(:1,:2)"),
									TBL_CATEGORY_FOR_CONTACTS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()	= contact_id;
		m_saCommand.Param(2).setAsShort()	= category_id;

		m_saCommand.Prepare();
		m_saCommand.Execute();
		doCommit();
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::removeCategoriesForContact(int contact_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where contact_id=:1"),TBL_CATEGORY_FOR_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort() = contact_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::categoryExists(CTransaction_category &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_CATEGORIES,rec.getID());
	return exists(sSQL);
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Handle Property
BOOL CLandMarkDB::propertyExists(CTransaction_property &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
							TBL_PROPERTIES,rec.getID());
	return exists(sSQL);
}

// PUBLIC
BOOL CLandMarkDB::getProperties(vecTransactionProperty &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_PROPERTIES);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_property(m_saCommand.Field(1).asShort(),
																					 (LPCTSTR)(m_saCommand.Field(2).asString()),
																					 (LPCTSTR)(m_saCommand.Field(3).asString()),
																					 (LPCTSTR)(m_saCommand.Field(4).asString()),
																					 (LPCTSTR)(m_saCommand.Field(5).asString()),
																					 (LPCTSTR)(m_saCommand.Field(6).asString()),
																					 (LPCTSTR)(m_saCommand.Field(7).asString()),
																					 (LPCTSTR)(m_saCommand.Field(8).asString()),
																					 (LPCTSTR)(m_saCommand.Field(9).asString()),
																					 (LPCTSTR)(m_saCommand.Field(10).asString()),
																					 (LPCTSTR)(m_saCommand.Field(11).asString()),
																					 m_saCommand.Field(12).asDouble(),
																					 m_saCommand.Field(13).asDouble(),
																					 (LPCTSTR)(m_saCommand.Field(14).asString()),
																					 (LPCTSTR)(m_saCommand.Field(15).asString()),
																					 (LPCTSTR)(m_saCommand.Field(16).asString()),
																					 (LPCTSTR)(m_saCommand.Field(17).asString()),_T("")) );
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::getProperties(CString sql,vecTransactionProperty &vec)
{
	CString sSQL = sql;
	try
	{
		vec.clear();
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_property(m_saCommand.Field(1).asShort(),
																					 (LPCTSTR)m_saCommand.Field(2).asString(),
																					 (LPCTSTR)m_saCommand.Field(3).asString(),
																					 (LPCTSTR)m_saCommand.Field(4).asString(),
																					 (LPCTSTR)m_saCommand.Field(5).asString(),
																					 (LPCTSTR)m_saCommand.Field(6).asString(),
																					 (LPCTSTR)m_saCommand.Field(7).asString(),
																					 (LPCTSTR)m_saCommand.Field(8).asString(),
																					 (LPCTSTR)m_saCommand.Field(9).asString(),
																					 (LPCTSTR)m_saCommand.Field(10).asString(),
																					 (LPCTSTR)m_saCommand.Field(11).asString(),
																					 m_saCommand.Field(12).asDouble(),
																					 m_saCommand.Field(13).asDouble(),
																					 (LPCTSTR)m_saCommand.Field(14).asString(),
																					 (LPCTSTR)m_saCommand.Field(15).asString(),
																					 (LPCTSTR)m_saCommand.Field(16).asString(),
																					 (LPCTSTR)(m_saCommand.Field(17).asString()),_T("")) );
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((const TCHAR*)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::addProperty(CTransaction_property &rec)
{
	CString sSQL,sPropFullName;
	BOOL bReturn = FALSE;
	try
	{
		if (!propertyExists(rec))
		{
			// Setup the full name of property. I.e. Name + Block:Unit; 091005 p�d
			if ((rec.getBlock().IsEmpty() || rec.getBlock() == _T("0")) &&
					(rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName = rec.getPropertyName();
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName.Format(_T("%s %s"),rec.getPropertyName(),rec.getBlock());
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (!rec.getUnit().IsEmpty() || rec.getUnit() != _T("0")))
			{
				sPropFullName.Format(_T("%s %s:%s"),rec.getPropertyName(),rec.getBlock(),rec.getUnit());
			}

			/*TODO: 
			// Added a check to see if proeprty already exists in database; 091217 p�d
			sSQL.Format(L"select 1 from %s where county_name=\'%s\' and municipal_name=\'%s\' and parish_name=\'%s\' and prop_name=\'%s\' and block_number=\'%s\' and unit_number=\'%s\'",
									TBL_PROPERTIES,
									rec.getCountyName().Left(31),
									rec.getMunicipalName().Left(31),
									rec.getParishName().Left(31),
									rec.getPropertyName(),
									rec.getBlock(),
									rec.getUnit());

			if (exists(sSQL)) 
				return FALSE;
*/
			sSQL.Empty();
			sSQL.Format(L"insert into %s (location_code,state_code,county_code,location_name,state_name,county_name,"
									L"prop_number,prop_name,block_number,unit_number,areal_ha,areal_measured_ha,created_by,obj_id,prop_full_name)"
									L"values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15)",TBL_PROPERTIES);

			m_saCommand.setCommandText((SAString)sSQL);

			// Check the PropertyNumber to see, first if it's a number and if so
			// remove any .0 in number; 091217 p�d
/*
			CString sPropNum(rec.getPropertyNum().Left(20));

			if (sPropNum.FindOneOf(L"ABCDEFGHIJKLMNOPQRETUVXYZ���") == -1)
			{
				sPropNum.Find(
			}
*/
			m_saCommand.Param(1).setAsString()	= rec.getCountyCode();
			m_saCommand.Param(2).setAsString()	= rec.getMunicipalCode();
			m_saCommand.Param(3).setAsString()	= rec.getParishCode();
			m_saCommand.Param(4).setAsString()	= rec.getCountyName().Left(31);
			m_saCommand.Param(5).setAsString()	= rec.getMunicipalName().Left(31);
			m_saCommand.Param(6).setAsString()	= rec.getParishName().Left(31);
			m_saCommand.Param(7).setAsString()	= rec.getPropertyNum().Left(20);
			m_saCommand.Param(8).setAsString()	= rec.getPropertyName().Left(40);
			m_saCommand.Param(9).setAsString()	= rec.getBlock().Left(5);
			m_saCommand.Param(10).setAsString()	= rec.getUnit().Left(5);
			m_saCommand.Param(11).setAsDouble()	= rec.getAreal();
			m_saCommand.Param(12).setAsDouble()	= rec.getArealMeasured();
			m_saCommand.Param(13).setAsString()	= getUserName().Left(20);
			m_saCommand.Param(14).setAsString()	= rec.getObjectID().Left(30);
			m_saCommand.Param(15).setAsString()	= sPropFullName.Left(40);
			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("ERROR!\n%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::updProperty(CTransaction_property &rec)
{
	CString sSQL,sPropFullName;
	BOOL bReturn = FALSE;
	try
	{
		if (propertyExists(rec))
		{
			// Setup the full name of property. I.e. Name + Block:Unit; 091005 p�d
			if ((rec.getBlock().IsEmpty() || rec.getBlock() == _T("0")) &&
					(rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName = rec.getPropertyName();
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName.Format(_T("%s %s"),rec.getPropertyName(),rec.getBlock());
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (!rec.getUnit().IsEmpty() || rec.getUnit() != _T("0")))
			{
				sPropFullName.Format(_T("%s %s:%s"),rec.getPropertyName(),rec.getBlock(),rec.getUnit());
			}

			sSQL.Format(_T("update %s set location_code=:1,state_code=:2,county_code=:3,location_name=:4,state_name=:5,county_name=:6,\
										 prop_number=:7,prop_name=:8,block_number=:9,unit_number=:10,areal_ha=:11,areal_measured_ha=:12,obj_id=:13,prop_full_name=:14 where id=:15"),
											TBL_PROPERTIES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getCountyCode();
			m_saCommand.Param(2).setAsString()	= rec.getMunicipalCode();
			m_saCommand.Param(3).setAsString()	= rec.getParishCode();
			m_saCommand.Param(4).setAsString()	= rec.getCountyName();
			m_saCommand.Param(5).setAsString()	= rec.getMunicipalName();
			m_saCommand.Param(6).setAsString()	= rec.getParishName();
			m_saCommand.Param(7).setAsString()	= rec.getPropertyNum();
			m_saCommand.Param(8).setAsString()	= rec.getPropertyName();
			m_saCommand.Param(9).setAsString()	= rec.getBlock();
			m_saCommand.Param(10).setAsString()	= rec.getUnit();
			m_saCommand.Param(11).setAsDouble()	= rec.getAreal();
			m_saCommand.Param(12).setAsDouble()	= rec.getArealMeasured();
			m_saCommand.Param(13).setAsString()	= rec.getObjectID();		
			m_saCommand.Param(14).setAsString()	= sPropFullName;		
			m_saCommand.Param(15).setAsShort()	= rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CLandMarkDB::runSQLQuestion(LPCTSTR sql)
{
	CString sSQL(sql);
	BOOL bReturn = FALSE;
	try
	{
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();	
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::updPropertyObjID(int prop_id,LPCTSTR obj_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set obj_id=:1 where id=:2"),TBL_PROPERTIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= obj_id;		
		m_saCommand.Param(2).setAsShort()		= prop_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::removeProperty(CTransaction_property &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (propertyExists(rec))
		{

			sSQL.Format(_T("delete from %s where id=:1"),TBL_PROPERTIES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.getID();

			m_saCommand.Execute();
			
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

// Gets the last id created in the IDENTITY field; 070102 p�d
int CLandMarkDB::getLastPropertyID(void)
{
	int nIdentity;
	CString sSQL;
	try
	{

		sSQL.Format(_T("select ident_current('%s')"),TBL_PROPERTIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		nIdentity = -1;
		while(m_saCommand.FetchNext())
		{
			nIdentity = m_saCommand.Field(1).asShort();
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return nIdentity;
}

// OBS! THIS FUNCTION IS USED ONLY ON AN EMPTY TABLE; 070116 p�d
// Resets the IDENTITY field to 0, i.e. first id = 1; 070116 p�d
BOOL CLandMarkDB::resetPropertyIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_PROPERTIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

int CLandMarkDB::getNumOfRecordsInProperty(void)
{
	return num_of_records(TBL_PROPERTIES);
}

int CLandMarkDB::getNumOfRecordsInProperty(LPCTSTR count_field,LPCTSTR count_value)
{
	CString sSQL,sValue(count_value);
	int nReturn = 0;
	try
	{
		sSQL.Format(_T("select count(id) as 'result' from %s where %s LIKE '%s'"),
			TBL_PROPERTIES,count_field,count_value);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nReturn = m_saCommand.Field(_T("result")).asShort();
		}
	
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return -1;
	}

	return nReturn;
}

// Handle Property owners


// PUBLIC
BOOL CLandMarkDB::getPropOwners(vecTransactionPropOwners &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_PROPERTY_OWNER);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				vec.push_back(CTransaction_prop_owners(m_saCommand.Field(1).asShort(),
																						 m_saCommand.Field(2).asShort(),
   																					 (LPCTSTR)(m_saCommand.Field(3).asString()),
																						 m_saCommand.Field(4).asShort(),
																						 (LPCTSTR)(m_saCommand.Field(5).asString())) );
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::getPropOwnersForProperty(int prop_id,vecTransactionPropOwnersForPropertyData &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select a.prop_id,a.is_contact,a.owner_share,b.* \
								 from %s a,%s b where a.contact_id = b.id and a.prop_id=:1"),
								 TBL_PROPERTY_OWNER,TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Param(1).setAsShort()		= prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_prop_owners_for_property_data(m_saCommand.Field(1).asShort(),
																															m_saCommand.Field(2).asShort(),
								 																							(LPCTSTR)(m_saCommand.Field(3).asString()),
																															CTransaction_contacts(m_saCommand.Field(_T("id")).asShort(),
																															(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
																															(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
																															m_saCommand.Field(_T("connect_id")).asShort(),
																															m_saCommand.Field(_T("type_of")).asShort(),_T(""),_T(""),_T(""),_T(""),_T(""))));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CLandMarkDB::addPropOwner(CTransaction_prop_owners &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!propOwnersExists(rec))
		{
			sSQL.Format(_T("insert into %s (prop_id,contact_id,is_contact,owner_share) values(:1,:2,:3,:4)"),TBL_PROPERTY_OWNER);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort()		= rec.getPropID();
			m_saCommand.Param(2).setAsShort()		= rec.getContactID();
			m_saCommand.Param(3).setAsShort()		= rec.getIsContact();
			m_saCommand.Param(4).setAsString()	= rec.getShareOff();
			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::removePropOwner(CTransaction_prop_owners &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (propOwnersExists(rec))
		{
			sSQL.Format(_T("delete from %s where prop_id=:1 and contact_id=:2"),TBL_PROPERTY_OWNER);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = rec.getPropID();
			m_saCommand.Param(2).setAsShort() = rec.getContactID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::removePropOwner(int prop_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (propOwnersExists(prop_id))
		{
			sSQL.Format(_T("delete from %s where prop_id=:1"),TBL_PROPERTY_OWNER);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = prop_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CLandMarkDB::removePropOwner(int prop_id,int contact_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (propOwnersExists(prop_id))
		{
			sSQL.Format(_T("delete from %s where prop_id=:1 and contact_id=:2"),TBL_PROPERTY_OWNER);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsShort() = prop_id;
			m_saCommand.Param(2).setAsShort() = contact_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::isPropertyOwnerUsed(int contact_id)
{
	CString sSQL;
	int nNumOfItemsInResultSet = 0;
	try
	{
		sSQL.Format(_T("select * from %s where contact_id=:1"),TBL_PROPERTY_OWNER);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()	= contact_id;
		m_saCommand.Execute();	
		while(m_saCommand.FetchNext()) nNumOfItemsInResultSet++;
		
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return (nNumOfItemsInResultSet > 0);	// Used; 081016 p�d
}

BOOL CLandMarkDB::propOwnersExists(CTransaction_prop_owners &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where prop_id=%d and contact_id=%d"),
							TBL_PROPERTY_OWNER,rec.getPropID(),rec.getContactID());
	return exists(sSQL);
}

BOOL CLandMarkDB::propOwnersExists(int prop_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where prop_id=%d"),
							TBL_PROPERTY_OWNER,prop_id);
	return exists(sSQL);
}


BOOL CLandMarkDB::getTractsForProperty(int propid, vecTransactionTCStand &vec)
{
	CString csSQL;
#ifndef USE_TC_CONTACT
	return FALSE;
#endif


	try
	{
		vec.clear();

		csSQL.Format(_T("SELECT * FROM %s WHERE PropID = :1 ORDER BY FileIndex"), TBL_STAND);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Param(1).setAsShort()		= propid;
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_TCStand(m_saCommand.Field(1).asShort(),
				m_saCommand.Field(2).asString().GetWideChars(),
				m_saCommand.Field(3).asDouble(),
				getCruiseMethod(m_saCommand.Field(5).asShort()),
				m_saCommand.Field(8).asString().GetWideChars(),
				m_saCommand.Field(9).asString().GetWideChars(),
				m_saCommand.Field(10).asString().GetWideChars(),
				m_saCommand.Field(11).asString().GetWideChars(),
				m_saCommand.Field(SAString(_T("PropID"))).asShort()

				));
		}
		doCommit();
	}
	
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(e.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}
#endif
		

	return TRUE;
}

BOOL CLandMarkDB::updRemovePropertyFromStand(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set PropID=:1 where FileIndex=:2"),
									TBL_STAND);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsNull();	// --> NULL == No property added to Stand; 090812 p�d
		m_saCommand.Param(2).setAsShort()	= trakt_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::updConnectPropertyToStand(int trakt_id,int prop_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set PropID=:1 where FileIndex=:2"),
									TBL_STAND);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()	= prop_id;
		m_saCommand.Param(2).setAsShort()	= trakt_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::getTraktNoProperty(vecTransactionTCStand &vec)
{
	CString csSQL;

	try
	{
		vec.clear();

		csSQL.Format(_T("select * from %s where PropID is NULL"), TBL_STAND);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_TCStand(m_saCommand.Field(1).asShort(),
				m_saCommand.Field(2).asString().GetWideChars(),
				m_saCommand.Field(3).asDouble(),
				getCruiseMethod(m_saCommand.Field(5).asShort()),
				m_saCommand.Field(8).asString().GetWideChars(),
				m_saCommand.Field(9).asString().GetWideChars(),
				m_saCommand.Field(10).asString().GetWideChars(),
				m_saCommand.Field(11).asString().GetWideChars()
#ifdef USE_TC_CONTACT
				,m_saCommand.Field(SAString(_T("PropID"))).asShort()
#else
				,-1
#endif
				));
		}
		doCommit();
	}
	
#ifdef SHOW_MY_EXTRA_MSG
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(e.ErrText().GetWideChars());
		return FALSE;
	}
#else
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}
#endif
		

	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Handle Project Types

// PUBLIC
BOOL CLandMarkDB::getProjectTypes(vecTransactionProjectType &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),TBL_PROJECT_TYPES);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_ProjectType(m_saCommand.Field(1).asShort(),
																					(LPCTSTR)(m_saCommand.Field(2).asString()),
																					(LPCTSTR)(m_saCommand.Field(3).asString()),
																					_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CLandMarkDB::addProjectType(CTransaction_ProjectType &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!ProjectTypeExists(rec))
		{
			sSQL.Format(_T("insert into %s (type,notes) values(:1,:2)"),
									TBL_PROJECT_TYPES);

			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsString()	= rec.getProjectType();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();

			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::updProjectType(CTransaction_ProjectType &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (ProjectTypeExists(rec))
		{

			sSQL.Format(_T("update %s set type=:1,notes=:2 where id=:3"),TBL_PROJECT_TYPES);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsString() = rec.getProjectType();
			m_saCommand.Param(2).setAsString() = rec.getNotes();
			m_saCommand.Param(3).setAsShort()  = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::removeProjectType(CTransaction_ProjectType &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (ProjectTypeExists(rec))
		{
			sSQL.Format(_T("delete from %s where id=:1"),TBL_PROJECT_TYPES);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsShort() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
#ifdef UNICODE
		CString csBuf;
		csBuf.Format(_T("%s"), e.ErrText());
		AfxMessageBox(csBuf);
#else
		AfxMessageBox((const char*)e.ErrText());
#endif
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CLandMarkDB::ProjectTypeExists(CTransaction_ProjectType &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_PROJECT_TYPES,rec.getID());
	return exists(sSQL);
}