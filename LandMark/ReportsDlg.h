#pragma once

#include "Resource.h"

// CReportsDlg dialog
/*
const FMTID PropSetfmtid ={
        0xf29f85e0,
        0x4ff9,
        0x1068,
        {0xab, 0x91, 0x08, 0x00, 0x2b, 0x27, 0xb3, 0xd9 }
        };
*/
class CReportsDlg : public CDialog
{
	DECLARE_DYNAMIC(CReportsDlg)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

protected:
	CXTListCtrl m_wndListCtrl;
	CXTHeaderCtrl m_wndHeaderCtrl;
	CMyExtStatic m_wndLbl;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	CString m_sStand;

	vecTransactionFiles vecFileData;

public:
	CReportsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CReportsDlg();

// Dialog Data
	enum { IDD = IDD_REPORTS_DLG };
	void setStand(LPCSTR stand) { m_sStand = stand; }

protected:
	void setLanguage(void);
	void setListCtrl(void);
	BOOL getFiles(void);
	CString getComments(LPCSTR path);
	CString getTitle(LPCSTR path);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
};
