#pragma once

#include "Resource.h"
#include "stdafx.h"
#include "LandMarkDB.h"
#include "ProgressDialog.h"
#include "afxwin.h"


// CImportAccessView form view


class CImportAccessView : public CFormView
{
	DECLARE_DYNCREATE(CImportAccessView)

	int m_nFileIndex;
	CString m_csMsg1;
	CString m_csMsg2;
	CString m_csMsg3;

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sDBErrorMsg;
	CString m_csErrMsgStand;
	CString m_csProgress;
	CString m_csInitErrMsg;
	CString m_csStrOk;
	CString m_csStrFailed;
	CString m_csStrInto;
	CString m_csStrLbl;

	CString m_csLogStr;

	CStringArray m_caFilePaths;
	CStringArray m_caStandOk;
	CStringArray m_caStandFailed;

	CLandMarkDB *m_pDB;	
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;

	
	CEdit m_wndEditStandOk;
	CEdit m_wndEditStandFailed;
	CStatic m_wndLblOk;
	CStatic m_wndLblFailed;

	BOOL m_bStandsOk;
	BOOL m_bStandsFailed;

	CProgressDialog m_wndProgressCtrl;

	BOOL m_bCruiseIsAuditType;	//new v6.0.4

protected:
	CImportAccessView();           // protected constructor used by dynamic creation
	virtual ~CImportAccessView();

	BOOL isDBConnected(void) { return m_pDB != NULL ? TRUE : FALSE;}
	BOOL convertDataToSQL(LPCTSTR database);

	void setupList(void);

	int getNextFileIndex();
	
	int getData(SAConnection &con, SACommand &cmd, LPCTSTR table);	
	BOOL setData(SACommand &cmd, LPCTSTR table, LPCTSTR csTract, CProgressDialog &dlg);
	void doCommit(void);
	void doRollback(void);

public:
	enum { IDD = IDD_FORMVIEW3 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnDestroy();
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg void OnBnClickedBtnImportOk();
};


