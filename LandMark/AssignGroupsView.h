#pragma once

#include "LandMarkDB.h"

class CAssignGroupsFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CAssignGroupsFilterEditControl)
public:
	CAssignGroupsFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	afx_msg void OnKeyUp(UINT, UINT, UINT);

	DECLARE_MESSAGE_MAP()
};

//CAssignGroupsReportRec
class CAssignGroupsReportRec : public CXTPReportRecord
{
	int m_nIndex;
	int m_nClassIndex;

	protected:

	class CFloatItem : public CXTPReportRecordItemNumber
	{
		double m_fValue;
	public:
		CFloatItem(double fValue, LPCTSTR fmt_str = sz1dec)
			: CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
		}

		void setFloatItem(double value)
		{
			m_fValue = value;
			SetValue(value);
		}

		double getFloatItem(void)
		{
			return m_fValue;
		}
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
		int m_nValue;
	public:
		CIntItem(int nValue)
			: CXTPReportRecordItemNumber(nValue)
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

		void setIntItem(int value)
		{
			m_nValue = value;
			SetValue(value);
		}

		int getIntItem(void)
		{
			return m_nValue;
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
		CString m_sText;
	public:
		CTextItem(CString sValue)
			: CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_sText = szText;
			SetValue(m_sText);
		}

		void setTextItem(LPCTSTR text)
		{
			m_sText = text;
			SetValue(m_sText);
		}

		CString getTextItem(void)
		{
			return m_sText;
		}
	};

public:

	CAssignGroupsReportRec(void)
	{
		m_nIndex = -1;
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
		m_nClassIndex = 0;
		AddItem(new CTextItem(_T("")));
	}

	CAssignGroupsReportRec(CTransaction_TCGroups &rec)
	{
		m_nIndex = rec.getID();
		AddItem(new CIntItem(rec.getPrintOrder()));
		AddItem(new CTextItem(rec.getGroupName()));
		m_nClassIndex = rec.getClassIndex();
		AddItem(new CTextItem(rec.getClassName()));
	}

	int getIndex(void)
	{
		return m_nIndex;
	}

	int getClassIndex(void)
	{
		return m_nClassIndex;
	}

	void setClassIndex(int index)
	{
		m_nClassIndex = index;
	}

	double getColumnFloat(int item)
	{
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item, double value)
	{
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)
	{
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item, int value)
	{
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)
	{
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item, LPCTSTR text)
	{
		((CTextItem*)GetItem(item))->setTextItem(text);
	}
};


// CAssignGroupsView view

class CAssignGroupsView : public CXTPReportView
{
	DECLARE_DYNCREATE(CAssignGroupsView)

protected:
	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_sDBErrorMsg;

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;
	CString m_sFilterOn;
	CString m_sErrorMsg;
	CString m_sPrintOrderError;
	CString m_sMenuSort;
	
	CXTPReportSubListControl m_wndSubList;
	CAssignGroupsFilterEditControl m_wndFilterEdit;

	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	int m_nSelectedColumn;

	void setLanguage();

	vecTransactionTCGroups m_vecGroups;
	vecTransactionTCClass m_vecClasses;
	void LoadReportState(void);
	void SaveReportState(void);

	CLandMarkDB *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;
	BOOL getGroups(void);

	CAssignGroupsView();           // protected constructor used by dynamic creation
	virtual ~CAssignGroupsView();

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnInitialUpdate();

protected:
	BOOL isDBConnected(void) { return m_pDB != NULL? TRUE : FALSE;}

	BOOL setupReport(void);
	void populateReport(void);
	void saveDataToDB(void);
	void updateComboList(LPARAM lParam);
	void removeClassFromRecord(LPARAM lParam);
	void updateClassNameInRecord(LPARAM lParam);
	void deleteGroupFromDB(void);
	void Refresh(void);
	void setPrintOrder(void);

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnShowFieldChooser(void);
	afx_msg void OnShowFieldFilter(void);
	afx_msg void OnShowFieldFilterOff(void);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
	afx_msg void OnTBBTNRefresh(void);
	afx_msg void OnReportItemClick(NMHDR *pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR *pNotifyStruct, LRESULT* result);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnValueChanged(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnTBBTNClasses(void);
	afx_msg void OnTBBTNAssignMulti(void);
	afx_msg void OnReportItemRClick(NMHDR *pNotifyStruct, LRESULT* /*result*/);
};


