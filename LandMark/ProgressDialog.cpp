// ProgressDialog.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "ProgressDialog.h"


// CProgressDialog dialog

IMPLEMENT_DYNAMIC(CProgressDialog, CDialog)

CProgressDialog::CProgressDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDialog::IDD, pParent)
{
	m_bInitialized = FALSE;
}

CProgressDialog::~CProgressDialog()
{
}

void CProgressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS, m_wndProgressCtrl);
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
}


BEGIN_MESSAGE_MAP(CProgressDialog, CDialog)
END_MESSAGE_MAP()


// CProgressDialog message handlers

BOOL CProgressDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	if(m_bInitialized == FALSE)
	{
		m_sAbrevLangSet = getLangSet();
		m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);


		SetWindowText(_T(""));
		m_wndLbl1.SetWindowText(_T(""));
		m_wndLbl2.SetWindowTextW(_T(""));

		m_wndProgressCtrl.SetRange(0,100);
		m_wndProgressCtrl.SetStep(1);
		m_wndProgressCtrl.SetPos(0);

		m_bInitialized = TRUE;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

