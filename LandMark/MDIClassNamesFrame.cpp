// MDIClassNamesFrame.cpp : implementation file
//

#include "stdafx.h"
#include "LandMark.h"
#include "MDIClassNamesFrame.h"
#include "Resource.h"

// CMDIClassNamesFrame

IMPLEMENT_DYNCREATE(CMDIClassNamesFrame, CChildFrameBase)

CMDIClassNamesFrame::CMDIClassNamesFrame()
{
	m_bFirstOpen = FALSE;
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bDBConnOk = TRUE;
}

CMDIClassNamesFrame::~CMDIClassNamesFrame()
{
}


BEGIN_MESSAGE_MAP(CMDIClassNamesFrame, CChildFrameBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CMDIClassNamesFrame message handlers

BOOL CMDIClassNamesFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTPFrameWndBase<CMDIChildWnd>::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CMDIClassNamesFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTPFrameWndBase<CMDIChildWnd>::OnCreate(lpCreateStruct) == -1)
		return -1;

	if(m_hIcon)
	{
		SetIcon(m_hIcon, TRUE);
		SetIcon(m_hIcon, FALSE);
	}

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			
		}	// if (xml->Load(m_sLangFN))
		delete xml;
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0;
}

void CMDIClassNamesFrame::OnSize(UINT nType, int cx, int cy)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

void CMDIClassNamesFrame::OnDestroy()
{
	//CXTPFrameWndBase<CMDIChildWnd>::OnDestroy();

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_TIMBERCRUISECLASSNAMESFRAME_WP);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
	}

LRESULT CMDIClassNamesFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE,wParam,lParam);

		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}



void CMDIClassNamesFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CLASSNAMES;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CLASSNAMES;

	CXTPFrameWndBase<CMDIChildWnd>::OnGetMinMaxInfo(lpMMI);
}

void CMDIClassNamesFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnMDIActivate(bActivate, pActivateWnd, pDeactivateWnd);

	::SendMessage(GetMDIFrame()->m_hWndMDIClient, WM_MDISETMENU, 0, 0);

	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE|RDW_FRAME|RDW_NOCHILDREN);
}

void CMDIClassNamesFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	//load placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_TIMBERCRUISECLASSNAMESFRAME_WP);
		LoadPlacement(this, csBuf);
	}
}

void CMDIClassNamesFrame::OnSetFocus(CWnd* pOldWnd)
{
	//CXTPFrameWndBase<CMDIChildWnd>::OnSetFocus(pOldWnd);

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	if(m_bDBConnOk == TRUE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);


	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

}

void CMDIClassNamesFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}
