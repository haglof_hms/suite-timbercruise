#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "Resource.h"


// CProgressDialog dialog

class CProgressDialog : public CDialog
{
	DECLARE_DYNAMIC(CProgressDialog)

	BOOL m_bInitialized;
	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CProgressCtrl m_wndProgressCtrl;
	CStatic m_wndLbl1;
	CStatic m_wndLbl2;
	

public:
	CProgressDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressDialog();

// Dialog Data
	enum { IDD = IDD_PROGRESS_DLG };

	inline void setProgressPos(int pos)
	{
		if(m_wndProgressCtrl.GetSafeHwnd() != NULL)
			m_wndProgressCtrl.SetPos(pos);
	}

	inline void setLbl1(LPCTSTR text)
	{
		m_wndLbl1.SetWindowTextW(text);
	}

	inline void setLbl2(LPCTSTR text)
	{
		m_wndLbl2.SetWindowTextW(text);
	}

	inline void setHeader(LPCTSTR text)
	{
		SetWindowText(text);
	}

	inline void stepIt()
	{
		if(m_wndProgressCtrl.GetSafeHwnd() != NULL)
		{
			m_wndProgressCtrl.StepIt();
		}
	}

	inline void setRange(int range)
	{
		if(m_wndProgressCtrl.GetSafeHwnd() != NULL)
			m_wndProgressCtrl.SetRange(0, range);
	}

	inline void setStep(int step)
	{
		if(m_wndProgressCtrl.GetSafeHwnd() != NULL)
			m_wndProgressCtrl.SetStep(step);
	}

	inline void updateWindow(void)
	{
		UpdateWindow();
	}

	inline void setFullBar(void)
	{
		if(m_wndProgressCtrl.GetSafeHwnd() != NULL)
		{
			m_wndProgressCtrl.SetRange(0,1);
			m_wndProgressCtrl.SetPos(1);
			UpdateWindow();
		}
	}

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
protected:
};
